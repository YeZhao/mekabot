# Generated by the protocol buffer compiler.  DO NOT EDIT!

from google.protobuf import descriptor
from google.protobuf import message
from google.protobuf import reflection
from google.protobuf import service
from google.protobuf import service_reflection
from google.protobuf import descriptor_pb2



_M3QAEXAMPLESTATUS = descriptor.Descriptor(
  name='M3QAExampleStatus',
  full_name='M3QAExampleStatus',
  filename='example.proto',
  containing_type=None,
  fields=[
    descriptor.FieldDescriptor(
      name='base', full_name='M3QAExampleStatus.base', index=0,
      number=1, type=11, cpp_type=10, label=1,
      default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='current_ma', full_name='M3QAExampleStatus.current_ma', index=1,
      number=2, type=1, cpp_type=5, label=1,
      default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],  # TODO(robinson): Implement.
  enum_types=[
  ],
  options=None)


_M3QAEXAMPLEPARAM = descriptor.Descriptor(
  name='M3QAExampleParam',
  full_name='M3QAExampleParam',
  filename='example.proto',
  containing_type=None,
  fields=[
    descriptor.FieldDescriptor(
      name='scalar', full_name='M3QAExampleParam.scalar', index=0,
      number=1, type=1, cpp_type=5, label=1,
      default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],  # TODO(robinson): Implement.
  enum_types=[
  ],
  options=None)


_M3QAEXAMPLECOMMAND = descriptor.Descriptor(
  name='M3QAExampleCommand',
  full_name='M3QAExampleCommand',
  filename='example.proto',
  containing_type=None,
  fields=[
    descriptor.FieldDescriptor(
      name='enable', full_name='M3QAExampleCommand.enable', index=0,
      number=3, type=8, cpp_type=7, label=1,
      default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],  # TODO(robinson): Implement.
  enum_types=[
  ],
  options=None)

import component_base_pb2

_M3QAEXAMPLESTATUS.fields_by_name['base'].message_type = component_base_pb2._M3BASESTATUS

class M3QAExampleStatus(message.Message):
  __metaclass__ = reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _M3QAEXAMPLESTATUS

class M3QAExampleParam(message.Message):
  __metaclass__ = reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _M3QAEXAMPLEPARAM

class M3QAExampleCommand(message.Message):
  __metaclass__ = reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _M3QAEXAMPLECOMMAND

