/* 
M3 -- Meka Robotics Real-Time Control System
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef USE_UART

#include "p33fxxxx.h"
#include "setup.h"
#include "uart1.h"
#include "timer3.h"
#include "i3dmgx3_Errors.h"

//#include "m3serial.h"

//#define FCY 40000000UL
#define FCY 37500000UL

#include "libpic30.h"

//Note, there is a bug with using the TX INT
//Should be easy to fix, but no need for it at the 
//moment
//#define USE_TX_INT

#define EOF -1
#define TX1_BUF_LEN 68
//#define RX1_BUF_LEN 50
#define RX1_BUF_LEN 250

char uart_str[64];

unsigned char tx1_buf[TX1_BUF_LEN];
//unsigned char rx1_buf[RX1_BUF_LEN];

//unsigned char rx1_buf[RX1_BUF_LEN];
unsigned char * rx1_buf;

unsigned char rx1_buf_A[RX1_BUF_LEN];
unsigned char rx1_buf_B[RX1_BUF_LEN];

//unsigned char rx1_buf2[RX1_BUF_LEN];

unsigned char * rx1_buf2;

unsigned char * rx1_buf_pntr;

unsigned char active_buf = 0;

unsigned char tx1_head, tx1_tail;
unsigned char rx1_head, rx1_tail;

volatile unsigned char tx1_cnt;
volatile unsigned char rx1_cnt;

volatile unsigned char rx1_cnt2;

#define TRUE 1
#define FALSE 0

unsigned char buf2_head = 0;

int last_match_cnt = 0;

unsigned char getchar_buf(void)
{
	buf2_head++;

	return rx1_buf2[buf2_head-1];
}

int ReadNextRecord(I3dmgx3Set* pRecord, unsigned char * cmd_return){
	//unsigned long dwcharsRead  = 0;
	int iTry            = 0;
	//int iMaxTry         = 20;  //read X chars looking for a start of packet befor giving up
	int i = 0;
	//int status = 0;
	unsigned char cmd_chk = 0;
	unsigned char Bresponse[100];// = {0}; //Current max buff request is 42
	unsigned short wChecksum;// = 0;
	unsigned short wCalculatedCheckSum;// = 0;
	float tmp0, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7, tmp8;
	float tmp00, tmp11, tmp22, tmp33, tmp44, tmp55, tmp66, tmp77, tmp88;
	volatile long time_out;
	//volatile long time_out_limit = 10000;
	//volatile long time_out_limit = 2;
	int found = 0;
    

	while( !found) 
	{
			
			
			//cmd_chk = (unsigned char)getchar1();
			cmd_chk = getchar_buf();
			

			if (cmd_chk == (unsigned char)0xCB) //Acceleration & Angular Mag packet
				found = TRUE;
			else if (cmd_chk == (unsigned char)0xC5) //orientation
				found = TRUE;
			else
				return (int)cmd_chk;
			/*if (cmd_chk == 0xC2) //Acceleration & Angular rate packet
				found = TRUE;
			
			if (cmd_chk == 0xCC) //Acceleration & Angular rate packet and mag + orientation
				found = TRUE;
			if (cmd_chk == 0xC8) //Acceleration & Angular rate packet + orientation
				found = TRUE;
			
			if (cmd_chk == 0xD3) //Delta Ang Velocity and Mag packet
				found = TRUE;
			if (cmd_chk == 0xC4) //Continuous mode confirmation packet
				found = TRUE;*/
			/*if (iTry++ >= iMaxTry);
			{
			    found = TRUE;
				return cmd_chk;
			}*/
		
	}

   		switch(cmd_chk)	{
			case 0xc2:
				  break;

				  //SetHeartbeatLED;
				  /*while (getchar1_cnt() < 30)
				  {
					  if (time_out++ > time_out_limit*30)
					  {
						__delay_us(5);
						*cmd_return = 0;
						ClrHeartbeatLED;
						return I3DMGX3_COMM_READ_ERROR;
					  } 
				  }*/
				  //ClrHeartbeatLED;
				  /*iTry = 0;
				  while (getchar1_cnt() < 30)
				  {
						if (iTry++ >= iMaxTry);
						    return -12;						
				  } 
					for (i = 0; i < 30; i++)
							Bresponse[i] = getchar1();
					//ToggleHeartbeatLED();
				  pRecord->timer = ((((((Bresponse[24]) << 8) & Bresponse[25]) << 8) & Bresponse[26]) << 8) & Bresponse[27];
				  pRecord->timer = convert2ulong(&Bresponse[24]);
				  wChecksum = convert2ushort(&Bresponse[28]);
	                          wCalculatedCheckSum = i3dmgx3_Checksum(&Bresponse[0], 28) + 0xC2; 
				  break; */
				 case 0xc5:
				  iTry = 0;
				  /*while (getchar1_cnt() < 42)
				  {
						if (iTry++ >= iMaxTry);
						    return -12;						
				  } */
				  for (i = 0; i < 42; i++)
					Bresponse[i] =  getchar_buf();
					//Bresponse[i] = getchar1();

				  pRecord->timer = ((((((Bresponse[36]) << 8) & Bresponse[37]) << 8) & Bresponse[38]) << 8) & Bresponse[39];
				  wChecksum = convert2ushort(&Bresponse[40]);
	               wCalculatedCheckSum = i3dmgx3_Checksum(&Bresponse[0], 40) + 0xC5; 
					break;	
		        case 0xcb:
				  iTry = 0;
				  /*while (getchar1_cnt() < 42)
				  {
						if (iTry++ >= iMaxTry);
						    return -12;						
				  } */
				  for (i = 0; i < 42; i++)
					Bresponse[i] =  getchar_buf();
					//Bresponse[i] = getchar1();

				  pRecord->timer = ((((((Bresponse[36]) << 8) & Bresponse[37]) << 8) & Bresponse[38]) << 8) & Bresponse[39];
				  wChecksum = convert2ushort(&Bresponse[40]);
	               wCalculatedCheckSum = i3dmgx3_Checksum(&Bresponse[0], 40) + 0xCB; 
					break;	
				 case 0xcc:
				  
				  for (i = 0; i < 78; i++)
					Bresponse[i] =  getchar_buf();

				  pRecord->timer = ((((((Bresponse[72]) << 8) & Bresponse[73]) << 8) & Bresponse[74]) << 8) & Bresponse[75];
				  wChecksum = convert2ushort(&Bresponse[76]);
	              wCalculatedCheckSum = i3dmgx3_Checksum(&Bresponse[0], 76) + 0xCC; 				  
				  break;
				case 0xc8:
				  
				  for (i = 0; i < 66; i++)
					Bresponse[i] =  getchar_buf();

				  pRecord->timer = ((((((Bresponse[60]) << 8) & Bresponse[61]) << 8) & Bresponse[62]) << 8) & Bresponse[63];
				  wChecksum = convert2ushort(&Bresponse[64]);
	              wCalculatedCheckSum = i3dmgx3_Checksum(&Bresponse[0], 64) + 0xC8; 				  
				  break;
                /*case 0xd3:
				  for (i = 0; i < 42; i++)
					Bresponse[i] = getchar1();
				  //status = receiveData(portNum, &Bresponse[0], 42);
				  pRecord->timer = ((((((Bresponse[36]) << 8) & Bresponse[37]) << 8) & Bresponse[38]) << 8) & Bresponse[39];
				  wChecksum = convert2ushort(&Bresponse[40]);
	                          wCalculatedCheckSum = i3dmgx3_Checksum(&Bresponse[0], 40) + 0xD3; 			  
				  break;
		        case 0xc4:
				  for (i = 0; i < 7; i++)
					Bresponse[i] = getchar1();
				  //status = receiveData(portNum, &Bresponse[0], 7);
				  pRecord->timer = ((((((Bresponse[1]) << 8) & Bresponse[2]) << 8) & Bresponse[3]) << 8) & Bresponse[4];
				  wChecksum = convert2ushort(&Bresponse[5]);
	                          wCalculatedCheckSum = i3dmgx3_Checksum(&Bresponse[0], 5) + 0xC4; 			  
				  break;*/
		   default: /* printf("Invalid Comand Code %x\n", cmd_chk);  */
					*cmd_return = cmd_chk;
			       return -11;
	}
    /*if (status <0 ){
		//printf("Read failed status is: %d\n",status);
		return status;
	}*/
    if(wChecksum != wCalculatedCheckSum){
		*cmd_return = cmd_chk;
		//ToggleHeartbeatLED();
		//return I3DMGX3_OK;
	    return I3DMGX3_CHECKSUM_ERROR;
	}
	if (cmd_chk != 0xc4) {   
		for (i = 0; i < 4; i++)
		{
			((unsigned char*)&tmp0)[3-i] = Bresponse[i];
			((unsigned char*)&tmp1)[3-i] = Bresponse[i+4];
			((unsigned char*)&tmp2)[3-i] = Bresponse[i+8];
			((unsigned char*)&tmp3)[3-i] = Bresponse[i+12];
			((unsigned char*)&tmp4)[3-i] = Bresponse[i+16];
			((unsigned char*)&tmp5)[3-i] = Bresponse[i+20];
			if (cmd_chk == 0xCB || cmd_chk == 0xCC || cmd_chk == 0xC8 || cmd_chk == 0xC5)
			{
				((unsigned char*)&tmp6)[3-i] = Bresponse[i+24];
				((unsigned char*)&tmp7)[3-i] = Bresponse[i+28];
				((unsigned char*)&tmp8)[3-i] = Bresponse[i+32];
			}
			/*if (cmd_chk == 0xC8)
			{
				((unsigned char*)&tmp33)[3-i] = Bresponse[i+36];
				((unsigned char*)&tmp44)[3-i] = Bresponse[i+40];
				((unsigned char*)&tmp55)[3-i] = Bresponse[i+44];
				((unsigned char*)&tmp66)[3-i] = Bresponse[i+48];
				((unsigned char*)&tmp77)[3-i] = Bresponse[i+52];
				((unsigned char*)&tmp88)[3-i] = Bresponse[i+56];
			}*/
		}

		if (cmd_chk == 0xCB)
		{
			pRecord->setA[0] = tmp0;
			pRecord->setA[1] = tmp1;
			pRecord->setA[2] = tmp2;
			pRecord->setB[0] = tmp3;
			pRecord->setB[1] = tmp4;
			pRecord->setB[2] = tmp5;
		}
		if (cmd_chk == 0xCB || cmd_chk == 0xCC)
		{
			pRecord->setC[0] = tmp6;
			pRecord->setC[1] = tmp7;
			pRecord->setC[2] = tmp8;
		}
		if (cmd_chk == 0xC8)
		{
			pRecord->setD[0] = tmp6;
			pRecord->setD[1] = tmp7;
			pRecord->setD[2] = tmp8;
			pRecord->setE[0] = tmp33;
			pRecord->setE[1] = tmp44;
			pRecord->setE[2] = tmp55;
			pRecord->setF[0] = tmp66;
			pRecord->setF[1] = tmp77;
			pRecord->setF[2] = tmp88;
		}
		if (cmd_chk == 0xC5)
		{
			pRecord->setD[0] = tmp0;
			pRecord->setD[1] = tmp1;
			pRecord->setD[2] = tmp2;
			pRecord->setE[0] = tmp3;
			pRecord->setE[1] = tmp4;
			pRecord->setE[2] = tmp5;
			pRecord->setF[0] = tmp6;
			pRecord->setF[1] = tmp7;
			pRecord->setF[2] = tmp8;
		}
		
	     /*for (i=0; i<3; i++) {
				//pRecord->setA[i] = FloatFromBytes(&Bresponse[0 + i*4]); //  Accel x y z
				//pRecord->setB[i] = FloatFromBytes(&Bresponse[12 + i*4]); // Ang rates x y z
				//if (cmd_chk == 0xCB || cmd_chk == 0xD3)
                  //                  pRecord->setC[i] = FloatFromBytes(&Bresponse[24 + i*4]); // Mag rates x y z                               
				  
	    }*/
	}
	//ToggleHeartbeatLED();
	*cmd_return = cmd_chk;
	return I3DMGX3_OK;
}

void setup_uart(void) {

  tx1_head = 0;
  tx1_tail = 0;
  tx1_cnt = 0;
  rx1_head = rx1_tail = rx1_cnt = 0;

#ifdef M3_FB_DEV_0_0
  // BRG = Fcy / (16 * baudrate) -1
  // -1 + 40e6 /16 / 57600

 // U1BRG = 21;  // (20.7014) 115.2Kb at 40 Mhz
	U1BRG = 10;  // (9.85) 230.400Kb at 40 Mhz
#else
  // BRG = Fcy / (16 * baudrate) -1
  // -1 + 40e6 /16 / 115200
  //U1BRG = 21;  // (20.7014) 115.2Kb at 40 Mhz
	U1BRG = 19;  // (19.3) 115.2Kb at 37.5 Mhz
#endif



  U1MODEbits.UARTEN = 0;			//Disable U1ART module

  //=================================================================
  // Configure Interrupt Priority
  _U1RXIF = 0;	//Clear Rx interrupt flags
  _U1TXIF = 0;	//Clear Tx interrupt flags
  _U1RXIE = 1;	//Receive interrupt: 0 disable, 1 enable 
  _U1TXIE = 1;	//Transmit interrupt: 0 disable, 1 enable

  U1STAbits.UTXISEL0 = 0;
  U1STAbits.UTXISEL1 = 0;

  U1MODEbits.UEN0 = 0;
  U1MODEbits.UEN1 = 0;
  
  //=================================================================
  // Configure Mode
  //  +--Default: 8N1, no loopback, no wake in sleep mode, continue in idle mode
  //  +--Diable autobaud detect
  //  +--Enable U1ART module
  U1MODEbits.ABAUD = 0;	//Disable Autobaud detect from U1RX 
  U1MODEbits.UARTEN = 1;	//U1ART enable
  
  //=================================================================
  // Configure Status
  //  +--Default: TxInt when a char is transmitted, no break char
  //  +--Default: RxInt when a char is received, no address detect, clear overflow
  //  +--Enable Transmit
  U1STAbits.UTXEN = 1;	//Tx enable

	//rx1_buf_pntr = rx1_buf;

	rx1_buf = (unsigned char *) rx1_buf_A;
	active_buf = 0; // A
}


void setup_uart_460800(void) {

  tx1_head = 0;
  tx1_tail = 0;
  tx1_cnt = 0;
  rx1_head = rx1_tail = rx1_cnt = 0;

#ifdef M3_FB_DEV_0_0
  // BRG = Fcy / (16 * baudrate) -1
  // -1 + 40e6 /16 / 57600

 // U1BRG = 21;  // (20.7014) 115.2Kb at 40 Mhz
	U1BRG = 10;  // (9.85) 230.400Kb at 40 Mhz
#else
  // BRG = Fcy / (16 * baudrate) -1
  // -1 + 40e6 /16 / 115200
  //U1BRG = 21;  // (20.7014) 115.2Kb at 40 Mhz
  //U1BRG = 10;  // (9.8506) 230.4Kb at 40 Mhz
	U1BRG = 4;  // (4.425) 460.8Kb at 40 Mhz
#endif



  U1MODEbits.UARTEN = 0;			//Disable U1ART module

  //=================================================================
  // Configure Interrupt Priority
  _U1RXIF = 0;	//Clear Rx interrupt flags
  _U1TXIF = 0;	//Clear Tx interrupt flags
  _U1RXIE = 1;	//Receive interrupt: 0 disable, 1 enable 
  _U1TXIE = 1;	//Transmit interrupt: 0 disable, 1 enable

  U1STAbits.UTXISEL0 = 0;
  U1STAbits.UTXISEL1 = 0;

  U1MODEbits.UEN0 = 0;
  U1MODEbits.UEN1 = 0;
  
  //=================================================================
  // Configure Mode
  //  +--Default: 8N1, no loopback, no wake in sleep mode, continue in idle mode
  //  +--Diable autobaud detect
  //  +--Enable U1ART module
  U1MODEbits.ABAUD = 0;	//Disable Autobaud detect from U1RX 
  U1MODEbits.UARTEN = 1;	//U1ART enable
  
  //=================================================================
  // Configure Status
  //  +--Default: TxInt when a char is transmitted, no break char
  //  +--Default: RxInt when a char is received, no address detect, clear overflow
  //  +--Enable Transmit
  U1STAbits.UTXEN = 1;	//Tx enable

  rx1_buf_pntr = rx1_buf;
}

void setup_uart_230400(void) {

  tx1_head = 0;
  tx1_tail = 0;
  tx1_cnt = 0;
  rx1_head = rx1_tail = rx1_cnt = 0;

#ifdef M3_FB_DEV_0_0
  // BRG = Fcy / (16 * baudrate) -1
  // -1 + 40e6 /16 / 57600

 // U1BRG = 21;  // (20.7014) 115.2Kb at 40 Mhz
	U1BRG = 10;  // (9.85) 230.400Kb at 40 Mhz
#else
  // BRG = Fcy / (16 * baudrate) -1
  // -1 + 40e6 /16 / 115200
  //U1BRG = 21;  // (20.7014) 115.2Kb at 40 Mhz
  U1BRG = 10;  // (9.8506) 230.4Kb at 40 Mhz
#endif



  U1MODEbits.UARTEN = 0;			//Disable U1ART module

  //=================================================================
  // Configure Interrupt Priority
  _U1RXIF = 0;	//Clear Rx interrupt flags
  _U1TXIF = 0;	//Clear Tx interrupt flags
  _U1RXIE = 1;	//Receive interrupt: 0 disable, 1 enable 
  _U1TXIE = 1;	//Transmit interrupt: 0 disable, 1 enable

  U1STAbits.UTXISEL0 = 0;
  U1STAbits.UTXISEL1 = 0;

  U1MODEbits.UEN0 = 0;
  U1MODEbits.UEN1 = 0;
  
  //=================================================================
  // Configure Mode
  //  +--Default: 8N1, no loopback, no wake in sleep mode, continue in idle mode
  //  +--Diable autobaud detect
  //  +--Enable U1ART module
  U1MODEbits.ABAUD = 0;	//Disable Autobaud detect from U1RX 
  U1MODEbits.UARTEN = 1;	//U1ART enable
  
  //=================================================================
  // Configure Status
  //  +--Default: TxInt when a char is transmitted, no break char
  //  +--Default: RxInt when a char is received, no address detect, clear overflow
  //  +--Enable Transmit
  U1STAbits.UTXEN = 1;	//Tx enable

	rx1_buf_pntr = rx1_buf;
}

void reset_rx_buff(void)
{
	  
  rx1_head = rx1_tail = rx1_cnt = 0;
}


void __attribute__((__interrupt__, no_auto_psv)) _U1RXInterrupt(void) {
  unsigned char ch;
	int i;
  _U1RXIF = 0;		//Clear the flag
	//char rxdata=(char)U1RXREG; 
	//ToggleHeartbeatLED();

#if 1
#ifdef M3_FB_DEV_0_0
ch = (unsigned char) U1RXREG; //Read the data from buffer
serial_eth_receive(ch); //Saving the reception byte in a buffer for the decoding function (look in m3serial.c)
#else
	unsigned char hit = 0;
   if ( U1STAbits.URXDA ){
    ch = (unsigned char) U1RXREG; //Read the data from buffer
	//if (ch == 0xcc)
	//if (ch == 0xc8)
	
	if (imu_mode)
	{
		if (ch == 0xC5)
			hit = 1;
	} else {
		if (ch == 0xCB)
			hit = 1;
	}

	if (hit)
	{
		//ToggleHeartbeatLED();
		if (!active_buf) // on buf A
		{
			rx1_buf2 = (unsigned char *)rx1_buf_A;
			rx1_buf = (unsigned char *)rx1_buf_B;
			active_buf = 1;
		} else {  // on buf B
			rx1_buf2 = (unsigned char *)rx1_buf_B;
			rx1_buf = (unsigned char *)rx1_buf_A;
			active_buf = 0;
		}
		rx1_cnt2 = rx1_cnt;
		reset_rx_buff();
		buf2_head = 0;

		//if (!parsing)
		/*if (last_match_cnt > 40)
		{
			for (i = 0; i < rx1_cnt; i++)
				rx1_buf2[i] = rx1_buf[i];
			buf2_head = 0;
			rx1_cnt2 = rx1_cnt;
			//reset_rx_buff();
			reset_rx_buff();
			last_match_cnt = 0;
		} else
			last_match_cnt++;*/
	}

    if (rx1_cnt < RX1_BUF_LEN) {
      rx1_buf[rx1_head++] = ch;
      ++rx1_cnt;
		//ToggleHeartbeatLED();
      if (rx1_head >= RX1_BUF_LEN) rx1_head -= RX1_BUF_LEN;

    } else {      // overrun
		reset_rx_buff();
		//ToggleHeartbeatLED();
    } // if rx1_cnt else

  } // if U1STAbits

#endif
#endif
}


void __attribute__((__interrupt__, no_auto_psv)) _U1TXInterrupt(void) {
  unsigned char ch;
  _U1TXIF = 0;	//Clear Interrupt Flag

  //  PORTD = LATD ^ B1;
#ifdef USE_TX_INT
  while(U1STAbits.UTXBF==0 && tx1_cnt) {
    ch = tx1_buf[tx1_tail++];
    U1TXREG = ch;
    tx1_cnt--;
    
    if (tx1_tail >= TX1_BUF_LEN) tx1_tail -= TX1_BUF_LEN;
  }
#endif
}


int getchar1_cnt(void) {
  int i;
  _U1RXIE = 0;
  i = rx1_cnt;
  _U1RXIE = 1;
  return i;
}

int getchar1(void) {
  int ch = EOF;
  _U1RXIF = 0;
  _U1RXIE = 0;
  if (rx1_cnt) {
    --rx1_cnt;
    ch = rx1_buf[rx1_tail++];
    _U1RXIE = 1;
    if (rx1_tail >= RX1_BUF_LEN) rx1_tail -= RX1_BUF_LEN;

  } else   _U1RXIE = 1;
  return ch;
}




int puts1(char *s) {
  int n=0;
  int r;
  char ch;

  if (!s) return EOF;

  while(*s) {
    //putchar(*s++);
    ch = *s++;
    do {
      r = putchar1(ch);
    } while (r == EOF);
    ++n;
  }
  return n;
}


int putchar1(unsigned char ch) {
#ifdef USE_TX_INT

  //DisableInterrupts(5);
  _U1TXIE = 0;	
  if (tx1_cnt >= TX1_BUF_LEN) {
    _U1TXIE = 1;	
    // LATD = LATD ^ B5;
    return EOF;
  }
  ++tx1_cnt;

  tx1_buf[tx1_head++] = ch;

  if (tx1_head >= TX1_BUF_LEN) tx1_head -= TX1_BUF_LEN;

  _U1TXIE = 1;	
  if (!U1STAbits.UTXBF) _U1TXIF = 1;

  return ch;
#else
  _U1TXIE = 0;	
  while(U1STAbits.UTXBF) ;
  U1TXREG = ch;
  return 0;
#endif
}

#endif
