/* 
M3 -- Meka Robotics Real-Time Control System
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef USE_TIMER3

#include "timer3.h"
#include "setup.h"
#include "ethercat.h"
#include "i3dmgx3_Errors.h"

//#define FCY 40000000UL
#define FCY 37500000UL

#include "libpic30.h"

int tmp_cnt;
int irq_cnt;
unsigned char parsing = 0;
int error_record = 0;
long valid_rec = 0;
unsigned char error_cmd;
I3dmgx3Set Record;

int send_orientation = 0;

I3dmgx3Set ecat_record;

#ifdef USE_TACTILE_PPS
int pps_trigger;
#endif

int imu_first_time;

void setup_timer3(void)
{
	// ToDo: Setup timer3 for Ethercat distributed clock jitter calculation
	// The ECAT_TIMER (16 bit timer) runs from 0 to 0xFFFF round and round, 
	// the ECAT_CAPTURE_REG captures the ECAT_TIMER_REG when the ESC interrupt goes active
	irq_cnt=0;
	T3CON = 0;
	TMR3 = 0x0000;
	T3CONbits.TCKPS=T3_TCKPS;					
	PR3 = (unsigned int)T3_PR3;					
	_T3IF = 0;									//Clear interrupt
	T3CONbits.TON = 1;							//Start Timer 3
	_T3IE = 1;									//Enable T3 ints
	send_orientation = 0;
	imu_first_time = 1;
	// TODO: set records to zero
	return;
}



//Every X us
void __attribute__((__interrupt__, no_auto_psv)) _T3Interrupt(void) {
	_T3IF = 0;

int i;

//Latch encoder timestamp on Rising edge.
#ifdef USE_TIMESTAMP_DC
	SetTimestampLatch;
	ClrTimestampLatch;
#endif

//ToggleHeartbeatLED();
#if defined USE_ENCODER_VERTX
#if defined M3_MAX2 || defined M3_BMW_A2R1 || defined M3_BMW_A2R2  || defined M3_HEX4_S2R1  || defined M3_BMW_A2R3
	step_vertx();
#endif
#endif

#ifdef USE_CURRENT
	step_current();
#endif

#ifdef USE_ADC_SPI
	step_adc_spi();
#endif

#if defined USE_ENCODER_QEI
#if defined M3_ELMO_Z1R1  || defined M3_ELMO_B1R1 || defined M3_MAX2_BDC_ARMH || defined  M3_MAX2_BDC_ARMH2 || defined M3_MAX2_BLDC_A2R3_QEI
	step_qei();
#endif
#endif



#if defined USE_LED_RGB
	step_led_rgb();
#endif

#if defined USE_LED_MDRV
	//if (load_led_mdrv())
	//	step_led_mdrv();
	//load_led_mdrv();
	load_led_mdrv();
	irq_cnt++;
	if (irq_cnt==LEDMDRV_IRQ_PER_TRIGGER)
	{
		irq_cnt=0;
		step_led_mdrv();
	}
	//ToggleHeartbeatLED();
#endif

#ifdef USE_AS5510
	step_as5510();
#endif

#if defined M3_WMA_0_1 || defined M3_BMA_A1R1 || defined M3_DAC_0_1 || defined M3_ELMO_RNA_R0 || defined M3_HMB_H1R1 \
	||defined M3_HB2_H2R1_J0J1 || defined M3_HB2_H2R1_J2J3J4  ||defined M3_HB2_H2R2_J0J1 || \
	defined M3_HB2_H2R2_J2J3J4 || defined M3_GMB_G1R1 || defined M3_PWR_0_2 || defined M3_PWR_0_3 || \
	defined M3_PWR_0_4 || defined M3_DEV || defined M3_MAX2 || defined M3_HEX2_S1R1 || defined M3_BMW_A2R1 || \
	defined M3_BMW_A2R2 || defined M3_ELMO_B1R1 || defined M3_PWR_0_5 || defined M3_ELMO_Z1R1  || defined M3_HEX4_S2R1 \
	 || defined M3_BMW_A2R3 || defined M3_HB2_H2R3_J0J1 || defined M3_HB2_H2R3_J2J3J4
#ifdef USE_CONTROL	
	step_control();
#endif
	irq_cnt++;
#ifdef USE_TACTILE_PPS
	if (irq_cnt==PPS_IRQ_PER_TRIGGER)
	{
		irq_cnt=0;
		pps_trigger=1;
	}
#endif
#endif

#ifdef USE_UART
	unsigned char dataType =  0xC2;
	unsigned char Bresponse[6];
	int status = 0;
    unsigned char GX3command[] = {0xD6, 0xC6, 0x6B, 0x00};
    unsigned char GX3toobuff[] = {0xD4, 0xA3, 0x47, 0x02};
	int rec_ret = 0;
	int time_out;
	int time_out_limit = 1000;
	float tmp0, tmp1, tmp2, tmp3, tmp4, tmp5;

	//putchar1(0x53);
	//ToggleHeartbeatLED();

	//ClrHeartbeatLED;

	/*reset_rx_buff();
	if (send_orientation)
		putchar1(0xC5);
	else
		putchar1(0xCB);*/
	//putchar1(0xC2);
	//putchar1(0x55);

	unsigned char cmd_return;
	//SetHeartbeatLED;
	//__delay_ms(4);
	//ms_delay(2);
	//while (getchar1_cnt() < 43);
	//ClrHeartbeatLED;

	/*for (i = 0; i < 31; i++)
		Bresponse[i] = 	getchar1();*/
	//rec_ret = ReadNextRecord(&Record, &cmd_return);
#if 0
	int cnt = getchar1_cnt();
	//if (cnt >= 86)
	parsing = 0;

	if (rx1_cnt2 >= 43)
		rec_ret = ReadNextRecord(&ecat_record, &cmd_return);
	parsing = 1;
	ecat_record.setA[0] = cnt;
#endif
	/*if (rec_ret == 1)
	{
		//ToggleHeartbeatLED();
		for (i = 0; i < 3; i++)
		{
			ecat_record.setA[i] = Record.setA[i];
			ecat_record.setB[i] = Record.setB[i];
		}
	}*/

	/*if (cmd_return == 0xC2)
		ToggleHeartbeatLED();*/
	/*((unsigned char*)&tmp)[3] = Bresponse[1];
	((unsigned char*)&tmp)[2] = Bresponse[2];
	((unsigned char*)&tmp)[1] = Bresponse[3];
	((unsigned char*)&tmp)[0] = Bresponse[4];*/

	/*for (i = 0; i < 4; i++)
	{
		((unsigned char*)&tmp0)[3-i] = Bresponse[i+1];
		((unsigned char*)&tmp1)[3-i] = Bresponse[i+5];
		((unsigned char*)&tmp2)[3-i] = Bresponse[i+9];
		((unsigned char*)&tmp3)[3-i] = Bresponse[i+13];
		((unsigned char*)&tmp4)[3-i] = Bresponse[i+17];
		((unsigned char*)&tmp5)[3-i] = Bresponse[i+21];
	}
	ecat_record.setA[0] = tmp0;
	ecat_record.setA[1] = tmp1;
	ecat_record.setA[2] = tmp2;
	ecat_record.setB[0] = tmp3;
	ecat_record.setB[1] = tmp4;
	ecat_record.setB[2] = tmp5;*/

	/*Record.setA[0] = 33.4;
	Record.setA[1] = 33.7;
	Record.setA[2] = 33.9;
	Record.setB[0] = 34.7;
	Record.setB[1] = 39.7;
	Record.setB[2] = 41.4;*/

	
	/*Record.setA[0] = Bresponse[1];
	Record.setA[1] = Bresponse[2];
	Record.setA[2] = Bresponse[3];
	Record.setB[0] = Bresponse[4];*/
	//Record.setA[0] = getchar1_cnt();
	//Record.setA[1] = 66;
	//Record.setA[2] = rec_ret;

#if 0
	if (imu_first_time < 10)
	{
		//imu_first_time = 0;
		imu_first_time++;
        GX3command[3] = dataType;
		//status = sendBuffData(portNum, &GX3command[0], 4);
		for (i = 0; i < 4; i++)
			putchar1(GX3command[i]);
      	/*if(status == 0){
	       status = receiveData(portNum, &Bresponse[0], 4);*/
		/*for (i = 0; i < 4; i++)
		{
			time_out = 0;
			while(getchar1_cnt() == 0)
			{
				if (time_out++ > time_out_limit)
					break;
			}
			Bresponse[i] = getchar1();
		}*/
	       //status = sendBuffData(portNum, &GX3toobuff[0], 4);
		for (i = 0; i < 4; i++)
			putchar1(GX3toobuff[i]);
	    /*if(status == 0) {
	        status = receiveData(portNum, &Bresponse[0], 4);
		}*/
		/*for (i = 0; i < 4; i++)
		{
			time_out = 0;
			while(getchar1_cnt() == 0)
			{
				if (time_out++ > time_out_limit)
					break;
			}
			Bresponse[i] = getchar1();
		}*/
	}

	unsigned char cmd_return;

	if(ReadNextRecord(&Record, &cmd_return) != SUCCESS)
			error_record++;
	if (cmd_return == 0xC2){
		//move to the acceleration position and print the data
	    /*sprintf(consoleBuff, "\t%2.6f\t\t%2.6f\t\t%2.6f", Record.setA[0], Record.setA[1], Record.setA[2]);
	    setConXY(Curs_posX, Curs_posY -5, &consoleBuff[0]);
		sprintf(consoleBuff, "\t%2.6f\t\t%2.6f\t\t%2.6f", Record.setB[0], Record.setB[1], Record.setB[2]);
	    setConXY(Curs_posX, Curs_posY -0, &consoleBuff[0]);*/
		valid_rec++;
	}else if (cmd_return != 0xc4){
		if((cmd_return == 0xCB || cmd_return == 0xD3) && error_record == 0)
		    error_cmd = cmd_return;
		else
			error_record++;
	}
#endif	

#ifdef M3_FB_DEV_0_0
	//Insert UART protocol here ! /////////////////////////////////

#endif

#endif


}

#endif
