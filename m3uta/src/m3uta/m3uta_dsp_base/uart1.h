/* 
M3 -- Meka Robotics Real-Time Control System
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef _UART1_H
#define _UART1_H
#ifdef USE_UART
extern char uart_str[];
//extern unsigned char rx1_buf2[];
extern unsigned char * rx1_buf2;
extern volatile unsigned char rx1_cnt2;

void setup_uart(void);

typedef struct _I3dmgx3set /* struct for 18 position float and dword timer */
{
	float setA[3]; /*First  x y z  -or- First  Matrix set m1.1 m1.2 m1.3 */
	float setB[3]; /*Second x y z  -or- Second Matrix set m2.1 m2.2 m2.3 */
	float setC[3]; /*Third  x y z  -or- Third  Matrix set m3.1 m3.2 m3.3 */
	float setD[3]; /*First  Matrix set m1.1 m1.2 m1.3 */
	float setE[3]; /*Second Matrix set m2.1 m2.2 m2.3 */
	float setF[3]; /*Third  Matrix set m3.1 m3.2 m3.3 */
	unsigned long timer;
} I3dmgx3Set;

void reset_rx_buff(void);
int ReadNextRecord(I3dmgx3Set* pRecord, unsigned char * cmd_return);
int putchar1(unsigned char ch);
int puts1(char *s);
int getchar1(void);
int getchar1_cnt(void);
void setup_uart_230400(void);
void setup_uart_460800(void);

#define GETCHAR  getchar1
#define PUTS  puts1
#endif
#endif
