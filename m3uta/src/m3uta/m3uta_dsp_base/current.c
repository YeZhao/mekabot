/* 
M3 -- Meka Robotics Real-Time Control System
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef USE_CURRENT


#include "setup.h"
#include "current.h"

float i_mA;
int i_zero;
unsigned int i_zero_cnt;
long i_zero_sum;
float get_current_ma()
{
  return i_mA;
}

void step_current()
{
  
  //Measure zero sensor reading at startup
  if (i_zero_cnt<17 && i_zero_cnt>0)
  {
      i_zero_sum=i_zero_sum+(int)get_avg_adc(ADC_CURRENT_A);
      if (i_zero_cnt==1)
		i_zero=(int)(i_zero_sum>>4);
  }
  i_zero_cnt=MAX(0,i_zero_cnt-1);
  
#if defined M3_MAX2_BDC_A2R4
  int x=(int)get_avg_adc(ADC_CURRENT_A)-i_zero;
  i_mA=(float)x * (float)ADC_CURRENT_MA_PER_TICK ;
#endif

}

void setup_current()
{
  i_mA=0;
  i_zero=0;
  i_zero_cnt=200;
  i_zero_sum=0;
}

#endif //USE_CURRENT
