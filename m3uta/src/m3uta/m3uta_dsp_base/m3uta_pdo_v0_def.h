

#define M3PWR_CONFIG_BUZZER 1 	   //BUZZER




#define int16_t short
#define int32_t long
#define uint16_t unsigned short
#define uint32_t unsigned long
#define uint64_t unsigned long long

#define MAX_PDO_ENTRY_SIZE 30
#define MAX_PDO_SIZE_BYTES 150  //5 entries. Can be larger in theory.


typedef struct 
{
	int16_t		config;					//Reserved
	int16_t		enable_motor;				//Software enable of motor bus voltage (RP12)
	int16_t		mode;				//IMU mode
}M3UTAPwrPdoV0Cmd;

typedef struct 
{
	float			accelerometer[3];
	float			magnetometer[3];
	float			ang_vel[3];
	float			orientation_mtx[9]; //M11,M12,M13,M21,M22,M23,M31,M32,M33
	uint64_t		timestamp;			//Time in us
	int16_t			motor_enabled;			//State of the motor bus relay (RP6)
	int16_t			adc_bus_voltage;		//Voltage input (AN0)
	int16_t			adc_current_digital;		//Digital logic power consumption (AN1)
	int16_t			adc_ext;			//Auxillary adc input (AN2)
	int16_t			flags;				//Reserved
}M3UTAPwrPdoV0Status;
