/* 
M3 -- Meka Robotics Robot Components
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef M3_TORQUE_SHM_UTA_SDS_H
#define M3_TORQUE_SHM_UTA_SDS_H

#include <m3rt/base/m3rt_def.h>
#include <m3rt/base/m3ec_def.h>
#include <m3/chains/joint_array_mode.pb.h>
#include <m3/shared_mem/torque_shm_sds.h>

#define MAX_NDOF 15  // per limb

typedef struct 
{
	mReal			accelerometer[3];
	mReal			magnetometer[3];
	mReal			ang_vel[3];
	mReal			orientation_mtx[9]; //M11,M12,M13,M21,M22,M23,M31,M32,M33
}M3UTATorqueShmSdsImuStatus;

typedef struct 
{	
	mReal			theta[MAX_NDOF];			
	mReal			thetadot[MAX_NDOF];			
	mReal			torque[MAX_NDOF];		
	JOINT_ARRAY_MODE	ctrl_mode[MAX_NDOF];
	M3UTATorqueShmSdsImuStatus	imu;
}M3UTATorqueShmSdsMobileBaseStatus;

typedef struct 
{
	mReal			tq_desired[MAX_NDOF];	
}M3UTATorqueShmSdsMobileBaseCommand;


typedef struct
{
  int64_t	timestamp;
    M3TorqueShmSdsBaseStatus right_arm;
    M3TorqueShmSdsBaseStatus left_arm;
    M3TorqueShmSdsBaseStatus torso;
    M3TorqueShmSdsBaseStatus head;
    M3TorqueShmSdsBaseStatus right_hand;
    M3UTATorqueShmSdsMobileBaseStatus mobile_base;
}M3UTATorqueShmSdsStatus;

typedef struct
{
  int64_t	timestamp;
  M3TorqueShmSdsBaseCommand right_arm;
    M3TorqueShmSdsBaseCommand left_arm;
    M3TorqueShmSdsBaseCommand torso;
    M3TorqueShmSdsBaseCommand head;
    M3TorqueShmSdsBaseCommand right_hand;
    M3UTATorqueShmSdsMobileBaseCommand mobile_base;
}M3UTATorqueShmSdsCommand;

#endif