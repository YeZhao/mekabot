
#include <stdio.h>
#include <m3rt/base/component.h>
//#include <m3uta/controllers/example.h>
#include <torque_shm_uta.h>
#include <uta_imu.h>
#include <uta_imu_ec.h>
#include <uta_pwr_ec.h>
#include <uta_log.h>

///////////////////////////////////////////////////////
extern "C" 
{
///////////////////////////////////////////////////////
//These names should match the create_xxx() and destroy_xxx() function names.
//They should also match the names used for component definition in m3_config.yml 
//#define M3_EXAMPLE_NAME "m3example"
#define M3UTATORQUE_SHM_TYPE_NAME "m3torque_shm_uta"
#define M3UTA_IMU_NAME "m3uta_imu"
#define M3UTA_IMU_EC_NAME "m3uta_imu_ec"
#define M3UTA_PWR_EC_NAME "m3uta_pwr_ec"
#define M3UTA_LOG_NAME "m3uta_log"

///////////////////////////////////////////////////////
//Creators
//m3rt::M3Component * create_m3example(){return new m3uta::M3Example;}
m3rt::M3Component * create_m3torque_shm_uta(){return new m3uta::M3UTATorqueShm;}
m3rt::M3Component * create_m3uta_imu(){return new m3uta::M3UTAImu;}
m3rt::M3Component * create_m3uta_imu_ec(){return new m3uta::M3UTAImuEc;}
m3rt::M3Component * create_m3uta_pwr_ec(){return new m3uta::M3UTAPwrEc;}

m3rt::M3Component * create_m3uta_log(){return new m3uta::M3UTALog;}

//Deletors
//void destroy_m3example(m3rt::M3Component* c) {delete c;}
void destroy_m3torque_shm_uta(m3rt::M3Component* c) {delete c;}
void destroy_m3uta_imu(m3rt::M3Component* c) {delete c;}
void destroy_m3uta_imu_ec(m3rt::M3Component* c) {delete c;}
void destroy_m3uta_pwr_ec(m3rt::M3Component* c) {delete c;}

void destroy_m3uta_log(m3rt::M3Component* c) {delete c;}

///////////////////////////////////////////////////////
class M3FactoryProxy 
{ 
public:
	M3FactoryProxy()
	{
		//m3rt::creator_factory[M3_EXAMPLE_NAME] = create_m3example;
		//m3rt::destroyer_factory[M3_EXAMPLE_NAME] =  destroy_m3example;
		
		m3rt::creator_factory[M3UTA_LOG_NAME] =	create_m3uta_log;
		m3rt::destroyer_factory[M3UTA_LOG_NAME] =  destroy_m3uta_log;
		
		m3rt::creator_factory[M3UTATORQUE_SHM_TYPE_NAME] =	create_m3torque_shm_uta;
		m3rt::destroyer_factory[M3UTATORQUE_SHM_TYPE_NAME] =  destroy_m3torque_shm_uta;
		
		m3rt::creator_factory[M3UTA_IMU_EC_NAME] =	create_m3uta_imu_ec;
		m3rt::destroyer_factory[M3UTA_IMU_EC_NAME] =  destroy_m3uta_imu_ec;
		
		m3rt::creator_factory[M3UTA_IMU_NAME] =	create_m3uta_imu;
		m3rt::destroyer_factory[M3UTA_IMU_NAME] =  destroy_m3uta_imu;
		
		m3rt::creator_factory[M3UTA_PWR_EC_NAME] =	create_m3uta_pwr_ec;
		m3rt::destroyer_factory[M3UTA_PWR_EC_NAME] =  destroy_m3uta_pwr_ec;
	}
};
///////////////////////////////////////////////////////
// The library's one instance of the proxy
M3FactoryProxy proxy;
}
