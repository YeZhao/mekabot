/* 
M3 -- Meka Robotics Robot Components
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <torque_shm_uta.h>
#include "m3rt/base/component_factory.h"


namespace m3uta{
	
using namespace m3rt;
using namespace std;
using namespace m3;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
M3BaseStatus * M3UTATorqueShm::GetBaseStatus()
{
	return status.mutable_base();
}

void  M3UTATorqueShm::Startup()
{  
  M3CompShm::Startup();
  
  sds_status_size = sizeof(M3UTATorqueShmSdsStatus);
  sds_cmd_size = sizeof(M3UTATorqueShmSdsCommand);  
  
  if (bot)
  {
    for (int i = 0; i < bot->GetNdof(RIGHT_ARM); i++)
    {
	status.mutable_right_arm()->add_theta(0);
	status.mutable_right_arm()->add_thetadot(0);
	status.mutable_right_arm()->add_torque(0);
	status.mutable_right_arm()->add_ctrl_mode(JOINT_ARRAY_MODE_OFF);
	command.mutable_right_arm()->add_tq_desired(0);
    }  
    /*for (int i = 0; i < 6; i++)
      status.mutable_right_arm()->add_loadx6(0);*/
    
    for (int i = 0; i < bot->GetNdof(LEFT_ARM); i++)
    {
	status.mutable_left_arm()->add_theta(0);
	status.mutable_left_arm()->add_thetadot(0);
	status.mutable_left_arm()->add_torque(0);
	status.mutable_left_arm()->add_ctrl_mode(JOINT_ARRAY_MODE_OFF);
	command.mutable_left_arm()->add_tq_desired(0);
    }  
    /*for (int i = 0; i < 6; i++)
      status.mutable_left_arm()->add_loadx6(0);*/
    
    for (int i = 0; i < bot->GetNdof(TORSO); i++)
    {   
	status.mutable_torso()->add_theta(0);
	status.mutable_torso()->add_thetadot(0);
	status.mutable_torso()->add_torque(0);
	status.mutable_torso()->add_ctrl_mode(JOINT_ARRAY_MODE_OFF);
	command.mutable_torso()->add_tq_desired(0);
    } 
    /*for (int i = 0; i < 6; i++)
      status.mutable_torso()->add_loadx6(0);*/
    
  for (int i = 0; i < bot->GetNdof(HEAD); i++)
  {
      status.mutable_head()->add_theta(0);
      status.mutable_head()->add_thetadot(0);
      status.mutable_head()->add_torque(0);
      status.mutable_head()->add_ctrl_mode(JOINT_ARRAY_MODE_OFF);
      command.mutable_head()->add_tq_desired(0);
      command.mutable_head()->add_q_desired(0);
  }  
    /*for (int i = 0; i < 6; i++)
      status.mutable_head()->add_loadx6(0);*/
  }
  
  if (right_hand)
  {
      for (int i = 0; i < right_hand->GetNumDof(); i++)
      {
	  status.mutable_right_hand()->add_theta(0);
	  status.mutable_right_hand()->add_thetadot(0);
	  status.mutable_right_hand()->add_torque(0);
	  status.mutable_right_hand()->add_ctrl_mode(JOINT_ARRAY_MODE_OFF);
	  command.mutable_right_hand()->add_tq_desired(0);
	  command.mutable_right_hand()->add_q_desired(0);
      }  
  }
  
  if (mobile_base)
  {
    for (int i = 0; i < mobile_base->GetNumDof(); i++)
    {
	status.mutable_mobile_base()->add_theta(0);
	status.mutable_mobile_base()->add_thetadot(0);
	status.mutable_mobile_base()->add_torque(0);
	status.mutable_mobile_base()->add_ctrl_mode(JOINT_ARRAY_MODE_OFF);
	
	command.mutable_mobile_base()->add_tq_desired(0);
	command.mutable_mobile_base()->add_q_desired(0);
	command.mutable_mobile_base()->add_slew_rate_q_desired(0);	
    }    
  }
  
  if (mobile_base_loadx6)
  {
    for (int i = 0; i < mobile_base->GetNumDof(); i++)
      command.mutable_mobile_base()->add_tq_desired(0);
  }
  
  if (imu)
  {
    for (int i = 0; i < 3; i++)
    {
      status.mutable_mobile_base()->mutable_imu()->add_accelerometer(0.);
      status.mutable_mobile_base()->mutable_imu()->add_magnetometer(0.);
      status.mutable_mobile_base()->mutable_imu()->add_ang_vel(0.);
    }
    for (int i = 0; i < 9; i++)
    {
      status.mutable_mobile_base()->mutable_imu()->add_orientation_mtx(0.);
    }
  }
}

void M3UTATorqueShm::ResetCommandSds(unsigned char * sds)
{
  
  memset(sds,0,sizeof(M3UTATorqueShmSdsCommand));
  
}


size_t M3UTATorqueShm::GetStatusSdsSize()
{
	return sds_status_size;
}

size_t M3UTATorqueShm::GetCommandSdsSize()
{
	return sds_cmd_size;
}

void M3UTATorqueShm::SetCommandFromSds(unsigned char * data)
{
  M3UTATorqueShmSdsCommand * sds = (M3UTATorqueShmSdsCommand *) data;
    request_command();
   memcpy(&command_from_sds, sds, GetCommandSdsSize()); 
    release_command();
       
    command.set_timestamp(command_from_sds.timestamp);
    
    int64_t dt = GetBaseStatus()->timestamp()-command.timestamp(); // microseconds
    bool shm_timeout = ABS(dt) > (timeout*1000);    
       
  if (bot != NULL)
  {
    for (int i = 0; i < bot->GetNdof(RIGHT_ARM); i++)
    { 
      command.mutable_right_arm()->set_tq_desired(i, command_from_sds.right_arm.tq_desired[i]);
      if (shm_timeout)
      {      
	bot->DisableTorqueShmRightArm();
      } else{
	bot->EnableTorqueShmRightArm();
	bot->SetTorqueSharedMem_mNm(RIGHT_ARM, i, command.right_arm().tq_desired(i));
      }
    }
    for (int i = 0; i < bot->GetNdof(LEFT_ARM); i++)
    { 
      command.mutable_left_arm()->set_tq_desired(i, command_from_sds.left_arm.tq_desired[i]);
      if (shm_timeout)
      {
	bot->DisableTorqueShmLeftArm();  
      } else{
	bot->EnableTorqueShmLeftArm();
	bot->SetTorqueSharedMem_mNm(LEFT_ARM, i, command.left_arm().tq_desired(i));    
      }
    }
    for (int i = 0; i < bot->GetNdof(TORSO); i++)
    { 
      command.mutable_torso()->set_tq_desired(i, command_from_sds.torso.tq_desired[i]);
      if (shm_timeout)
      {
	bot->DisableTorqueShmTorso();
      } else{
	bot->EnableTorqueShmTorso();
	bot->SetTorqueSharedMem_mNm(TORSO, i, command.torso().tq_desired(i));    
      }
    }
    for (int i = 0; i < bot->GetNdof(HEAD); i++)
    { 
      command.mutable_head()->set_q_desired(i, command_from_sds.head.q_desired[i]);
      if (shm_timeout)
      {
	bot->DisableAngleShmHead();
      } else{
	bot->EnableAngleShmHead();
	bot->SetThetaSharedMem_Deg(HEAD, i, command.head().q_desired(i));
	bot->SetSlewRateSharedMem_Deg(HEAD, i, command_from_sds.head.slew_rate_q_desired[i]);    
      }
    }
  }
  
  if (right_hand)
  { 
      for (int i = 0; i < right_hand->GetNumDof(); i++)
      {
	((M3JointArrayCommand*)right_hand->GetCommand())->set_q_stiffness(i, command_from_sds.right_hand.q_stiffness[i]);      
	if (i == 0)
	{
	  command.mutable_right_hand()->set_q_desired(i, command_from_sds.right_hand.q_desired[i]);
	  if (shm_timeout)
	  {
	    ((M3JointArrayCommand*)right_hand->GetCommand())->set_ctrl_mode(i, JOINT_ARRAY_MODE_OFF);      
	  } else{
	    ((M3JointArrayCommand*)right_hand->GetCommand())->set_ctrl_mode(i, JOINT_ARRAY_MODE_THETA_GC);      
	    ((M3JointArrayCommand*)right_hand->GetCommand())->set_q_desired(i, command.right_hand().q_desired(i));
	    ((M3JointArrayCommand*)right_hand->GetCommand())->set_q_slew_rate(i, command_from_sds.right_hand.slew_rate_q_desired[i]);      
	  }
	} else {
	  command.mutable_right_hand()->set_tq_desired(i, command_from_sds.right_hand.tq_desired[i]);
	  if (shm_timeout)
	  {
	    ((M3JointArrayCommand*)right_hand->GetCommand())->set_ctrl_mode(i, JOINT_ARRAY_MODE_OFF);      
	  } else{
	    ((M3JointArrayCommand*)right_hand->GetCommand())->set_ctrl_mode(i, JOINT_ARRAY_MODE_TORQUE_GC);      
	    ((M3JointArrayCommand*)right_hand->GetCommand())->set_tq_desired(i, command.right_hand().tq_desired(i));      
	  }
	  
	}
      }
  }
  
  if (mobile_base != NULL) // only enable motor power if base present
  {
    for (int i = 0; i < mobile_base->GetNumDof(); i++)
    { 
      //M3_DEBUG("tq cmd: %f\n",command_from_sds.mobile_base.tq_desired[i]);
      command.mutable_mobile_base()->set_tq_desired(i, command_from_sds.mobile_base.tq_desired[i]);
      if (shm_timeout)
      {
	((M3JointArrayCommand*)mobile_base->GetCommand())->set_ctrl_mode(i, JOINT_ARRAY_MODE_OFF);      
      } else{
	((M3JointArrayCommand*)mobile_base->GetCommand())->set_ctrl_mode(i, JOINT_ARRAY_MODE_TORQUE);      
	((M3JointArrayCommand*)mobile_base->GetCommand())->set_tq_desired(i, command.mobile_base().tq_desired(i));      
      }    
    }
    if (pwr != NULL)
    {
      if (startup_motor_pwr_on)
	pwr->SetMotorEnable(true);
      if (imu)
      {
	if (imu_mode == 0)
	  ((M3UTAImuParam*)imu->GetParam())->set_mode(IMU_MODE_ACCEL_ANG_RATE_MAG);
	else if (imu_mode == 1)
	  ((M3UTAImuParam*)imu->GetParam())->set_mode(IMU_MODE_ORIENTATION);
      }
    }
  }
  
  
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void M3UTATorqueShm::SetSdsFromStatus(unsigned char * data)
{  
  status_to_sds.timestamp = GetBaseStatus()->timestamp(); 
  
  
  if (bot)
  {
    for (int i = 0; i < bot->GetNdof(RIGHT_ARM); i++)
    {    
      status.mutable_right_arm()->set_theta(i, bot->GetThetaDeg(RIGHT_ARM,i));
      status.mutable_right_arm()->set_thetadot(i, bot->GetThetaDotDeg(RIGHT_ARM,i));
      status.mutable_right_arm()->set_torque(i, bot->GetTorque_mNm(RIGHT_ARM,i));
      status.mutable_right_arm()->set_ctrl_mode(i, bot->GetMode(RIGHT_ARM,i));
      status_to_sds.right_arm.theta[i] = status.right_arm().theta(i);
      status_to_sds.right_arm.thetadot[i] = status.right_arm().thetadot(i);
      status_to_sds.right_arm.torque[i] = status.right_arm().torque(i);
      status_to_sds.right_arm.ctrl_mode[i] = status.right_arm().ctrl_mode(i);
    }
    if (right_loadx6)
    {
      for (int i = 0; i < 6; i++)
      {
	//status.mutable_right_arm()->set_loadx6(i, right_loadx6->GetWrench(i));
	status_to_sds.right_arm.wrench[i] = right_loadx6->GetWrench(i);
      }

    }
    for (int i = 0; i < bot->GetNdof(LEFT_ARM); i++)
    {    
      status.mutable_left_arm()->set_theta(i, bot->GetThetaDeg(LEFT_ARM,i));
      status.mutable_left_arm()->set_thetadot(i, bot->GetThetaDotDeg(LEFT_ARM,i));
      status.mutable_left_arm()->set_torque(i, bot->GetTorque_mNm(LEFT_ARM,i));
      status.mutable_left_arm()->set_ctrl_mode(i, bot->GetMode(LEFT_ARM,i));
      status_to_sds.left_arm.theta[i] = status.left_arm().theta(i);
      status_to_sds.left_arm.thetadot[i] = status.left_arm().thetadot(i);
      status_to_sds.left_arm.torque[i] = status.left_arm().torque(i);
      status_to_sds.left_arm.ctrl_mode[i] = status.left_arm().ctrl_mode(i);
    }
    if (left_loadx6)
    {
      for (int i = 0; i < 6; i++)
      {
	//status.mutable_left_arm()->set_loadx6(i, left_loadx6->GetWrench(i));
	status_to_sds.left_arm.wrench[i] = left_loadx6->GetWrench(i);
      }
    }
    for (int i = 0; i < bot->GetNdof(TORSO); i++)
    {    
      status.mutable_torso()->set_theta(i, bot->GetThetaDeg(TORSO,i));
      status.mutable_torso()->set_thetadot(i, bot->GetThetaDotDeg(TORSO,i));
      status.mutable_torso()->set_torque(i, bot->GetTorque_mNm(TORSO,i));
      status.mutable_torso()->set_ctrl_mode(i, bot->GetMode(TORSO,i));
      status_to_sds.torso.theta[i] = status.torso().theta(i);
      status_to_sds.torso.thetadot[i] = status.torso().thetadot(i);
      status_to_sds.torso.torque[i] = status.torso().torque(i);
      status_to_sds.torso.ctrl_mode[i] = status.torso().ctrl_mode(i);
    }
    
    for (int i = 0; i < bot->GetNdof(HEAD); i++)
    {    
      status.mutable_head()->set_theta(i, bot->GetThetaDeg(HEAD,i));
      status.mutable_head()->set_thetadot(i, bot->GetThetaDotDeg(HEAD,i));
      status.mutable_head()->set_torque(i, bot->GetTorque_mNm(HEAD,i));
      status.mutable_head()->set_ctrl_mode(i, bot->GetMode(HEAD,i));
      status_to_sds.head.theta[i] = status.head().theta(i);
      status_to_sds.head.thetadot[i] = status.head().thetadot(i);
      status_to_sds.head.torque[i] = status.head().torque(i);
      status_to_sds.head.ctrl_mode[i] = status.head().ctrl_mode(i);
    }
  }
  
  if (right_hand)
  { 
      for (int i = 0; i < right_hand->GetNumDof(); i++)
      {
	status.mutable_right_hand()->set_theta(i, right_hand->GetThetaDeg(i));
	status.mutable_right_hand()->set_thetadot(i, right_hand->GetThetaDotDeg(i));
	status.mutable_right_hand()->set_torque(i, right_hand->GetTorque(i));
	status.mutable_right_hand()->set_ctrl_mode(i, ((M3JointArrayCommand*)right_hand->GetCommand())->ctrl_mode(i));
	status_to_sds.right_hand.theta[i] = status.right_hand().theta(i);
	status_to_sds.right_hand.thetadot[i] = status.right_hand().thetadot(i);
	status_to_sds.right_hand.torque[i] = status.right_hand().torque(i);
	status_to_sds.right_hand.ctrl_mode[i] = status.right_hand().ctrl_mode(i);
      }
  }
  
  if (mobile_base)
  {
    for (int i = 0; i < mobile_base->GetNumDof(); i++)
    {    
      status.mutable_mobile_base()->set_theta(i, mobile_base->GetThetaDeg(i));
      status.mutable_mobile_base()->set_thetadot(i, mobile_base->GetThetaDotDeg(i));
      //status.mutable_mobile_base()->set_torque(i, mobile_base->GetTorque(i));
            
      status.mutable_mobile_base()->set_ctrl_mode(i, ((M3JointArrayCommand*)mobile_base->GetCommand())->ctrl_mode(i));
      status_to_sds.mobile_base.theta[i] = status.mobile_base().theta(i);
      status_to_sds.mobile_base.thetadot[i] = status.mobile_base().thetadot(i);      
      status_to_sds.mobile_base.ctrl_mode[i] = status.mobile_base().ctrl_mode(i);
    }    
  }
  
  if (mobile_base_loadx6)
  {
	M3LoadX6EcStatus * ec_status=(M3LoadX6EcStatus *)mobile_base_loadx6->GetStatus();
				
	status_to_sds.mobile_base.torque[0] = (mReal)ec_status->adc_load_0();
	status_to_sds.mobile_base.torque[1] = (mReal)ec_status->adc_load_1();
	status_to_sds.mobile_base.torque[2] = (mReal)ec_status->adc_load_2();
	//status_to_sds.mobile_base.torque[3] = (mReal)ec_status->adc_load_3();
	//status_to_sds.mobile_base.torque[4] = (mReal)ec_status->adc_load_4();
	//status_to_sds.mobile_base.torque[5] = (mReal)ec_status->adc_load_5();	
	
	for (int i = 0; i < mobile_base->GetNumDof(); i++)
	  status.mutable_mobile_base()->set_torque(i, status_to_sds.mobile_base.torque[i]);
  }
  
  if (imu)
    {
      for (int i = 0; i < 3; i++)
      {		
	status_to_sds.mobile_base.imu.accelerometer[i] = ((M3UTAImuStatus*)imu->GetStatus())->imu().accelerometer(i);
	status_to_sds.mobile_base.imu.magnetometer[i] = ((M3UTAImuStatus*)imu->GetStatus())->imu().magnetometer(i);
	status_to_sds.mobile_base.imu.ang_vel[i] = ((M3UTAImuStatus*)imu->GetStatus())->imu().ang_vel(i);
      }
      for (int i = 0; i < 9; i++)
	status_to_sds.mobile_base.imu.orientation_mtx[i] = ((M3UTAImuStatus*)imu->GetStatus())->imu().orientation_mtx(i);
    }
  
  
  M3UTATorqueShmSdsStatus * sds = (M3UTATorqueShmSdsStatus *) data;
  request_status();  
  memcpy(sds, &status_to_sds, GetStatusSdsSize());  
  release_status();
	

}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool M3UTATorqueShm::LinkDependentComponents()
{
	tmp_cnt = 0;
	if (humanoid_name.size()!=0)
	{		
		bot=(M3Humanoid*)factory->GetComponent(humanoid_name);
		
		if (bot==NULL)
		{
			//M3_DEBUG("a\n");
			M3_WARN("M3Humanoid component %s declared for M3UTATorqueShm but could not be linked\n", humanoid_name.c_str());
			//M3_DEBUG("b %d\n", tmp_cnt++);
		    //return false;
		}		
		right_loadx6=(M3LoadX6*)factory->GetComponent(right_loadx6_name); //May be null if not on this robot model
		left_loadx6=(M3LoadX6*)factory->GetComponent(left_loadx6_name);//May be null if not on this robot model
	}
	if (mobile_base_name.size()!=0)
	{		
		mobile_base=(M3JointArray*)factory->GetComponent(mobile_base_name);
		
		if (mobile_base==NULL)
		{
			M3_WARN("M3JointArray component %s declared for M3UTATorqueShm but could not be linked\n",
					mobile_base_name.c_str());
		    //return false;
		}		
	}
	
	if (mobile_base_loadx6_name.size()!=0)
	{			
		mobile_base_loadx6=(M3LoadX6Ec*)factory->GetComponent(mobile_base_loadx6_name);
		
		if (mobile_base_loadx6==NULL)
		{
			M3_WARN("M3LoadX6Ec component %s declared for M3UTATorqueShm but could not be linked\n",
					mobile_base_loadx6_name.c_str());
		    //return false;
		}				
	}
	
	if (pwr_name.size()!=0)
	{		
	    
		pwr=(M3Pwr*)factory->GetComponent(pwr_name);
		
		if (pwr==NULL)
		{
			M3_WARN("M3Pwr component %s declared for M3TorqueShm but could not be linked\n",
					pwr_name.c_str());
		    //return false;
		}				
	}
	
	if (imu_name.size()!=0)
	{		
	    
		imu=(M3UTAImu*)factory->GetComponent(imu_name);
		
		if (pwr==NULL)
		{
			M3_WARN("M3UTAImu component %s declared for M3TorqueShm but could not be linked\n",
					imu_name.c_str());
		    //return false;
		}				
	}
	
	if (right_hand_name.size()!=0)
	{
		right_hand=(M3Hand*)factory->GetComponent(right_hand_name);//May be null if not on this robot model
		if (right_hand==NULL)
		{
			M3_WARN("M3Hand component %s declared for M3TorqueShm but could not be linked\n",
					right_hand_name.c_str());
		    //return false;
		}				
	}
		
		
	if (pwr || mobile_base || bot || imu || right_hand)
	{	 
	  return true;	  
	} else {	  
	  M3_ERR("Could not link any components for UTA shared memory.\n");
	  return false;
	}
}

bool M3UTATorqueShm::ReadConfig(const char * filename)
{	
	if (!M3CompShm::ReadConfig(filename))
		return false;

	YAML::Node doc;	
	GetYamlDoc(filename, doc);	
	
	try{
	  doc["right_hand_component"] >>right_hand_name;	
	}
	catch(YAML::KeyNotFound& e)
	{
	  left_loadx6_name="";
	}
	
	try{
	  doc["humanoid_component"] >> humanoid_name;
	}
	catch(YAML::KeyNotFound& e)
	{
	  humanoid_name="";
	}
	
	
	try{
	  doc["mobile_base_loadx6_component"] >>mobile_base_loadx6_name;	
	}
	catch(YAML::KeyNotFound& e)
	{
	  mobile_base_loadx6_name="";
	}
	
	try{
	  doc["right_loadx6_component"] >>right_loadx6_name;	
	}
	catch(YAML::KeyNotFound& e)
	{
	  right_loadx6_name="";
	}
	
	try{
	  doc["left_loadx6_component"] >>left_loadx6_name;	
	}
	catch(YAML::KeyNotFound& e)
	{
	  left_loadx6_name="";
	}
	
	try{
	  doc["mobile_base_component"] >>mobile_base_name;	
	}
	catch(YAML::KeyNotFound& e)
	{
	  mobile_base_name="";
	}
	
	try{
	  doc["pwr_component"] >> pwr_name;	 
	}
	catch(YAML::KeyNotFound& e)
	{	  
	  pwr_name="";
	}
	
	try{
	  doc["imu_component"] >> imu_name;	 
	}
	catch(YAML::KeyNotFound& e)
	{	  
	  imu_name="";
	}
	
	
	try{
	  doc["startup_motor_pwr_on_mobile_base_only"]>>startup_motor_pwr_on;
	}
	catch(YAML::KeyNotFound& e)
	{
	  startup_motor_pwr_on=false;
	}
	
	try{
	  doc["imu_mode"]>>imu_mode;
	}
	catch(YAML::KeyNotFound& e)
	{
	  imu_mode = 1;
	}
	
	doc["timeout"] >> timeout;	
	
	return true;
}
}
   
