/* 
M3 -- Meka Robotics Robot Components
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef M3UTA_IMU_EC_H
#define M3UTA_IMU_EC_H

#include <m3rt/base/component.h>
#include <m3rt/base/component_ec.h>
#include "uta_imu_ec.pb.h"
#include <google/protobuf/message.h>


namespace m3uta{
using namespace std;


class M3UTAImuEc : public  m3rt::M3ComponentEc{
	public:
		M3UTAImuEc(): M3ComponentEc()
		{
			  RegisterVersion("default",DEFAULT);		
			  RegisterPdo("uta_pwr_pdo_v0",UTA_PWR_PDO_V0);	//HRL
		}
		google::protobuf::Message * GetCommand(){return &command;}
		google::protobuf::Message * GetStatus(){return &status;}
		google::protobuf::Message * GetParam(){return &param;}
		void Startup();
	protected:
		enum {DEFAULT};
		enum {UTA_PWR_PDO_V0};
		M3EtherCATStatus * GetEcStatus(){return status.mutable_ethercat();}
		size_t GetStatusPdoSize();
		size_t GetCommandPdoSize();
		void SetStatusFromPdo(unsigned char * data);
		void SetPdoFromCommand(unsigned char * data);
		
		M3BaseStatus * GetBaseStatus(){return status.mutable_base();}
		M3UTAImuEcStatus status;
		M3UTAImuEcCommand command;
		M3UTAImuEcParam param;
		int tmp_cnt;
		
};

}
#endif


