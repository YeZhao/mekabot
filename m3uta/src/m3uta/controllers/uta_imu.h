/* 
M3 -- Meka Robotics Robot Components
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef M3_UTA_IMU_H
#define M3_UTA_IMU_H



#include "uta_imu_ec.h"
#include "uta_imu.pb.h"
#include <m3rt/base/component.h>


namespace m3uta
{
	using namespace std;
	
	
class M3UTAImu : public m3rt::M3Component
{
public:
    M3UTAImu(): m3rt::M3Component()
        {
            RegisterVersion("default",DEFAULT);
        }
    google::protobuf::Message * GetCommand(){return &command;}
    google::protobuf::Message * GetStatus(){return &status;}
    google::protobuf::Message * GetParam(){return &param;}
public:
    void setIMU_Mode(IMU_MODE  mode){ param.set_mode(mode); }
    bool ReadConfig(const char * filename);		
protected:
    enum {DEFAULT};
    void Startup();
    void Shutdown(){}
    void StepStatus();
    void StepCommand();
    bool LinkDependentComponents();
    M3UTAImuStatus status;
    M3UTAImuCommand command;
    M3UTAImuParam param;
    M3BaseStatus * GetBaseStatus(){return status.mutable_base();}
private:
    
    M3UTAImuEc * ecc; 	//Corresponding EtherCAT component 
    string ecc_name;
};
    
    
}
                              
#endif


