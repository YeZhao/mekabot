/* 
M3 -- Meka Robotics Robot Components
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "uta_imu.h"
#include "m3rt/base/component_factory.h"

namespace m3uta{
	
using namespace m3rt;
using namespace std;

				 
///////////////////////////////////////////////////////

		
///////////////////////////////////////////////////////


void M3UTAImu::Startup()
{    
  
    for (int i = 0; i < 3; i++)
    {
      status.mutable_imu()->add_accelerometer(0.);
      status.mutable_imu()->add_magnetometer(0.);
      status.mutable_imu()->add_ang_vel(0.);		
    }
    
    for (int i = 0; i < 9; i++)
      status.mutable_imu()->add_orientation_mtx(0.);
}


bool M3UTAImu::LinkDependentComponents()
{
	ecc=(M3UTAImuEc*) factory->GetComponent(ecc_name);
	if (ecc==NULL)
	{
		M3_INFO("M3UTAPwrEc component %s not found for component %s\n",ecc_name.c_str(),GetName().c_str());
		return false;
	}
	return true;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void M3UTAImu::StepStatus()
{
	if (IsStateError())
	{
		return;
	}
	
	M3UTAImuEcStatus * uta_ec_status = (M3UTAImuEcStatus *)ecc->GetStatus();
	for (int i = 0; i < 3; i++)
	{
	  status.mutable_imu()->set_accelerometer(i, uta_ec_status->imu().accelerometer(i));
	  status.mutable_imu()->set_magnetometer(i, uta_ec_status->imu().magnetometer(i));
	  status.mutable_imu()->set_ang_vel(i, uta_ec_status->imu().ang_vel(i));
	}
	for (int i = 0; i < 9; i++)
	{
	  status.mutable_imu()->set_orientation_mtx(i,uta_ec_status->imu().orientation_mtx(i));
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool M3UTAImu::ReadConfig(const char * filename)
{
	YAML::Node doc;
	if (!M3Component::ReadConfig(filename))
		return false;
	GetYamlDoc(filename, doc);
	
	doc["ec_component"] >> ecc_name;
	return true;
}

void M3UTAImu::StepCommand()
{
	M3UTAImuEcParam * ec_param = (M3UTAImuEcParam *)ecc->GetParam();
	ec_param->set_mode(param.mode());
//        printf("imu mode in comp: %i \n", param.mode());
}

}
