/* 
M3 -- Meka Robotics Robot Components
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef M3_TORQUE_UTA_SHM_H
#define M3_TORQUE_UTA_SHM_H

#include "m3/shared_mem/comp_shm.h"
#include "torque_shm_uta.pb.h"
#include "m3/shared_mem/torque_shm.pb.h"
#include "m3/robots/humanoid.h"
#include "m3/chains/joint_array.h"
#include "m3/hardware/loadx6_ec.h"
#include "m3/hardware/loadx6.h"
#include "m3/hardware/pwr.h"
#include "m3/chains/hand.h"
#include "torque_shm_uta_sds.h"
#include "uta_imu.h"


namespace m3uta{
using namespace std;
using namespace m3;

class M3UTATorqueShm : public  m3::M3CompShm{
	public:
		M3UTATorqueShm(): sds_status_size(0),sds_cmd_size(0),M3CompShm(),bot(NULL),mobile_base(NULL),imu(NULL),pwr(NULL),mobile_base_loadx6(NULL),right_hand(NULL), right_loadx6(NULL), left_loadx6(NULL), tmp_cnt(0)
		{		  
		  RegisterVersion("default",DEFAULT);
		  RegisterVersion("iss",ISS);		  
		}
		google::protobuf::Message * GetCommand(){return &command;}
		google::protobuf::Message * GetStatus(){return &status;}
		google::protobuf::Message * GetParam(){return &param;}
	protected:
		bool ReadConfig(const char * filename);
		//M3ShmStatus * GetShmStatus(){return status.mutable_shm();}		
		size_t GetStatusSdsSize();
		size_t GetCommandSdsSize();
		void SetCommandFromSds(unsigned char * data);
		void SetSdsFromStatus(unsigned char * data);
		bool LinkDependentComponents();
		void ResetCommandSds(unsigned char * sds);
		void Startup();
		
		enum {DEFAULT, ISS};
		M3BaseStatus * GetBaseStatus();		
		M3UTATorqueShmCommand command;
		M3UTATorqueShmParam param;
		M3UTATorqueShmStatus status;
		M3Humanoid * bot;
		M3JointArray * mobile_base;
		M3LoadX6Ec * mobile_base_loadx6;		
		M3LoadX6 * right_loadx6;
		M3LoadX6 * left_loadx6;		
		M3Hand * right_hand;
		M3Pwr * pwr; // base pwr
		M3UTAImu * imu; 
		M3UTATorqueShmSdsCommand command_from_sds;
		M3UTATorqueShmSdsStatus status_to_sds;
		int sds_status_size;
		int sds_cmd_size;	
		string humanoid_name,right_loadx6_name,left_loadx6_name,mobile_base_loadx6_name, mobile_base_name, pwr_name, imu_name,right_hand_name;
		int64_t timeout;
		int tmp_cnt;
		int imu_mode;
		bool startup_motor_pwr_on;
};

}
#endif


