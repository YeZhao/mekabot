/* 
M3 -- Meka Robotics Robot Components
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef M3UTA_PWR_EC_H
#define M3UTA_PWR_EC_H

#include <m3rt/base/component.h>
#include <m3rt/base/component_ec.h>
#include <m3/hardware/pwr_ec.pb.h>
#include <m3/hardware/pwr_ec.h>
#include <google/protobuf/message.h>


namespace m3uta{
  
using namespace std;
using namespace m3;


class M3UTAPwrEc : public  M3PwrEc{
	public:
		M3UTAPwrEc(): M3PwrEc()
		{
			RegisterPdo("uta_pwr_pdo_v0",UTA_PWR_PDO_V0);	//HRL
		}
		
	protected:	
	      enum {UTA_PWR_PDO_V0};
		size_t GetStatusPdoSize();
		size_t GetCommandPdoSize();
		void SetStatusFromPdo(unsigned char * data);
		void SetPdoFromCommand(unsigned char * data);
		
	protected:
		
};

}
#endif


