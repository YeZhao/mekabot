/* 
M3 -- Meka Robotics Robot Components
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "uta_pwr_ec.h"
#include "m3uta_pdo_v0_def.h"
//#include "m3rt/base/m3ec_pdo_v2_def.h"
#include "m3rt/base/m3ec_def.h"

// should be commented out when using IMU
//#define USE_PWR_V2_PD0  // to test using normal pwr board w/o imu



namespace m3uta{
	
using namespace m3rt;
using namespace std;


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void M3UTAPwrEc::SetStatusFromPdo(unsigned char * data)
{
#ifdef USE_PWR_V2_PD0
	M3PwrPdoV2Status * ec = (M3PwrPdoV2Status *) data;
#else
	M3UTAPwrPdoV0Status * ec = (M3UTAPwrPdoV0Status *) data;
#endif	
	status.set_timestamp(ec->timestamp);
	status.set_motor_enabled(ec->motor_enabled);
	status.set_adc_bus_voltage(ec->adc_bus_voltage);
	status.set_adc_current_digital(ec->adc_current_digital);
	status.set_adc_ext_0(ec->adc_ext);
	status.set_flags(ec->flags);
		
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

size_t M3UTAPwrEc::GetStatusPdoSize()
{
#ifdef USE_PWR_V2_PD0  
	return sizeof(M3PwrPdoV2Status);
#else	
	return sizeof(M3UTAPwrPdoV0Status);
#endif	
}

size_t M3UTAPwrEc::GetCommandPdoSize()
{
#ifdef USE_PWR_V2_PD0  
	return sizeof(M3PwrPdoV2Cmd);
#else	
	return sizeof(M3UTAPwrPdoV0Cmd);
#endif	
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void M3UTAPwrEc::SetPdoFromCommand(unsigned char * data)
{
#ifdef USE_PWR_V2_PD0       
	M3PwrPdoV2Cmd * ec = (M3PwrPdoV2Cmd *) data;
#else	
	M3UTAPwrPdoV0Cmd * ec = (M3UTAPwrPdoV0Cmd *) data;
#endif	
	ec->config=param.config();
	ec->enable_motor=command.enable_motor();
	//Set buzzer if present
	if (enable_buzzer)
		ec->config=ec->config | M3PWR_CONFIG_BUZZER; 
	else
		ec->config=ec->config & ~M3PWR_CONFIG_BUZZER; 
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


}
   
