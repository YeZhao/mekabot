// M3 -- Meka Robotics Robot Components
// Copyright (c) 2010 Meka Robotics
// Author: edsinger@mekabot.com (Aaron Edsinger)
// 
// M3 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// M3 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M3.  If not, see <http://www.gnu.org/licenses/>.

option optimize_for = SPEED;
import "component_base.proto";

enum IMU_MODE{
		IMU_MODE_ACCEL_ANG_RATE_MAG = 0;					
		IMU_MODE_ORIENTATION = 1;					//Direct PWM control
}		

message M3UTAImuBaseStatus
{	
   repeated double		    accelerometer=1;					//
    repeated double			magnetometer=2;					//
    repeated double			ang_vel=3;				// 
    repeated double		orientation_mtx = 4;				//M11,M12,M13,M21,M22,M23,M31,M32,M33
}


message M3UTAImuEcStatus {
	optional M3BaseStatus			base=1;						//Reserved
	optional M3EtherCATStatus		ethercat =2;				//EtherCAT info
	optional M3UTAImuBaseStatus		imu=4;
}

message M3UTAImuEcCommand{
	optional double					test=1;				//Software enable of motor bus voltage
}

message M3UTAImuEcParam{
	optional IMU_MODE			mode=1;					//Reserved
}
