/* 
M3 -- Meka Robotics Robot Components
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "uta_imu_ec.h"
#include "m3uta_pdo_v0_def.h"
//#include "m3rt/base/m3ec_pdo_v2_def.h"

namespace m3uta{
	
using namespace m3rt;
using namespace std;



// should be commented out when using IMU
//#define DISABLE_IMU  // to test using normal pwr board

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void M3UTAImuEc::SetStatusFromPdo(unsigned char * data)
{
		M3UTAPwrPdoV0Status * ec = (M3UTAPwrPdoV0Status *) data;
#ifdef 	DISABLE_IMU		
		for (int i = 0; i < 3; i++)
		{
		  status.mutable_imu()->set_accelerometer(i, i);
		  status.mutable_imu()->set_magnetometer(i, i+1);
		  status.mutable_imu()->set_ang_vel(i, i+2);		
		}
		for (int i = 0; i < 9; i++)
		{
		  status.mutable_imu()->set_orientation_mtx(i, i+3);
		}
#else		
		for (int i = 0; i < 3; i++)
		{
		  status.mutable_imu()->set_accelerometer(i, ec->accelerometer[i]);
		  status.mutable_imu()->set_magnetometer(i, ec->magnetometer[i]);
		  status.mutable_imu()->set_ang_vel(i, ec->ang_vel[i]);
//                  printf("ang_vel: %f \n", ec->ang_vel[i]);
		}
		for (int i = 0; i < 9; i++)
		{
                    status.mutable_imu()->set_orientation_mtx(i, ec->orientation_mtx[i]);
//                    printf("ori: %f \n", ec->orientation_mtx[i]);
		}
//                printf("base state: %i \n" ,status.mutable_base()->state());
#endif		
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

size_t M3UTAImuEc::GetStatusPdoSize()
{
	
	return sizeof(M3UTAPwrPdoV0Status);	
}

size_t M3UTAImuEc::GetCommandPdoSize()
{
	
	return sizeof(M3UTAPwrPdoV0Cmd);
}

void M3UTAImuEc::Startup()
{
    M3ComponentEc::Startup();
  
    for (int i = 0; i < 3; i++)
    {
      status.mutable_imu()->add_accelerometer(0.);
      status.mutable_imu()->add_magnetometer(0.);
      status.mutable_imu()->add_ang_vel(0.);		
    }
    
    for (int i = 0; i < 9; i++)
        status.mutable_imu()->add_orientation_mtx(0.);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void M3UTAImuEc::SetPdoFromCommand(unsigned char * data)
{
    M3UTAPwrPdoV0Cmd * ec = (M3UTAPwrPdoV0Cmd *) data;
    ec->mode = param.mode();
//    printf("imu mode: %i \n", ec->mode);
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
   
