#! /usr/bin/python
# -*- coding: utf-8 -*-

#Copyright  2010, Meka Robotics
#All rights reserved.
#http://mekabot.com

#Redistribution and use in source and binary forms, with or without
#modification, are permitted. 


#THIS SOFTWARE IS PROVIDED BY THE Copyright HOLDERS AND CONTRIBUTORS
#"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#Copyright OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES INCLUDING,
#BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#POSSIBILITY OF SUCH DAMAGE.

import time
import m3.rt_proxy as m3p
import m3.toolbox as m3t
import m3.toolbox_beh as m3b
import m3.component_factory as m3f
import m3.unit_conversion as m3u
#import Numeric as nu
import numpy as nu
import m3.pwr
import math
import random
import m3.humanoid
import yaml

import m3uta_demo_head
import m3uta_demo_arm_beh


class TorsoBehaviors:
    def __init__(self,bot):
        self.bot=bot#m3.humanoid.M3Humanoid('x')#
	self.pan_des=0.0
	self.pan_slew=m3t.M3Slew()
	self.tilt_des=0.0
	self.tilt_slew=m3t.M3Slew()
    def stop(self):
	self.bot.set_mode_off('torso')
    def zero(self):
	self.bot.set_mode_theta_gc('torso')
	self.bot.set_theta_deg('torso',[0,0,0])
	self.bot.set_stiffness('torso',[0.5,1.0,1.0])
	self.bot.set_slew_rate_proportion('torso',[0.4,0.4,0.4])
	self.pan_des=0.0
	self.pan_slew.val=0
	self.tilt_des=0.0
	self.tilt_slew.val=0
    def bow(self):
	self.bot.set_mode_theta_gc('torso')
	self.bot.set_theta_deg('torso',[bot.get_theta_deg('torso')[0],5.0,5.0])
	self.bot.set_stiffness('torso',[0.5,1.0,1.0])
	self.bot.set_slew_rate_proportion('torso',[0.4,0.4,0.4])
	self.pan_des=0.0
	self.pan_slew.val=0
	self.tilt_des=0.0
	self.tilt_slew.val=0
    def track_head(self):
	#Pan
	qmax=30.0
	db=10.0
	slew_rate=0.5
	eye_pan=self.bot.get_theta_deg('head')[5]
	if abs(eye_pan)>db:
	    if eye_pan>0:
		self.pan_des=self.pan_slew.step(qmax,slew_rate)
	    else:
		self.pan_des=self.pan_slew.step(-qmax,slew_rate)
	#print 'Eye Pan',eye_pan,'Torso des',self.pan_des
	self.pan_des=min(max(-45.0,self.pan_des),45.0)
	#Tilt
	db=4.0
	slew_rate=0.5
	neck_tilt=self.bot.get_theta_deg('head')[0]
	#if abs(neck_tilt)>db:
	    #if neck_tilt>0:
		#self.tilt_des=self.pan_slew.step(10.0,slew_rate)
	    #else:
		#self.tilt_des=self.pan_slew.step(-2.9,slew_rate)
	self.tilt_des=self.tilt_slew.step(min(max(-0.25*neck_tilt,-4.0),4.0)+2.0,slew_rate)
	#print 'Neck tilt',neck_tilt,'Torso des',self.tilt_des
	
	self.bot.set_mode_theta_gc('torso')
	self.bot.set_stiffness('torso',[0.5,1.0,1.0])
	self.bot.set_slew_rate_proportion('torso',[0.3,0.4,0.4])
	self.bot.set_theta_deg('torso',[self.pan_des,self.tilt_des,self.tilt_des])

# ###################################################### 
class HandBehaviors:
    def __init__(self,hand,beh,body):
        self.hand=hand
	self.beh=beh
	self.body=body
	self.first_step=True
	self.open_start=True
	
    def is_contact(self):
	return self.body.is_contact()
    
    def stop(self):
	self.hand.set_mode_off()
    def open(self):
	self.hand.set_mode_theta_gc()
	self.hand.set_theta_deg([90.0,0,0,0,0])
	self.hand.set_stiffness([1.0]*5)
	self.hand.set_slew_rate_proportion([1.0]*5)
	if self.open_start:
	    self.time_start=time.time()
	self.open_start=False
	if time.time()-self.time_start>5.0: #turn off after 5s, just to avoid hearing chatter
	    self.hand.set_mode_off()
    def grasp(self):
	self.open_start=True
	self.hand.set_mode_theta_gc([0])
	self.hand.set_theta_deg([90.0],[0])
	self.hand.set_stiffness([1.0]*5)
	self.hand.set_slew_rate_proportion([1.0]*5)
	self.hand.set_mode_torque_gc([1,2,3,4])
	postures_close=[90.0,150.0,150.0,150.0,150.0]
	self.hand.set_torque_mNm(postures_close,[1,2,3,4])
	
	
#Glue class
# ###################################################### 
#class Sleeper:
    #def __init__(self):
	#pass
    #def start(self,proxy,bot,beh,head,arm,hand,torso):
	#self.bot=bot
	#self.beh=beh
	#self.head=head
	#self.body=body
	#self.hand=hand
	#self.torso=torso
	#self.first=True
    #def do_sleep(self):
	#self.beh.set_priority('eyelids','sleepy',5)
	#if self.first:
	    #self.first=False
	    #self.ts=time.time()
	#if time.time()-self.ts>25.0:
	    #print 'Waking...'
	    #self.beh.restore_priority('eyelids','sleepy')

    #def stop(self):
	#pass
# ###################################################### 	
if __name__ == '__main__':
    #Main creates bot and proxy, otherwise can be nested with other behavior script
    proxy = m3p.M3RtProxy()
    proxy.start()
    bot_name=m3t.get_robot_name()
    bot=m3f.create_component(bot_name)
    proxy.publish_param(bot) 
    proxy.subscribe_status(bot)
    proxy.publish_command(bot)
    proxy.make_operational_all()
    bot.set_motor_power_on()
    
    beh=m3b.M3BehaviorEngine(rate=.03)
    bb=m3uta_demo_arm_beh.BodyBehaviors()
    bb.start(proxy,bot,beh)
    
    print 'Use head? [y]'
    use_head=m3t.get_yes_no('y')
    print 'Use torso? [y]'
    use_torso=m3t.get_yes_no('y')
    print 'Use hand? [y]'
    use_hand=m3t.get_yes_no('y')

    
    if use_head:
	hb=m3uta_demo_head.HeadBehaviors()
	hb.start(proxy,bot,beh)
	
    if use_torso:
	tb=TorsoBehaviors(bot)
	beh.define_resource('torso')
	beh.always('torso','zero',priority=0,action=tb.zero)
	beh.random('torso','track_head',priority=1,action=tb.track_head,chance=0.1,timeout=30.0, inhibit=8.0)
	beh.random('torso','bow',priority=2,action=tb.bow,chance=0.005,timeout=4.0, inhibit=15.0)

    if use_hand:
	#Hand 
	right_hand=m3f.create_component('m3hand_mh8')
	proxy.subscribe_status(right_hand)
	proxy.publish_command(right_hand)
	proxy.publish_param(right_hand) 
	hdb=HandBehaviors(right_hand,beh,bb)
	beh.define_resource('right_hand')
	beh.always('right_hand','open',priority=0,action=hdb.open)
	#beh.always('step','contact_right',priority=0,action=beh_right_hand.step_contact)
	beh.whenever('right_hand','grasp',priority=1.0,action=hdb.grasp,cond=hdb.is_contact,timeout=5.0,inhibit=25.0)
    
	
    ts=time.time()
    proxy.step() #Initialize data
    try:
	while True:
	    proxy.step() 
	    beh.step(verbose=True)
    except (KeyboardInterrupt,EOFError):
	pass
    proxy.step()
    print 'Exiting...'
    if use_head:
	hb.stop()
    bb.stop()
    if use_torso:
	tb.stop()
    if use_hand:
	hdb.stop()
    bot.set_motor_power_off()
    proxy.step()
    time.sleep(0.25)
    proxy.stop()
