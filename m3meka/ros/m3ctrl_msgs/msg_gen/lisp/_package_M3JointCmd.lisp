(cl:in-package m3ctrl_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          CHAIN-VAL
          CHAIN
          CHAIN_IDX-VAL
          CHAIN_IDX
          STIFFNESS-VAL
          STIFFNESS
          VELOCITY-VAL
          VELOCITY
          POSITION-VAL
          POSITION
          EFFORT-VAL
          EFFORT
          CONTROL_MODE-VAL
          CONTROL_MODE
          SMOOTHING_MODE-VAL
          SMOOTHING_MODE
))