/* Auto-generated by genmsg_cpp for file /home/meka/mekabot/m3meka/ros/m3meka_msgs/srv/M3JointArrayCmd.srv */
#ifndef M3MEKA_MSGS_SERVICE_M3JOINTARRAYCMD_H
#define M3MEKA_MSGS_SERVICE_M3JOINTARRAYCMD_H
#include <string>
#include <vector>
#include <ostream>
#include "ros/serialization.h"
#include "ros/builtin_message_traits.h"
#include "ros/message_operations.h"
#include "ros/message.h"
#include "ros/time.h"

#include "ros/service_traits.h"




namespace m3meka_msgs
{
template <class ContainerAllocator>
struct M3JointArrayCmdRequest_ : public ros::Message
{
  typedef M3JointArrayCmdRequest_<ContainerAllocator> Type;

  M3JointArrayCmdRequest_()
  : tq_desired()
  , pwm_desired()
  , q_stiffness()
  , ctrl_mode()
  , q_desired()
  , pos_desired()
  , qdot_desired()
  , q_slew_rate()
  {
  }

  M3JointArrayCmdRequest_(const ContainerAllocator& _alloc)
  : tq_desired(_alloc)
  , pwm_desired(_alloc)
  , q_stiffness(_alloc)
  , ctrl_mode(_alloc)
  , q_desired(_alloc)
  , pos_desired(_alloc)
  , qdot_desired(_alloc)
  , q_slew_rate(_alloc)
  {
  }

  typedef std::vector<float, typename ContainerAllocator::template rebind<float>::other >  _tq_desired_type;
  std::vector<float, typename ContainerAllocator::template rebind<float>::other >  tq_desired;

  typedef std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other >  _pwm_desired_type;
  std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other >  pwm_desired;

  typedef std::vector<float, typename ContainerAllocator::template rebind<float>::other >  _q_stiffness_type;
  std::vector<float, typename ContainerAllocator::template rebind<float>::other >  q_stiffness;

  typedef std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other >  _ctrl_mode_type;
  std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other >  ctrl_mode;

  typedef std::vector<float, typename ContainerAllocator::template rebind<float>::other >  _q_desired_type;
  std::vector<float, typename ContainerAllocator::template rebind<float>::other >  q_desired;

  typedef std::vector<float, typename ContainerAllocator::template rebind<float>::other >  _pos_desired_type;
  std::vector<float, typename ContainerAllocator::template rebind<float>::other >  pos_desired;

  typedef std::vector<float, typename ContainerAllocator::template rebind<float>::other >  _qdot_desired_type;
  std::vector<float, typename ContainerAllocator::template rebind<float>::other >  qdot_desired;

  typedef std::vector<float, typename ContainerAllocator::template rebind<float>::other >  _q_slew_rate_type;
  std::vector<float, typename ContainerAllocator::template rebind<float>::other >  q_slew_rate;


  ROSCPP_DEPRECATED uint32_t get_tq_desired_size() const { return (uint32_t)tq_desired.size(); }
  ROSCPP_DEPRECATED void set_tq_desired_size(uint32_t size) { tq_desired.resize((size_t)size); }
  ROSCPP_DEPRECATED void get_tq_desired_vec(std::vector<float, typename ContainerAllocator::template rebind<float>::other > & vec) const { vec = this->tq_desired; }
  ROSCPP_DEPRECATED void set_tq_desired_vec(const std::vector<float, typename ContainerAllocator::template rebind<float>::other > & vec) { this->tq_desired = vec; }
  ROSCPP_DEPRECATED uint32_t get_pwm_desired_size() const { return (uint32_t)pwm_desired.size(); }
  ROSCPP_DEPRECATED void set_pwm_desired_size(uint32_t size) { pwm_desired.resize((size_t)size); }
  ROSCPP_DEPRECATED void get_pwm_desired_vec(std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other > & vec) const { vec = this->pwm_desired; }
  ROSCPP_DEPRECATED void set_pwm_desired_vec(const std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other > & vec) { this->pwm_desired = vec; }
  ROSCPP_DEPRECATED uint32_t get_q_stiffness_size() const { return (uint32_t)q_stiffness.size(); }
  ROSCPP_DEPRECATED void set_q_stiffness_size(uint32_t size) { q_stiffness.resize((size_t)size); }
  ROSCPP_DEPRECATED void get_q_stiffness_vec(std::vector<float, typename ContainerAllocator::template rebind<float>::other > & vec) const { vec = this->q_stiffness; }
  ROSCPP_DEPRECATED void set_q_stiffness_vec(const std::vector<float, typename ContainerAllocator::template rebind<float>::other > & vec) { this->q_stiffness = vec; }
  ROSCPP_DEPRECATED uint32_t get_ctrl_mode_size() const { return (uint32_t)ctrl_mode.size(); }
  ROSCPP_DEPRECATED void set_ctrl_mode_size(uint32_t size) { ctrl_mode.resize((size_t)size); }
  ROSCPP_DEPRECATED void get_ctrl_mode_vec(std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other > & vec) const { vec = this->ctrl_mode; }
  ROSCPP_DEPRECATED void set_ctrl_mode_vec(const std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other > & vec) { this->ctrl_mode = vec; }
  ROSCPP_DEPRECATED uint32_t get_q_desired_size() const { return (uint32_t)q_desired.size(); }
  ROSCPP_DEPRECATED void set_q_desired_size(uint32_t size) { q_desired.resize((size_t)size); }
  ROSCPP_DEPRECATED void get_q_desired_vec(std::vector<float, typename ContainerAllocator::template rebind<float>::other > & vec) const { vec = this->q_desired; }
  ROSCPP_DEPRECATED void set_q_desired_vec(const std::vector<float, typename ContainerAllocator::template rebind<float>::other > & vec) { this->q_desired = vec; }
  ROSCPP_DEPRECATED uint32_t get_pos_desired_size() const { return (uint32_t)pos_desired.size(); }
  ROSCPP_DEPRECATED void set_pos_desired_size(uint32_t size) { pos_desired.resize((size_t)size); }
  ROSCPP_DEPRECATED void get_pos_desired_vec(std::vector<float, typename ContainerAllocator::template rebind<float>::other > & vec) const { vec = this->pos_desired; }
  ROSCPP_DEPRECATED void set_pos_desired_vec(const std::vector<float, typename ContainerAllocator::template rebind<float>::other > & vec) { this->pos_desired = vec; }
  ROSCPP_DEPRECATED uint32_t get_qdot_desired_size() const { return (uint32_t)qdot_desired.size(); }
  ROSCPP_DEPRECATED void set_qdot_desired_size(uint32_t size) { qdot_desired.resize((size_t)size); }
  ROSCPP_DEPRECATED void get_qdot_desired_vec(std::vector<float, typename ContainerAllocator::template rebind<float>::other > & vec) const { vec = this->qdot_desired; }
  ROSCPP_DEPRECATED void set_qdot_desired_vec(const std::vector<float, typename ContainerAllocator::template rebind<float>::other > & vec) { this->qdot_desired = vec; }
  ROSCPP_DEPRECATED uint32_t get_q_slew_rate_size() const { return (uint32_t)q_slew_rate.size(); }
  ROSCPP_DEPRECATED void set_q_slew_rate_size(uint32_t size) { q_slew_rate.resize((size_t)size); }
  ROSCPP_DEPRECATED void get_q_slew_rate_vec(std::vector<float, typename ContainerAllocator::template rebind<float>::other > & vec) const { vec = this->q_slew_rate; }
  ROSCPP_DEPRECATED void set_q_slew_rate_vec(const std::vector<float, typename ContainerAllocator::template rebind<float>::other > & vec) { this->q_slew_rate = vec; }
private:
  static const char* __s_getDataType_() { return "m3meka_msgs/M3JointArrayCmdRequest"; }
public:
  ROSCPP_DEPRECATED static const std::string __s_getDataType() { return __s_getDataType_(); }

  ROSCPP_DEPRECATED const std::string __getDataType() const { return __s_getDataType_(); }

private:
  static const char* __s_getMD5Sum_() { return "5772a6ced2d04d1b8557faf86efbafdb"; }
public:
  ROSCPP_DEPRECATED static const std::string __s_getMD5Sum() { return __s_getMD5Sum_(); }

  ROSCPP_DEPRECATED const std::string __getMD5Sum() const { return __s_getMD5Sum_(); }

private:
  static const char* __s_getServerMD5Sum_() { return "4432a41b7a22c858d4c20d1bddbf0718"; }
public:
  ROSCPP_DEPRECATED static const std::string __s_getServerMD5Sum() { return __s_getServerMD5Sum_(); }

  ROSCPP_DEPRECATED const std::string __getServerMD5Sum() const { return __s_getServerMD5Sum_(); }

private:
  static const char* __s_getMessageDefinition_() { return "float32[] tq_desired\n\
int32[] pwm_desired\n\
float32[] q_stiffness\n\
int32[] ctrl_mode\n\
float32[] q_desired\n\
float32[] pos_desired\n\
float32[] qdot_desired\n\
float32[] q_slew_rate\n\
\n\
"; }
public:
  ROSCPP_DEPRECATED static const std::string __s_getMessageDefinition() { return __s_getMessageDefinition_(); }

  ROSCPP_DEPRECATED const std::string __getMessageDefinition() const { return __s_getMessageDefinition_(); }

  ROSCPP_DEPRECATED virtual uint8_t *serialize(uint8_t *write_ptr, uint32_t seq) const
  {
    ros::serialization::OStream stream(write_ptr, 1000000000);
    ros::serialization::serialize(stream, tq_desired);
    ros::serialization::serialize(stream, pwm_desired);
    ros::serialization::serialize(stream, q_stiffness);
    ros::serialization::serialize(stream, ctrl_mode);
    ros::serialization::serialize(stream, q_desired);
    ros::serialization::serialize(stream, pos_desired);
    ros::serialization::serialize(stream, qdot_desired);
    ros::serialization::serialize(stream, q_slew_rate);
    return stream.getData();
  }

  ROSCPP_DEPRECATED virtual uint8_t *deserialize(uint8_t *read_ptr)
  {
    ros::serialization::IStream stream(read_ptr, 1000000000);
    ros::serialization::deserialize(stream, tq_desired);
    ros::serialization::deserialize(stream, pwm_desired);
    ros::serialization::deserialize(stream, q_stiffness);
    ros::serialization::deserialize(stream, ctrl_mode);
    ros::serialization::deserialize(stream, q_desired);
    ros::serialization::deserialize(stream, pos_desired);
    ros::serialization::deserialize(stream, qdot_desired);
    ros::serialization::deserialize(stream, q_slew_rate);
    return stream.getData();
  }

  ROSCPP_DEPRECATED virtual uint32_t serializationLength() const
  {
    uint32_t size = 0;
    size += ros::serialization::serializationLength(tq_desired);
    size += ros::serialization::serializationLength(pwm_desired);
    size += ros::serialization::serializationLength(q_stiffness);
    size += ros::serialization::serializationLength(ctrl_mode);
    size += ros::serialization::serializationLength(q_desired);
    size += ros::serialization::serializationLength(pos_desired);
    size += ros::serialization::serializationLength(qdot_desired);
    size += ros::serialization::serializationLength(q_slew_rate);
    return size;
  }

  typedef boost::shared_ptr< ::m3meka_msgs::M3JointArrayCmdRequest_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::m3meka_msgs::M3JointArrayCmdRequest_<ContainerAllocator>  const> ConstPtr;
}; // struct M3JointArrayCmdRequest
typedef  ::m3meka_msgs::M3JointArrayCmdRequest_<std::allocator<void> > M3JointArrayCmdRequest;

typedef boost::shared_ptr< ::m3meka_msgs::M3JointArrayCmdRequest> M3JointArrayCmdRequestPtr;
typedef boost::shared_ptr< ::m3meka_msgs::M3JointArrayCmdRequest const> M3JointArrayCmdRequestConstPtr;


template <class ContainerAllocator>
struct M3JointArrayCmdResponse_ : public ros::Message
{
  typedef M3JointArrayCmdResponse_<ContainerAllocator> Type;

  M3JointArrayCmdResponse_()
  : response(0)
  {
  }

  M3JointArrayCmdResponse_(const ContainerAllocator& _alloc)
  : response(0)
  {
  }

  typedef int32_t _response_type;
  int32_t response;


private:
  static const char* __s_getDataType_() { return "m3meka_msgs/M3JointArrayCmdResponse"; }
public:
  ROSCPP_DEPRECATED static const std::string __s_getDataType() { return __s_getDataType_(); }

  ROSCPP_DEPRECATED const std::string __getDataType() const { return __s_getDataType_(); }

private:
  static const char* __s_getMD5Sum_() { return "f45f68e2feefb1307598e828e260b7d7"; }
public:
  ROSCPP_DEPRECATED static const std::string __s_getMD5Sum() { return __s_getMD5Sum_(); }

  ROSCPP_DEPRECATED const std::string __getMD5Sum() const { return __s_getMD5Sum_(); }

private:
  static const char* __s_getServerMD5Sum_() { return "4432a41b7a22c858d4c20d1bddbf0718"; }
public:
  ROSCPP_DEPRECATED static const std::string __s_getServerMD5Sum() { return __s_getServerMD5Sum_(); }

  ROSCPP_DEPRECATED const std::string __getServerMD5Sum() const { return __s_getServerMD5Sum_(); }

private:
  static const char* __s_getMessageDefinition_() { return "int32 response\n\
\n\
\n\
\n\
"; }
public:
  ROSCPP_DEPRECATED static const std::string __s_getMessageDefinition() { return __s_getMessageDefinition_(); }

  ROSCPP_DEPRECATED const std::string __getMessageDefinition() const { return __s_getMessageDefinition_(); }

  ROSCPP_DEPRECATED virtual uint8_t *serialize(uint8_t *write_ptr, uint32_t seq) const
  {
    ros::serialization::OStream stream(write_ptr, 1000000000);
    ros::serialization::serialize(stream, response);
    return stream.getData();
  }

  ROSCPP_DEPRECATED virtual uint8_t *deserialize(uint8_t *read_ptr)
  {
    ros::serialization::IStream stream(read_ptr, 1000000000);
    ros::serialization::deserialize(stream, response);
    return stream.getData();
  }

  ROSCPP_DEPRECATED virtual uint32_t serializationLength() const
  {
    uint32_t size = 0;
    size += ros::serialization::serializationLength(response);
    return size;
  }

  typedef boost::shared_ptr< ::m3meka_msgs::M3JointArrayCmdResponse_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::m3meka_msgs::M3JointArrayCmdResponse_<ContainerAllocator>  const> ConstPtr;
}; // struct M3JointArrayCmdResponse
typedef  ::m3meka_msgs::M3JointArrayCmdResponse_<std::allocator<void> > M3JointArrayCmdResponse;

typedef boost::shared_ptr< ::m3meka_msgs::M3JointArrayCmdResponse> M3JointArrayCmdResponsePtr;
typedef boost::shared_ptr< ::m3meka_msgs::M3JointArrayCmdResponse const> M3JointArrayCmdResponseConstPtr;

struct M3JointArrayCmd
{

typedef M3JointArrayCmdRequest Request;
typedef M3JointArrayCmdResponse Response;
Request request;
Response response;

typedef Request RequestType;
typedef Response ResponseType;
}; // struct M3JointArrayCmd
} // namespace m3meka_msgs

namespace ros
{
namespace message_traits
{
template<class ContainerAllocator>
struct MD5Sum< ::m3meka_msgs::M3JointArrayCmdRequest_<ContainerAllocator> > {
  static const char* value() 
  {
    return "5772a6ced2d04d1b8557faf86efbafdb";
  }

  static const char* value(const  ::m3meka_msgs::M3JointArrayCmdRequest_<ContainerAllocator> &) { return value(); } 
  static const uint64_t static_value1 = 0x5772a6ced2d04d1bULL;
  static const uint64_t static_value2 = 0x8557faf86efbafdbULL;
};

template<class ContainerAllocator>
struct DataType< ::m3meka_msgs::M3JointArrayCmdRequest_<ContainerAllocator> > {
  static const char* value() 
  {
    return "m3meka_msgs/M3JointArrayCmdRequest";
  }

  static const char* value(const  ::m3meka_msgs::M3JointArrayCmdRequest_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator>
struct Definition< ::m3meka_msgs::M3JointArrayCmdRequest_<ContainerAllocator> > {
  static const char* value() 
  {
    return "float32[] tq_desired\n\
int32[] pwm_desired\n\
float32[] q_stiffness\n\
int32[] ctrl_mode\n\
float32[] q_desired\n\
float32[] pos_desired\n\
float32[] qdot_desired\n\
float32[] q_slew_rate\n\
\n\
";
  }

  static const char* value(const  ::m3meka_msgs::M3JointArrayCmdRequest_<ContainerAllocator> &) { return value(); } 
};

} // namespace message_traits
} // namespace ros


namespace ros
{
namespace message_traits
{
template<class ContainerAllocator>
struct MD5Sum< ::m3meka_msgs::M3JointArrayCmdResponse_<ContainerAllocator> > {
  static const char* value() 
  {
    return "f45f68e2feefb1307598e828e260b7d7";
  }

  static const char* value(const  ::m3meka_msgs::M3JointArrayCmdResponse_<ContainerAllocator> &) { return value(); } 
  static const uint64_t static_value1 = 0xf45f68e2feefb130ULL;
  static const uint64_t static_value2 = 0x7598e828e260b7d7ULL;
};

template<class ContainerAllocator>
struct DataType< ::m3meka_msgs::M3JointArrayCmdResponse_<ContainerAllocator> > {
  static const char* value() 
  {
    return "m3meka_msgs/M3JointArrayCmdResponse";
  }

  static const char* value(const  ::m3meka_msgs::M3JointArrayCmdResponse_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator>
struct Definition< ::m3meka_msgs::M3JointArrayCmdResponse_<ContainerAllocator> > {
  static const char* value() 
  {
    return "int32 response\n\
\n\
\n\
\n\
";
  }

  static const char* value(const  ::m3meka_msgs::M3JointArrayCmdResponse_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator> struct IsFixedSize< ::m3meka_msgs::M3JointArrayCmdResponse_<ContainerAllocator> > : public TrueType {};
} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

template<class ContainerAllocator> struct Serializer< ::m3meka_msgs::M3JointArrayCmdRequest_<ContainerAllocator> >
{
  template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
  {
    stream.next(m.tq_desired);
    stream.next(m.pwm_desired);
    stream.next(m.q_stiffness);
    stream.next(m.ctrl_mode);
    stream.next(m.q_desired);
    stream.next(m.pos_desired);
    stream.next(m.qdot_desired);
    stream.next(m.q_slew_rate);
  }

  ROS_DECLARE_ALLINONE_SERIALIZER;
}; // struct M3JointArrayCmdRequest_
} // namespace serialization
} // namespace ros


namespace ros
{
namespace serialization
{

template<class ContainerAllocator> struct Serializer< ::m3meka_msgs::M3JointArrayCmdResponse_<ContainerAllocator> >
{
  template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
  {
    stream.next(m.response);
  }

  ROS_DECLARE_ALLINONE_SERIALIZER;
}; // struct M3JointArrayCmdResponse_
} // namespace serialization
} // namespace ros

namespace ros
{
namespace service_traits
{
template<>
struct MD5Sum<m3meka_msgs::M3JointArrayCmd> {
  static const char* value() 
  {
    return "4432a41b7a22c858d4c20d1bddbf0718";
  }

  static const char* value(const m3meka_msgs::M3JointArrayCmd&) { return value(); } 
};

template<>
struct DataType<m3meka_msgs::M3JointArrayCmd> {
  static const char* value() 
  {
    return "m3meka_msgs/M3JointArrayCmd";
  }

  static const char* value(const m3meka_msgs::M3JointArrayCmd&) { return value(); } 
};

template<class ContainerAllocator>
struct MD5Sum<m3meka_msgs::M3JointArrayCmdRequest_<ContainerAllocator> > {
  static const char* value() 
  {
    return "4432a41b7a22c858d4c20d1bddbf0718";
  }

  static const char* value(const m3meka_msgs::M3JointArrayCmdRequest_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator>
struct DataType<m3meka_msgs::M3JointArrayCmdRequest_<ContainerAllocator> > {
  static const char* value() 
  {
    return "m3meka_msgs/M3JointArrayCmd";
  }

  static const char* value(const m3meka_msgs::M3JointArrayCmdRequest_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator>
struct MD5Sum<m3meka_msgs::M3JointArrayCmdResponse_<ContainerAllocator> > {
  static const char* value() 
  {
    return "4432a41b7a22c858d4c20d1bddbf0718";
  }

  static const char* value(const m3meka_msgs::M3JointArrayCmdResponse_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator>
struct DataType<m3meka_msgs::M3JointArrayCmdResponse_<ContainerAllocator> > {
  static const char* value() 
  {
    return "m3meka_msgs/M3JointArrayCmd";
  }

  static const char* value(const m3meka_msgs::M3JointArrayCmdResponse_<ContainerAllocator> &) { return value(); } 
};

} // namespace service_traits
} // namespace ros

#endif // M3MEKA_MSGS_SERVICE_M3JOINTARRAYCMD_H

