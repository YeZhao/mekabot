# Generated by the protocol buffer compiler.  DO NOT EDIT!

from google.protobuf import descriptor
from google.protobuf import message
from google.protobuf import reflection
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)


import component_base_pb2
import smoothing_mode_pb2

DESCRIPTOR = descriptor.FileDescriptor(
  name='joint.proto',
  package='',
  serialized_pb='\n\x0bjoint.proto\x1a\x14\x63omponent_base.proto\x1a\x14smoothing_mode.proto\"\xac\x02\n\rM3JointStatus\x12\x1b\n\x04\x62\x61se\x18\x01 \x01(\x0b\x32\r.M3BaseStatus\x12\x12\n\nmotor_temp\x18\x02 \x01(\x01\x12\x10\n\x08\x61mp_temp\x18\x03 \x01(\x01\x12\x0f\n\x07\x63urrent\x18\x04 \x01(\x01\x12\x0e\n\x06torque\x18\x06 \x01(\x01\x12\x11\n\ttorquedot\x18\x07 \x01(\x01\x12\r\n\x05theta\x18\x08 \x01(\x01\x12\x10\n\x08thetadot\x18\t \x01(\x01\x12\x13\n\x0bthetadotdot\x18\n \x01(\x01\x12\x16\n\x0etorque_gravity\x18\x0b \x01(\x01\x12\x0f\n\x07pwm_cmd\x18\x0c \x01(\x05\x12\x14\n\x0c\x61mbient_temp\x18\r \x01(\x01\x12\x11\n\tcase_temp\x18\x0e \x01(\x01\x12\r\n\x05power\x18\x0f \x01(\x01\x12\r\n\x05\x66lags\x18\x10 \x01(\x05\"\xa0\x03\n\x0cM3JointParam\x12\x0c\n\x04kq_p\x18\x01 \x01(\x01\x12\x0c\n\x04kq_i\x18\x02 \x01(\x01\x12\x0c\n\x04kq_d\x18\x03 \x01(\x01\x12\x12\n\nkq_i_limit\x18\x04 \x01(\x01\x12\r\n\x05max_q\x18\x05 \x01(\x01\x12\r\n\x05min_q\x18\x06 \x01(\x01\x12\x0c\n\x04kt_p\x18\x07 \x01(\x01\x12\x0c\n\x04kt_i\x18\x08 \x01(\x01\x12\x0c\n\x04kt_d\x18\t \x01(\x01\x12\x12\n\nkt_i_limit\x18\n \x01(\x01\x12\x0c\n\x04kq_g\x18\x0b \x01(\x01\x12\x17\n\x0fmax_q_slew_rate\x18\x0c \x01(\x01\x12\x12\n\nkq_i_range\x18\r \x01(\x01\x12\x12\n\nkt_i_range\x18\x0e \x01(\x01\x12\x11\n\tmax_q_pad\x18\x0f \x01(\x01\x12\x11\n\tmin_q_pad\x18\x10 \x01(\x01\x12\x11\n\tkq_d_pose\x18\x11 \x01(\x01\x12\x12\n\nkq_p_tq_gm\x18\x12 \x01(\x01\x12\x12\n\nkq_i_tq_gm\x18\x13 \x01(\x01\x12\x12\n\nkq_d_tq_gm\x18\x14 \x01(\x01\x12\x18\n\x10kq_i_limit_tq_gm\x18\x15 \x01(\x01\x12\x18\n\x10kq_i_range_tq_gm\x18\x16 \x01(\x01\"\xe8\x01\n\x0eM3JointCommand\x12\x12\n\ntq_desired\x18\x01 \x01(\x01\x12\x11\n\tq_desired\x18\x02 \x01(\x01\x12\x13\n\x0bpwm_desired\x18\x03 \x01(\x05\x12\x13\n\x0bq_stiffness\x18\x04 \x01(\x01\x12\x1e\n\tctrl_mode\x18\x05 \x01(\x0e\x32\x0b.JOINT_MODE\x12\x14\n\x0cqdot_desired\x18\x06 \x01(\x01\x12\x13\n\x0bq_slew_rate\x18\x07 \x01(\x01\x12\x11\n\tbrake_off\x18\x08 \x01(\x08\x12\'\n\x0esmoothing_mode\x18\t \x01(\x0e\x32\x0f.SMOOTHING_MODE*\x80\x02\n\nJOINT_MODE\x12\x12\n\x0eJOINT_MODE_OFF\x10\x00\x12\x12\n\x0eJOINT_MODE_PWM\x10\x01\x12\x15\n\x11JOINT_MODE_TORQUE\x10\x02\x12\x14\n\x10JOINT_MODE_THETA\x10\x03\x12\x18\n\x14JOINT_MODE_TORQUE_GC\x10\x04\x12\x17\n\x13JOINT_MODE_THETA_GC\x10\x05\x12\x17\n\x13JOINT_MODE_THETA_MJ\x10\x06\x12\x1a\n\x16JOINT_MODE_THETA_GC_MJ\x10\x07\x12\x13\n\x0fJOINT_MODE_POSE\x10\x08\x12 \n\x1cJOINT_MODE_TORQUE_GRAV_MODEL\x10\tB\x02H\x01')

_JOINT_MODE = descriptor.EnumDescriptor(
  name='JOINT_MODE',
  full_name='JOINT_MODE',
  filename=None,
  file=DESCRIPTOR,
  values=[
    descriptor.EnumValueDescriptor(
      name='JOINT_MODE_OFF', index=0, number=0,
      options=None,
      type=None),
    descriptor.EnumValueDescriptor(
      name='JOINT_MODE_PWM', index=1, number=1,
      options=None,
      type=None),
    descriptor.EnumValueDescriptor(
      name='JOINT_MODE_TORQUE', index=2, number=2,
      options=None,
      type=None),
    descriptor.EnumValueDescriptor(
      name='JOINT_MODE_THETA', index=3, number=3,
      options=None,
      type=None),
    descriptor.EnumValueDescriptor(
      name='JOINT_MODE_TORQUE_GC', index=4, number=4,
      options=None,
      type=None),
    descriptor.EnumValueDescriptor(
      name='JOINT_MODE_THETA_GC', index=5, number=5,
      options=None,
      type=None),
    descriptor.EnumValueDescriptor(
      name='JOINT_MODE_THETA_MJ', index=6, number=6,
      options=None,
      type=None),
    descriptor.EnumValueDescriptor(
      name='JOINT_MODE_THETA_GC_MJ', index=7, number=7,
      options=None,
      type=None),
    descriptor.EnumValueDescriptor(
      name='JOINT_MODE_POSE', index=8, number=8,
      options=None,
      type=None),
    descriptor.EnumValueDescriptor(
      name='JOINT_MODE_TORQUE_GRAV_MODEL', index=9, number=9,
      options=None,
      type=None),
  ],
  containing_type=None,
  options=None,
  serialized_start=1017,
  serialized_end=1273,
)


JOINT_MODE_OFF = 0
JOINT_MODE_PWM = 1
JOINT_MODE_TORQUE = 2
JOINT_MODE_THETA = 3
JOINT_MODE_TORQUE_GC = 4
JOINT_MODE_THETA_GC = 5
JOINT_MODE_THETA_MJ = 6
JOINT_MODE_THETA_GC_MJ = 7
JOINT_MODE_POSE = 8
JOINT_MODE_TORQUE_GRAV_MODEL = 9



_M3JOINTSTATUS = descriptor.Descriptor(
  name='M3JointStatus',
  full_name='M3JointStatus',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    descriptor.FieldDescriptor(
      name='base', full_name='M3JointStatus.base', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='motor_temp', full_name='M3JointStatus.motor_temp', index=1,
      number=2, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='amp_temp', full_name='M3JointStatus.amp_temp', index=2,
      number=3, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='current', full_name='M3JointStatus.current', index=3,
      number=4, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='torque', full_name='M3JointStatus.torque', index=4,
      number=6, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='torquedot', full_name='M3JointStatus.torquedot', index=5,
      number=7, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='theta', full_name='M3JointStatus.theta', index=6,
      number=8, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='thetadot', full_name='M3JointStatus.thetadot', index=7,
      number=9, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='thetadotdot', full_name='M3JointStatus.thetadotdot', index=8,
      number=10, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='torque_gravity', full_name='M3JointStatus.torque_gravity', index=9,
      number=11, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='pwm_cmd', full_name='M3JointStatus.pwm_cmd', index=10,
      number=12, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='ambient_temp', full_name='M3JointStatus.ambient_temp', index=11,
      number=13, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='case_temp', full_name='M3JointStatus.case_temp', index=12,
      number=14, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='power', full_name='M3JointStatus.power', index=13,
      number=15, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='flags', full_name='M3JointStatus.flags', index=14,
      number=16, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=60,
  serialized_end=360,
)


_M3JOINTPARAM = descriptor.Descriptor(
  name='M3JointParam',
  full_name='M3JointParam',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    descriptor.FieldDescriptor(
      name='kq_p', full_name='M3JointParam.kq_p', index=0,
      number=1, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='kq_i', full_name='M3JointParam.kq_i', index=1,
      number=2, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='kq_d', full_name='M3JointParam.kq_d', index=2,
      number=3, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='kq_i_limit', full_name='M3JointParam.kq_i_limit', index=3,
      number=4, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='max_q', full_name='M3JointParam.max_q', index=4,
      number=5, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='min_q', full_name='M3JointParam.min_q', index=5,
      number=6, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='kt_p', full_name='M3JointParam.kt_p', index=6,
      number=7, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='kt_i', full_name='M3JointParam.kt_i', index=7,
      number=8, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='kt_d', full_name='M3JointParam.kt_d', index=8,
      number=9, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='kt_i_limit', full_name='M3JointParam.kt_i_limit', index=9,
      number=10, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='kq_g', full_name='M3JointParam.kq_g', index=10,
      number=11, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='max_q_slew_rate', full_name='M3JointParam.max_q_slew_rate', index=11,
      number=12, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='kq_i_range', full_name='M3JointParam.kq_i_range', index=12,
      number=13, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='kt_i_range', full_name='M3JointParam.kt_i_range', index=13,
      number=14, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='max_q_pad', full_name='M3JointParam.max_q_pad', index=14,
      number=15, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='min_q_pad', full_name='M3JointParam.min_q_pad', index=15,
      number=16, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='kq_d_pose', full_name='M3JointParam.kq_d_pose', index=16,
      number=17, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='kq_p_tq_gm', full_name='M3JointParam.kq_p_tq_gm', index=17,
      number=18, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='kq_i_tq_gm', full_name='M3JointParam.kq_i_tq_gm', index=18,
      number=19, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='kq_d_tq_gm', full_name='M3JointParam.kq_d_tq_gm', index=19,
      number=20, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='kq_i_limit_tq_gm', full_name='M3JointParam.kq_i_limit_tq_gm', index=20,
      number=21, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='kq_i_range_tq_gm', full_name='M3JointParam.kq_i_range_tq_gm', index=21,
      number=22, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=363,
  serialized_end=779,
)


_M3JOINTCOMMAND = descriptor.Descriptor(
  name='M3JointCommand',
  full_name='M3JointCommand',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    descriptor.FieldDescriptor(
      name='tq_desired', full_name='M3JointCommand.tq_desired', index=0,
      number=1, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='q_desired', full_name='M3JointCommand.q_desired', index=1,
      number=2, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='pwm_desired', full_name='M3JointCommand.pwm_desired', index=2,
      number=3, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='q_stiffness', full_name='M3JointCommand.q_stiffness', index=3,
      number=4, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='ctrl_mode', full_name='M3JointCommand.ctrl_mode', index=4,
      number=5, type=14, cpp_type=8, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='qdot_desired', full_name='M3JointCommand.qdot_desired', index=5,
      number=6, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='q_slew_rate', full_name='M3JointCommand.q_slew_rate', index=6,
      number=7, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='brake_off', full_name='M3JointCommand.brake_off', index=7,
      number=8, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='smoothing_mode', full_name='M3JointCommand.smoothing_mode', index=8,
      number=9, type=14, cpp_type=8, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=782,
  serialized_end=1014,
)

_M3JOINTSTATUS.fields_by_name['base'].message_type = component_base_pb2._M3BASESTATUS
_M3JOINTCOMMAND.fields_by_name['ctrl_mode'].enum_type = _JOINT_MODE
_M3JOINTCOMMAND.fields_by_name['smoothing_mode'].enum_type = smoothing_mode_pb2._SMOOTHING_MODE
DESCRIPTOR.message_types_by_name['M3JointStatus'] = _M3JOINTSTATUS
DESCRIPTOR.message_types_by_name['M3JointParam'] = _M3JOINTPARAM
DESCRIPTOR.message_types_by_name['M3JointCommand'] = _M3JOINTCOMMAND

class M3JointStatus(message.Message):
  __metaclass__ = reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _M3JOINTSTATUS
  
  # @@protoc_insertion_point(class_scope:M3JointStatus)

class M3JointParam(message.Message):
  __metaclass__ = reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _M3JOINTPARAM
  
  # @@protoc_insertion_point(class_scope:M3JointParam)

class M3JointCommand(message.Message):
  __metaclass__ = reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _M3JOINTCOMMAND
  
  # @@protoc_insertion_point(class_scope:M3JointCommand)

# @@protoc_insertion_point(module_scope)
