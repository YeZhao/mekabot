# Generated by the protocol buffer compiler.  DO NOT EDIT!

from google.protobuf import descriptor
from google.protobuf import message
from google.protobuf import reflection
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)


import component_base_pb2

DESCRIPTOR = descriptor.FileDescriptor(
  name='loadx1.proto',
  package='',
  serialized_pb='\n\x0cloadx1.proto\x1a\x14\x63omponent_base.proto\"P\n\x0eM3LoadX1Status\x12\x1b\n\x04\x62\x61se\x18\x01 \x01(\x0b\x32\r.M3BaseStatus\x12\x0e\n\x06torque\x18\x06 \x01(\x01\x12\x11\n\ttorquedot\x18\x07 \x01(\x01\"\x1e\n\rM3LoadX1Param\x12\r\n\x05\x64ummy\x18\x01 \x01(\x01\" \n\x0fM3LoadX1Command\x12\r\n\x05\x64ummy\x18\x01 \x01(\x01\x42\x02H\x01')




_M3LOADX1STATUS = descriptor.Descriptor(
  name='M3LoadX1Status',
  full_name='M3LoadX1Status',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    descriptor.FieldDescriptor(
      name='base', full_name='M3LoadX1Status.base', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='torque', full_name='M3LoadX1Status.torque', index=1,
      number=6, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='torquedot', full_name='M3LoadX1Status.torquedot', index=2,
      number=7, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=38,
  serialized_end=118,
)


_M3LOADX1PARAM = descriptor.Descriptor(
  name='M3LoadX1Param',
  full_name='M3LoadX1Param',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    descriptor.FieldDescriptor(
      name='dummy', full_name='M3LoadX1Param.dummy', index=0,
      number=1, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=120,
  serialized_end=150,
)


_M3LOADX1COMMAND = descriptor.Descriptor(
  name='M3LoadX1Command',
  full_name='M3LoadX1Command',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    descriptor.FieldDescriptor(
      name='dummy', full_name='M3LoadX1Command.dummy', index=0,
      number=1, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=152,
  serialized_end=184,
)

_M3LOADX1STATUS.fields_by_name['base'].message_type = component_base_pb2._M3BASESTATUS
DESCRIPTOR.message_types_by_name['M3LoadX1Status'] = _M3LOADX1STATUS
DESCRIPTOR.message_types_by_name['M3LoadX1Param'] = _M3LOADX1PARAM
DESCRIPTOR.message_types_by_name['M3LoadX1Command'] = _M3LOADX1COMMAND

class M3LoadX1Status(message.Message):
  __metaclass__ = reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _M3LOADX1STATUS
  
  # @@protoc_insertion_point(class_scope:M3LoadX1Status)

class M3LoadX1Param(message.Message):
  __metaclass__ = reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _M3LOADX1PARAM
  
  # @@protoc_insertion_point(class_scope:M3LoadX1Param)

class M3LoadX1Command(message.Message):
  __metaclass__ = reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _M3LOADX1COMMAND
  
  # @@protoc_insertion_point(class_scope:M3LoadX1Command)

# @@protoc_insertion_point(module_scope)
