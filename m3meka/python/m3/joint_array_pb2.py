# Generated by the protocol buffer compiler.  DO NOT EDIT!

from google.protobuf import descriptor
from google.protobuf import message
from google.protobuf import reflection
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)


import component_base_pb2
import trajectory_pb2
import joint_pb2
import smoothing_mode_pb2
import joint_array_mode_pb2

DESCRIPTOR = descriptor.FileDescriptor(
  name='joint_array.proto',
  package='',
  serialized_pb='\n\x11joint_array.proto\x1a\x14\x63omponent_base.proto\x1a\x10trajectory.proto\x1a\x0bjoint.proto\x1a\x14smoothing_mode.proto\x1a\x16joint_array_mode.proto\"\xff\x01\n\x12M3JointArrayStatus\x12\x1b\n\x04\x62\x61se\x18\x01 \x01(\x0b\x32\r.M3BaseStatus\x12\x12\n\nmotor_temp\x18\x02 \x03(\x01\x12\x10\n\x08\x61mp_temp\x18\x03 \x03(\x01\x12\x0f\n\x07\x63urrent\x18\x04 \x03(\x01\x12\x0e\n\x06torque\x18\x05 \x03(\x01\x12\x11\n\ttorquedot\x18\x06 \x03(\x01\x12\r\n\x05theta\x18\x07 \x03(\x01\x12\x10\n\x08thetadot\x18\x08 \x03(\x01\x12\x13\n\x0bthetadotdot\x18\t \x03(\x01\x12\x1c\n\x14\x63ompleted_spline_idx\x18\n \x01(\x05\x12\x0f\n\x07pwm_cmd\x18\x0b \x03(\x05\x12\r\n\x05\x66lags\x18\x0c \x03(\x05\"\x90\x02\n\x13M3JointArrayCommand\x12\x12\n\ntq_desired\x18\x01 \x03(\x01\x12\x13\n\x0bpwm_desired\x18\x02 \x03(\x05\x12\x13\n\x0bq_stiffness\x18\x03 \x03(\x01\x12$\n\tctrl_mode\x18\x04 \x03(\x0e\x32\x11.JOINT_ARRAY_MODE\x12\x19\n\x04vias\x18\x05 \x03(\x0b\x32\x0b.M3JointVia\x12\x11\n\tq_desired\x18\x06 \x03(\x01\x12\x13\n\x0bpos_desired\x18\x07 \x03(\x01\x12\x14\n\x0cqdot_desired\x18\x08 \x03(\x01\x12\x13\n\x0bq_slew_rate\x18\t \x03(\x01\x12\'\n\x0esmoothing_mode\x18\n \x03(\x0e\x32\x0f.SMOOTHING_MODE\"!\n\x11M3JointArrayParam\x12\x0c\n\x04test\x18\x01 \x01(\x01\x42\x02H\x01')




_M3JOINTARRAYSTATUS = descriptor.Descriptor(
  name='M3JointArrayStatus',
  full_name='M3JointArrayStatus',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    descriptor.FieldDescriptor(
      name='base', full_name='M3JointArrayStatus.base', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='motor_temp', full_name='M3JointArrayStatus.motor_temp', index=1,
      number=2, type=1, cpp_type=5, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='amp_temp', full_name='M3JointArrayStatus.amp_temp', index=2,
      number=3, type=1, cpp_type=5, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='current', full_name='M3JointArrayStatus.current', index=3,
      number=4, type=1, cpp_type=5, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='torque', full_name='M3JointArrayStatus.torque', index=4,
      number=5, type=1, cpp_type=5, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='torquedot', full_name='M3JointArrayStatus.torquedot', index=5,
      number=6, type=1, cpp_type=5, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='theta', full_name='M3JointArrayStatus.theta', index=6,
      number=7, type=1, cpp_type=5, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='thetadot', full_name='M3JointArrayStatus.thetadot', index=7,
      number=8, type=1, cpp_type=5, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='thetadotdot', full_name='M3JointArrayStatus.thetadotdot', index=8,
      number=9, type=1, cpp_type=5, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='completed_spline_idx', full_name='M3JointArrayStatus.completed_spline_idx', index=9,
      number=10, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='pwm_cmd', full_name='M3JointArrayStatus.pwm_cmd', index=10,
      number=11, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='flags', full_name='M3JointArrayStatus.flags', index=11,
      number=12, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=121,
  serialized_end=376,
)


_M3JOINTARRAYCOMMAND = descriptor.Descriptor(
  name='M3JointArrayCommand',
  full_name='M3JointArrayCommand',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    descriptor.FieldDescriptor(
      name='tq_desired', full_name='M3JointArrayCommand.tq_desired', index=0,
      number=1, type=1, cpp_type=5, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='pwm_desired', full_name='M3JointArrayCommand.pwm_desired', index=1,
      number=2, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='q_stiffness', full_name='M3JointArrayCommand.q_stiffness', index=2,
      number=3, type=1, cpp_type=5, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='ctrl_mode', full_name='M3JointArrayCommand.ctrl_mode', index=3,
      number=4, type=14, cpp_type=8, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='vias', full_name='M3JointArrayCommand.vias', index=4,
      number=5, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='q_desired', full_name='M3JointArrayCommand.q_desired', index=5,
      number=6, type=1, cpp_type=5, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='pos_desired', full_name='M3JointArrayCommand.pos_desired', index=6,
      number=7, type=1, cpp_type=5, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='qdot_desired', full_name='M3JointArrayCommand.qdot_desired', index=7,
      number=8, type=1, cpp_type=5, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='q_slew_rate', full_name='M3JointArrayCommand.q_slew_rate', index=8,
      number=9, type=1, cpp_type=5, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='smoothing_mode', full_name='M3JointArrayCommand.smoothing_mode', index=9,
      number=10, type=14, cpp_type=8, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=379,
  serialized_end=651,
)


_M3JOINTARRAYPARAM = descriptor.Descriptor(
  name='M3JointArrayParam',
  full_name='M3JointArrayParam',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    descriptor.FieldDescriptor(
      name='test', full_name='M3JointArrayParam.test', index=0,
      number=1, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=653,
  serialized_end=686,
)

_M3JOINTARRAYSTATUS.fields_by_name['base'].message_type = component_base_pb2._M3BASESTATUS
_M3JOINTARRAYCOMMAND.fields_by_name['ctrl_mode'].enum_type = joint_array_mode_pb2._JOINT_ARRAY_MODE
_M3JOINTARRAYCOMMAND.fields_by_name['vias'].message_type = trajectory_pb2._M3JOINTVIA
_M3JOINTARRAYCOMMAND.fields_by_name['smoothing_mode'].enum_type = smoothing_mode_pb2._SMOOTHING_MODE
DESCRIPTOR.message_types_by_name['M3JointArrayStatus'] = _M3JOINTARRAYSTATUS
DESCRIPTOR.message_types_by_name['M3JointArrayCommand'] = _M3JOINTARRAYCOMMAND
DESCRIPTOR.message_types_by_name['M3JointArrayParam'] = _M3JOINTARRAYPARAM

class M3JointArrayStatus(message.Message):
  __metaclass__ = reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _M3JOINTARRAYSTATUS
  
  # @@protoc_insertion_point(class_scope:M3JointArrayStatus)

class M3JointArrayCommand(message.Message):
  __metaclass__ = reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _M3JOINTARRAYCOMMAND
  
  # @@protoc_insertion_point(class_scope:M3JointArrayCommand)

class M3JointArrayParam(message.Message):
  __metaclass__ = reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _M3JOINTARRAYPARAM
  
  # @@protoc_insertion_point(class_scope:M3JointArrayParam)

# @@protoc_insertion_point(module_scope)
