// Generated by the protocol buffer compiler.  DO NOT EDIT!

#define INTERNAL_SUPPRESS_PROTOBUF_FIELD_DEPRECATION
#include "head_s2csp_ctrl.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/once.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>
// @@protoc_insertion_point(includes)

namespace {

const ::google::protobuf::Descriptor* M3HeadS2CSPCtrlStatus_descriptor_ = NULL;
const ::google::protobuf::internal::GeneratedMessageReflection*
  M3HeadS2CSPCtrlStatus_reflection_ = NULL;
const ::google::protobuf::Descriptor* M3HeadS2CSPCtrlCommand_descriptor_ = NULL;
const ::google::protobuf::internal::GeneratedMessageReflection*
  M3HeadS2CSPCtrlCommand_reflection_ = NULL;
const ::google::protobuf::Descriptor* M3HeadS2CSPCtrlParam_descriptor_ = NULL;
const ::google::protobuf::internal::GeneratedMessageReflection*
  M3HeadS2CSPCtrlParam_reflection_ = NULL;

}  // namespace


void protobuf_AssignDesc_head_5fs2csp_5fctrl_2eproto() {
  protobuf_AddDesc_head_5fs2csp_5fctrl_2eproto();
  const ::google::protobuf::FileDescriptor* file =
    ::google::protobuf::DescriptorPool::generated_pool()->FindFileByName(
      "head_s2csp_ctrl.proto");
  GOOGLE_CHECK(file != NULL);
  M3HeadS2CSPCtrlStatus_descriptor_ = file->message_type(0);
  static const int M3HeadS2CSPCtrlStatus_offsets_[3] = {
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3HeadS2CSPCtrlStatus, base_),
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3HeadS2CSPCtrlStatus, xe_),
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3HeadS2CSPCtrlStatus, theta_des_),
  };
  M3HeadS2CSPCtrlStatus_reflection_ =
    new ::google::protobuf::internal::GeneratedMessageReflection(
      M3HeadS2CSPCtrlStatus_descriptor_,
      M3HeadS2CSPCtrlStatus::default_instance_,
      M3HeadS2CSPCtrlStatus_offsets_,
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3HeadS2CSPCtrlStatus, _has_bits_[0]),
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3HeadS2CSPCtrlStatus, _unknown_fields_),
      -1,
      ::google::protobuf::DescriptorPool::generated_pool(),
      ::google::protobuf::MessageFactory::generated_factory(),
      sizeof(M3HeadS2CSPCtrlStatus));
  M3HeadS2CSPCtrlCommand_descriptor_ = file->message_type(1);
  static const int M3HeadS2CSPCtrlCommand_offsets_[3] = {
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3HeadS2CSPCtrlCommand, target_),
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3HeadS2CSPCtrlCommand, enable_),
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3HeadS2CSPCtrlCommand, theta_des_j2_),
  };
  M3HeadS2CSPCtrlCommand_reflection_ =
    new ::google::protobuf::internal::GeneratedMessageReflection(
      M3HeadS2CSPCtrlCommand_descriptor_,
      M3HeadS2CSPCtrlCommand::default_instance_,
      M3HeadS2CSPCtrlCommand_offsets_,
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3HeadS2CSPCtrlCommand, _has_bits_[0]),
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3HeadS2CSPCtrlCommand, _unknown_fields_),
      -1,
      ::google::protobuf::DescriptorPool::generated_pool(),
      ::google::protobuf::MessageFactory::generated_factory(),
      sizeof(M3HeadS2CSPCtrlCommand));
  M3HeadS2CSPCtrlParam_descriptor_ = file->message_type(2);
  static const int M3HeadS2CSPCtrlParam_offsets_[5] = {
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3HeadS2CSPCtrlParam, target_slew_),
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3HeadS2CSPCtrlParam, slew_des_),
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3HeadS2CSPCtrlParam, theta_db_),
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3HeadS2CSPCtrlParam, ratio_j0_),
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3HeadS2CSPCtrlParam, origin_),
  };
  M3HeadS2CSPCtrlParam_reflection_ =
    new ::google::protobuf::internal::GeneratedMessageReflection(
      M3HeadS2CSPCtrlParam_descriptor_,
      M3HeadS2CSPCtrlParam::default_instance_,
      M3HeadS2CSPCtrlParam_offsets_,
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3HeadS2CSPCtrlParam, _has_bits_[0]),
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3HeadS2CSPCtrlParam, _unknown_fields_),
      -1,
      ::google::protobuf::DescriptorPool::generated_pool(),
      ::google::protobuf::MessageFactory::generated_factory(),
      sizeof(M3HeadS2CSPCtrlParam));
}

namespace {

GOOGLE_PROTOBUF_DECLARE_ONCE(protobuf_AssignDescriptors_once_);
inline void protobuf_AssignDescriptorsOnce() {
  ::google::protobuf::GoogleOnceInit(&protobuf_AssignDescriptors_once_,
                 &protobuf_AssignDesc_head_5fs2csp_5fctrl_2eproto);
}

void protobuf_RegisterTypes(const ::std::string&) {
  protobuf_AssignDescriptorsOnce();
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedMessage(
    M3HeadS2CSPCtrlStatus_descriptor_, &M3HeadS2CSPCtrlStatus::default_instance());
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedMessage(
    M3HeadS2CSPCtrlCommand_descriptor_, &M3HeadS2CSPCtrlCommand::default_instance());
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedMessage(
    M3HeadS2CSPCtrlParam_descriptor_, &M3HeadS2CSPCtrlParam::default_instance());
}

}  // namespace

void protobuf_ShutdownFile_head_5fs2csp_5fctrl_2eproto() {
  delete M3HeadS2CSPCtrlStatus::default_instance_;
  delete M3HeadS2CSPCtrlStatus_reflection_;
  delete M3HeadS2CSPCtrlCommand::default_instance_;
  delete M3HeadS2CSPCtrlCommand_reflection_;
  delete M3HeadS2CSPCtrlParam::default_instance_;
  delete M3HeadS2CSPCtrlParam_reflection_;
}

void protobuf_AddDesc_head_5fs2csp_5fctrl_2eproto() {
  static bool already_here = false;
  if (already_here) return;
  already_here = true;
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  ::protobuf_AddDesc_component_5fbase_2eproto();
  ::google::protobuf::DescriptorPool::InternalAddGeneratedFile(
    "\n\025head_s2csp_ctrl.proto\032\024component_base."
    "proto\"S\n\025M3HeadS2CSPCtrlStatus\022\033\n\004base\030\001"
    " \001(\0132\r.M3BaseStatus\022\n\n\002xe\030\002 \003(\001\022\021\n\ttheta"
    "_des\030\003 \003(\001\"N\n\026M3HeadS2CSPCtrlCommand\022\016\n\006"
    "target\030\001 \003(\001\022\016\n\006enable\030\002 \001(\005\022\024\n\014theta_de"
    "s_j2\030\003 \001(\001\"q\n\024M3HeadS2CSPCtrlParam\022\023\n\013ta"
    "rget_slew\030\001 \001(\001\022\020\n\010slew_des\030\002 \003(\001\022\020\n\010the"
    "ta_db\030\003 \003(\001\022\020\n\010ratio_j0\030\004 \001(\001\022\016\n\006origin\030"
    "\005 \003(\001B\002H\001", 329);
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedFile(
    "head_s2csp_ctrl.proto", &protobuf_RegisterTypes);
  M3HeadS2CSPCtrlStatus::default_instance_ = new M3HeadS2CSPCtrlStatus();
  M3HeadS2CSPCtrlCommand::default_instance_ = new M3HeadS2CSPCtrlCommand();
  M3HeadS2CSPCtrlParam::default_instance_ = new M3HeadS2CSPCtrlParam();
  M3HeadS2CSPCtrlStatus::default_instance_->InitAsDefaultInstance();
  M3HeadS2CSPCtrlCommand::default_instance_->InitAsDefaultInstance();
  M3HeadS2CSPCtrlParam::default_instance_->InitAsDefaultInstance();
  ::google::protobuf::internal::OnShutdown(&protobuf_ShutdownFile_head_5fs2csp_5fctrl_2eproto);
}

// Force AddDescriptors() to be called at static initialization time.
struct StaticDescriptorInitializer_head_5fs2csp_5fctrl_2eproto {
  StaticDescriptorInitializer_head_5fs2csp_5fctrl_2eproto() {
    protobuf_AddDesc_head_5fs2csp_5fctrl_2eproto();
  }
} static_descriptor_initializer_head_5fs2csp_5fctrl_2eproto_;


// ===================================================================

#ifndef _MSC_VER
const int M3HeadS2CSPCtrlStatus::kBaseFieldNumber;
const int M3HeadS2CSPCtrlStatus::kXeFieldNumber;
const int M3HeadS2CSPCtrlStatus::kThetaDesFieldNumber;
#endif  // !_MSC_VER

M3HeadS2CSPCtrlStatus::M3HeadS2CSPCtrlStatus()
  : ::google::protobuf::Message() {
  SharedCtor();
}

void M3HeadS2CSPCtrlStatus::InitAsDefaultInstance() {
  base_ = const_cast< ::M3BaseStatus*>(&::M3BaseStatus::default_instance());
}

M3HeadS2CSPCtrlStatus::M3HeadS2CSPCtrlStatus(const M3HeadS2CSPCtrlStatus& from)
  : ::google::protobuf::Message() {
  SharedCtor();
  MergeFrom(from);
}

void M3HeadS2CSPCtrlStatus::SharedCtor() {
  _cached_size_ = 0;
  base_ = NULL;
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

M3HeadS2CSPCtrlStatus::~M3HeadS2CSPCtrlStatus() {
  SharedDtor();
}

void M3HeadS2CSPCtrlStatus::SharedDtor() {
  if (this != default_instance_) {
    delete base_;
  }
}

void M3HeadS2CSPCtrlStatus::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const ::google::protobuf::Descriptor* M3HeadS2CSPCtrlStatus::descriptor() {
  protobuf_AssignDescriptorsOnce();
  return M3HeadS2CSPCtrlStatus_descriptor_;
}

const M3HeadS2CSPCtrlStatus& M3HeadS2CSPCtrlStatus::default_instance() {
  if (default_instance_ == NULL) protobuf_AddDesc_head_5fs2csp_5fctrl_2eproto();  return *default_instance_;
}

M3HeadS2CSPCtrlStatus* M3HeadS2CSPCtrlStatus::default_instance_ = NULL;

M3HeadS2CSPCtrlStatus* M3HeadS2CSPCtrlStatus::New() const {
  return new M3HeadS2CSPCtrlStatus;
}

void M3HeadS2CSPCtrlStatus::Clear() {
  if (_has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    if (has_base()) {
      if (base_ != NULL) base_->::M3BaseStatus::Clear();
    }
  }
  xe_.Clear();
  theta_des_.Clear();
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  mutable_unknown_fields()->Clear();
}

bool M3HeadS2CSPCtrlStatus::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!(EXPRESSION)) return false
  ::google::protobuf::uint32 tag;
  while ((tag = input->ReadTag()) != 0) {
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // optional .M3BaseStatus base = 1;
      case 1: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_LENGTH_DELIMITED) {
          DO_(::google::protobuf::internal::WireFormatLite::ReadMessageNoVirtual(
               input, mutable_base()));
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectTag(17)) goto parse_xe;
        break;
      }
      
      // repeated double xe = 2;
      case 2: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_FIXED64) {
         parse_xe:
          DO_((::google::protobuf::internal::WireFormatLite::ReadRepeatedPrimitive<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 1, 17, input, this->mutable_xe())));
        } else if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag)
                   == ::google::protobuf::internal::WireFormatLite::
                      WIRETYPE_LENGTH_DELIMITED) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadPackedPrimitiveNoInline<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 input, this->mutable_xe())));
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectTag(17)) goto parse_xe;
        if (input->ExpectTag(25)) goto parse_theta_des;
        break;
      }
      
      // repeated double theta_des = 3;
      case 3: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_FIXED64) {
         parse_theta_des:
          DO_((::google::protobuf::internal::WireFormatLite::ReadRepeatedPrimitive<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 1, 25, input, this->mutable_theta_des())));
        } else if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag)
                   == ::google::protobuf::internal::WireFormatLite::
                      WIRETYPE_LENGTH_DELIMITED) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadPackedPrimitiveNoInline<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 input, this->mutable_theta_des())));
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectTag(25)) goto parse_theta_des;
        if (input->ExpectAtEnd()) return true;
        break;
      }
      
      default: {
      handle_uninterpreted:
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          return true;
        }
        DO_(::google::protobuf::internal::WireFormat::SkipField(
              input, tag, mutable_unknown_fields()));
        break;
      }
    }
  }
  return true;
#undef DO_
}

void M3HeadS2CSPCtrlStatus::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // optional .M3BaseStatus base = 1;
  if (has_base()) {
    ::google::protobuf::internal::WireFormatLite::WriteMessageMaybeToArray(
      1, this->base(), output);
  }
  
  // repeated double xe = 2;
  for (int i = 0; i < this->xe_size(); i++) {
    ::google::protobuf::internal::WireFormatLite::WriteDouble(
      2, this->xe(i), output);
  }
  
  // repeated double theta_des = 3;
  for (int i = 0; i < this->theta_des_size(); i++) {
    ::google::protobuf::internal::WireFormatLite::WriteDouble(
      3, this->theta_des(i), output);
  }
  
  if (!unknown_fields().empty()) {
    ::google::protobuf::internal::WireFormat::SerializeUnknownFields(
        unknown_fields(), output);
  }
}

::google::protobuf::uint8* M3HeadS2CSPCtrlStatus::SerializeWithCachedSizesToArray(
    ::google::protobuf::uint8* target) const {
  // optional .M3BaseStatus base = 1;
  if (has_base()) {
    target = ::google::protobuf::internal::WireFormatLite::
      WriteMessageNoVirtualToArray(
        1, this->base(), target);
  }
  
  // repeated double xe = 2;
  for (int i = 0; i < this->xe_size(); i++) {
    target = ::google::protobuf::internal::WireFormatLite::
      WriteDoubleToArray(2, this->xe(i), target);
  }
  
  // repeated double theta_des = 3;
  for (int i = 0; i < this->theta_des_size(); i++) {
    target = ::google::protobuf::internal::WireFormatLite::
      WriteDoubleToArray(3, this->theta_des(i), target);
  }
  
  if (!unknown_fields().empty()) {
    target = ::google::protobuf::internal::WireFormat::SerializeUnknownFieldsToArray(
        unknown_fields(), target);
  }
  return target;
}

int M3HeadS2CSPCtrlStatus::ByteSize() const {
  int total_size = 0;
  
  if (_has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    // optional .M3BaseStatus base = 1;
    if (has_base()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::MessageSizeNoVirtual(
          this->base());
    }
    
  }
  // repeated double xe = 2;
  {
    int data_size = 0;
    data_size = 8 * this->xe_size();
    total_size += 1 * this->xe_size() + data_size;
  }
  
  // repeated double theta_des = 3;
  {
    int data_size = 0;
    data_size = 8 * this->theta_des_size();
    total_size += 1 * this->theta_des_size() + data_size;
  }
  
  if (!unknown_fields().empty()) {
    total_size +=
      ::google::protobuf::internal::WireFormat::ComputeUnknownFieldsSize(
        unknown_fields());
  }
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void M3HeadS2CSPCtrlStatus::MergeFrom(const ::google::protobuf::Message& from) {
  GOOGLE_CHECK_NE(&from, this);
  const M3HeadS2CSPCtrlStatus* source =
    ::google::protobuf::internal::dynamic_cast_if_available<const M3HeadS2CSPCtrlStatus*>(
      &from);
  if (source == NULL) {
    ::google::protobuf::internal::ReflectionOps::Merge(from, this);
  } else {
    MergeFrom(*source);
  }
}

void M3HeadS2CSPCtrlStatus::MergeFrom(const M3HeadS2CSPCtrlStatus& from) {
  GOOGLE_CHECK_NE(&from, this);
  xe_.MergeFrom(from.xe_);
  theta_des_.MergeFrom(from.theta_des_);
  if (from._has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    if (from.has_base()) {
      mutable_base()->::M3BaseStatus::MergeFrom(from.base());
    }
  }
  mutable_unknown_fields()->MergeFrom(from.unknown_fields());
}

void M3HeadS2CSPCtrlStatus::CopyFrom(const ::google::protobuf::Message& from) {
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void M3HeadS2CSPCtrlStatus::CopyFrom(const M3HeadS2CSPCtrlStatus& from) {
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool M3HeadS2CSPCtrlStatus::IsInitialized() const {
  
  return true;
}

void M3HeadS2CSPCtrlStatus::Swap(M3HeadS2CSPCtrlStatus* other) {
  if (other != this) {
    std::swap(base_, other->base_);
    xe_.Swap(&other->xe_);
    theta_des_.Swap(&other->theta_des_);
    std::swap(_has_bits_[0], other->_has_bits_[0]);
    _unknown_fields_.Swap(&other->_unknown_fields_);
    std::swap(_cached_size_, other->_cached_size_);
  }
}

::google::protobuf::Metadata M3HeadS2CSPCtrlStatus::GetMetadata() const {
  protobuf_AssignDescriptorsOnce();
  ::google::protobuf::Metadata metadata;
  metadata.descriptor = M3HeadS2CSPCtrlStatus_descriptor_;
  metadata.reflection = M3HeadS2CSPCtrlStatus_reflection_;
  return metadata;
}


// ===================================================================

#ifndef _MSC_VER
const int M3HeadS2CSPCtrlCommand::kTargetFieldNumber;
const int M3HeadS2CSPCtrlCommand::kEnableFieldNumber;
const int M3HeadS2CSPCtrlCommand::kThetaDesJ2FieldNumber;
#endif  // !_MSC_VER

M3HeadS2CSPCtrlCommand::M3HeadS2CSPCtrlCommand()
  : ::google::protobuf::Message() {
  SharedCtor();
}

void M3HeadS2CSPCtrlCommand::InitAsDefaultInstance() {
}

M3HeadS2CSPCtrlCommand::M3HeadS2CSPCtrlCommand(const M3HeadS2CSPCtrlCommand& from)
  : ::google::protobuf::Message() {
  SharedCtor();
  MergeFrom(from);
}

void M3HeadS2CSPCtrlCommand::SharedCtor() {
  _cached_size_ = 0;
  enable_ = 0;
  theta_des_j2_ = 0;
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

M3HeadS2CSPCtrlCommand::~M3HeadS2CSPCtrlCommand() {
  SharedDtor();
}

void M3HeadS2CSPCtrlCommand::SharedDtor() {
  if (this != default_instance_) {
  }
}

void M3HeadS2CSPCtrlCommand::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const ::google::protobuf::Descriptor* M3HeadS2CSPCtrlCommand::descriptor() {
  protobuf_AssignDescriptorsOnce();
  return M3HeadS2CSPCtrlCommand_descriptor_;
}

const M3HeadS2CSPCtrlCommand& M3HeadS2CSPCtrlCommand::default_instance() {
  if (default_instance_ == NULL) protobuf_AddDesc_head_5fs2csp_5fctrl_2eproto();  return *default_instance_;
}

M3HeadS2CSPCtrlCommand* M3HeadS2CSPCtrlCommand::default_instance_ = NULL;

M3HeadS2CSPCtrlCommand* M3HeadS2CSPCtrlCommand::New() const {
  return new M3HeadS2CSPCtrlCommand;
}

void M3HeadS2CSPCtrlCommand::Clear() {
  if (_has_bits_[1 / 32] & (0xffu << (1 % 32))) {
    enable_ = 0;
    theta_des_j2_ = 0;
  }
  target_.Clear();
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  mutable_unknown_fields()->Clear();
}

bool M3HeadS2CSPCtrlCommand::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!(EXPRESSION)) return false
  ::google::protobuf::uint32 tag;
  while ((tag = input->ReadTag()) != 0) {
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // repeated double target = 1;
      case 1: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_FIXED64) {
         parse_target:
          DO_((::google::protobuf::internal::WireFormatLite::ReadRepeatedPrimitive<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 1, 9, input, this->mutable_target())));
        } else if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag)
                   == ::google::protobuf::internal::WireFormatLite::
                      WIRETYPE_LENGTH_DELIMITED) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadPackedPrimitiveNoInline<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 input, this->mutable_target())));
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectTag(9)) goto parse_target;
        if (input->ExpectTag(16)) goto parse_enable;
        break;
      }
      
      // optional int32 enable = 2;
      case 2: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_VARINT) {
         parse_enable:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::int32, ::google::protobuf::internal::WireFormatLite::TYPE_INT32>(
                 input, &enable_)));
          set_has_enable();
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectTag(25)) goto parse_theta_des_j2;
        break;
      }
      
      // optional double theta_des_j2 = 3;
      case 3: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_FIXED64) {
         parse_theta_des_j2:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 input, &theta_des_j2_)));
          set_has_theta_des_j2();
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectAtEnd()) return true;
        break;
      }
      
      default: {
      handle_uninterpreted:
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          return true;
        }
        DO_(::google::protobuf::internal::WireFormat::SkipField(
              input, tag, mutable_unknown_fields()));
        break;
      }
    }
  }
  return true;
#undef DO_
}

void M3HeadS2CSPCtrlCommand::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // repeated double target = 1;
  for (int i = 0; i < this->target_size(); i++) {
    ::google::protobuf::internal::WireFormatLite::WriteDouble(
      1, this->target(i), output);
  }
  
  // optional int32 enable = 2;
  if (has_enable()) {
    ::google::protobuf::internal::WireFormatLite::WriteInt32(2, this->enable(), output);
  }
  
  // optional double theta_des_j2 = 3;
  if (has_theta_des_j2()) {
    ::google::protobuf::internal::WireFormatLite::WriteDouble(3, this->theta_des_j2(), output);
  }
  
  if (!unknown_fields().empty()) {
    ::google::protobuf::internal::WireFormat::SerializeUnknownFields(
        unknown_fields(), output);
  }
}

::google::protobuf::uint8* M3HeadS2CSPCtrlCommand::SerializeWithCachedSizesToArray(
    ::google::protobuf::uint8* target) const {
  // repeated double target = 1;
  for (int i = 0; i < this->target_size(); i++) {
    target = ::google::protobuf::internal::WireFormatLite::
      WriteDoubleToArray(1, this->target(i), target);
  }
  
  // optional int32 enable = 2;
  if (has_enable()) {
    target = ::google::protobuf::internal::WireFormatLite::WriteInt32ToArray(2, this->enable(), target);
  }
  
  // optional double theta_des_j2 = 3;
  if (has_theta_des_j2()) {
    target = ::google::protobuf::internal::WireFormatLite::WriteDoubleToArray(3, this->theta_des_j2(), target);
  }
  
  if (!unknown_fields().empty()) {
    target = ::google::protobuf::internal::WireFormat::SerializeUnknownFieldsToArray(
        unknown_fields(), target);
  }
  return target;
}

int M3HeadS2CSPCtrlCommand::ByteSize() const {
  int total_size = 0;
  
  if (_has_bits_[1 / 32] & (0xffu << (1 % 32))) {
    // optional int32 enable = 2;
    if (has_enable()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int32Size(
          this->enable());
    }
    
    // optional double theta_des_j2 = 3;
    if (has_theta_des_j2()) {
      total_size += 1 + 8;
    }
    
  }
  // repeated double target = 1;
  {
    int data_size = 0;
    data_size = 8 * this->target_size();
    total_size += 1 * this->target_size() + data_size;
  }
  
  if (!unknown_fields().empty()) {
    total_size +=
      ::google::protobuf::internal::WireFormat::ComputeUnknownFieldsSize(
        unknown_fields());
  }
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void M3HeadS2CSPCtrlCommand::MergeFrom(const ::google::protobuf::Message& from) {
  GOOGLE_CHECK_NE(&from, this);
  const M3HeadS2CSPCtrlCommand* source =
    ::google::protobuf::internal::dynamic_cast_if_available<const M3HeadS2CSPCtrlCommand*>(
      &from);
  if (source == NULL) {
    ::google::protobuf::internal::ReflectionOps::Merge(from, this);
  } else {
    MergeFrom(*source);
  }
}

void M3HeadS2CSPCtrlCommand::MergeFrom(const M3HeadS2CSPCtrlCommand& from) {
  GOOGLE_CHECK_NE(&from, this);
  target_.MergeFrom(from.target_);
  if (from._has_bits_[1 / 32] & (0xffu << (1 % 32))) {
    if (from.has_enable()) {
      set_enable(from.enable());
    }
    if (from.has_theta_des_j2()) {
      set_theta_des_j2(from.theta_des_j2());
    }
  }
  mutable_unknown_fields()->MergeFrom(from.unknown_fields());
}

void M3HeadS2CSPCtrlCommand::CopyFrom(const ::google::protobuf::Message& from) {
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void M3HeadS2CSPCtrlCommand::CopyFrom(const M3HeadS2CSPCtrlCommand& from) {
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool M3HeadS2CSPCtrlCommand::IsInitialized() const {
  
  return true;
}

void M3HeadS2CSPCtrlCommand::Swap(M3HeadS2CSPCtrlCommand* other) {
  if (other != this) {
    target_.Swap(&other->target_);
    std::swap(enable_, other->enable_);
    std::swap(theta_des_j2_, other->theta_des_j2_);
    std::swap(_has_bits_[0], other->_has_bits_[0]);
    _unknown_fields_.Swap(&other->_unknown_fields_);
    std::swap(_cached_size_, other->_cached_size_);
  }
}

::google::protobuf::Metadata M3HeadS2CSPCtrlCommand::GetMetadata() const {
  protobuf_AssignDescriptorsOnce();
  ::google::protobuf::Metadata metadata;
  metadata.descriptor = M3HeadS2CSPCtrlCommand_descriptor_;
  metadata.reflection = M3HeadS2CSPCtrlCommand_reflection_;
  return metadata;
}


// ===================================================================

#ifndef _MSC_VER
const int M3HeadS2CSPCtrlParam::kTargetSlewFieldNumber;
const int M3HeadS2CSPCtrlParam::kSlewDesFieldNumber;
const int M3HeadS2CSPCtrlParam::kThetaDbFieldNumber;
const int M3HeadS2CSPCtrlParam::kRatioJ0FieldNumber;
const int M3HeadS2CSPCtrlParam::kOriginFieldNumber;
#endif  // !_MSC_VER

M3HeadS2CSPCtrlParam::M3HeadS2CSPCtrlParam()
  : ::google::protobuf::Message() {
  SharedCtor();
}

void M3HeadS2CSPCtrlParam::InitAsDefaultInstance() {
}

M3HeadS2CSPCtrlParam::M3HeadS2CSPCtrlParam(const M3HeadS2CSPCtrlParam& from)
  : ::google::protobuf::Message() {
  SharedCtor();
  MergeFrom(from);
}

void M3HeadS2CSPCtrlParam::SharedCtor() {
  _cached_size_ = 0;
  target_slew_ = 0;
  ratio_j0_ = 0;
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

M3HeadS2CSPCtrlParam::~M3HeadS2CSPCtrlParam() {
  SharedDtor();
}

void M3HeadS2CSPCtrlParam::SharedDtor() {
  if (this != default_instance_) {
  }
}

void M3HeadS2CSPCtrlParam::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const ::google::protobuf::Descriptor* M3HeadS2CSPCtrlParam::descriptor() {
  protobuf_AssignDescriptorsOnce();
  return M3HeadS2CSPCtrlParam_descriptor_;
}

const M3HeadS2CSPCtrlParam& M3HeadS2CSPCtrlParam::default_instance() {
  if (default_instance_ == NULL) protobuf_AddDesc_head_5fs2csp_5fctrl_2eproto();  return *default_instance_;
}

M3HeadS2CSPCtrlParam* M3HeadS2CSPCtrlParam::default_instance_ = NULL;

M3HeadS2CSPCtrlParam* M3HeadS2CSPCtrlParam::New() const {
  return new M3HeadS2CSPCtrlParam;
}

void M3HeadS2CSPCtrlParam::Clear() {
  if (_has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    target_slew_ = 0;
    ratio_j0_ = 0;
  }
  slew_des_.Clear();
  theta_db_.Clear();
  origin_.Clear();
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  mutable_unknown_fields()->Clear();
}

bool M3HeadS2CSPCtrlParam::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!(EXPRESSION)) return false
  ::google::protobuf::uint32 tag;
  while ((tag = input->ReadTag()) != 0) {
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // optional double target_slew = 1;
      case 1: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_FIXED64) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 input, &target_slew_)));
          set_has_target_slew();
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectTag(17)) goto parse_slew_des;
        break;
      }
      
      // repeated double slew_des = 2;
      case 2: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_FIXED64) {
         parse_slew_des:
          DO_((::google::protobuf::internal::WireFormatLite::ReadRepeatedPrimitive<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 1, 17, input, this->mutable_slew_des())));
        } else if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag)
                   == ::google::protobuf::internal::WireFormatLite::
                      WIRETYPE_LENGTH_DELIMITED) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadPackedPrimitiveNoInline<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 input, this->mutable_slew_des())));
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectTag(17)) goto parse_slew_des;
        if (input->ExpectTag(25)) goto parse_theta_db;
        break;
      }
      
      // repeated double theta_db = 3;
      case 3: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_FIXED64) {
         parse_theta_db:
          DO_((::google::protobuf::internal::WireFormatLite::ReadRepeatedPrimitive<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 1, 25, input, this->mutable_theta_db())));
        } else if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag)
                   == ::google::protobuf::internal::WireFormatLite::
                      WIRETYPE_LENGTH_DELIMITED) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadPackedPrimitiveNoInline<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 input, this->mutable_theta_db())));
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectTag(25)) goto parse_theta_db;
        if (input->ExpectTag(33)) goto parse_ratio_j0;
        break;
      }
      
      // optional double ratio_j0 = 4;
      case 4: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_FIXED64) {
         parse_ratio_j0:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 input, &ratio_j0_)));
          set_has_ratio_j0();
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectTag(41)) goto parse_origin;
        break;
      }
      
      // repeated double origin = 5;
      case 5: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_FIXED64) {
         parse_origin:
          DO_((::google::protobuf::internal::WireFormatLite::ReadRepeatedPrimitive<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 1, 41, input, this->mutable_origin())));
        } else if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag)
                   == ::google::protobuf::internal::WireFormatLite::
                      WIRETYPE_LENGTH_DELIMITED) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadPackedPrimitiveNoInline<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 input, this->mutable_origin())));
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectTag(41)) goto parse_origin;
        if (input->ExpectAtEnd()) return true;
        break;
      }
      
      default: {
      handle_uninterpreted:
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          return true;
        }
        DO_(::google::protobuf::internal::WireFormat::SkipField(
              input, tag, mutable_unknown_fields()));
        break;
      }
    }
  }
  return true;
#undef DO_
}

void M3HeadS2CSPCtrlParam::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // optional double target_slew = 1;
  if (has_target_slew()) {
    ::google::protobuf::internal::WireFormatLite::WriteDouble(1, this->target_slew(), output);
  }
  
  // repeated double slew_des = 2;
  for (int i = 0; i < this->slew_des_size(); i++) {
    ::google::protobuf::internal::WireFormatLite::WriteDouble(
      2, this->slew_des(i), output);
  }
  
  // repeated double theta_db = 3;
  for (int i = 0; i < this->theta_db_size(); i++) {
    ::google::protobuf::internal::WireFormatLite::WriteDouble(
      3, this->theta_db(i), output);
  }
  
  // optional double ratio_j0 = 4;
  if (has_ratio_j0()) {
    ::google::protobuf::internal::WireFormatLite::WriteDouble(4, this->ratio_j0(), output);
  }
  
  // repeated double origin = 5;
  for (int i = 0; i < this->origin_size(); i++) {
    ::google::protobuf::internal::WireFormatLite::WriteDouble(
      5, this->origin(i), output);
  }
  
  if (!unknown_fields().empty()) {
    ::google::protobuf::internal::WireFormat::SerializeUnknownFields(
        unknown_fields(), output);
  }
}

::google::protobuf::uint8* M3HeadS2CSPCtrlParam::SerializeWithCachedSizesToArray(
    ::google::protobuf::uint8* target) const {
  // optional double target_slew = 1;
  if (has_target_slew()) {
    target = ::google::protobuf::internal::WireFormatLite::WriteDoubleToArray(1, this->target_slew(), target);
  }
  
  // repeated double slew_des = 2;
  for (int i = 0; i < this->slew_des_size(); i++) {
    target = ::google::protobuf::internal::WireFormatLite::
      WriteDoubleToArray(2, this->slew_des(i), target);
  }
  
  // repeated double theta_db = 3;
  for (int i = 0; i < this->theta_db_size(); i++) {
    target = ::google::protobuf::internal::WireFormatLite::
      WriteDoubleToArray(3, this->theta_db(i), target);
  }
  
  // optional double ratio_j0 = 4;
  if (has_ratio_j0()) {
    target = ::google::protobuf::internal::WireFormatLite::WriteDoubleToArray(4, this->ratio_j0(), target);
  }
  
  // repeated double origin = 5;
  for (int i = 0; i < this->origin_size(); i++) {
    target = ::google::protobuf::internal::WireFormatLite::
      WriteDoubleToArray(5, this->origin(i), target);
  }
  
  if (!unknown_fields().empty()) {
    target = ::google::protobuf::internal::WireFormat::SerializeUnknownFieldsToArray(
        unknown_fields(), target);
  }
  return target;
}

int M3HeadS2CSPCtrlParam::ByteSize() const {
  int total_size = 0;
  
  if (_has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    // optional double target_slew = 1;
    if (has_target_slew()) {
      total_size += 1 + 8;
    }
    
    // optional double ratio_j0 = 4;
    if (has_ratio_j0()) {
      total_size += 1 + 8;
    }
    
  }
  // repeated double slew_des = 2;
  {
    int data_size = 0;
    data_size = 8 * this->slew_des_size();
    total_size += 1 * this->slew_des_size() + data_size;
  }
  
  // repeated double theta_db = 3;
  {
    int data_size = 0;
    data_size = 8 * this->theta_db_size();
    total_size += 1 * this->theta_db_size() + data_size;
  }
  
  // repeated double origin = 5;
  {
    int data_size = 0;
    data_size = 8 * this->origin_size();
    total_size += 1 * this->origin_size() + data_size;
  }
  
  if (!unknown_fields().empty()) {
    total_size +=
      ::google::protobuf::internal::WireFormat::ComputeUnknownFieldsSize(
        unknown_fields());
  }
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void M3HeadS2CSPCtrlParam::MergeFrom(const ::google::protobuf::Message& from) {
  GOOGLE_CHECK_NE(&from, this);
  const M3HeadS2CSPCtrlParam* source =
    ::google::protobuf::internal::dynamic_cast_if_available<const M3HeadS2CSPCtrlParam*>(
      &from);
  if (source == NULL) {
    ::google::protobuf::internal::ReflectionOps::Merge(from, this);
  } else {
    MergeFrom(*source);
  }
}

void M3HeadS2CSPCtrlParam::MergeFrom(const M3HeadS2CSPCtrlParam& from) {
  GOOGLE_CHECK_NE(&from, this);
  slew_des_.MergeFrom(from.slew_des_);
  theta_db_.MergeFrom(from.theta_db_);
  origin_.MergeFrom(from.origin_);
  if (from._has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    if (from.has_target_slew()) {
      set_target_slew(from.target_slew());
    }
    if (from.has_ratio_j0()) {
      set_ratio_j0(from.ratio_j0());
    }
  }
  mutable_unknown_fields()->MergeFrom(from.unknown_fields());
}

void M3HeadS2CSPCtrlParam::CopyFrom(const ::google::protobuf::Message& from) {
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void M3HeadS2CSPCtrlParam::CopyFrom(const M3HeadS2CSPCtrlParam& from) {
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool M3HeadS2CSPCtrlParam::IsInitialized() const {
  
  return true;
}

void M3HeadS2CSPCtrlParam::Swap(M3HeadS2CSPCtrlParam* other) {
  if (other != this) {
    std::swap(target_slew_, other->target_slew_);
    slew_des_.Swap(&other->slew_des_);
    theta_db_.Swap(&other->theta_db_);
    std::swap(ratio_j0_, other->ratio_j0_);
    origin_.Swap(&other->origin_);
    std::swap(_has_bits_[0], other->_has_bits_[0]);
    _unknown_fields_.Swap(&other->_unknown_fields_);
    std::swap(_cached_size_, other->_cached_size_);
  }
}

::google::protobuf::Metadata M3HeadS2CSPCtrlParam::GetMetadata() const {
  protobuf_AssignDescriptorsOnce();
  ::google::protobuf::Metadata metadata;
  metadata.descriptor = M3HeadS2CSPCtrlParam_descriptor_;
  metadata.reflection = M3HeadS2CSPCtrlParam_reflection_;
  return metadata;
}


// @@protoc_insertion_point(namespace_scope)

// @@protoc_insertion_point(global_scope)
