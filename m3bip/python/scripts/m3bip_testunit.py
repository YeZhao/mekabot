#!/usr/bin/python

# MEKA CONFIDENTIAL
# 
# Copyright 2011 
# Meka Robotics LLC
# All Rights Reserved.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Meka Robotics LLC. The intellectual and 
# technical concepts contained herein are proprietary to 
# Meka Robotics LLC and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Meka Robotics LLC.

import time
import os
from optparse import OptionParser
import m3.toolbox as m3t
import m3.rt_proxy as m3p
import m3.component_factory as mcf
import m3bip.lcj_signal_gen_pb2
from m3bip.bip_controller1_pb2 import M3BipController1Command


class M3BipTest:

    comp_names = ['m3bip_controller1_j0','m3pwr_pwr010','m3bip_actuator_ec_lg0j2']
    comp_names_short = ['ctrl','pwr','actec']
    comps = {}

    def __init__(self):
        self.parser = OptionParser()
        self.parser.add_option('-l','--logfile',dest='logname')
        self.parser.add_option('-f','--log_frequency',default=100, type=int,
                          dest='log_frequency')
        self.parser.add_option('-d','--duration',dest='duration',type=float,
                          default=100)
#        self.parser.add_option('-m','--mode',dest='control_mode',type=str,default='mp')
        

    def extrainit(self):
        pass

    def extraparse(self):
        pass

    def m3init(self):
        self.extrainit()
        (self.opts,args) = self.parser.parse_args()
        

        self.do_log = (self.opts.log_frequency > 0)

        if self.do_log and (self.opts.logname is None or 
            os.path.isdir(m3t.get_m3_log_path()+self.opts.logname)):
            print "Enter log name [log]"
            while 1:
                self.opts.logname = m3t.get_string('log')
                if not (os.path.isdir(m3t.get_m3_log_path()+self.opts.logname)):
                    break
                else:
                    print self.opts.logname + " already exists"

        self.samples_per_file = self.opts.log_frequency

        self.proxy = m3p.M3RtProxy()
        self.proxy.start()
        for short_cname,cname in zip(self.comp_names_short,self.comp_names):
            self.comps[short_cname] = mcf.create_component(cname)     

        print self.comps

        for component in self.comps.itervalues():
            self.proxy.subscribe_status(component)
            self.proxy.publish_command(component)
            self.proxy.publish_param(component)
        self.proxy.make_operational_all()

        if self.do_log:
            for c in self.comps.itervalues():
                self.proxy.register_log_component(c)

   #     control_mode_map = {'mp': M3LcjCtrlDevCommand.MOTOR_POSITION,
   #                         'jpfs': M3LcjCtrlDevCommand.JOINT_POSITION_FS}
   #     self.control_mode = control_mode_map[self.opts.control_mode]

        self.extraparse()

    def loop_fun(self):
        pass

    def print_fun(self):
        return 1#[time.time()-self.tstart,
  #              self.comps['signal'].status.trajectory.x,
  #              self.comps['simple'].status.joint.joint_theta,
  #              self.comps['simple'].status.motor.motor_theta,
  #              self.comps['simple'].status.motor.motor_current]
        
    def run(self):
        self.m3init()
        self.comps['pwr'].set_motor_power_on()
  #      print "Control Mode: {0}\n".format(self.control_mode)
  #      self.comps['ctrl'].command.control_mode = self.control_mode
  #      self.comps['ctrl'].set_enable_on()	
  #      self.comps['simple'].set_mode_motor_torque()

        if self.do_log:
            self.proxy.start_log_service(self.opts.logname,
                                         sample_freq_hz=self.opts.log_frequency,
                                         samples_per_file=self.samples_per_file)
        try:
            self.tstart = time.time()
            self.i = 0
            while (time.time()-self.tstart < self.opts.duration):
                self.i += 1
                time.sleep(0.1)
                self.proxy.step()
                
                self.loop_fun()
                printlist = self.print_fun()
                print_format = "\t".join("{0: 8.5f}".format(x) 
                                         for x in printlist)
                print print_format
                
        except(KeyboardInterrupt):
            print "Keyboard Interrupt, hopefully exiting..."
        
        if self.do_log:
            self.proxy.stop_log_service()
        self.proxy.stop()





