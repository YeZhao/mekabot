#Copyright  2008, Meka Robotics
#All rights reserved.
#http://mekabot.com

#Redistribution and use in source and binary forms, with or without
#modification, are permitted. 


#THIS SOFTWARE IS PROVIDED BY THE Copyright HOLDERS AND CONTRIBUTORS
#"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#Copyright OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES INCLUDING,
#BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#POSSIBILITY OF SUCH DAMAGE.

import time
import numpy.numarray as na
import Numeric as nu
import math
import os 
import sys
import yaml
import m3.unit_conversion as m3u
from m3qa.calibrate import *
from m3qa.calibrate_sensors import *
from m3bip.bip_calibrate_actuator_ec import *
import m3bip.bip_actuator_ec_pb2 as aec
import m3bip.bip_config_act_array as aa

# ###################################### A2 J0 ##############################################################
config_default_aa_j0={
	'calib':aa.config_act_array_actuator_j0['calib'],
	'param':aa.config_act_array_actuator_j0['param'],
	'param_internal':
	{
		'calib_tq_lc_amp':2500.0,
		'calib_lever_mass':327.6,
		'calib_lever_com':157.5,
		'calib_lever_len':304.8,
		'joint_limits': {'both_arms':[-80.0,200.0],'note':'Positive is reaching upward'},
		'calib_tq_degree':1,
		'calib_hub_diam':70
	}
}

# ######################################## A2 J1 ############################################################
config_default_aa_j1={
	'calib':aa.config_act_array_actuator_j1['calib'],
	'param':aa.config_act_array_actuator_j1['param'],
	'param_internal':
	{
		'calib_tq_lc_amp':2500.0,
		'calib_lever_mass':327.6,
		'calib_lever_com':157.5,
		'calib_lever_len':304.8,
		'joint_limits': {'right_arm':[-24.1,150.46],'left_arm':[-150.46,24.1],'note':'positive is elbow to its right'},
		'calib_tq_degree':1,
		'calib_hub_diam':70
	}
}
# ########################################## A2 J2 ##########################################################
config_default_aa_j2={
	'calib':aa.config_act_array_actuator_j2['calib'],
	'param':aa.config_act_array_actuator_j2['param'],
	'param_internal':
	{
		'calib_tq_lc_amp':2500.0,
		'calib_lever_mass':327.6,
		'calib_lever_com':157.5,
		'calib_lever_len':304.8,
		'joint_limits': {'both_arms':[-85.0,85.0],'note':'positive is reaching to its right'},
		'calib_tq_degree':1,
		'calib_hub_diam':70
	}
}

# ###########################################################################

class M3BipCalibrate_ActArray(M3BipCalibrateActuatorEc):
	def __init__(self):
		M3BipCalibrateActuatorEc.__init__(self)
		
		self.joint_names=['Shoulder J0',
				  'Shoulder J1',
				  'Shoulder J2']
		self.config_default=[
			config_default_aa_j0,
			config_default_aa_j1,
			config_default_aa_j2]
		
	def start(self,ctype):
		if not M3BipCalibrateActuatorEc.start(self,ctype):
			return False
		self.jid=int(self.comp_ec.name[self.comp_ec.name.find('j')+1:])
		self.param_internal=self.config_default[self.jid]['param_internal']
		self.calib_default=self.config_default[self.jid]['calib']
		self.param_default=self.config_default[self.jid]['param']
		#print 'Calibrating joint',self.joint_names[self.jid]
		return True

	    