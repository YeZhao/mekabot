# -*- coding: utf-8 -*-
#Copyright  2010, Meka Robotics
#All rights reserved.
#http://mekabot.com

#Redistribution and use in source and binary forms, with or without
#modification, are permitted. 


#THIS SOFTWARE IS PROVIDED BY THE Copyright HOLDERS AND CONTRIBUTORS
#"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#Copyright OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES INCLUDING,
#BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#POSSIBILITY OF SUCH DAMAGE.

import copy
import m3qa.config_toolbox as ct

# ########################## ACTUATOR_EC A2.R3.J0-J6 ########################################

config_act_array_actuator_ec={
    'chid': 0,
    'ethercat': {'pdo_version': 'm3bip_act_pdo_v0',
                 'product_code': 1099,
                 'serial_number': 0},
    'name': 'm3bip_actuator_ec_lgxj0',
    'version':'default',    
    'param': {'config': 0,
              'dac_max': 2048,
              'kp_j': -500,
              'kp_t': 2,
              'kp_tj': 0,
              'kp_v': 10},
    'pwr_component': 'm3pwr_pwr016'}

config_act_array_actuator_ec_j0=copy.deepcopy(config_act_array_actuator_ec)
config_act_array_actuator_ec_j1=copy.deepcopy(config_act_array_actuator_ec)
config_act_array_actuator_ec_j2=copy.deepcopy(config_act_array_actuator_ec)


# ########################## ACTUATOR A2.R3.J0-J1 ########################################

config_act_array_actuator_j0={
    'calib': {'ambient_temp': {'cb_bias': 0.0,
                           'cb_mV_at_25C': 750.0,
                           'cb_mV_per_C': 10.0,
                           'cb_scale': 1.0,
                           'name': 'Analog TMP36',
                           'type': 'adc_linear_3V3'},
               'joint_calib': {'hub_r': 0.032},    
               'joint_theta_df': {'x_df': {'cutoff_freq': 80,
                                        'order': 3,
                                        'type': 'butterworth'},
                           'xdot_df': {'cutoff_freq': 20,
                                           'order': 3,
                                           'type': 'diff_butterworth'},
                           'xdotdot_df': {'cutoff_freq': 20,
                                              'order': 3,
                                              'type': 'diff_butterworth'},
                           'type': 'df_chain'},              
              'motor_calib': {
                        'adc_zero': 2048,
                        'alpha_cu': 0.0038999999999999998,
                        'cb_scale': 1.0,               
                        'cb_ticks_per_rev': 4096.0, 
                        'design_voltage': 36.0,
                        'mA_per_adc': 15.0,
                        'mA_per_dac': 9.765625,
                        'max_motor_temp': 35.0,
                        'max_winding_temp': 155.0,
                        'mechanical_time_constant': 2.6000000000000001,
                        'no_load_speed': 8800.0,
                        'rotor_inertia': 106.0,
                        'stall_torque': 315,
                        'thermal_resistance_housing_ambient': 3.9300000000000002,
                        'thermal_resistance_rotor_housing': 1.8899999999999999,
                        'thermal_time_constant_motor': 714.0,
                        'thermal_time_constant_winding': 16.190000000000001,
                        'torque_constant': 0.037999999999999999,
                        'winding_inductance': 0.41999999999999998,
                        'winding_resistance': 0.34699999999999998},
              'motor_theta_df': {'x_df': {'type': 'identity'},
                           'xdot_df': {'cutoff_freq': 20,
                                           'order': 3,
                                           'type': 'diff_butterworth'},
                           'xdotdot_df': {'cutoff_freq': 20,
                                              'order': 3,
                                              'type': 'diff_butterworth'},
                           'type': 'df_chain'},
              'motor_torque_df': {'x_df': {'cutoff_freq': 80,
                                        'order': 3,
                                        'type': 'butterworth'},
                           'xdot_df': {'cutoff_freq': 20,
                                           'order': 3,
                                           'type': 'diff_butterworth'},
                           'xdotdot_df': {'cutoff_freq': 20,
                                              'order': 3,
                                              'type': 'diff_butterworth'},
                           'type': 'df_chain'},
              'spring_calib': {'ball_screw_efficiency': 0.81999999999999995,
                        'ball_screw_pitch': 5.0,
                        'cb_bias': 0.0,
                        'cb_scale': 1.0,
                        'encoder_shaft_r': 53.022500000000001,
                        'spring_const': 4650.0},
              'spring_force_df': {'x_df': {'cutoff_freq': 80,
                                        'order': 3,
                                        'type': 'butterworth'},
                           'xdot_df': {'cutoff_freq': 20,
                                           'order': 3,
                                           'type': 'diff_butterworth'},
                           'xdotdot_df': {'cutoff_freq': 20,
                                              'order': 3,
                                              'type': 'diff_butterworth'},
                           'type': 'df_chain'}},    
    'act_ec_component': 'm3bip_actuator_ec_lgxjx',
    'ignore_bounds': 0,        
    'name': 'm3bip_actuator_lgxjx',
    'version':'default',
    'param': {'calibration':{'max_overload_time': 3.0,
                        'max_amp_temp': 125.0,
                        'max_amp_current': 20000.0,
                        'max_tq': 40000.0,
                        'min_tq': -40000.0},
              'k_danger': 0.40000000000000002,
              'softlimits':{'current': 0.40000000000000002,
                        'joint_theta_max': 1000.4,
                        'joint_theta_min': -1000.3,
                        'max_overload_time': 3.0}}}

config_act_array_actuator_j1=copy.deepcopy(config_act_array_actuator_j0)
config_act_array_actuator_j2=copy.deepcopy(config_act_array_actuator_j0)


config_act_array={
    'name': 'm3arm_xxx',
    'ndof': 3,
    'limb_name': 'xxx_leg',
    'version':'iss',
    'dynamatics_component': 'm3dynamatics_xxx',
'joint_components':{'J0': 'm3joint_xxx_j0',
                    'J1': 'm3joint_xxx_j1',
                    'J2': 'm3joint_xxx_j2'}}


# ########################## A2.R3 ########################################

config_full_act_array={'actuator_ec':[config_act_array_actuator_ec_j0,
                                 config_act_array_actuator_ec_j1,
                                 config_act_array_actuator_ec_j2],
                  'actuator':[config_act_array_actuator_j0,
                              config_act_array_actuator_j1,
                              config_act_array_actuator_j2,],                  
                  'arm':config_act_array}
