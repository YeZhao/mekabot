#Copyright  2008, Meka Robotics
#All rights reserved.
#http://mekabot.com

#Redistribution and use in source and binary forms, with or without
#modification, are permitted. 


#THIS SOFTWARE IS PROVIDED BY THE Copyright HOLDERS AND CONTRIBUTORS
#"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#Copyright OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES INCLUDING,
#BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#POSSIBILITY OF SUCH DAMAGE.


import time
import numpy.numarray as na
import Numeric as nu
import math
import os 
import sys
import yaml
import m3.unit_conversion as m3u
from m3qa.calibrate import *
from m3qa.calibrate_sensors import *
import m3bip.bip_actuator_ec_pb2 as aec
import serial
from threading import Thread

class M3BipCalibrateActuatorEc(M3Calibrate):
	def __init__(self):
		M3Calibrate.__init__(self)
		#self.param_internal={
			#'calib_tq_lc_amp':100.0,
			#'analyze_tq_lc_amp':1000.0,
			#'calib_lever_mass':327.6,
			#'calib_lever_com':157.5,
			#'calib_lever_len':304.8,
			#'calib_tq_degree':1,
			#'calib_hub_diam':70,
			#'joint_limits': [-10,10]
			#}
		
	def start(self,ctype):
		if not M3Calibrate.start(self,ctype):
			return False
		self.ext_temp=M3TempSensor(self.comp_rt.config['calib']['ambient_temp']['type'])
		
		#TODO: enable 3 below
		#self.current=M3CurrentSensor(self.comp_rt.config['calib']['current']['type'])
		self.theta=M3AngleSensor('vertx_14bit')
		#self.torque=M3TorqueSensor(self.comp_rt.config['calib']['torque']['type'])
		
		#self.name_j=self.name_rt.replace(ctype['comp_rt'],ctype['comp_j'])
		#self.comp_j=m3f.create_component(self.name_j)
				
		#if self.comp_j is None:
		#	print 'Joint component not found. Proceeding without it...'
		#self.comp_l=None
		#print 'Select component'
		#ac=self.proxy.get_available_components('m3loadx1')
		#if len(ac)==0:
		#	print 'Required loadx1 components not available. Proceeding without it'
		#	return True
		#self.name_load=m3t.user_select_components_interactive(ac,single=True)[0]
		#self.comp_l=m3f.create_component(self.name_load)
		#self.proxy.subscribe_status(self.comp_l)
		return True
		
	def zero_temp(self,sensor,adc_sensor):
		print 'Zero sensor manually [y]?'
		if m3t.get_yes_no('y'):
			avg_adc=int(self.get_sensor_list_avg([adc_sensor],1.0)[adc_sensor])
			print 'Enter temp (C)'
			cm=m3t.get_float()
			if sensor=='ambient_temp':
				cs=self.ext_temp.raw_2_C(self.comp_rt.config['calib'][sensor],avg_adc)
			if sensor=='amp_temp':
				cs=self.amp_temp.raw_2_C(self.comp_rt.config['calib'][sensor],avg_adc)
			print 'Old calibrated temp',cs
			print 'Delta',cm-cs
			self.comp_rt.config['calib'][sensor]['cb_bias']+=cm-cs
			if sensor=='ext_temp':
				print 'New calibrated temp',self.ext_temp.raw_2_C(self.comp_rt.config['calib'][sensor],avg_adc)
			if sensor=='amp_temp':
				print 'New calibrated temp',self.amp_temp.raw_2_C(self.comp_rt.config['calib'][sensor],avg_adc)
				
	def zero_current(self):
		print 'Zero sensor [y]?'
		if m3t.get_yes_no('y'):
			print 'Hit enter when motor power is off'
			raw_input()
			avg=self.get_sensor_list_avg(['adc_current_a','adc_current_b'],1.0)
			avg_a=int(avg['adc_current_a'])
			avg_b=int(avg['adc_current_b'])
			io=self.current.raw_2_mA(self.comp_rt.config['calib']['current'],avg_a,avg_b)
			print 'Old calibrated current',io
			self.comp_rt.config['calib']['current']['cb_ticks_at_zero_a']=avg_a
			self.comp_rt.config['calib']['current']['cb_ticks_at_zero_b']=avg_b
			print 'New calibrated current',self.current.raw_2_mA(self.comp_rt.config['calib']['current'],avg_a,avg_b)
		
	def zero_torque(self):
		print 'Zeroing torque'
		print 'Place actuator in unloaded configuration'
		print 'Give a small impulse in first direction, let return to zero'
		print 'Hit any key when ready'
		raw_input()
		raw_a=int(self.get_sensor_list_avg(['raw_act_torque'],1.0)['raw_act_torque'])
		
		print 'Give a small impulse in second direction, let return to zero'
		print 'Hit any key when ready'
		raw_input()
		raw_b=int(self.get_sensor_list_avg(['raw_act_torque'],1.0)['raw_act_torque'])
		raw_c=raw_a+(raw_b-raw_a)/2.0
		print 'Delta',raw_b-raw_a,'Mid',raw_c
		#tqo=self.torque.raw_2_mNm(self.comp_rt.config['calib']['torque'],raw_c)
		#print 'Old torque (mNm):',tqo
		print 'Old bias',self.comp_rt.config['param']['calibration']['zero_spring_theta']
		self.comp_rt.config['param']['calibration']['zero_spring_theta']-=raw_c
		#print 'New torque (mNm):',self.torque.raw_2_mNm(self.comp_rt.config['calib']['torque'],raw_c)
		print 'New bias',self.comp_rt.config['param']['calibration']['zero_spring_theta']

	def zero_joint_theta(self): #joint
		print 'Zeroing joint theta. This assumes that sensor is calibrated but need bias adjustment'
		print 'Proceed [y]?'
		if not m3t.get_yes_no('y'):
			return
		print 'Manually place actuator at zero hard-stop'
		print 'Hit any key when ready'
		raw_input()
		
		q_on=int(self.get_sensor_list_avg(['raw_joint_angle'],1.0)['raw_joint_angle'])
		
		#theta_calib = {}		
		#theta_calib['cb_scale'] = 1.0
		#theta_calib['cb_bias'] = self.comp_rt.config['param']['calibration']['raw_joint_angle']
		#bb=self.theta.raw_2_deg(theta_calib,q_on,0,0)
		
		print 'Old bias',self.comp_rt.config['param']['calibration']['zero_joint_theta']
		self.comp_rt.config['param']['calibration']['zero_joint_theta']-=q_on
		self.proxy.step()
		q_on=self.comp_ec.status.raw_joint_angle

		#bb=self.theta.raw_2_deg(self.comp_rt.config['param']['calibration']['zero_joint_theta'],q_on,0,0)
		#print 'New theta (deg):',bb
		print 'New bias',self.comp_rt.config['param']['calibration']['zero_joint_theta']
		
		
	def zero_motor_theta(self):
		print 'Zeroing motor theta. This assumes that sensor is calibrated but need bias adjustment'
		print 'Proceed [y]?'
		if not m3t.get_yes_no('y'):
			return
		print 'Manually place actuator at zero hard-stop'
		print 'Hit any key when ready'
		raw_input()
		
		q_on=int(self.get_sensor_list_avg(['raw_motor_angle'],1.0)['raw_motor_angle'])
		
		theta_calib = {}		
		theta_calib['cb_scale'] = 1.0
		theta_calib['cb_bias'] = self.comp_rt.config['param']['calibration']['zero_joint_theta']
		#bb=self.theta.raw_2_deg(theta_calib,q_on,0,0)
		
		print 'Old bias',self.comp_rt.config['param']['calibration']['zero_motor_theta']
		self.comp_rt.config['param']['calibration']['zero_motor_theta']-=q_on
		self.proxy.step()
		q_on=self.comp_ec.status.raw_motor_angle

		#bb=self.theta.raw_2_deg(self.comp_rt.config['param']['calibration']['zero_motor_theta'],q_on,0,0)
		print 'New theta (deg):',q_on
		print 'New bias',self.comp_rt.config['param']['calibration']['zero_motor_theta']
		
	

	def display_sensors(self):
		M3Calibrate.display_sensors(self)
		raw=self.comp_ec.status.raw_temp
		c=self.ext_temp.raw_2_C(self.comp_rt.config['calib']['ambient_temp'],raw)
		print 'Ambient Temp: (F) : '+'%3.2f'%m3u.C2F(c)+' (C): '+'%d'%c+' (ADC) '+'%d'%raw
		#raw=self.comp_ec.status.adc_amp_temp
		#c=self.amp_temp.raw_2_C(self.comp_rt.config['calib']['amp_temp'],raw)
		#print 'Amp Temp:   (F) : '+'%3.2f'%m3u.C2F(c)+' (C): '+'%3.2f'%c+' (ADC) '+'%d'%raw
		#raw_a=self.comp_ec.status.adc_current_a
		#raw_b=self.comp_ec.status.adc_current_b
		#c=self.current.raw_2_mA(self.comp_rt.config['calib']['current'],raw_a,raw_b)
		#print 'Current:   (mA) : '+'%3.2f'%c+' (ADC_A): '+'%d'%raw_a+' (ADC_B) '+'%d'%raw_b
		#q_on=self.comp_ec.status.qei_on
		#q_p=self.comp_ec.status.qei_period
		#q_r=self.comp_ec.status.qei_rollover
		#c=self.theta.raw_2_deg(self.comp_rt.config['calib']['theta'],q_on,q_p,q_r)
		#print 'Theta:    (Deg) : '+'%3.3f'%c+' Qei On '+'%d'%q_on+' Qei Period '+'%d'%q_p+' Qei Rollover '+'%d'%q_r
		#raw=self.comp_ec.status.adc_torque
		#c=self.torque.raw_2_mNm(self.comp_rt.config['calib']['torque'],raw)
		#print 'Torque:   (mNm) : '+'%3.2f'%c+' (inLb): '+'%3.2f'%m3u.mNm2inLb(c)+' (ADC) '+'%d'%raw
		#if self.comp_l is not None:
		#	print 'Load cell (mNm)',self.comp_l.get_torque_mNm()
			
	def do_task(self,ct):		
		#if ct=='ii':
		#	self.reset_sensor('current')
		#	self.zero_current()
		#	self.write_config()
		#	return True
		if ct=='et' :
			self.reset_sensor('ambient_temp')
			self.zero_temp('ambient_temp','raw_temp')
			self.write_config()
			return True				
		if ct=='zq':
			self.zero_torque()
			self.write_config()
			return True
		if ct=='zt':
			self.zero_joint_theta()
			self.write_config()
			return True
		if ct=='zm':
			self.zero_motor_theta()
			self.write_config()
			return True
			
		if M3Calibrate.do_task(self,ct):
			return True
		return False
	
	def print_tasks(self):
		M3Calibrate.print_tasks(self)
		print 'et: ambient_temp'
		#print 'at: amp_temp'
		#print 'sa: sensor analyze'
		#print 'ii: calibrate current'
		#print 'tt: calibrate theta'
		print 'zt: zero joint theta'
		print 'zm: zero motor theta'
		print 'zq: zero joint torque spring'
		#print 'cl: calibrate torque loadcell'
		#print 'cf: calibrate torque loadcell from file'
		#print 'al: analyze   torque loadcell'
		#print 'cw: calibrate torque weighted lever'
		#print 'dt: display torque calibration'
		#print 'gc: analyze torque gravity comp'
		
	def write_config(self):
		print 'Save calibration (y/n)?'
		save=raw_input()
		if save=='y':
			self.comp_rt.config['calibration_date']=time.asctime()
			self.comp_rt.write_config()
			if self.comp_ec is not None:
				self.comp_ec.write_config()
			#if self.comp_j is not None:
			#	self.comp_j.config['calibration_date']=time.asctime()
			#	self.comp_j.write_config()
				

				
