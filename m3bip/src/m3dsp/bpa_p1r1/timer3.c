/* 
M3 -- Meka Robotics Real-Time Control System
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "timer3.h"
#include "setup.h"



int irq_cnt;
unsigned int last_status;

void setup_timer3(void)
{
	last_status = 0xFFFF;
	irq_cnt=0;
	T3CON = 0;
	TMR3 = 0x0000;
	T3CONbits.TCKPS=T3_TCKPS;
	PR3 = (unsigned int)T3_PR3;
//	IPC2bits.T3IP	= 0x01;
	_T3IF = 0;									//Clear interrupt
	T3CONbits.TON = 1;							//Start Timer 3
	_T3IE = 1;									//Enable T3 ints
}

void __attribute__((__interrupt__, no_auto_psv)) _T3Interrupt(void) {
	//__asm__ volatile ("disi #0x3FFF"); // disable interrupts
	
	//_T3IF = 0;
//	SetHeartbeatLED;
	

		

//Latch encoder timestamp on Rising edge.
#ifdef USE_TIMESTAMP_DC
	SetTimestampLatch;
	ClrTimestampLatch;
#endif


	step_vertx();

	step_qei();
	step_period();

	step_control();

	//__asm__ volatile ("disi #0x0000"); // enable interrupts
//	ClrHeartbeatLED;
	
	_T3IF = 0;
}

