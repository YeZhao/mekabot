/* 
M3 -- Meka Robotics Real-Time Control System
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef _ENCODER_QEI_H
#define _ENCODER_QEI_H

#ifdef USE_ENCODER_QEI

//Setup for use with incremental quadarature encoder

void	setup_qei(void);
int		qei_error();
int		qei_position_HW();
int		qei_position_LW();
long	qei_position();
int		qei_calibrated_flag();
void	qei_zero();
void	step_qei();
int		limit_switch_neg_flag();

void	step_period();
int32_t get_period();
int16_t get_period_16();

#define QEI_PPR			1024
#define QEI_MAXCNT		(4*QEI_PPR-1)	// Counts between index pulse (in 4x mode)
//#define QEI_USE_INDEX	1

#endif
#endif
