/* 
M3 -- Meka Robotics Real-Time Control System
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "p33FJ32MC204.h"
#include "setup.h"
#include "encoder_qei.h"
#include "ethercat.h"
#include "timer3.h"
#include "dio.h"

// Position vars
static int32_t qei_pos;
static int32_t qei_roll;
static int16_t qei_err;
int16_t qei_calibrate_flag_last;
int16_t qei_calibration_req;
int16_t qei_limitswitch_last;
int16_t qei_zero_last;
int16_t qei_calibrated;
int16_t qei_index_cnt;

// Period vars
static uint16_t	last_t;
static int32_t	last_per;

static int32_t	qei_per;

static uint32_t sum_ec;
static uint16_t last_tmr2;
static uint16_t samp_flag;
static uint32_t last_qei;
static int		qei_tmp;

static int32_t t1;
static int32_t t2;




int qei_error()
{
	return qei_err;
}

//return lower word
int  qei_position_LW()
{
	return (int)(qei_pos & 0X0000FFFF);
}
int  qei_position_HW()
{
	return (int)((qei_pos>>16)& 0X0000FFFF);
}

int32_t qei_position()
{
	return qei_pos;
}
int qei_calibrated_flag()
{
  return qei_calibrated;
}

//TODO
int limit_switch_neg_flag()
{
  return 1;
}


void qei_zero()
{
	POSCNT=0;
	qei_pos=0;
	qei_roll=0;
}


void step_qei()
{
	//Signal for calibration on transition from 0 to 1 on the flag from the master
	if (!qei_calibration_req)
		qei_calibration_req=(ec_cmd.config & M3BIP_ACT_CONFIG_CALIB_QEI_LIMITSWITCH_NEG) && !qei_calibrate_flag_last;

	if ( qei_calibration_req)
	{
		if (limit_switch_neg_flag() && ! qei_limitswitch_last) //require off-to-on transition
		{
			POSCNT=0;
			qei_pos=0;
			qei_roll=0;
			qei_calibrated=M3BIP_ACT_FLAG_QEI_CALIBRATED;
			qei_calibration_req=0;
		}
	}
	qei_limitswitch_last=limit_switch_neg_flag();
	qei_calibrate_flag_last=ec_cmd.config & M3BIP_ACT_CONFIG_CALIB_QEI_LIMITSWITCH_NEG;
	//if (ABS(qei_roll)<QEI_32B_LIMIT)
	qei_pos = qei_roll+POSCNT;
	
}

//TODO: Fix index code, provide error counts
void __attribute__((__interrupt__, no_auto_psv)) _QEIInterrupt(void)
{	
	if (_UPDN) //forward
		qei_roll+=(QEI_MAXCNT+1);
	else
		qei_roll-=(QEI_MAXCNT+1);
	qei_pos = qei_roll+POSCNT;

//Latch encoder timestamp on Rising edge.
#ifdef USE_TIMESTAMP_DC
	SetTimestampLatch;
	ClrTimestampLatch;
#endif
	if (_CNTERR )
	{
		qei_err++;
		_CNTERR=0;
	}
	_QEIIF = 0;		//Clear the flag
}


// Timer 2 interrupt
void __attribute__((__interrupt__, no_auto_psv)) _T2Interrupt(void) {
	_T2IF = 0;
}

int32_t get_period()
{
	return qei_per;
}

int16_t get_period_16()
{
	if (qei_per > 32767)
		return (int16_t)32767;
	else if (qei_per < -32767)
		return (int16_t)-32767;
	else
		return (int16_t)qei_per;
}

void step_period()
{
	static int32_t per;
	static uint16_t snap_tmr2;
	static uint32_t tmp;
	

	snap_tmr2 = TMR2;
	
	
	if (IC1CONbits.ICBNE == 0)
	{
		qei_tmp = 0x0001;
		if (samp_flag == 1)
		{
			qei_tmp = 0x0002;
			if (last_t > snap_tmr2)
				sum_ec = (uint32_t)(PR2 - last_t) + snap_tmr2;
			else
				sum_ec = snap_tmr2 - (uint32_t)last_t;

			samp_flag = 0;
		}
		else
		{
			qei_tmp = 0x0003;
			if (last_tmr2 > snap_tmr2)
				tmp = (uint32_t)(PR2 - last_tmr2) + snap_tmr2;
			else
				tmp = snap_tmr2 - (uint32_t)last_tmr2;
			
			if (0x7FFFFFFF - sum_ec < tmp)
				sum_ec = 0x7FFFFFFF;
			else
				sum_ec += tmp;
		}
		
		
		if (ABS(last_per) > sum_ec)
		{
			qei_tmp = 0x0004;
			per = last_per;
		}
		else
		{
			qei_tmp = 0x0005;
			if (QEICONbits.UPDN == 0)	// negative
				per = -sum_ec;
			else
				per = sum_ec;
		}
		
		if (per == 0)
		{
			qei_tmp = 0x0006;
			per = last_per;
		}

	}
	else
	{
		t1	= IC1BUF;
		
		if (IC1CONbits.ICBNE == 1)
		{
			t2 = IC1BUF;
			while(IC1CONbits.ICBNE == 1) last_t = IC1BUF;
			if(t2>t1)
			{
				qei_tmp = 0x0010;
				per	= (t2 - t1);
			}
			else if (t2 == t1)	// this is bad
			{
				qei_tmp = 0x0020;
				// pretend it didn't happen...
				per = last_per;
				
			}
			else
			{
				qei_tmp = 0x0030;
				per	= (PR2 - t1) + t2;
			}
			
			last_t	= t2;

		}
		else
		{
			if (samp_flag == 1)
			{
				qei_tmp = 0x0040;
				if (t1 > last_t)
					per = t1 - last_t;
				else
					per = (PR2 - last_t) + t1;
			}
			else
			{
				qei_tmp = 0x0050;
				if (t1 > last_tmr2)
					per = sum_ec + t1 - last_tmr2;
				else
					per = sum_ec + (PR2 - last_tmr2) + t1;
			}
				
			last_t	= t1;
		}

		if (per <= 1)
		{
			qei_tmp |= 0x0100;
			per = last_per;
		}
		else
		{
			// set the direction...
			if (QEICONbits.UPDN == 0)
			{
				per = -per;
			}
			last_per = per;
		}



		last_qei	= qei_pos;
		
		samp_flag	= 1;
		
		sum_ec		= 0;

	}


	last_tmr2 = snap_tmr2;
	
	
	
	qei_per = per;

	
} // end get_per




void setup_qei(void) {

	qei_pos					= 0;
	qei_roll				= 0;
	qei_err					= 0;
	qei_limitswitch_last	= 1;	//avoids startup at switch condition
	qei_calibrate_flag_last	= 0;	//calibration will be enabled when master passes down flag for first time
	qei_calibration_req		= 0;	//not req until signaled from master
	qei_zero_last			= 0;

// Setup QEI
	MAXCNT				= QEI_MAXCNT;
	QEICON				= 0;
	POSCNT				= 0;	 
	QEICONbits.CNTERR	= 0; //Clear errors

	QEICONbits.POSRES	= 0; //Disable index pulse reset
	QEICONbits.QEIM		= 7;   //x4 mode with MAXCNT match reset

	//QEICONbits.SWPAB	= 0;		//Phase A/B Input not swapped
	QEICONbits.SWPAB	= 1;		//Phase A/B Input swapped

// Setup Digital Filter
	//Pin must be high for 3 clock cycles (Tqeck) to be counted
	//For 1:2 divide, Tqeck=50ns, So pin must be high for 150ns.
	//For 2Mhz encoder, Tqei=500ns, pin is high for 250ns min. 
	DFLTCON = 0;
	//DFLTCONbits.CEID	= 0;	//enable count error ints
	//DFLTCONbits.CEID	= 1;	//disable count error ints
	//DFLTCONbits.QECK	= 4;	//0x1;	// Digital filter clock  1:2 divide
	//DFLTCONbits.QEOUT	= 0;	// Enable filter
	//DFLTCONbits.IMV0	= 0;	//Set Phase A for Index
	//DFLTCONbits.IMV1	= 0;	/Set Phase B for Index

// Setup input capture  
	IC1CONbits.ICM		= 0b00;		// Disable Input Capture 1 module
	IC1CONbits.ICTMR	= 1;		// Select Timer2 as the IC1 Time base
	IC1CONbits.ICI		= 0b00;		// Interrupt on every capture event
	IC1CONbits.ICM		= 0b011;	// Generate capture event on rising edge
		
	IPC0bits.IC1IP = 1; // Setup IC1 interrupt priority level
	IFS0bits.IC1IF = 0; // Clear IC1 Interrupt Status Flag
	IEC0bits.IC1IE = 0; // Enable IC1 interrupt

// Setup timer 2
	T2CON			= 0;
	TMR2			= 0x0000;
	T2CONbits.TCKPS	= 0b01;
	PR2				= 0xFFFF;
//	IPC2bits.T2IP	= 0x01;
	_T2IF			= 0;			//Clear interrupt
	T2CONbits.TON	= 1;			//Start Timer 2
	_T2IE			= 0;			//Enable T2 ints

// init globals
	samp_flag 		= 0;
	last_t			= 0;
	last_per		= 0;
	sum_ec			= 0;
	last_tmr2		= 0;
	last_qei		= 0;
	

	
	_QEIIE=1;				//Enable interrupt

}

