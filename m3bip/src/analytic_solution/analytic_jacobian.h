#ifndef ANALYTIC_JACOBIAN
#define ANALYTIC_JACOBIAN

#include "util/wrap_eigen.hpp"
#include <vector>
#include "Hume_Controller/HumeSystem.h"
#include <stdio.h>

void getAnalyticJacobian(const std::vector<double> & q, LinkID link_id, jspace::Matrix & J);


#endif
