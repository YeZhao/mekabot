#ifndef GAIN_MANAGER
#define GAIN_MANAGER

#include "util/Hume_Thread.h"
#include "hume_protocol.h"
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Value_Slider.H>
#include <FL/Fl_Slider.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Float_Input.H>
#include <vector>
#include <string>

class ManageWindow : public Fl_Double_Window 
{
public:
    ManageWindow(int width, int height, int num_slot, const char * title, char** slider_name);


    virtual void resize(int x, int y, int w, int h);	
    Fl_Button	*quit;
    Fl_Button *get;
    Fl_Button *set;
    std::vector<Fl_Value_Slider*> mSlider_;
    std::vector<Fl_Float_Input*>   mInput_;
    std::vector<Fl_Button*>        mReset_;


    static void cb_reset(Fl_Widget *widget, void *param);
    static void cb_quit(Fl_Widget *widget, void *param);
    static void cb_head(Fl_Widget *widget, void *param);
    static void cb_get(Fl_Widget *widget, void *param);
    static void cb_set(Fl_Widget *widget, void *param);
    
    int num_slot_;
    int height_for_one_slot_;
    int height_;
    int button_width_;
    int boundary_;
    int upper_limit_;
    int lower_limit_;
    
    std::vector<double> base_info_;
    std::vector<double> default_value_;
    std::vector<double> set_value_;

    bool b_set_;
    void set_default_info(int idx, double value);
};

class Gain_Manager : public Hume_Thread{
public:
    Gain_Manager();
    virtual ~Gain_Manager();

    void add_manager(char* title, int num_slot, char** name_list);
    void recieve_send_pd_gain(int idx, int num_gain, int & recieve_socket, int & send_socket, int recieve_port, int send_port);
    void recieve_send_pi_gain(int idx, int & recieve_socket, int & send_socket, int recieve_port, int send_port);
    void set_gain_set(int win_idx, int num_gain, void * send_gain_set);
    void set_default_value(int win_idx, int num_gain, void * recieve_gain_set);
    std::vector<ManageWindow*> win_vec_;
    
    int win_width_;
};
class Gain_Manager_Height_Tilting_Left_Foot: public Gain_Manager{
public:
    Gain_Manager_Height_Tilting_Left_Foot();
    virtual ~Gain_Manager_Height_Tilting_Left_Foot(){}
    virtual void run();

protected:
    int socket_height_tilting_left_foot_recieve_;
    int socket_height_tilting_left_foot_send_;
};

class Gain_Manager_Height_Tilting_Right_Foot: public Gain_Manager{
public:
    Gain_Manager_Height_Tilting_Right_Foot();
    virtual ~Gain_Manager_Height_Tilting_Right_Foot(){}
    virtual void run();

protected:
    int socket_height_tilting_right_foot_recieve_;
    int socket_height_tilting_right_foot_send_;
};

class Gain_Manager_Body_XZ : public Gain_Manager {
public:
    Gain_Manager_Body_XZ();
    virtual ~Gain_Manager_Body_XZ(){}

    virtual void run();
    
protected:
    int socket_body_xz_recieve_;
    int socket_body_xz_send_;
};

class Gain_Manager_Tilting : public Gain_Manager {
public:
    Gain_Manager_Tilting();
    virtual ~Gain_Manager_Tilting(){}

    virtual void run();

protected:
    int socket_tilting_recieve_;
    int socket_tilting_send_;
};

class Gain_Manager_Actuator: public Gain_Manager{
public:
    Gain_Manager_Actuator();
    virtual ~Gain_Manager_Actuator(){}
    virtual void run();

protected:
    int socket_actuator_recieve_;
    int socket_actuator_send_;
};

class Gain_Manager_Height: public Gain_Manager{
public:
    Gain_Manager_Height();
    virtual ~Gain_Manager_Height(){}

    virtual void run();

protected:
    int socket_height_recieve_;
    int socket_height_send_;
};

class Gain_Manager_Foot_Left: public Gain_Manager{
public:
    Gain_Manager_Foot_Left();
    virtual ~Gain_Manager_Foot_Left(){}
    virtual void run();
protected:
    int socket_foot_recieve_;
    int socket_foot_send_;
};

class Gain_Manager_Foot_Right: public Gain_Manager{
public:
    Gain_Manager_Foot_Right();
    virtual ~Gain_Manager_Foot_Right(){}
    virtual void run();
protected:
    int socket_foot_recieve_;
    int socket_foot_send_;
};

class Gain_Manager_Height_Tilting_Left_Foot_XZ: public Gain_Manager{
public:
    Gain_Manager_Height_Tilting_Left_Foot_XZ();
    virtual ~Gain_Manager_Height_Tilting_Left_Foot_XZ(){}
    virtual void run();

protected:
    int socket_height_tilting_left_foot_xz_recieve_;
    int socket_height_tilting_left_foot_xz_send_;
};

class Gain_Manager_Height_Tilting_Right_Foot_XZ: public Gain_Manager{
public:
    Gain_Manager_Height_Tilting_Right_Foot_XZ();
    virtual ~Gain_Manager_Height_Tilting_Right_Foot_XZ(){}
    virtual void run();

protected:
    int socket_height_tilting_right_foot_xz_recieve_;
    int socket_height_tilting_right_foot_xz_send_;
};


#endif
