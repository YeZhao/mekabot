/* 
M3 -- Meka Robotics Robot Components
Copyright (c) 2011 Meka Robotics
Author: lee@mekabot.com (Lee Magnusson)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef M3_LCJ_SIGNAL_GEN_H
#define M3_LCJ_SIGNAL_GEN_H

#include "m3rt/base/component.h"
#include "m3/toolbox/toolbox.h"
#include "lcj_signal_gen.pb.h"
//#include "m3lcj/components/lcj_simple.h"
#include <google/protobuf/message.h>


namespace m3bip
{
	using namespace std;
	using namespace m3;
//	using namespace ros;
	
class M3LcjSignalGenerator : public m3rt::M3Component
{
    public:
	M3LcjSignalGenerator();
	//void Status(mReal time_seconds);  
	const M3LcjSignalGenStatus::M3LcjTrajectory * GetTrajectory() {return &status.trajectory();}
    
	google::protobuf::Message * GetCommand(){return &command;}
	google::protobuf::Message * GetStatus(){return &status;}
	google::protobuf::Message * GetParam(){return &param;}
    
    protected:
	enum {DEFAULT};
      	bool ReadConfig(const char * filename);
	void Startup();
	void Shutdown();
	void StepStatus();
	void StepCommand();
	bool LinkDependentComponents();
	M3BaseStatus * GetBaseStatus(){return status.mutable_base();}
	M3LcjSignalGenStatus status;
	M3LcjSignalGenCommand command;
	M3LcjSignalGenParam param;
	
    private:
//	M3LcjSignalGenParam::M3LcjTrajectoryParam * tp;
//	M3LcjSignalGenStatus::M3LcjTrajectory * t_out;
	mReal zero_modifier_value, ramp_mult;
	mReal t_start;
};

mReal powint(mReal x, int e);

}

#endif
