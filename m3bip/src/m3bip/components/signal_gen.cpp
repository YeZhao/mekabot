/* 
M3 -- Meka Robotics Robot Components
Copyright (c) 2011 Meka Robotics
Author: lee@mekabot.com (Lee Magnusson)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "signal_gen.h"

namespace signalgen{

void SignalGenerator::Init(param pin) {p = pin; }

void SignalGenerator::Start(mReal t)
{
	tstart = t;
}

traj SignalGenerator::Step(mReal t)
{
	traj tout;
	mReal freq_rad;
	if (t-tstart>p.chirp_duration)
		Start(t);
	
	t -= tstart;
	switch (p.command) {
		default:
		case ZERO:
			tout.x_dot = 0;
			tout.x_ddot = 0;
			tout.x = p.zero;
			break;
		case SIN:
			freq_rad = 2*M_PI*p.freq_start;
			tout.x = p.amp*sin(freq_rad*t) + p.zero;
			tout.x_dot = p.amp*freq_rad*cos(freq_rad*t);
			tout.x_ddot = -p.amp*freq_rad*freq_rad*sin(freq_rad*t);
			break;
		case CHIRP:
			tout = chirp_calc(1,t);
			break;
		case QUAD_CHIRP:
			tout = chirp_calc(2,t);
			break;
		case SQUARE: // TODO
			tout.x = p.zero;
			break;
	}
	return(tout);
}

traj SignalGenerator::chirp_calc(mReal pval, mReal t)
{
	traj tout;
	mReal beta;
	beta = (p.freq_end - p.freq_start) / powint(p.chirp_duration,pval);
	tout.x = p.amp*cos(2*M_PI*(beta/(1+pval)*powint(t,1+pval) + p.freq_start*t - .25));

	if (t < p.chirp_duration) {
		tout.x += p.zero, tout.x_dot = tout.x_ddot = 0;
	} else
		tout.x = tout.x_dot = tout.x_ddot = 0;
	return(tout);
}

mReal powint(mReal x, int e)
{
    mReal x_st = x;
    for(int i=1;i<e;i++)
	x *= x_st;
    return(x);
}


}