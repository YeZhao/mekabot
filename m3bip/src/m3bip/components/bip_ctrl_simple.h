/* 
M3 -- Meka Robotics Robot Components
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef M3_BIP_CTRL_SIMPLE_H
#define M3_BIP_CTRL_SIMPLE_H


#include <google/protobuf/message.h>

#include <m3rt/base/component.h>

#include <m3/toolbox/toolbox.h>
#include <m3/toolbox/dfilter.h>

#include "m3bip/components/bip_ctrl_simple.pb.h"

#include "m3bip/components/bip_actuator.h"
#include "m3bip/components/bip_actuator.pb.h"

namespace m3bip
{
	using namespace std;
	using namespace m3;
	using namespace m3rt;
//	using namespace ros;
/////////////////////////////////////////////////////////////////////////



class M3BipCtrlSimple: public  m3rt::M3Component
{
	public:
		M3BipCtrlSimple(): m3rt::M3Component(JOINT_PRIORITY),tmp_cnt(0),act(NULL)
		{
			RegisterVersion("default",DEFAULT);	
		}
		
	//Access to Status Messages
		M3BipCtrlSimpleStatusCommand *	StatusCommand()	{return status.mutable_command();}
		M3BipCtrlSimpleStatusMotor *	StatusMotor()	{return status.mutable_motor();}
		M3BipCtrlSimpleStatusJoint *	StatusJoint()	{return status.mutable_joint();}
		
	
	//Access to Traj Params
		M3BipParamTrajectory * 		ParamTrajMotorTorque()	{return param.mutable_traj_motor_torque();}
		M3BipParamTrajectory * 		ParamTrajMotorTheta()	{return param.mutable_traj_motor_theta();}

		M3BipParamTrajectory * 		ParamTrajJointTorque()	{return param.mutable_traj_joint_torque();}
		M3BipParamTrajectory * 		ParamTrajJointTheta()	{return param.mutable_traj_joint_theta();}
		
		
	//Conversions
		virtual	bool	IsEstopOn()	{if (!GetActuator()) return false; return act->IsEstopOn();}
		M3BipActuator * GetActuator()	{return act;}


	// motor getters
		mReal	GetMotorTorque()		{return StatusMotor()->torque();}
		mReal	GetMotorTorqueDot()		{return StatusMotor()->torquedot();}
		
		mReal	GetMotorTheta()			{return StatusMotor()->theta();}
		mReal	GetMotorThetaDot()		{return StatusMotor()->thetadot();}
		mReal	GetMotorThetaDotDot()	{return StatusMotor()->thetadotdot();}

	// joint getters
		mReal GetJointTorque()		{return StatusJoint()->torque();}
		mReal GetJointTorqueDot()	{return StatusJoint()->torquedot();}
		
		mReal GetJointTheta()		{return StatusJoint()->theta();}
		mReal GetJointThetaDot()	{return StatusJoint()->thetadot();}
		
		
		int64_t GetTimestamp()		{return GetBaseStatus()->timestamp();}
		
		
		
		google::protobuf::Message * GetCommand(){return &command;}
		google::protobuf::Message * GetStatus(){return &status;}
		google::protobuf::Message * GetParam(){return &param;}



	protected:

		// required:
		enum{DEFAULT};
		void Startup();
		void Shutdown();
		void StepStatus();
		void StepCommand();
		bool LinkDependentComponents();
		bool ReadConfig(const char * filename);
		M3BaseStatus * GetBaseStatus(){return status.mutable_base();}
		
		M3BipCtrlSimpleStatus	status;
		M3BipCtrlSimpleCommand	command;
		M3BipCtrlSimpleParam	param;
		
		//////////////////////

		void CommandActOff();		
		
		M3BipActuator * act;
		string			act_name;
		
		int tmp_cnt;
		
		
};


}

#endif


