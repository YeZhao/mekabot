/* 
   M3 -- Meka Robotics Robot Components
   Copyright (c) 2010 Meka Robotics
   Author: edsinger@mekabot.com (Aaron Edsinger)

   M3 is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   M3 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef M3BIP_SHM_H
#define M3BIP_SHM_H

#include <m3rt/base/component_shm.h>
#include "bip_shm.pb.h"
#include "bip_shm_sds.h"
#include <m3uta/controllers/uta_imu.h>
#include "bip_act_array.h"
#include <fstream>
#include <iostream>
#include "Hume_Controller/HumeSystem.h"


namespace m3bip{
    using namespace std;
    using namespace m3;
    using namespace m3uta;


    
    class M3BipShm : public  m3::M3Component{
    public:
        M3BipShm(): sds_status_size(0),sds_cmd_size(0),M3Component(6),imu(NULL),pwr(NULL),left_leg(NULL),right_leg(NULL),tmp_cnt(0),latencyBit(false)
            {
                printf("In M3BipShm constructor");
                RegisterVersion("default",DEFAULT);
                remove("meka_time.txt");
                sf_meka.open("meka_time.txt");
            }
        google::protobuf::Message * GetCommand(){return &command;}
        google::protobuf::Message * GetStatus(){return &status;}
        google::protobuf::Message * GetParam(){return &param;}

        void imu_mode_orientation(){ imu_mode_ = IMU_MODE_ORIENTATION; }
        void imu_mode_acc_ang_vel(){ imu_mode_ = IMU_MODE_ACCEL_ANG_RATE_MAG;}

        
    protected:
        IMU_MODE imu_mode_;
        bool ReadConfig(const char * filename);
        //M3ShmStatus * GetShmStatus(){return status.mutable_shm();}		
        size_t GetStatusSdsSize();
        size_t GetCommandSdsSize();
        //void SetCommandFromSds(unsigned char * data);
        //void SetSdsFromStatus(unsigned char * data);
	void StepStatus();
	void StepCommand();
        bool LinkDependentComponents();
        void ResetCommandSds(unsigned char * sds);
        void Startup();	
	void Shutdown(){}
	void doControl();	
			
        enum {DEFAULT};
        M3BaseStatus * GetBaseStatus();		
        M3BipShmCommand command;
        M3BipShmParam param;
        M3BipShmStatus status;
        M3BipActArray * left_leg;
        M3BipActArray * right_leg;
        M3Pwr * pwr;
        M3UTAImu * imu; 
        M3BipShmSdsCommand commandStruct;
        M3BipShmSdsStatus statusStruct;
        int sds_status_size;
        int sds_cmd_size;	
        string left_leg_name,right_leg_name,pwr_name, imu_name;
        int64_t timeout;
        int tmp_cnt;
        int imu_mode;
        bool startup_motor_pwr_on;
        bool latencyBit;
        std::ofstream sf_meka;
        HUME_System* hume_system_;

    private:
        void ChangeSO3toEulerXYZ(double * ori_mtx, std::vector<double> & Euler_Ang);
    };

}
#endif


