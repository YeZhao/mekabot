/* 
M3 -- Meka Robotics Robot Components
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.

*/
#include <cmath>

#include <m3rt/base/m3rt_def.h>
#include <m3rt/base/component_factory.h>

#include "bip_ctrl_simple.h"

namespace m3bip{
	
using namespace m3rt;
using namespace std;
using namespace m3;
//using namespace ros;


////////////////////////////////////////////////////////////////////////////////

bool M3BipCtrlSimple::ReadConfig(const char * filename)
{
	YAML::Node doc;
	mReal val;
	
	if (!M3Component::ReadConfig(filename))
		return false;

	GetYamlDoc(filename, doc);	
	
	//Misc
	doc["act_component"] >> act_name;


	
//Trajectoreis
	doc["param"]["traj_motor_theta"]["freq"] >> val;
	ParamTrajMotorTheta()->set_freq(val);
	doc["param"]["traj_motor_theta"]["amp"] >> val;
	ParamTrajMotorTheta()->set_amp(val);
	doc["param"]["traj_motor_theta"]["zero"] >> val;
	ParamTrajMotorTheta()->set_zero(val);

	doc["param"]["traj_motor_torque"]["freq"] >> val;
	ParamTrajMotorTorque()->set_freq(val);
	doc["param"]["traj_motor_torque"]["amp"] >> val;
	ParamTrajMotorTorque()->set_amp(val);
	doc["param"]["traj_motor_torque"]["zero"] >> val;
	ParamTrajMotorTorque()->set_zero(val);	
	
	doc["param"]["traj_joint_theta"]["freq"] >> val;
	ParamTrajJointTheta()->set_freq(val);
	doc["param"]["traj_joint_theta"]["amp"] >> val;
	ParamTrajJointTheta()->set_amp(val);
	doc["param"]["traj_joint_theta"]["zero"] >> val;
	ParamTrajJointTheta()->set_zero(val);

	doc["param"]["traj_joint_torque"]["freq"] >> val;
	ParamTrajJointTorque()->set_freq(val);
	doc["param"]["traj_joint_torque"]["amp"] >> val;
	ParamTrajJointTorque()->set_amp(val);
	doc["param"]["traj_joint_torque"]["zero"] >> val;
	ParamTrajJointTorque()->set_zero(val);

	return true;
} // end ReadConfig

bool M3BipCtrlSimple::LinkDependentComponents()
{
	act = (M3BipActuator*) factory->GetComponent(act_name);
	if (act==NULL)
	{
		M3_INFO("M3BipActuator component %s not found for component %s\n",act_name.c_str(),GetName().c_str());
		return false;
	}
	return true;
}


void M3BipCtrlSimple::Startup()
{
	if (act!=NULL)
		SetStateSafeOp();
	else
		SetStateError();
}

void M3BipCtrlSimple::Shutdown()
{
}

////////////////////////////////////////////////////////////////////////////////
//			STATUS


void M3BipCtrlSimple::StepStatus()
{ 

	M3BipActuatorStatusMotor * act_motor_status;
	M3BipActuatorStatusJoint * act_joint_status;

	if (IsStateError())
		return;
//	M3_INFO("AAA %s \n",act->GetName().c_str());//Access to actuator bombs???

// Copy motor status
	act_motor_status  = act->StatusMotor();

	StatusMotor()->set_theta(		act_motor_status->theta()		);
	StatusMotor()->set_thetadot(	act_motor_status->thetadot()	);
	StatusMotor()->set_thetadotdot(	act_motor_status->thetadotdot()	);
	
	StatusMotor()->set_torque(		act_motor_status->torque()		);
	StatusMotor()->set_torquedot(	act_motor_status->torquedot()	);
	
// Copy joint status
	act_joint_status  = act->StatusJoint();

	StatusJoint()->set_theta(		act_joint_status->theta()		);
	StatusJoint()->set_thetadot(	act_joint_status->thetadot()	);
	StatusJoint()->set_thetadotdot(	act_joint_status->thetadotdot()	);
	
	StatusJoint()->set_torque(		act_joint_status->torque()	);
	StatusJoint()->set_torquedot(	act_joint_status->torquedot()	);
	

} // end StepStatus


////////////////////////////////////////////////////////////////////////////////
//			COMMAND



void M3BipCtrlSimple::CommandActOff()
{
	act->SetCommandMode(BIP_ACT_MODE_OFF);
	act->SetCommandMotorTorque(0);
}


void M3BipCtrlSimple::StepCommand()
{
    tmp_cnt++;
    //DH TEST
//        printf("running simple command step\n");
	if (!act || IsStateSafeOp())
		return;
	
	if(IsStateError())
	{
		CommandActOff();
		return;
	}

	if ( IsEstopOn() )
	{
		CommandActOff();
		return;
	}
	
	
	mReal	desired_motor_theta;
	mReal	desired_motor_torque;
	mReal	desired_joint_theta;
	mReal	desired_joint_torque;
	
	mReal	motor_torque;		// torque result from PID

	int control_mode = command.ctrl_mode();


	
	//Trajectories
	mReal dt			= GetTimestamp()/1000000.0;//seconds

	mReal tm_theta	= sin( 2*M_PI * dt * ParamTrajMotorTheta()->freq());
	mReal tm_torque	= sin( 2*M_PI * dt * ParamTrajMotorTorque()->freq());
		
	mReal tj_theta	= sin( 2*M_PI * dt * ParamTrajJointTheta()->freq());
	mReal tj_torque	= sin( 2*M_PI * dt * ParamTrajJointTorque()->freq());
	
	// Grab the commmand values
	desired_motor_theta		= command.desired_motor_theta();
	desired_motor_torque	= command.desired_motor_torque();
	desired_joint_theta		= command.desired_joint_theta();
	desired_joint_torque	= command.desired_joint_torque();
	
	
	switch(command.traj_mode())
	{
		case TRAJ_MOTOR_TORQUE_SQUARE:
			if (tm_torque>0)
				desired_motor_torque = ParamTrajMotorTorque()->zero()+ParamTrajMotorTorque()->amp();
			else
				desired_motor_torque = ParamTrajMotorTorque()->zero()-ParamTrajMotorTorque()->amp();
			break;

		case TRAJ_MOTOR_TORQUE_SINE:
			desired_motor_torque = ParamTrajMotorTorque()->zero()+ParamTrajMotorTorque()->amp()*tm_torque;
			break;

		case TRAJ_MOTOR_THETA_SQUARE:
			if (tm_theta>0)
				desired_motor_theta = ParamTrajMotorTheta()->zero()+ParamTrajMotorTheta()->amp();
			else
				desired_motor_theta = ParamTrajMotorTheta()->zero()-ParamTrajMotorTheta()->amp();
			break;

		case TRAJ_MOTOR_THETA_SINE:
			desired_motor_theta = ParamTrajMotorTheta()->zero()+ParamTrajMotorTheta()->amp()*tm_theta;
			break;

		case TRAJ_JOINT_TORQUE_SQUARE:
			if (tj_torque>0)
				desired_joint_torque = ParamTrajJointTorque()->zero()+ParamTrajJointTorque()->amp();
			else
				desired_joint_torque = ParamTrajJointTorque()->zero()-ParamTrajJointTorque()->amp();
			break;

		case TRAJ_JOINT_TORQUE_SINE:
			desired_joint_torque = ParamTrajJointTorque()->zero()+ParamTrajJointTorque()->amp()*tj_torque;
			break;

		case TRAJ_JOINT_THETA_SQUARE:
			if (tj_theta>0)
				desired_joint_theta = ParamTrajJointTheta()->zero()+ParamTrajJointTheta()->amp();
			else
				desired_joint_theta = ParamTrajJointTheta()->zero()-ParamTrajJointTheta()->amp();
			break;

		case TRAJ_JOINT_THETA_SINE:
			desired_joint_theta = ParamTrajJointTheta()->zero()+ParamTrajJointTheta()->amp()*tj_theta;
			break;

		case TRAJ_OFF:
			default:
			break;
	};

	
//Reset
	// motor
	StatusMotor()->set_theta_error(0);
	StatusMotor()->set_torque_error(0);
	// joint
	StatusJoint()->set_theta_error(0);
	StatusJoint()->set_torque_error(0);
	

	
	//Handle Inner Control Loops
	switch(control_mode)
	{
		case BIP_CTRL_MODE_MOTOR_TORQUE:

			act->SetCommandMode(BIP_ACT_MODE_MOTOR_TORQUE);
			act->SetCommandMotorTorque(desired_motor_torque);
			
			// update personal status
			StatusCommand()->set_motor_torque(desired_motor_torque);
			
			break;
		

		case BIP_CTRL_MODE_MOTOR_THETA:
			
			act->SetCommandMode(BIP_ACT_MODE_MOTOR_THETA);
			act->SetCommandMotorTheta(desired_motor_theta);
			
			// update personal status
			StatusCommand()->set_motor_theta(desired_motor_theta);
			
			break;

		case BIP_CTRL_MODE_JOINT_TORQUE:
			
			act->SetCommandMode(BIP_ACT_MODE_JOINT_TORQUE);
			act->SetCommandJointTorque(desired_joint_torque);
			
			// update personal status
			StatusCommand()->set_joint_torque(desired_joint_torque);
			break;

		case BIP_CTRL_MODE_JOINT_THETA:
			
			act->SetCommandMode(BIP_ACT_MODE_JOINT_THETA);
			act->SetCommandJointTheta(desired_joint_theta);
			act->SetCommandJointStiffness(command.desired_joint_stiffness());
			
			// update personal status
			StatusCommand()->set_joint_theta(desired_joint_theta);
			break;

		case BIP_CTRL_MODE_OFF:
		default:
			CommandActOff();
			break;
	};

} 


} // end StepCommand
