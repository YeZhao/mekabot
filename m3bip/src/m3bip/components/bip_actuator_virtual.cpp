/* 
M3 -- Meka Robotics Robot Components
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/

#define _USE_MATH_DEFINES

#include <cmath>
#include <math.h>


#include "m3rt/base/m3rt_def.h"
#include "m3rt/base/component_factory.h"

#include "bip_actuator_virtual.h"

namespace m3bip{
	
using namespace m3rt;
using namespace std;
using namespace m3;
//using namespace ros;


////////////////////////////////////////////////////////////////////////////////


void M3BipActuatorVirtual::Startup()
{


		SetStateSafeOp();
}

bool M3BipActuatorVirtual::LinkDependentComponents()
{
	return true;
}


void M3BipActuatorVirtual::Shutdown()
{
}


void M3BipActuatorVirtual::StepStatus()
{ 
	if (IsStateError())
		return;
	


	if (command.ctrl_mode() != BIP_ACT_MODE_OFF)
	{
		motor_torque_df.Step(command.motor_torque(),GetTimestamp());
	  
		StatusMotor()->set_torque(		command.motor_torque());
		StatusMotor()->set_torquedot(	motor_torque_df.GetXDot() );
		StatusMotor()->set_thetadot(	command.motor_velocity());
	  
		joint_theta_df.Step( command.joint_theta(),GetTimestamp());
		
		StatusJoint()->set_theta(		command.joint_theta());
		StatusJoint()->set_thetadot(	joint_theta_df.GetXDot() );
	  
	  
		StatusJoint()->set_torque(		command.joint_torque());
		StatusJoint()->set_torquedot(	0);
	}

	
} // end StepStatus



void M3BipActuatorVirtual::StepCommand()
{

} 


} // end StepCommand
