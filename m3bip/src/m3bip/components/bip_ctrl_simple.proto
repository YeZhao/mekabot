// M3 -- Meka Robotics Robot Components
// Copyright (c) 2010 Meka Robotics
// Author: edsinger@mekabot.com (Aaron Edsinger)
// 
// M3 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// M3 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M3.  If not, see <http://www.gnu.org/licenses/>.


option optimize_for = SPEED;

import "component_base.proto";


enum BIP_CTRL_SIMPLE_MODE{	
	BIP_CTRL_MODE_OFF			= 0;	// Actuator off

	BIP_CTRL_MODE_MOTOR_TORQUE	= 1;	// Motor torque control
	BIP_CTRL_MODE_MOTOR_THETA	= 2;	// Motor angle control

	BIP_CTRL_MODE_JOINT_TORQUE	= 3;	// Joint position control
	BIP_CTRL_MODE_JOINT_THETA	= 4;	// Joint position control
}

enum BIP_CTRL_SIMPLE_TRAJ_MODE{	
	TRAJ_OFF					= 0;

	TRAJ_MOTOR_TORQUE_SQUARE	= 1;
	TRAJ_MOTOR_TORQUE_SINE		= 2;
	TRAJ_MOTOR_THETA_SQUARE		= 3;
	TRAJ_MOTOR_THETA_SINE		= 4;

	TRAJ_JOINT_TORQUE_SQUARE	= 5;
	TRAJ_JOINT_TORQUE_SINE		= 6;
	TRAJ_JOINT_THETA_SQUARE		= 7;
	TRAJ_JOINT_THETA_SINE		= 8;
}

////////////////////////////////////////////////////////////////////////////////
//								status

message M3BipCtrlSimpleStatusCommand{
	optional double		motor_theta		= 1;	// (Rad) 
	optional double		motor_torque	= 2;	// (Nm)
	optional double		joint_torque	= 3;	// (Nm)
	optional double		joint_theta		= 4;	// (Rad)
}


message M3BipCtrlSimpleStatusMotor{
	optional double		torque			= 1;	// (Nm) based on motor current measurement
	optional double		torquedot		= 2;	// (Nm) based on motor current measurement
	optional double		torque_error	= 3;	// (Nm) difference between setpoint and sensor

	optional double		theta			= 4;	// (Rad) calibrated from motor encoder
	optional double		thetadot		= 5;	// (Rad/s) digital filter
	optional double		thetadotdot		= 6;	// (Rad/s2) digital filter
	optional double		theta_error		= 7;	// (Rad) difference between setpoint and sensor
}


message M3BipCtrlSimpleStatusJoint{
	optional double		theta			= 1;	// (rad)
	optional double		thetadot		= 2;	// (m/s) digital filter
	optional double		thetadotdot		= 3;	// (Rad/s2) digital filter
	optional double		theta_error		= 4;	// (Rad) difference between setpoint and sensor

	optional double		torque			= 5;	// (Nm) force sensor
	optional double		torquedot		= 6;	// (Nm/s) digital filter
	optional double		torque_error	= 7;	// (Nm) difference between setpoint and sensor
}


message M3BipCtrlSimpleStatus{
	message M3BipCtrlSimpleStatusVelocity{
		optional double theta_dot_diff			= 1;
		optional double theta_dot_cap			= 2;
		optional double theta_diff_motor_joint	= 3;
	}
	optional M3BaseStatus					base		= 1;	//Reserved
	optional M3BipCtrlSimpleStatusCommand	command		= 3;	//Current values commanded 
	optional M3BipCtrlSimpleStatusMotor		motor		= 4;	//Motor frame sensor/ctrl state
	optional M3BipCtrlSimpleStatusJoint		joint		= 5;	//Output frame sensor/ctrl state
	optional M3BipCtrlSimpleStatusVelocity	vel			= 6;	// 
}

////////////////////////////////////////////////////////////////////////////////
//								params


message M3BipParamTrajectory{
	optional double		freq	= 1;		//Hz
	optional double		amp		= 2;		//Nm or rad
	optional double		zero	= 3;		//Nm or rad
}

message M3BipCtrlSimpleParam{
	optional M3BipParamTrajectory	traj_motor_torque	= 1;	//Trajectory settings
	optional M3BipParamTrajectory	traj_motor_theta	= 2;	//Trajectory settings
	optional M3BipParamTrajectory	traj_joint_torque	= 3;	//Trajectory settings
	optional M3BipParamTrajectory	traj_joint_theta	= 4;	//Trajectory settings
	
}

////////////////////////////////////////////////////////////////////////////////
//								cmd

message M3BipCtrlSimpleCommand{
	optional BIP_CTRL_SIMPLE_MODE			ctrl_mode				= 1;	//Desired control mode
	optional BIP_CTRL_SIMPLE_TRAJ_MODE		traj_mode				= 2;	//Desired trajectory mode
	
	optional double						desired_motor_torque	= 3;	//Desired motor torque (mNm)
	optional double						desired_motor_theta		= 4;	//Desired motor angle (Rad)
	
	optional double						desired_joint_torque	= 5;	//Desired joint torque (mNm)
	optional double						desired_joint_theta		= 6;	//Desired joint angle (Rad)
	optional double						desired_joint_stiffness	= 7;
}
