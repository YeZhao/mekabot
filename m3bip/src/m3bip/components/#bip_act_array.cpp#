/* 
M3 -- Meka Robotics Robot Components
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "m3bip/components/bip_act_array.h"
#include "m3rt/base/component_factory.h"
#include "m3rt/base/toolbox.h"
#include "m3rt/base/m3rt_def.h"
#include <iostream>

namespace m3bip
{
	
using namespace m3rt;
using namespace m3;
using namespace std;
using namespace KDL;
//using namespace ros;

///////////////////////////////////////////////////////
//Success so long as least one dependent comp. available
bool M3BipActArray::LinkDependentComponents()
{	
	int nl=0;
	if (act_names.size()>0)
		acts.assign(act_names.size(),(M3BipActuator*)NULL);
	for (int i=0;i<act_names.size();i++)
	{
		if(act_names[i].size())
		{
			acts[i]=(M3BipActuator*)factory->GetComponent(act_names[i]);			
			if (acts[i]!=NULL)
				nl++;
		}
		else
			acts[i]=NULL;
	}	
		
	if (nl<=0)
	{
		M3_ERR("M3BipActArray %s found %d acts \n",GetName().c_str(),nl);	
		return false;
	}
	if (nl!=ndof)
	{
		M3_ERR("M3BipActArray %s found %d acts. Expected %d. Continuing with missing joints. \n",
		       GetName().c_str(),nl,ndof);	
	}
	return true;
}

void M3BipActArray::Startup()
{		
	acts.resize(ndof);
	act_names.resize(ndof);	

	for (int i=0;i<ndof;i++)
	{
		if (act_names[i].size()==0)
		{
			M3_WARN("M3BipActArray joint %d not configured properly\n",i);
			SetStateError();
			return;
		}
		command.add_torque(0);
		command.add_ctrl_mode(BIP_ACT_MODE_OFF);
		command.add_joint_theta(0);
		command.add_joint_torque(0);
		command.add_motor_velocity(0);
		command.add_joint_stiffness(0);
		command.add_dsp_latency_bit_cmd(false);
                command.add_joint_thetadot(0);
                
		
		status.add_motor_temp_winding(0);
		status.add_motor_temp_ambient(0);
		status.add_motor_temp_housing(0);
		status.add_current_cmd(0);
		status.add_motor_torque_cmd(0);		
		status.add_joint_torque_cmd(0);
		status.add_joint_theta_cmd(0);
		status.add_current(0);
		status.add_motor_torque(0);
		status.add_motor_torquedot(0);
		status.add_motor_theta(0);
		status.add_motor_thetadot(0);
		status.add_motor_thetadotdot(0);
		status.add_joint_theta(0);
		status.add_joint_thetadot(0);
		status.add_joint_thetadotdot(0);
		status.add_joint_torque(0);
		status.add_joint_torquedot(0);
		status.add_spring_theta(0);
		status.add_spring_force(0);
		status.add_spring_forcedot(0);
		status.add_dsp_latency_bit_status(false);
		status.add_foot_contact(false);
		
		
		status.add_flags(0);
	}

	
	SetStateSafeOp();

}


void M3BipActArray::Shutdown()
{

}



void M3BipActArray::StepCommand()
{
    //DH TEST
    printf("bip act array step command is running \n");

    for (int i(0); i< ndof; ++i){
        printf("[act array] control mode of %d: %d \n", i, command.ctrl_mode(i));

    }
    Hume_ctrl.getCommand(status, param, command);
    
	if (IsStateSafeOp() || IsStateError())
		return;
		

	//Individual controllers
	latencyBit=command.dsp_latency_bit_cmd(2);
	for (int i=0;i<ndof;i++)
	{

		if (acts[i]!=NULL)
		{		
			acts[i]->SetCommandMotorTorque(command.torque(i));			
			acts[i]->SetCommandJointTheta(command.joint_theta(i));
			acts[i]->SetCommandJointTorque(command.joint_torque(i));
			acts[i]->SetCommandMotorVelocity(command.motor_velocity(i));
			acts[i]->SetCommandJointStiffness(command.joint_stiffness(i));
			acts[i]->SetCommandDSPLatencyBit(command.dsp_latency_bit_cmd(i));

                        //Command_Joint_ThetaDot
                        acts[i]->SetCommandJointThetadot(command.joint_thetadot(i));
			
			switch(command.ctrl_mode(i))
			{
				case BIP_ACT_MODE_OFF:
					acts[i]->SetCommandMode(BIP_ACT_MODE_OFF);
					break;
				case BIP_ACT_MODE_JOINT_TORQUE:
					acts[i]->SetCommandMode(BIP_ACT_MODE_JOINT_TORQUE);
					//M3_DEBUG("%f\n", command.torque(i));
					break;
				case BIP_ACT_MODE_MOTOR_TORQUE:
					acts[i]->SetCommandMode(BIP_ACT_MODE_MOTOR_TORQUE);
					//M3_DEBUG("%f\n", command.torque(i));
					break;
				case BIP_ACT_MODE_JOINT_THETA:
					acts[i]->SetCommandMode(BIP_ACT_MODE_JOINT_THETA);
					break;
				case BIP_ACT_MODE_MOTOR_VELOCITY:
					acts[i]->SetCommandMode(BIP_ACT_MODE_MOTOR_VELOCITY);
					break;				
				default:
					acts[i]->SetCommandMode(BIP_ACT_MODE_OFF);
			};
		}
	}
}

void M3BipActArray::StepStatus()
{	
	if (IsStateError())
		return;
	for (int i=0;i<ndof;i++)
	{
		if (acts[i]!=NULL)
		{
			status.set_motor_temp_winding(i,acts[i]->StatusTemp()->winding());
			status.set_motor_temp_ambient(i,acts[i]->StatusTemp()->ambient());
			status.set_motor_temp_housing(i,acts[i]->StatusTemp()->housing());
			status.set_current_cmd(i,acts[i]->StatusCommand()->current());
			status.set_motor_torque_cmd(i,acts[i]->StatusCommand()->torque());		
			status.set_joint_torque_cmd(i,acts[i]->StatusCommand()->joint_torque());
			status.set_joint_theta_cmd(i,acts[i]->StatusCommand()->joint_theta());
			status.set_current(i,acts[i]->GetMotorCurrent());
			status.set_motor_torque(i,acts[i]->GetMotorTorque());
			status.set_motor_torquedot(i,acts[i]->GetMotorTorqueDot());
			status.set_motor_theta(i,acts[i]->GetMotorTheta());
			status.set_motor_thetadot(i,acts[i]->GetMotorThetaDot());
			status.set_motor_thetadotdot(i,acts[i]->GetMotorThetaDotDot());
			status.set_joint_theta(i,acts[i]->GetJointTheta());
			status.set_joint_thetadot(i,acts[i]->GetJointThetaDot());
			status.set_joint_thetadotdot(i,acts[i]->StatusJoint()->thetadotdot());
			status.set_joint_torque(i,acts[i]->GetJointTorque());
			status.set_joint_torquedot(i,acts[i]->GetJointTorqueDot());
			status.set_spring_theta(i,acts[i]->StatusSpring()->theta());
			status.set_spring_force(i,acts[i]->GetSpringForce());
			status.set_spring_forcedot(i,acts[i]->GetSpringForceDot());
			status.set_dsp_latency_bit_status(i,acts[i]->GetDSPLatencyBit());
			status.set_flags(i,acts[i]->StatusRaw()->flags());

                        status.set_foot_contact(i,acts[i]->GetFootContact());
                        // status.set_foot_contact(i,false);
		}
	}
        
	status.set_dsp_latency_bit_status(2, latencyBit);
//        printf("Latency bit: %s\n", latencyBit?"1":"0"); //works

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool M3BipActArray::ReadConfig(const char * filename)
{
	if (!M3Component::ReadConfig(filename))
		return false;
	YAML::Node doc;
	GetYamlDoc(filename, doc);
	doc["ndof"] >> ndof;
	for(YAML::Iterator it=doc["act_components"].begin();it!=doc["act_components"].end();++it) 
	{
   		string key, value;
    		it.first() >> key;
    		it.second() >> value;
		int id=atoi(key.substr(1).c_str()); //"J0" gives 0, etc
		if (act_names.size() <= (id+1))
		{
			act_names.resize(id+1);
		}
		act_names[id]=value;
	}
	
	return true;
}


}
