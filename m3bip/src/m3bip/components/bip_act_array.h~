/* 
M3 -- Meka Robotics Robot Components
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef M3BIP_ACT_ARRAY_H
#define M3BIP_ACT_ARRAY_H

#include "m3rt/base/component.h"
#include "m3/toolbox/toolbox.h"
#include <google/protobuf/message.h>
#include "m3bip/components/bip_actuator.h"
#include "m3bip/components/bip_act_array.pb.h"
#include "m3bip/hume_controller/Hume_Controller.h"


namespace m3bip
{
	using namespace std;
	using namespace KDL;
//	using namespace ros;
	using namespace m3;
///////////////////////////////////////////////////////////////////////////

class M3BipActArray : public m3rt::M3Component
{
	public:
		M3BipActArray(): m3rt::M3Component(CHAIN_PRIORITY), latencyBit(false)
		{
			RegisterVersion("default",DEFAULT);	
			
		}
		google::protobuf::Message * GetCommand(){return &command;}
		google::protobuf::Message * GetStatus(){return &status;}
		google::protobuf::Message * GetParam(){return &param;}
		int GetNumDof(){return ndof;}
		M3BipActuator * GetActuator(int idx){return acts[idx];}		
		
		string GetActName(int i){return act_names[i];}		
	protected:
		enum {DEFAULT};	
		bool ReadConfig(const char * filename);
		void Startup();
		void Shutdown();
		void StepStatus();
		void StepCommand();
		bool LinkDependentComponents();
	
		M3BaseStatus * GetBaseStatus(){return status.mutable_base();}
		M3BipActArrayStatus status;
		M3BipActArrayCommand command;
		M3BipActArrayParam param;			
		int ndof;
		vector<M3BipActuator *> acts;		
    vector<string> act_names;
    Hume_Controller Hume_ctrl;
    
    
		int tmp_cnt;	
		bool latencyBit;
};

}

#endif


