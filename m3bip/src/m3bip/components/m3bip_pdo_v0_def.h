/* 
M3 -- Meka Robotics Real-Time Control System
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef M3BIP_PDO_V0_H
#define M3BIP_PDO_V0_H

#ifdef __KERNEL__
#define int16_t short
#define int32_t int
#else
#ifndef EMBEDDED
#include <sys/types.h>
#include <stdint.h>
#endif
#endif

///////////////////////////////  M3BIP_ACT_PDO_V0 /////////////////////////////////////////////////////
#define M3BIP_ACT_CONFIG_CALIB_QEI_LIMITSWITCH_NEG	1
#define M3BIP_ACT_CONFIG_TORQUE_SMOOTH				2
#define M3BIP_ACT_CONFIG_VERTX_FILTER_OFF			3


typedef struct 
{
	int32_t		desired;					// Desired control value

	int16_t		config;						// Reserved
	int16_t		status;						// Must change each command to reset dsp watchdog
	int16_t		mode;						// Mode

	int16_t		dac_max;

	int16_t		joint_theta_max;
	int16_t		joint_theta_min;

	int16_t		kp_danger;
	int16_t		kp_velocity;					// motor velocity P gain

	int16_t		kp_motor;					// motor theta P gain
	int16_t		ki_motor;
	int16_t		ki_motor_limit;

	int16_t		kp_joint;					// joint theta P gain
	int16_t		ki_joint;
	int16_t		ki_joint_limit;

	int16_t		kp_torque;					// joint torque P gain
	int16_t		ki_torque;
	int16_t		ki_torque_limit;
	
	int16_t		latency_bytes; // latency calculation bytes
    int16_t kp_embeddamp;
} M3BipActPdoV0Cmd;



#define M3BIP_ACT_FLAG_AMP_ENABLED		1
#define M3BIP_ACT_FLAG_AMP_FAULT		2	//Not implemented on BPA v0
#define M3BIP_ACT_FLAG_QEI_CALIBRATED	3

typedef struct 
{
	uint64_t    timestamp;		    // Time in us
	int32_t		raw_motor_angle;	// Motor QEI enc (encoder ticks)
	int32_t		raw_period;			// 
	int16_t		raw_temp;			// temp (adc ticks)
	int16_t		raw_motor_current;	// Motor current (adc ticks)
	int16_t		raw_act_torque;		// Force (Vert-X ticks)
	int16_t		raw_joint_angle;	// Joint position (Vert-X)
	int16_t		raw_dac_cmd;		// Current dac command to motor
	int16_t		raw_cmd;			// Current top-level command into the controllers
	int16_t		raw_velocity_cmd;
	int16_t		ctrl_state;
	int16_t		foot_contact_and_latency_bytes;
	int16_t		debug;				// Reserved
	int16_t		flags;				// Reserved
}M3BipActPdoV0Status;



#endif
