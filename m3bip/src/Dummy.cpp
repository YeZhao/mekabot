#ifdef Body_Test

    // TEST:
    Vector print(3);
    print[0] = pDyn_env->m_Hume->Full_Joint_State_[HumeID::SIM_Y] ->m_rValue[3];
    print[1] = pDyn_env->m_Hume->Full_Joint_State_[3+HumeID::SIM_Rz]->m_rValue[3];
    print[2] = pDyn_env->m_Hume->Full_Joint_State_[3+HumeID::SIM_Rx]->m_rValue[3];

    Matrix A;
    Vector grav;
    std::vector<double> jpos(SIM_NUM_JOINT);
    Vector acc(SIM_NUM_JOINT);
    Vector tor(SIM_NUM_JOINT);
    for (int i(0); i<SIM_NUM_JOINT; ++i){
        jpos[i] = pDyn_env->m_Hume->Full_Joint_State_[i]->m_rValue[0];
        acc[i] = pDyn_env->m_Hume->Full_Joint_State_[i]->m_rValue[2];
        tor[i] = pDyn_env->m_Hume->Full_Joint_State_[i]->m_rValue[3];
    }
    getMassMatrix(jpos, A);
    getGrav(jpos, grav);
    Vector col = tor - (A*acc + grav);
//    pretty_print(col, std::cout, "colioris", "");
    
#endif




////////////////
void saveLandingLocation(){
    Ctrl_3D_Walking* ctrl_3d = (Ctrl_3D_Walking*)pDyn_env->hume_system_->controller_;
    static double theta(0.0);
    static Vec3 offset;
    static bool set_initial(false);
    static int phase_previous(ctrl_3d->phase_);
    if(ctrl_3d->phase_ == LsRm || ctrl_3d->phase_ == RsLm){

        if(phase_previous !=ctrl_3d->phase_ ){
            set_initial = false;
        }
        Vector land_loc(2);
        theta  = pDyn_env->m_Hume->Full_Joint_State_[SIM_NUM_PASSIVE_P]->m_rValue[0];
        offset = pDyn_env->m_Hume->m_Link[ctrl_3d->stance_foot_].GetMassCenter();
        land_loc[0] = ctrl_3d->landing_location_[0]*cos(theta) - ctrl_3d->landing_location_[1]*sin(theta);
        land_loc[1] = ctrl_3d->landing_location_[0]*sin(theta) + ctrl_3d->landing_location_[1]*cos(theta);
        land_loc[0] += offset[0];
        land_loc[1] += offset[1];
        pDyn_env->landing_loc_ = land_loc;
        
        if(!set_initial){
            set_initial = true;
        }
        phase_previous = ctrl_3d->phase_;
    }

}


// HumeSystem.cpp
    // Rotation
    if(hume_system_count_ < 100){
        for (int i(0); i< 3; ++i){
            tot_configuration_[i+3] = euler_ang[2-i];
        }
    }
    else {
        for(int i(0); i<3; ++i){
            tot_configuration_[i+3] += Body_ori_vel_[i]*SERVO_RATE;
            Body_ori_[i] = tot_configuration_[i+3];
        }
    }
    // Rz = 0
    tot_configuration_[Rz] = 0.0;
    
    // Matrix J_rfoot, J_lfoot, Jinv;
    // getAnalyticJacobian(tot_configuration_, RFOOT, J_rfoot);
    // getAnalyticJacobian(tot_configuration_, LFOOT, J_lfoot);
    // pretty_print(J_rfoot, std::cout, "J for right", "");
    // pretty_print(J_lfoot, std::cout, "J for left", "");
    
    // pseudoInverse(J_rfoot.transpose(), 0.0001, Jinv);
    // RFoot_force_ = Jinv*tot_torque;
    // pretty_print(Jinv, std::cout, "Jinv for right", "");
    // pseudoInverse(J_lfoot.transpose(), 0.0001, Jinv);
    // LFoot_force_ = Jinv*tot_torque;
    // pretty_print(Jinv, std::cout, "Jinv for left", "");
    // printf("right foot contact: %i \n", right_foot_contact_);
    // printf("left foot contact: %i \n", left_foot_contact_);
    
    // getForwardKinematics(tot_configuration_, HIP, Body_pos_);
    // getForwardKinematics(tot_configuration_, RFOOT, RFoot_pos_);
    // getForwardKinematics(tot_configuration_, LFOOT, LFoot_pos_);

    // // Velocity
    // Matrix J_body, J_rfoot, J_lfoot;

    // // COM: Assume Prismatic Velocity is Zero
    // getAnalyticJacobian(tot_configuration_, HIP, J_body);
    // Body_vel_ =  J_body*tot_vel_;
    // // Right Foot
    // getAnalyticJacobian(tot_configuration_, RFOOT, J_rfoot);
    // RFoot_vel_ = J_rfoot*tot_vel_;
    // // Left Foot
    // getAnalyticJacobian(tot_configuration_, LFOOT, J_lfoot);
    // LFoot_vel_ = J_lfoot*tot_vel_;

    // Force
    Vector tot_torque(NUM_JOINT);
    for(int i(0); i<NUM_PASSIVE; ++i){
        tot_torque[i] = 0.0;
    }
    for(int i(0); i< NUM_ACT_JOINT; ++i){
        tot_torque[i + NUM_PASSIVE] = curr_torque_[i];
    }
