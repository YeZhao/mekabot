#include "controller_3D_Walking.h"

#include <math.h>
#include <stdio.h>
#include "analytic_solution/analytic_sol.h"
#include "analytic_solution/analytic_jacobian.h"
#include "analytic_solution/analytic_forward_kin.h"
#include "util/pseudo_inverse.hpp"
#include "Process.h"

#include "Time_Landing_planner.h"
#include "Gray_planner.h"
#include "task_library.h"

// #define LANDING_SUPPORT_TRANSITION_TIME 0.04
// #define SUPPORT_LIFT_TRANSITION_TIME 0.04
// #define LANDING_TIME 0.24
// #define LIFTING_TIME 0.24
// #define SUPPORTING_TIME 0.02

#define LANDING_SUPPORT_TRANSITION_TIME 0.03
#define SUPPORT_LIFT_TRANSITION_TIME 0.03
#define LANDING_TIME 0.23
#define LIFTING_TIME 0.23
#define SUPPORTING_TIME 0.025


// #define STEP_TEST

Ctrl_3D_Walking::Ctrl_3D_Walking(HUME_System* _hume_system): Virtual_Ctrl_Step(_hume_system),
                                                             body_vel_ave_(Vector::Zero(3)),
                                                             body_vel_pre_(Vector::Zero(3)),
                                                             body_vel_pre_2_(Vector::Zero(3)),
                                                             body_vel_pre_3_(Vector::Zero(3)),
                                                             vel_est_(0.0),
                                                             walking_length_(0.0),
                                                             a(Vector::Zero(2)), b(Vector::Zero(2)), c(Vector::Zero(2)), d(Vector::Zero(2))
{
    com_x_offset_ = -0.0; // 0.8* 5.786/(5.786 + 22.87)
    com_z_offset_ = -0.2;
    //
    stance_foot_ = RFOOT;
    swing_foot_ = LFOOT;
    lifting_time_ = LIFTING_TIME;
    landing_time_ = LANDING_TIME;
    supp_time_    = SUPPORTING_TIME;

    supp_lift_transition_time_ = SUPPORT_LIFT_TRANSITION_TIME;
    land_supp_transition_time_ = LANDING_SUPPORT_TRANSITION_TIME;

    lifting_height_ = 0.16;

    // for Planner
    achieving_ratio_ = 0.85;
    middle_time_x_ = 0.24;
    middle_time_y_ = 0.24;
    
    constraint_ = new Hume_Contact_Both();
    constraint_left_ = new Hume_Point_Contact(LFOOT);
    constraint_right_ = new Hume_Point_Contact(RFOOT);

    // COM
    dual_contact_task_ = new COM_Height_Tilting_Roll_Task(hume_system_);
    single_contact_lfoot_ = new COM_Height_Tilting_Roll_Foot_Task(hume_system_, LFOOT);
    single_contact_rfoot_ = new COM_Height_Tilting_Roll_Foot_Task(hume_system_, RFOOT);
    
    filter_x_vel_ = new digital_lp_filter(CUT_OFF_FRE_X, SERVO_RATE);
    // filter_x_vel_ = new butterworth_filter((int)1.0/SERVO_RATE, SERVO_RATE, CUT_OFF_FRE_X);
    // filter_x_vel_ = new moving_average_filter(10);

    // butter_filter_x_vel_.SetSampleRate(1.0/SERVO_RATE);
    // butter_filter_x_vel_.Set(20.f, 0.0);
    
    printf("[3D Walking Control] Start\n");
    
    planner_ = new Time_Landing_planner();
    planner_->start();
}

Ctrl_3D_Walking::~Ctrl_3D_Walking(){
}

void Ctrl_3D_Walking::getCurrentCommand(std::vector<double> & command){
    _PreProcessing_Command();
    ++count_command_;
    
    double curr_time = ((double)count_command_)*SERVO_RATE;
    
    if(pro_stable_->do_process(this)){
        _PostProcessing_Command(command);
        count_command_ = 0;
        return;
    }
    // static int curr_phase = LsDu;
    static int curr_phase = RsDu;
    phase_ = curr_phase;
    // Velocity Filter
    _Filter_Setting();
    
    if(_IsEndCurrState(curr_phase)){
        phase_ = curr_phase;
        ++curr_phase;
        //Stop
        if(walking_length_  + landing_loc_[0] > 3.0 && (curr_phase == RsLm || curr_phase == LsRm)){
            curr_phase = STOP;
        }
        
        if(curr_phase == LsRs || curr_phase == RsLs){
            ++num_step_;
            printf("Number of Step: %i \n", num_step_);
        }
        if(curr_phase > 9) { curr_phase = 0; }
        // Landing Location Save
        if(curr_phase == LsDu || curr_phase == RsDu){
            jspace::saveVector(hume_system_->getFoot(swing_foot_), "landing_loc");
            walking_length_ += hume_system_->getFoot(swing_foot_)[0];
            printf("******LANDING LOCATION: %f \n", hume_system_->getFoot(swing_foot_)[0]);
            printf("******WALKING LENGTH: %f \n", walking_length_);
        }
        _printf_phase(curr_phase);
    }
    _PostProcessing_Command(command);
}
bool Ctrl_3D_Walking::_Stop(){
}

bool Ctrl_3D_Walking::CalculateLandingLocation(const Vector & stance_foot_loc, double current_time, double local_switch_time, LinkID stance_foot){

    // COM
    Vector COM_Position(hume_system_->COM_pos_);
    // X offset
    COM_Position[0] += com_x_offset_;
    // COM_Position[0] = pos_est_;
    // Z offset
    COM_Position[2] += com_z_offset_;
    
    if(planner_->Calculate_SwitchTime(COM_Position,
                                      // COM
                                      hume_system_->COM_vel_,
                                      // body_vel_ave_,
                                      stance_foot_loc,
                                      stance_foot_,
                                      landing_time_,
                                      lifting_time_,
                                      achieving_ratio_,
                                      LANDING_SUPPORT_TRANSITION_TIME,
                                      SUPPORT_LIFT_TRANSITION_TIME,
                                      supp_time_,
                                      middle_time_x_,
                                      middle_time_y_)){
        //
        landing_time_ = planner_->GetSwitchingTime()
            - (1.0 - achieving_ratio_) * lifting_time_
            - land_supp_transition_time_;
        printf("landing time: %f \n", landing_time_);
        planner_->CopyNextFootLoc(landing_loc_);
#ifdef STEP_TEST
        landing_time_ = LANDING_TIME;
        landing_loc_[0] = hume_system_->COM_pos_[0];

        if(fabs(landing_loc_[0])<0.05){
            landing_loc_[0] = 0.0;
        }
#endif
        return true;
    }
    return false;
}

void Ctrl_3D_Walking::_set_land_location(){
    // Defined in CalculateLandingLocation
}
void Ctrl_3D_Walking::_set_apex_foot_location(){
    apex_foot_loc_ = foot_pre_;
    apex_foot_loc_[2] += lifting_height_;
}
bool Ctrl_3D_Walking::_Do_Supp_Lift(){
    double moving_ratio = 2.5;//0.5;
#ifdef STEP_TEST
    moving_ratio = 0.0;
#endif
    const Vector & COM_vel = hume_system_->getBody_vel();
    
    static bool Iscalculated(false);
    //COMMAND
    for (int i(0); i<2; ++i){ 
        foot_des_[i] += moving_ratio*body_vel_ave_[i]*SERVO_RATE;
        foot_vel_des_[i] = body_vel_ave_[i] * moving_ratio;
    }
    
    //Foot Height
    foot_des_[2] = _smooth_changing(foot_pre_[2], apex_foot_loc_[2], lifting_time_, state_machine_time_);
    foot_vel_des_[2] = _smooth_changing_vel(foot_pre_[2], apex_foot_loc_[2], lifting_time_, state_machine_time_);
    _set_single_contact_task();

    if( state_machine_time_ > achieving_ratio_ *lifting_time_ && !Iscalculated){
        Iscalculated = CalculateLandingLocation(hume_system_->getFoot(stance_foot_), state_machine_time_, switching_time_  + tot_transition_time_, swing_foot_);
    }

    switch(swing_foot_){
    case RFOOT:
        task_array_.push_back(single_contact_rfoot_);
        wbc_->MakeTorque(curr_conf_, task_array_, constraint_left_, gamma_);
        break;
    case LFOOT:
        task_array_.push_back(single_contact_lfoot_);
        wbc_->MakeTorque(curr_conf_, task_array_, constraint_right_, gamma_);
        break;
    }
    /////////////////////////////////////////////////////
    if(state_machine_time_ > lifting_time_){
        if(Iscalculated){
            _end_action();
            Iscalculated = false;
            _set_foot_x_traj_coeff(foot_vel_des_);
            return false;
        }
        else{
            printf("Cannot go to the next phase on time\n");
            _end_action();
            Iscalculated = false;
            exit(0);
            return false;
        }
    }
    state_machine_time_ += SERVO_RATE;        
    return true;
}


void Ctrl_3D_Walking::_set_foot_x_traj_coeff(const Vector& curr_vel){
    double tf(landing_time_);
    double xf_dot(0.0);
    double xf, x0, x0_dot;

    for (int i(0);i <2; ++i){
        xf = landing_loc_[i];
        x0 = foot_pre_[i];
        x0_dot = curr_vel[i];
        
        d[i] = x0;
        c[i] = x0_dot;

        Matrix tmp_inv(2,2);
        Matrix tmp_coeff(2,2);
        tmp_coeff(0,0) = pow(tf, 3.0);
        tmp_coeff(0,1) = pow(tf,2.0);
        //
        tmp_coeff(1,0) = 3*pow(tf, 2.0);
        tmp_coeff(1,1) = 2*tf;
        
        pseudoInverse(tmp_coeff, 0.001, tmp_inv, 0);
        
        Vector tmp_vec(2);
        Vector coeff(2);
        tmp_vec[0] = xf - (c[i]*tf + d[i]);
        tmp_vec[1] = xf_dot - c[i];
        
        coeff = tmp_inv * tmp_vec;
        
        a[i] = coeff[0];
        b[i] = coeff[1];
    }
}

bool Ctrl_3D_Walking::_Do_Supp_Land(){
    // Foot z
    foot_des_[2] = _smooth_changing(foot_pre_[2], landing_loc_[2],
                                    landing_time_,
                                    state_machine_time_);
    foot_vel_des_[2] = _smooth_changing_vel(foot_pre_[2], landing_loc_[2],
                                            landing_time_,
                                            state_machine_time_);
    // Foot x, y
    for (int i(0); i<2; ++i){
        foot_des_[i] = a[i] * pow(state_machine_time_, 3.0) +
            b[i] * pow(state_machine_time_, 2.0) +
            c[i] * state_machine_time_ +
            d[i] ;
        foot_vel_des_[i] = 3* a[i] * pow(state_machine_time_, 2.0) +
            2* b[i] * state_machine_time_ +
            c[i];
    }
    
    if(state_machine_time_>landing_time_){
        printf("Contact is not occured Yet, But Move on...\n");
        _end_action();
        return false;
    }
    _set_single_contact_task();
    
    switch(swing_foot_){
    case RFOOT:
        task_array_.push_back(single_contact_rfoot_);
        
        if(hume_system_->RFoot_Contact()){
            printf("Right foot contact occurs\n");
            _end_action();
            return false;
        }
        wbc_->MakeTorque(curr_conf_, task_array_, constraint_left_, gamma_);
        break;

    case LFOOT:
        task_array_.push_back(single_contact_lfoot_);

        if(hume_system_->LFoot_Contact()){
            printf("Left foot contact occurs\n");
            _end_action();
            return false;
        }
        wbc_->MakeTorque(curr_conf_, task_array_, constraint_right_, gamma_);
    }
    state_machine_time_ += SERVO_RATE;
    return true;
}



///////////  Filter //////////////
void Ctrl_3D_Walking::inverted_pendulum_filter(double pos, double vel, double ratio, double foot_pos, double height){
    vel_est_ = ratio * vel_est_ + (1.0 - ratio) * vel;
    _pendulum_update(pos, foot_pos, height);
    
}
void Ctrl_3D_Walking::_pendulum_update(double pos, double foot_pos, double height){
    double g(9.8);
    double acc = g/height * (pos - foot_pos);
    vel_est_ = vel_est_  + acc * SERVO_RATE;
}

void Ctrl_3D_Walking::_Filter_Setting(){
    // Filter ***
    // filter_x_vel_ -> input(hume_system_->getBody_vel()[0]);

    // COM
    filter_x_vel_ -> input(hume_system_->COM_vel_[0]);

    // Butterworth **
    // body_vel_ave_[0] = butter_filter_x_vel_.Run(hume_system_->getBody_vel()[0]);
    // For Save (Temp)

    // Model Base ******
    // 0.5 ^ ( 1 / t )
    // t (ms: SERVO RATE): low pass filter half life

    // Body 
    // inverted_pendulum_filter(hume_system_->getBody()[0] + com_x_offset_,
    //                          hume_system_->getBody_vel()[0],
    //                          0.9965,
    //                          hume_system_->getFoot(stance_foot_)[0],
    //                          hume_system_->getBody()[2] + com_z_offset_);
    // COM
    inverted_pendulum_filter(hume_system_->COM_pos_[0] + com_x_offset_,
                             hume_system_->COM_vel_[0],
                             0.9965,
                             hume_system_->getFoot(stance_foot_)[0],
                             hume_system_->COM_pos_[2] + com_z_offset_);

    if(num_step_ < 1){
        body_vel_ave_[0] = filter_x_vel_->output();
    }
    else {
        body_vel_ave_[0] = vel_est_;
    }
    hume_system_->body_vel_ave_ = body_vel_ave_[0];    
    // Just for Save...I need to fix this later
    // hume_system_->body_vel_ave_ = vel_est_;
    // hume_system_->body_pos_ave_ = pos_est_;
}
