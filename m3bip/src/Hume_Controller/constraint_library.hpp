#ifndef CONSTRAINT_LIBRARY_H
#define CONSTRAINT_LIBRARY_H

#include "Constraint.hpp"

namespace jspace {
    class Hume_Fixed_Constraint: public WBC_Constraint{
    public:
        Hume_Fixed_Constraint():WBC_Constraint(){}
        virtual ~Hume_Fixed_Constraint(){}
        virtual bool updateJcU(const vector<double> & conf, HUME_System * _model);

    };

    class Hume_No_Constraint : public WBC_Constraint{
    public:
        Hume_No_Constraint():WBC_Constraint(){}
        virtual ~Hume_No_Constraint(){}
        virtual bool updateJcU(const vector<double> & conf, HUME_System * _model);
    };
    
    class Hume_Contact_Both : public WBC_Constraint
    {
    public:
        Hume_Contact_Both();
        virtual ~Hume_Contact_Both(){}
        virtual bool updateJcU(const vector<double> & conf, HUME_System * _model);
        //////////////////////////////////////////////////////////////////////////
        void Update_Internal_Matrix(HUME_System* _hume_system);
        virtual void set_constraint_weight(const Vector & weight);
        virtual void getDesInternalForce(Vector & des_int_force);
    };
    class Hume_Contact_Both_Abduction : public WBC_Constraint
    {
    public:
        Hume_Contact_Both_Abduction();
        virtual ~Hume_Contact_Both_Abduction(){}
        virtual bool updateJcU(const vector<double> & conf, HUME_System * _model);
        virtual void set_constraint_weight(const Vector & weight);
        virtual bool getSelection(Vector & Fr, Matrix & S);
        //////////////////////////////////////////////////////////////////////////
        void Update_Internal_Matrix(HUME_System* _hume_system);
        virtual void getDesInternalForce(Vector & des_int_force);
    };


    class Hume_Contact_Both_Boom : public WBC_Constraint{
    public:
        Hume_Contact_Both_Boom();
        virtual ~Hume_Contact_Both_Boom(){}
        virtual bool updateJcU(const vector<double> &conf, HUME_System *_model);
        virtual bool getSelection(Vector & Fr, Matrix & S);
        ////////////////////////////////////////////////////////////////////////
        void Update_Internal_Matrix(HUME_System* _hume_system);
        virtual void set_constraint_weight(const Vector & weight);
        virtual void getDesInternalForce(Vector & des_int_force);

    };

    class Hume_Contact_Both_Boom_X : public WBC_Constraint{
    public:
        Hume_Contact_Both_Boom_X();
        virtual ~Hume_Contact_Both_Boom_X(){}
        virtual bool updateJcU(const vector<double> & conf, HUME_System * _model);
        virtual bool getSelection(Vector & Fr, Matrix & S);
        ////////////////////////////////////////////////////////////////////////
        void Update_Internal_Matrix(HUME_System* _hume_system);
        virtual void set_constraint_weight(const Vector & weight);
        virtual void set_internal_force_weight(const Vector & int_weight);
        virtual void getDesInternalForce(Vector & des_int_force);
    };
    
    class Hume_Point_Contact_Boom_X : public WBC_Constraint{
    public:
        Hume_Point_Contact_Boom_X(LinkID id);
        virtual ~Hume_Point_Contact_Boom_X(){}
        virtual bool updateJcU(const vector<double> & conf, HUME_System * _model);
        ////////////////////////////////////////////////////////////////////////
        void Update_Internal_Matrix(HUME_System* _hume_system);
        virtual void set_constraint_weight(const Vector & weight);
        virtual void getDesInternalForce(Vector & des_int_force);
    public:
        LinkID foot_;
    };
    
    class Hume_Contact_Both_Y_Yaw : public WBC_Constraint{
    public:
        Hume_Contact_Both_Y_Yaw():WBC_Constraint(){}
        virtual ~Hume_Contact_Both_Y_Yaw(){}
        virtual bool updateJcU(const vector<double> &conf, HUME_System *_model);
    };

    
    class Hume_Contact_XZ_Both : public WBC_Constraint
    {
    public:
        Hume_Contact_XZ_Both():WBC_Constraint(){}
        virtual ~Hume_Contact_XZ_Both(){}
        virtual bool updateJcU(const vector<double> & conf, HUME_System * _model);
    };
    class Hume_Point_Contact : public WBC_Constraint{
    public:
        Hume_Point_Contact(LinkID _foot);
        virtual ~Hume_Point_Contact(){}
        virtual bool updateJcU(const vector<double> & conf, HUME_System* _model);
    public:
        LinkID foot_;
    };

    class Hume_Point_Contact_Abduction : public WBC_Constraint{
    public:
        Hume_Point_Contact_Abduction(LinkID _foot);
        virtual ~Hume_Point_Contact_Abduction(){}
        virtual bool updateJcU(const vector<double> & conf, HUME_System* _model);
    public:
        LinkID foot_;
    };
    
    
    class Hume_Point_Contact_Boom : public WBC_Constraint {
    public:
        Hume_Point_Contact_Boom(LinkID _foot);
        virtual bool updateJcU(const vector<double> & conf, HUME_System * _model);
        
    protected:
        LinkID foot_;
    };
}

#endif
