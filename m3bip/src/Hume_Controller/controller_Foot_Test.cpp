#include "controller_Foot_Test.h"
#include <fstream>

Ctrl_Foot_Test::Ctrl_Foot_Test(HUME_System * hume_system, LinkID _target_foot):
    Controller_Hume(hume_system),
    target_foot_(_target_foot),
    Foot_des_(3),
    Foot_vel_des_(3),
    omega_(3.1415),
    amp_(0.05),
    phase_shift_(0.0),
    center_pt_(3)
{
    center_pt_(0) = 0.1;
    center_pt_(1) =  0.13;
    center_pt_(2) = -0.73;
    constraint_ = new Hume_Fixed_Constraint();
    foot_task_ = new FOOT_Task(hume_system_, target_foot_);
}

Ctrl_Foot_Test::~Ctrl_Foot_Test(){
    delete foot_task_;
}

void Ctrl_Foot_Test::getCurrentCommand(std::vector<double> & command){
    _PreProcessing_Command();
    _save_initial_pos();
    _MakeTrajectory();

    task_array_.push_back(foot_task_);
    wbc_->MakeTorque(curr_conf_, task_array_, constraint_, gamma_);

    _PostProcessing_Command(command);
}

void Ctrl_Foot_Test::_save_initial_pos(){
    static bool init(false);

    if(!init){
        hume_system_-> CopyFoot(Foot_ini_, target_foot_);

        init = true;
    }
}

void Ctrl_Foot_Test::_MakeTrajectory(){
    Foot_des_(0) = Foot_ini_[0] + amp_*sin(hume_system_->getCurrentTime()*omega_);  
    Foot_des_(1) = Foot_ini_[1] ;   
    Foot_des_(2) = Foot_ini_[2] + amp_*cos(hume_system_->getCurrentTime()*omega_);

    Foot_vel_des_(0) = amp_*omega_*cos(hume_system_->getCurrentTime()*omega_);
    Foot_vel_des_(1) = 0.0;
    Foot_vel_des_(2) = -amp_*omega_*sin(hume_system_->getCurrentTime()*omega_);

    foot_task_->SetTask(Foot_des_, Foot_vel_des_);
}

void Ctrl_Foot_Test::setCurrentConfiguration(){
    curr_conf_ = hume_system_->tot_configuration_;
}
