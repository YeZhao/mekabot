#ifndef		HUME_SYSTEM
#define		HUME_SYSTEM

#include <vector>
#include "util/wrap_eigen.hpp"
#include "Orientation_Calculator.h"
#include "DataGathering.h"
#include "GainTuner.h"

#define NUM_ACT_JOINT 6
#define NUM_JOINT 12
#define NUM_PASSIVE_P 3
#define NUM_PASSIVE 6
#define SERVO_RATE 0.001
#define TORQUE_LIMIT 1000.0
#define TORQUE_LIMIT_AIR 1000.0
#define TORQUE_LIMIT_KNEE 1000.0
#define SPEED_LIMIT 10.0

#define M_ABDUCTION 2.1656 
#define M_THIGH     2.4194     
#define M_BODY      14.11 //10.61+3.5       
#define M_CALF      0.20

using namespace jspace;

#define CUT_OFF_FRE 500.0
//#define THIS_COM "/home/dhkim/new_control_pc_mekabot/m3bip/"
//#define THIS_COM "/home/meka/mekabot/m3bip/"
#define THIS_COM "/Users/anqiwu/wedgejumping/mekabot/m3bip/"

//#define SIMULATION

// #define BOOM_SYSTEM
#define SIM_BOOM_SYSTEM
// #define SIM_BOOM_X_SYSTEM
#define SIM_ROBUST_BOOMSYSTEM
/// Chose Test // Require Re-compile
#define Body_Test
// #define Stepping_Test
// #define WALKING_2D
// #define WALKING_3D
//#define Foot_Test
class Controller_Hume;

enum LinkID{ 
    HIP = 0,
    RAbduct = 1,
    RThigh = 2,
    RCalf = 3,
    LAbduct = 4,
    LThigh = 5,
    LCalf = 6,
    LFOOT = 7,
    RFOOT = 8
};

enum Passive{
    Px = 0,
    Py = 1,
    Pz = 2,
    //////////
    Rz = 3,
    Ry = 4,
    Rx = 5
};

enum Active{
    // Right
    RAbduction = 0,
    RHip = 1,
    RKnee = 2,
    // Left
    LAbduction = 3,
    LHip = 4,
    LKnee = 5
};

class HUME_System
{
public:
    HUME_System();
    ~HUME_System();
    
    void GetFullJacobian(const std::vector<double> & conf, LinkID link_id, jspace::Matrix & J);
    void getPosition(const std::vector<double> & q, LinkID link_id, jspace::Vector & pos);
    bool getInverseMassInertia(jspace::Matrix & ainv);
    bool getGravity(jspace::Vector & grav);

    void UpdateInverseMassInertia(const std::vector<double> & tot_conf);
    void UpdateGravity(const std::vector<double> & tot_conf);
    
    void getTorqueInput( const std::vector<double> & jpos,
                         const std::vector<double> & jvel,
                         const std::vector<double> & torque,
                         const std::vector<double> & euler_ang,
                         const std::vector<double> & ang_vel,
                         bool _left_foot_contact,
                         bool _right_foot_contact,
                         std::vector<int32_t> & torque_kp,
                         std::vector<int32_t> & torque_ki,
                         std::vector<double> & command,
                         bool & task_start_);

    void setCurrentStatus(const std::vector<double> & jpos,
                          const std::vector<double> & jvel,
                          const std::vector<double> & torque,
                           const std::vector<double> & euler_ang,
                           const std::vector<double> & ang_vel,
                           bool _left_foot_contact,
                           bool _right_foot_contact);
    void UpdateStatus();

    void setCommand(std::vector<double> & command);
    void Safty_Stop( const std::vector<double> & jvel, std::vector<double> & command);

    const Vector & getBody(){ return Body_pos_; }
    const Vector & getBody_vel() { return Body_vel_; }
    const Vector & getOri() { return Body_ori_; }
    const Vector & getOri_vel() { return Body_ori_vel_; }

    void CopyBody( Vector & _body) { _body = Body_pos_; }
    void CopyOri( Vector & _ori){ _ori = Body_ori_; }
    
    bool RFoot_Contact() { return right_foot_contact_; }
    bool LFoot_Contact() { return left_foot_contact_; }

    int getCount(){return hume_system_count_; }
    double getCurrentTime() { return SERVO_RATE * ((double)hume_system_count_); }

    // For Save
    std::vector<double> tot_configuration_;
    std::vector<double> curr_torque_;
    jspace::Vector tot_vel_;
    
////////////////////////////////////////////////////////    
///               FOOT
////////////////////////////////////////////////////////
    const Vector & getFoot(LinkID _link_id) {
        switch (_link_id){
        case RFOOT:
            return RFoot_pos_;
        case LFOOT:
            return LFoot_pos_;
        }
    }
    const Vector & getFoot_vel(LinkID _link_id) {
        switch (_link_id){
        case RFOOT:  
            return RFoot_vel_; 
        case LFOOT:
            return LFoot_vel_;
        }
    }
    double getFootValue(){ return LFoot_pos_[1];}
    void CopyFoot( Vector & _foot, LinkID _link_id){ 
        switch (_link_id){
        case RFOOT:
            _foot = RFoot_pos_;
            break;
        case LFOOT:
            _foot = LFoot_pos_;
            break;
        }
    }
    void CopyFootVel( Vector & _foot_vel, LinkID _link_id){
        switch (_link_id){
        case RFOOT:
            _foot_vel = RFoot_vel_;
            break;
        case LFOOT:
            _foot_vel = LFoot_vel_;
            break;
        }
    }
    bool IsUpdated() { return Is_StateUpdated_; }
    void getCOMJacobian(const vector<double> & conf, jspace::Matrix & J);
    //For save
    std::vector<double> torque_input_;
    Controller_Hume*  controller_;

    Vector curr_LFoot_force_;
    Vector curr_RFoot_force_;
    Vector ori_int_force_;
    Vector des_int_force_;
    Vector COM_pos_;
    Vector COM_vel_;
    bool task_start_;
    bool change_imu_mode_;
    int preparing_count_;

    //0: Mocap Ori, IMU vel,
    //1: sensor low pass filter fusion,
    //2: sensor fusion includig time_delay
    //3: IMU vel accumulation
    int ori_mode_;

    std::vector<int32_t> torque_gain_kp_;
    std::vector<int32_t> torque_gain_ki_;
    Hume_Thread* data_gathering_;
    Hume_Thread* act_gain_tuner_;
    // Temp Filtered Velocity;
    double body_vel_ave_;
    double body_pos_ave_;
    
protected:

    Matrix Ainv_;
    Vector grav_;
    
    Orientation_Calculator ori_cal_;
    std::vector<double> initial_ang_;
    std::vector<double> offset_ori_;
    bool Is_StateUpdated_;
    bool right_foot_contact_;
    bool left_foot_contact_;
    int hume_system_count_;

    Vector Body_pos_;
    Vector Body_vel_;
    Vector Body_ori_;
    Vector Body_ori_vel_;

    Vector LFoot_pos_;
    Vector RFoot_pos_;
    // At first time, foot velocities are calculated with the assumption
    // Virtual prismatic joint velocities are zero
    Vector LFoot_vel_;
    Vector RFoot_vel_;

    void setGain(std::vector<int32_t> & kp, std::vector<int32_t> &ki);
    void _set_orientation(const std::vector<double> & euler_ang, const std::vector<double> & ang_vel);
};

#endif
