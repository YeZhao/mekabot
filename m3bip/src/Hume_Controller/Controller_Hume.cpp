#include "Controller_Hume.h"
#include <math.h>
#include "util/pseudo_inverse.hpp"
#include "analytic_solution/analytic_sol.h"
#include "analytic_solution/analytic_jacobian.h"
#include "analytic_solution/analytic_forward_kin.h"

Controller_Hume::Controller_Hume(HUME_System* _hume_system)
    : hume_system_(_hume_system),
      constraint_(NULL), motion_start_(false), phase_(10),
      pos_ini_(3), ori_ini_(3), left_foot_ini_(3), right_foot_ini_(3),
      b_int_gain_right_(true), b_int_gain_left_(true),
      b_force_int_gain_left_knee_(false), b_force_int_gain_right_knee_(false)
{
    wbc_ = new Controller_WBC(_hume_system,1 );
    wbc_2_ = new Controller_WBC(_hume_system, 2);
    U_ = Matrix::Zero(NUM_ACT_JOINT, NUM_JOINT);
    U_.block(0, NUM_PASSIVE, NUM_ACT_JOINT, NUM_ACT_JOINT) = Matrix::Identity(NUM_ACT_JOINT, NUM_ACT_JOINT);

}

Controller_Hume::~Controller_Hume(){
    if(constraint_ !=NULL){
        delete constraint_;
    }
}

void Controller_Hume::_PreProcessing_Command(){
    b_int_gain_right_ = true;
    b_int_gain_left_ = true;
    b_force_int_gain_left_knee_ = false;
    b_force_int_gain_right_knee_ = false;
    task_array_.clear();
    Cal_curr_FootForce(LFOOT, hume_system_->curr_LFoot_force_);
    Cal_curr_FootForce(RFOOT, hume_system_->curr_RFoot_force_);
//    setCurrentConfiguration();
}

void Controller_Hume::_PostProcessing_Command(std::vector<double> & command){
    Cal_FootForce(RFOOT, gamma_,  RFoot_force_ );
    Cal_FootForce(LFOOT, gamma_,  LFoot_force_ );
    
    command.resize(NUM_ACT_JOINT);
    for (int i(0); i<NUM_ACT_JOINT; ++i){
        command[i] = gamma_(i);
    }
}

void Controller_Hume::Cal_FootForce(LinkID _link_id, const Vector& torque, Vector & _force){
    Matrix Jsbar, ainv, Lambda, Jtmp;
    Vector grav;
    Matrix Js(Matrix::Zero(6, NUM_JOINT));
    
    //Right Leg
    getAnalyticJacobian(hume_system_->tot_configuration_, RFOOT, Jtmp);
    Js.block(0,0,3, NUM_JOINT) = Jtmp;
    
    //Left Leg
    getAnalyticJacobian(hume_system_->tot_configuration_, LFOOT, Jtmp);
    Js.block(3,0,3, NUM_JOINT) = Jtmp;

    hume_system_->getGravity(grav);
    hume_system_->getInverseMassInertia(ainv);
    
    pseudoInverse(Js * ainv * Js.transpose(), 0.001, Lambda, 0);
    Jsbar = ainv * Js.transpose() * Lambda;

    Vector force = Jsbar.transpose() * U_.transpose() * torque - Jsbar.transpose() * grav;

    int idx_offset(0);
    if(_link_id == LFOOT){
        idx_offset = 3;
    }
    _force = Vector::Zero(3);
    for (int i(0); i< 3; ++i){
        _force[i] = force[idx_offset + i];
    }
}

void Controller_Hume::Cal_curr_FootForce(LinkID _link_id, Vector & _force){
    Vector torque (Vector::Zero(NUM_ACT_JOINT));
    for (int i(0); i< NUM_ACT_JOINT; ++i){
        torque[i] = hume_system_->curr_torque_[i];
    }
    Cal_FootForce(_link_id, torque, _force);
}

double Controller_Hume::_smooth_changing(double ini, double end, double moving_duration, double curr_time){
    double ret;
    ret = ini + (end - ini)*0.5*(1-cos(curr_time/moving_duration * M_PI));
    if(curr_time>moving_duration){
        ret = end;
    }
    return ret;
}

double Controller_Hume::_smooth_changing_vel(double ini, double end, double moving_duration, double curr_time){
    double ret;
    ret = (end - ini)*0.5*(M_PI/moving_duration)*sin(curr_time/moving_duration*M_PI);
    if(curr_time>moving_duration){
        ret = 0.0;
    }
    return ret;
}

void Controller_Hume::_SetLocalConfiguration2StanceFoot(LinkID _foot){
    curr_conf_ = hume_system_->tot_configuration_;
    jspace::Vector curr_vel (hume_system_->tot_vel_ );
    
    Vector Foot_pos;
    Vector Foot_vel;
    Matrix J_foot;

    if(_foot == RFOOT){
        getForwardKinematics(curr_conf_, RFOOT, Foot_pos);
        getAnalyticJacobian(curr_conf_, RFOOT, J_foot);
        Foot_vel = J_foot*curr_vel;
    }
    else if(_foot == LFOOT){
        getForwardKinematics(curr_conf_, LFOOT, Foot_pos);
        getAnalyticJacobian(curr_conf_, LFOOT, J_foot);
        Foot_vel = J_foot*curr_vel;
    }

    for (int i(0); i< 3; ++i){
        curr_conf_[i] = -Foot_pos[i];
    }

    for (int i(0); i<3; ++i){
        curr_vel[i] = -Foot_vel[i];
    }
    _COM_Calculation(curr_conf_, curr_vel);
    hume_system_->tot_configuration_ = curr_conf_;
    hume_system_->tot_vel_ = curr_vel;
    hume_system_->UpdateStatus();
}

void Controller_Hume::_COM_Calculation(const vector<double> & curr_conf, const Vector & curr_vel){
    // COM_Pose
    jspace::Vector body      ;
    jspace::Vector Rabduction;
    jspace::Vector Labduction;
    jspace::Vector Rthigh    ;
    jspace::Vector Lthigh    ;
    jspace::Vector Rcalf     ;
    jspace::Vector Lcalf     ;
    double tot_mass = M_BODY + 2*M_ABDUCTION + 2*M_THIGH + 2*M_CALF;
    getForwardKinematics(curr_conf, HIP, body);
    
    getForwardKinematics(curr_conf, RAbduct, Rabduction);
    getForwardKinematics(curr_conf, LAbduct, Labduction);

    getForwardKinematics(curr_conf, RThigh, Rthigh);
    getForwardKinematics(curr_conf, LThigh, Lthigh);

    getForwardKinematics(curr_conf, RCalf, Rcalf);
    getForwardKinematics(curr_conf, LCalf, Lcalf);

    hume_system_->COM_pos_ = (M_BODY * body + M_ABDUCTION * Rabduction + M_ABDUCTION * Labduction + M_THIGH * Rthigh + M_THIGH * Lthigh + M_CALF * Rcalf + M_CALF * Lcalf)/tot_mass;

    // COM Velocity
    Matrix J_com;
    hume_system_->getCOMJacobian(curr_conf, J_com);
    hume_system_->COM_vel_ = J_com * curr_vel;
}

