#include "HumeSystem.h"
#include <iostream>
#include <stdio.h>

#include "util/pseudo_inverse.hpp"
#include "analytic_solution/analytic_sol.h"
#include "analytic_solution/analytic_jacobian.h"
#include "analytic_solution/analytic_forward_kin.h"
#include "analytic_solution/analytic_grav.h"
#include "Controller_Hume.h"
// BOOM System Stepping Test
#include "controller_XZ_Step_Test.h"
#include "controller_XZ_Walking.h"
#include "controller_3D_Walking.h"
#include "controller_Foot_Test.h"
#include "controller_Body_Test.h"

using namespace jspace;

HUME_System::HUME_System():tot_vel_(NUM_JOINT),
                           Body_pos_(Vector::Zero(3)),
                           Body_vel_(Vector::Zero(3)),
                           Body_ori_(Vector::Zero(3)),
                           Body_ori_vel_(Vector::Zero(3)),
                           LFoot_pos_(Vector::Zero(3)),
                           LFoot_vel_(Vector::Zero(3)),
                           RFoot_pos_(Vector::Zero(3)),
                           RFoot_vel_(Vector::Zero(3)),
                           curr_RFoot_force_(Vector::Zero(3)),
                           curr_LFoot_force_(Vector::Zero(3)),
                           right_foot_contact_(false),   
                           left_foot_contact_(false),
                           Is_StateUpdated_(false),
                           preparing_count_(6000),
                           //Ori Mode 2: Gray,  4: IMU orientation Mtx
                           task_start_(false), ori_mode_(2), hume_system_count_(0),
                           change_imu_mode_(false)
{

    printf("[Hume System] constructor\n");
    hume_system_count_ = 0;

// Predefine Torque Kp, Ki
    torque_gain_kp_.resize(NUM_ACT_JOINT);
    torque_gain_ki_.resize(NUM_ACT_JOINT);
    
    for(int i(0); i< NUM_ACT_JOINT; ++i){
        torque_gain_kp_[i] = 45;
        torque_gain_ki_[i] = 0;
    }
    //Abduction
    torque_gain_kp_[0] = 5;
    torque_gain_ki_[0] = 0;

    torque_gain_kp_[3] = 5;
    torque_gain_ki_[3] = 0;
    // Hip
    torque_gain_kp_[1] = 40;
    torque_gain_kp_[4] = 40;
    // Knee
    // torque_gain_kp_[2] = 100;
    // torque_gain_kp_[5] = 100;

    
#ifdef Foot_Test
   controller_ = new Ctrl_Foot_Test(this, RFOOT);
#endif

#ifdef Body_Test 
    controller_ = new Ctrl_Body_Test(this);
#endif

#ifdef WALKING_2D
    controller_ = new Ctrl_XZ_Walking(this);
#endif

#ifdef WALKING_3D
    controller_ = new Ctrl_3D_Walking(this);
#endif
    
#ifdef Stepping_Test
    controller_ = new Ctrl_XZ_Step_Test(this);
#endif
    
    tot_configuration_.resize(NUM_JOINT);
    curr_torque_.resize(NUM_ACT_JOINT);
    torque_input_.resize(NUM_ACT_JOINT);
    
    initial_ang_.resize(3);
    offset_ori_.resize(3);
    
    ////  Hume Thread     //////////////////////////////////
    data_gathering_ = new Data_Gathering(this);
    data_gathering_->start();

    // Actuator ///////////////////////////////////////////
    act_gain_tuner_ = new Act_gain_tuner(this);
    act_gain_tuner_->start();
}

HUME_System::~HUME_System(){
    printf("destructor\n");
    delete controller_;
    delete data_gathering_;
}

void HUME_System::getTorqueInput(const std::vector<double> & jpos,
                                 const std::vector<double> & jvel,
                                 const std::vector<double> & torque,
                                 const std::vector<double> & euler_ang,
                                 const std::vector<double> & ang_vel,
                                 bool _left_foot_contact,
                                 bool _right_foot_contact,
                                 std::vector<int32_t> & torque_gain_kp,
                                 std::vector<int32_t> & torque_gain_ki,
                                 std::vector<double> & command,
                                 bool & imu_mode){
    // Require
    imu_mode = change_imu_mode_;

    setCurrentStatus(jpos, jvel, torque, euler_ang, ang_vel,
                     _left_foot_contact, _right_foot_contact);
     
    controller_->getCurrentCommand(command); 
    torque_input_ = command;
    setCommand(command);
    Safty_Stop(jvel, command);
    setGain(torque_gain_kp, torque_gain_ki);
    ++hume_system_count_;

    if(hume_system_count_> preparing_count_){
        task_start_ = true;
    }
}

void HUME_System::Safty_Stop(const std::vector<double> & jvel, std::vector<double> & command){
    for (int i(0); i<NUM_ACT_JOINT; ++i){
        if(jvel[i]> SPEED_LIMIT || jvel[i]< -SPEED_LIMIT){      \
            printf("%ith joint hit the velocity limit: %f \n", i, jvel[i]);
            command[i] = 0.0;
        }
    }
}

void HUME_System::setGain(std::vector<int32_t> & kp, std::vector<int32_t> & ki){
    kp = torque_gain_kp_;
    ki = torque_gain_ki_;

    kp[2] = -kp[2];
    ki[2] = -ki[2];

    kp[4] = -kp[4];
    ki[4] = -ki[4];
}

void HUME_System::setCommand(std::vector<double> & command){
    // Knee Joint Positive Direction Torque Limit
    if(command[RKnee] > TORQUE_LIMIT_KNEE){
        command[RKnee] = TORQUE_LIMIT_KNEE;
    }
    if(command[LKnee] > TORQUE_LIMIT_KNEE){
        command[LKnee] = TORQUE_LIMIT_KNEE;
    }
        
    for (int i(0); i<NUM_ACT_JOINT; ++i){
        command[i] = - command[i];

        if(command[i] > TORQUE_LIMIT){
            command[i] = TORQUE_LIMIT;
        }
        if(command[i] < -TORQUE_LIMIT){ 
            command[i] = -TORQUE_LIMIT;
        }
    }

    
    for(int i(0); i< NUM_ACT_JOINT; ++i){
        torque_gain_kp_[i] = 80;
        torque_gain_ki_[i] = 0;
    }
    torque_gain_kp_[1] = 80;
    torque_gain_kp_[4] = 80;
    torque_gain_kp_[2] = 80;
    torque_gain_kp_[5] = 80;

#ifdef WALKING_2D
    // Desired Joint Position
    torque_gain_kp_[0] = 5;
    torque_gain_ki_[0] = 0;

    torque_gain_kp_[3] = 5;
    torque_gain_ki_[3] = 0;
    
    command[0] = 0.0; 
    command[3] = 0.0;
#endif
    
    // //Hip Joint
    // torque_gain_kp_[1] = 130;
    // torque_gain_kp_[4] = 130;

    // torque_gain_ki_[1] = 20;
    // torque_gain_ki_[4] = 20;

    // //Knee Joint
    // torque_gain_kp_[2] = 130;
    // torque_gain_kp_[5] = 130;
    
    // torque_gain_ki_[2] = 17;
    // torque_gain_ki_[5] = 17;
    
    // Hip Right
    if(controller_->Is_Integral_Gain_Right()
       &&(fabs(command[1]) < 5.5 || !right_foot_contact_)){
        torque_gain_kp_[1] = 200;
        torque_gain_ki_[1] = 22;
    }
    // Hip Left
    if(controller_->Is_Integral_Gain_Left()
       && (fabs(command[4]) < 5.5 || !left_foot_contact_)){
        torque_gain_kp_[4] = 180;
        torque_gain_ki_[4] = 15;
    }

    // Knee Right
    if(controller_->Force_Integral_Gain_Right_Knee() ||
        fabs(command[2]) < 15.5 || !right_foot_contact_){
        torque_gain_kp_[2] = 260;
        torque_gain_ki_[2] = 35;
    }
    // Knee Left
    if(controller_->Force_Integral_Gain_Left_Knee() ||
        fabs(command[5]) < 15.5 || !left_foot_contact_){
        torque_gain_kp_[5] = 255;
        torque_gain_ki_[5] = 30;
    }
    
    if(!right_foot_contact_){
        for (int i(0); i< 3; ++i){
            if(command[i] > TORQUE_LIMIT_AIR){
                command[i] = TORQUE_LIMIT_AIR; 
            }
            if(command[i] < -TORQUE_LIMIT_AIR){ 
                command[i] = -TORQUE_LIMIT_AIR;
            }
        }
    }
    if(!left_foot_contact_){
        for (int i(3); i< 6; ++i){
            if(command[i] > TORQUE_LIMIT_AIR){
                command[i] = TORQUE_LIMIT_AIR;
            }
            if(command[i] < -TORQUE_LIMIT_AIR){
                command[i] = -TORQUE_LIMIT_AIR;
            }
        }
    }
}
void HUME_System::GetFullJacobian(const std::vector<double> & conf, LinkID link_id, Matrix & J){
    getAnalyticJacobian(conf, link_id, J);
}

bool HUME_System::getInverseMassInertia( Matrix & ainv){
    ainv = Ainv_;
    return true;
}

void HUME_System::UpdateInverseMassInertia(const std::vector<double> & q){
    Matrix a(NUM_JOINT, NUM_JOINT);
    getMassMatrix(q, a);
    pseudoInverse(a, 0.001, Ainv_, 0);
}

bool HUME_System::getGravity(Vector & grav){
    grav = grav_;
    return true;
}
void HUME_System::UpdateGravity(const std::vector<double> & q){
    getGrav(q, grav_);
}

void HUME_System::_set_orientation(const std::vector<double> & euler_ang,
                                   const std::vector<double> & ang_vel){
    // IMU: Roll, Pitch, Yaw
    // System: Yaw, Pitch, Roll
    switch (ori_mode_){
    case 0:
        change_imu_mode_ = ori_cal_.mocap_ori_imu_vel(hume_system_count_, preparing_count_,
                                                 euler_ang, ang_vel,
                                                 Body_ori_, Body_ori_vel_);
        break;

    case 1:
        change_imu_mode_ = ori_cal_.mocap_imu_vel_fusion(hume_system_count_, preparing_count_,
                                                    SERVO_RATE,
                                                    euler_ang, ang_vel,
                                                    Body_ori_, Body_ori_vel_);
        break;

    case 2: // Gray's
        change_imu_mode_ = ori_cal_.mocap_default_pose_estimator(hume_system_count_, preparing_count_,
                                                            euler_ang, ang_vel,
                                                            Body_ori_, Body_ori_vel_);
        break;

    case 3:
        change_imu_mode_ = ori_cal_.imu_vel_int(hume_system_count_, preparing_count_, SERVO_RATE,
                                           euler_ang, ang_vel,
                                           Body_ori_, Body_ori_vel_);
        break;

    case 4:
        change_imu_mode_ = ori_cal_.imu_ori_filter_vel(hume_system_count_,
                                                  preparing_count_,
                                                  euler_ang, ang_vel,
                                                  Body_ori_, Body_ori_vel_);
    }
}

void HUME_System::setCurrentStatus(const std::vector<double> & jpos,
                                   const std::vector<double> & jvel,
                                   const std::vector<double> & torque,
                                   const std::vector<double> & euler_ang,
                                   const std::vector<double> & ang_vel,
                                   bool _left_foot_contact,
                                   bool _right_foot_contact){
    _set_orientation(euler_ang, ang_vel); 
    // P x, y, z
    for (int i(0); i< NUM_PASSIVE_P; ++i){
        tot_configuration_[i] = 0.0;
        tot_vel_[i] = 0.0;
    }
    // R z, y, x
    for (int i(0); i<3 ;++i){
        tot_configuration_[i + NUM_PASSIVE_P] = Body_ori_[i];
        tot_vel_[i + NUM_PASSIVE_P] = Body_ori_vel_[i];
    }
    // Joint 
    for(int i(0); i< NUM_ACT_JOINT; ++i){
        tot_configuration_[NUM_PASSIVE + i] = -jpos[i];
        tot_vel_[i + NUM_PASSIVE]  = -jvel[i];
        curr_torque_[i] = -torque[i];
    }
    
    

    //Contact
    right_foot_contact_ = _right_foot_contact;
    left_foot_contact_ = _left_foot_contact;
    
    controller_->setCurrentConfiguration();

    Is_StateUpdated_ = true;
}

void HUME_System::UpdateStatus(){
    getForwardKinematics(tot_configuration_, HIP, Body_pos_);
    getForwardKinematics(tot_configuration_, RFOOT, RFoot_pos_);
    getForwardKinematics(tot_configuration_, LFOOT, LFoot_pos_);

    Matrix J_body, J_rfoot, J_lfoot;
    // COM: Assume Prismatic Velocity is Zero
    getAnalyticJacobian(tot_configuration_, HIP, J_body);
    Body_vel_ =  J_body*tot_vel_;
    // Right Foot
    getAnalyticJacobian(tot_configuration_, RFOOT, J_rfoot);
    RFoot_vel_ = J_rfoot*tot_vel_;
    // Left Foot
    getAnalyticJacobian(tot_configuration_, LFOOT, J_lfoot);
    LFoot_vel_ = J_lfoot*tot_vel_;

    UpdateGravity(tot_configuration_);
    UpdateInverseMassInertia(tot_configuration_);
}

void HUME_System::getCOMJacobian(const vector<double> & conf, Matrix & J){
    Matrix J_body, J_rabduction, J_labduction, J_rthigh, J_lthigh, J_rcalf, J_lcalf;
    double tot_mass = M_BODY + 2*M_ABDUCTION + 2*M_THIGH + 2*M_CALF;
    
    getAnalyticJacobian(conf, HIP, J_body);

    getAnalyticJacobian(conf, RAbduct, J_rabduction);
    getAnalyticJacobian(conf, LAbduct, J_labduction);

    getAnalyticJacobian(conf, RThigh, J_rthigh);
    getAnalyticJacobian(conf, LThigh, J_lthigh);
    
    getAnalyticJacobian(conf, RCalf, J_rcalf);
    getAnalyticJacobian(conf, LCalf, J_lcalf);

    J = (M_BODY * J_body +
        M_ABDUCTION * J_rabduction +
        M_ABDUCTION * J_labduction +
        M_THIGH * J_rthigh +
        M_THIGH * J_lthigh +
        M_CALF * J_rcalf +
         M_CALF * J_lcalf)/tot_mass;
}
