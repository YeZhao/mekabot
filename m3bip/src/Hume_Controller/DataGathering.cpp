#include "DataGathering.h"
#include "Hume_Controller/HumeSystem.h"
#include "Hume_Controller/Controller_Hume.h"

#include "util/wrap_eigen.hpp"
#include <stdio.h>

#include "util/comm_udp.h"
#include "Hume_Supervisor/hume_protocol.h"

Data_Gathering::Data_Gathering(HUME_System* hume_system) : Hume_Thread(),
                                                           Data_Gathering_Holding_(10),
                                                           socket_(0),
                                                           socket_actuator_(0),
                                                           socket_display_(0)
{
    hume_system_ = hume_system;
}
void Data_Gathering::_LoadingHumeSystemData(){
    HumeProtocol::hume_system_data hume_data;

    hume_data.count = hume_system_->getCount();
    hume_data.curr_time = hume_system_->getCurrentTime();
    
    for (int i(0); i< 3; ++i){
        hume_data.ori[i] = hume_system_->getOri()      [i];
        hume_data.ori_vel[i] = hume_system_->getOri_vel() [i];
    }

    for(int i(0); i< NUM_JOINT; ++i){
        hume_data.tot_conf[i] = hume_system_->tot_configuration_[i];
        hume_data.tot_vel[i] = hume_system_->tot_vel_[i];
    }

    for (int i(0); i< NUM_ACT_JOINT; ++i){
        hume_data.torque_input[i] = hume_system_->torque_input_[i];
        hume_data.curr_torque[i] = hume_system_->curr_torque_[i];
    }
    COMM::send_data(socket_, PORT_HUME_SYSTEM, &hume_data, sizeof(HumeProtocol::hume_system_data), IP_ADDR_OLD_PC);
    COMM::send_data(socket_display_, PORT_HUME_SYSTEM_GRAPH, & hume_data, sizeof(HumeProtocol::hume_system_data), IP_ADDR_OLD_PC);
}

void Data_Gathering::_LoadingTaskData(){
    std::vector<Task*>::iterator it = (hume_system_->controller_->task_array_).begin();
    std::vector<Task*>::iterator endit = (hume_system_->controller_->task_array_).end();

    for(; it < endit; ++it){
        (*it)->send();
    }
}

void Data_Gathering::_LoadingActuatorData(){
    HumeProtocol::pi_joint_gain gain_data;

    for (int i(0); i< NUM_ACT_JOINT; ++i){
        gain_data.kp[i] = hume_system_->torque_gain_kp_[i];
        gain_data.ki[i] = hume_system_->torque_gain_ki_[i];
    }
    
    COMM::send_data(socket_actuator_, PORT_JOINT_ACT_GAIN_FROM_SYSTEM, & gain_data, sizeof(HumeProtocol::pi_joint_gain), IP_ADDR_MYSELF);
}

void Data_Gathering::run()
{
    printf("[Data Gathering] Start\n");
    while (true)
    {
        _LoadingHumeSystemData();
        
        if(hume_system_->task_start_){
            _LoadingTaskData();
            _LoadingActuatorData();
        
            jspace::saveVector(hume_system_->tot_configuration_, "curr_conf");
            jspace::saveVector(hume_system_->tot_vel_, "curr_vel");
            jspace::saveVector(hume_system_->torque_input_, "gamma");
            jspace::saveVector(hume_system_->curr_torque_, "torque");
            jspace::saveValue(hume_system_->getCurrentTime(), "time");

            jspace::saveVector(hume_system_->controller_->RFoot_force_, "Right_foot_force");
            jspace::saveVector(hume_system_->controller_->LFoot_force_, "Left_foot_force");
            jspace::saveVector(hume_system_->curr_RFoot_force_, "Curr_Right_foot_force");
            jspace::saveVector(hume_system_->curr_LFoot_force_, "Curr_Left_foot_force");
            jspace::saveVector(hume_system_->ori_int_force_, "Ori_internal_force");
            jspace::saveVector(hume_system_->des_int_force_, "Des_internal_force");
            jspace::saveValue(hume_system_->controller_->phase_, "phase");
            jspace::saveValue(hume_system_->RFoot_Contact(), "rfoot_contact");
            jspace::saveValue(hume_system_->LFoot_Contact(), "lfoot_contact");
            jspace::saveValue(hume_system_->body_vel_ave_, "filter_body_vel_x");
            jspace::saveValue(hume_system_->body_pos_ave_, "filter_body_pos_x");
            hume_system_->controller_->wbc_->SaveMatrix();
        }
        usleep(Data_Gathering_Holding_);
    }
}

            // hume_system_->controller_->wbc_->SaveMatrix();
            // hume_system_->controller_->wbc_2_->SaveMatrix();
            // jspace::saveVector(hume_system_->getBody(), "Body_pos");
            // jspace::saveVector(hume_system_->getBody_vel(), "Body_vel");         
            
            // jspace::saveVector(hume_system_->getFoot(LFOOT), "Left_foot");
            // jspace::saveVector(hume_system_->getFoot(RFOOT), "Right_foot");
            // jspace::saveVector(hume_system_->getFoot_vel(LFOOT), "Left_foot_vel");             
            // jspace::saveVector(hume_system_->getFoot_vel(RFOOT), "Right_foot_vel");
            // jspace::saveVector(hume_system_->RFoot_force_, "Right_foot_force");
            // jspace::saveVector(hume_system_->LFoot_force_, "Left_foot_force");

//         pthread_testcancel();
// #ifdef PRINT_DATA
//         printf("count %i: ", hume_system_->getCount());
//         jspace::pretty_print(hume_system_->torque_input_, "command");
//         jspace::pretty_print(hume_system_->tot_configuration_, "Full Configuration");
//         jspace::pretty_print(hume_system_->tot_vel_, std::cout, "Full Velocity", "");
//         jspace::pretty_print(hume_system_->getBody(), std::cout, "Body position", "");
//         jspace::pretty_print(hume_system_->getOri(), std::cout, "Body orientation", "");
//         jspace::pretty_print(hume_system_->getFoot(RFOOT), std::cout, "Right Foot", "");
//         jspace::pretty_print(hume_system_->getFoot(LFOOT), std::cout, "Left Foot", "");
//         printf("Right Foot Contact: %i\n", hume_system_->RFoot_Contact());
//         printf("Left Foot Contact: %i\n", hume_system_->LFoot_Contact());
// #ifdef Body_Test
//         jspace::pretty_print(((Ctrl_Body_Test*)hume_system_->controller_)->pos_des_, std::cout, "Body_des", "");
//         jspace::pretty_print(((Ctrl_Body_Test*)hume_system_->controller_)->ori_des_, std::cout, "Ori_des", "");
//         jspace::pretty_print(((Ctrl_Body_Test*)hume_system_->controller_)->ori_vel_des_, std::cout, "Ori_vel_des", "");
// #endif


        
// #ifdef WALKING_3D
//         jspace::pretty_print(((Ctrl_3D_Walking*)hume_system_->controller_)->COM_d_, std::cout, "Body_des", "");
//         jspace::pretty_print(((Ctrl_3D_Walking*)hume_system_->controller_)->Foot_d_, std::cout, "Foot_des", "");
//         jspace::pretty_print(((Ctrl_3D_Walking*)hume_system_->controller_)->COM_ori_d_, std::cout, "Ori_des", "");
// #endif
//          printf("\n");
// #endif
//          /////////////////////// Save File

//          if(hume_system_->IsUpdated()){
// //         if(true){
//              jspace::saveVector(hume_system_->getBody(), "Body_pos");
//              jspace::saveVector(hume_system_->getBody_vel(), "Body_vel");         

//              jspace::saveVector(hume_system_->getFoot(LFOOT), "Left_foot");
//              jspace::saveVector(hume_system_->getFoot(RFOOT), "Right_foot");
//              jspace::saveVector(hume_system_->getFoot_vel(LFOOT), "Left_foot_vel");             
//              jspace::saveVector(hume_system_->getFoot_vel(RFOOT), "Right_foot_vel");
//              jspace::saveVector(hume_system_->RFoot_force_, "Right_foot_force");
//              jspace::saveVector(hume_system_->LFoot_force_, "Left_foot_force");

//              jspace::saveVector(hume_system_->getOri(), "Body_ori");
//              jspace::saveVector(hume_system_->getOri_vel(), "Body_ori_vel");
//              jspace::saveVector(hume_system_->tot_configuration_, "curr_conf");
//              jspace::saveVector(hume_system_->tot_vel_, "curr_vel");
//              jspace::saveVector(hume_system_->torque_input_, "gamma");
//              jspace::saveVector(hume_system_->curr_torque_, "torque");
//              jspace::saveValue(hume_system_->getCurrentTime(), "time");
             
// #ifdef Body_Test
//              jspace::saveVector(((Ctrl_Body_Test*)hume_system_->controller_)->rfoot_pos_des_, "right_foot_des");
//              jspace::saveVector(((Ctrl_Body_Test*)hume_system_->controller_)->lfoot_pos_des_, "left_foot_des");

//              jspace::saveVector(((Ctrl_Body_Test*)hume_system_->controller_)->pos_des_, "Body_des");
//              jspace::saveVector(((Ctrl_Body_Test*)hume_system_->controller_)->ori_des_, "Ori_des");
//              jspace::saveVector(((Ctrl_Body_Test*)hume_system_->controller_)->vel_des_, "Body_vel_des");
//              jspace::saveVector(((Ctrl_Body_Test*)hume_system_->controller_)->ori_vel_des_, "Ori_vel_des");
//              jspace::saveVector(((Ctrl_Body_Test*)hume_system_->controller_)->curr_rfoot_pos_, "curr_Right_foot");
//              jspace::saveVector(((Ctrl_Body_Test*)hume_system_->controller_)->curr_lfoot_pos_, "curr_Left_foot");
// #endif
             
// #ifdef WALKING_3D 
//              jspace::saveVector(((Ctrl_3D_Walking*)hume_system_->controller_)->COM_d_, "Body_des");
//              jspace::saveVector(((Ctrl_3D_Walking*)hume_system_->controller_)->Foot_d_, "Foot_des");
//              jspace::saveVector(((Ctrl_3D_Walking*)hume_system_->controller_)->Foot_vel_d_, "Foot_vel_des");
//              jspace::saveVector(((Ctrl_3D_Walking*)hume_system_->controller_)->COM_ori_d_, "Ori_des");
//              jspace::saveValue(((Ctrl_3D_Walking*)hume_system_->controller_)->phase_, "phase");  
// #endif
//          }
//         usleep(Data_Gathering_Holding_);
//     }
//}

