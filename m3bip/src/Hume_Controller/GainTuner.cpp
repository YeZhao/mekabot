#include "GainTuner.h"
#include "HumeSystem.h"
#include "Controller_Hume.h"
#include "util/comm_udp.h"

Gain_Tuner::Gain_Tuner(Task* task): Hume_Thread(), socket_(0){
    task_ = task;
}

Gain_Tuner::~Gain_Tuner(){}

void Gain_Tuner::run(){
    printf("[Gain Tuner] Start\n");
    
    while(true){
        task_->set_Gain();
    }
}

Act_gain_tuner::Act_gain_tuner(HUME_System* hume_system): socket_(0){
    hume_system_ = hume_system;
}

void Act_gain_tuner::run(void){
    printf("[Act Gain Tuner] Start\n");
    
    HumeProtocol::pi_joint_gain gain_data;
    while(true){
        COMM::recieve_data(socket_, PORT_JOINT_ACT_GAIN, & gain_data, sizeof(HumeProtocol::pi_joint_gain), IP_ADDR_MYSELF);
        for(int i(0); i< NUM_ACT_JOINT; ++i){
            hume_system_->torque_gain_kp_[i] = gain_data.kp[i];
            hume_system_->torque_gain_ki_[i] = gain_data.ki[i];
        }
        printf("[Act Gain Tuner] Set Acuator Gains\n");
    }
}
