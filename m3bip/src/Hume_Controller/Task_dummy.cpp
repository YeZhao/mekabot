
ORI_Task::ORI_Task(HUME_System* hume_system):Task(hume_system){
    Kp_ = Vector(3);
    Kd_ = Vector(3);
    for (int i(0); i< 3; ++i){
        Kp_[i] = 150.0;
        Kd_[i] = 20.0;
    }
}

Body_Task::Body_Task(HUME_System* hume_system):Task(hume_system){
    Kp_ = Vector(3);
    Kd_ = Vector(3);
    for (int i(0); i< 3; ++i){
        Kp_(i) = (90.0);
        Kd_(i) = (8.0);
    }
    Kp_[1] = 30.0;
    Kd_[1] = 3.0;
}   

void Body_Task::SetTask(const Vector & com_des, const Vector & com_vel_des){
    Body_d_ = com_des;
    Body_vel_d_ = com_vel_des;

    b_settask_ = true;
}

Matrix Body_Task::getJacobian(const vector<double> & conf){
    Matrix J;
    hume_system_->GetFullJacobian(conf, HIP, J);
    return J;
}

Vector Body_Task::getCommand(){
    Vector input(3);
    for (int i(0); i< 3; ++i){
        input(i) = Kp_(i)*(Body_d_(i) - hume_system_->getBody()(i))
             + Kd_(i)*(Body_vel_d_(i) - hume_system_->getBody_vel()(i));
    }
    _PostProcess_Task();
    return input; 
}

Vector Body_Task::getForce(){
    Vector force(Vector::Zero(3));
    return force;
}
///////////////////////////////////////////////////////
//////////         Body XZ TASK
///////////////////////////////////////////////////////
Body_XZ_Task::Body_XZ_Task(HUME_System* hume_system):Task(hume_system){
    Kp_ = Vector(2);
    Kd_ = Vector(2);
    for (int i(0); i< 2; ++i){
        Kp_(i) = 200.0;
        Kd_(i) = 10.0;
    }
     Kp_[1] = 250.0;
     Kd_[1] = 15.0;
    // Kp_(1) = 100.0;
    // Kd_(1) = 5.0;
}

void Body_XZ_Task::SetTask(const Vector & body_des, const Vector & body_vel_des){
    Body_d_ = body_des;
    Body_vel_d_ = body_vel_des;
    b_settask_ = true;
}

Matrix Body_XZ_Task::getJacobian(const vector<double> & conf){
    Matrix J = Matrix::Zero(2, NUM_JOINT);
    J(0, Px) = 1;
    J(1, Pz) = 1;

    if(b_set_weight_){
        for(int i(0); i<2; ++i){
            J.block(i,0, 1, NUM_JOINT) = weight_[i] * J.block(i, 0, 1, NUM_JOINT);
        }
        b_set_weight_ = false;
    }
    return J;
}

void Body_XZ_Task::set_weight(const Vector & weight){
    weight_ = weight;
    b_set_weight_ = true;
}

Vector Body_XZ_Task::getCommand(){
    Vector input(2);
    
    input(0) = Kp_(0)*(Body_d_(0) - hume_system_->getBody()(Px))
        + Kd_(0)*(Body_vel_d_(0) - hume_system_ -> getBody_vel()(Px));
    input(1) = Kp_(1)*(Body_d_(1) - hume_system_->getBody()(Pz))
        + Kd_(1)*(Body_vel_d_(1) - hume_system_ -> getBody_vel()(Pz));

    data_.body_des[0] = Body_d_[0];
    data_.body_des[2] = Body_d_[1];
    data_.body_vel_des[0] = Body_vel_d_[0];
    data_.body_vel_des[2] = Body_vel_d_[0];

    data_.body_pos[0] = hume_system_->getBody()[Px];
    data_.body_pos[2] = hume_system_->getBody()[Pz];

    data_.body_vel[0] = hume_system_->getBody_vel()[Px];
    data_.body_vel[2] = hume_system_->getBody_vel()[Pz];

    for ( int i(0); i< 2; ++i){
        input[i] = crop_value(input[i], -100, 100, "Body XZ Task");
    }
    
    // Post Process
    _PostProcess_Task();

    return input;
}
void Body_XZ_Task::send(){
    HumeProtocol::pd_2_gain gain_data_send;
    for(int i(0); i<2; ++i){
        gain_data_send.kp[i] = Kp_[i];
        gain_data_send.kd[i] = Kd_[i];
    }
    
    COMM::send_data(socket_, PORT_BODY_TASK, & data_, sizeof(HumeProtocol::body_task_data), IP_ADDR_MYSELF);
    COMM::send_data(socket_gain_send_, PORT_BODY_XZ_GAIN_FROM_TASK, & gain_data_send, sizeof(HumeProtocol::pd_2_gain), IP_ADDR_MYSELF);

    //Save Vector
    jspace::saveVector(data_.body_des, "Body_des", 3);
    jspace::saveVector(data_.body_vel_des, "Body_vel_des", 3);
    jspace::saveVector(data_.body_pos, "Body_pos", 3 );
    jspace::saveVector(data_.body_vel, "Body_vel", 3);
}

void Body_XZ_Task::set_Gain(){
    printf("[Body XZ Task] In setting Gain\n");
    COMM::recieve_data(socket_recieve_, PORT_BODY_XZ_GAIN, &gain_data_, sizeof(HumeProtocol::pd_2_gain), IP_ADDR_MYSELF);

    for (int i(0); i<2; ++i){
        Kp_[i] = gain_data_.kp[i];
        Kd_[i] = gain_data_.kd[i];
    }
    printf("[Body XZ Task] recieve gain set\n");
}

Vector Body_XZ_Task::getForce(){
    Vector force(Vector::Zero(2)); 
    return force;
}

///////////////////////////////////////////////////////
//////////         HEIGHT TASK
///////////////////////////////////////////////////////
Height_Task::Height_Task(HUME_System* hume_system):Task(hume_system){
    Kp_ = Vector(1);
    Kp_(0) = 180.0;
    Kd_ = Vector(1);
    Kd_(0) = 5.0;
}

void Height_Task::SetTask(const Vector & height_des, const Vector & height_vel_des){
    height_d_ = height_des;
    height_vel_d_ = height_vel_des;
    b_settask_ = true;
}

Matrix Height_Task::getJacobian(const vector<double> & conf){
    Matrix J = Matrix::Zero(1, NUM_JOINT);
    Matrix J_body;
    hume_system_->GetFullJacobian(conf, HIP, J_body);
    J = J_body.block(2,0, 1, NUM_JOINT);
    return J;
}

Vector Height_Task::getCommand(){
    Vector input(1);
    input(0) = Kp_(0)*(height_d_(0) - hume_system_->getBody()(2))
        + Kd_(0)*(height_vel_d_(0) - hume_system_ -> getBody_vel()(2));
    //Save Data
    data_.body_des[2] = height_d_[0];
    data_.body_vel_des[2] = height_vel_d_[0];
    data_.body_pos[2] = hume_system_->getBody()[2];
    data_.body_vel[2] = hume_system_->getBody_vel()[2];

    double limit = 200.0;
    input[0] = crop_value(input[0], -limit, limit, "Height");
    // Post Process
    _PostProcess_Task();

    return input;
}

Vector Height_Task::getForce(){
    Vector force(Vector::Zero(1));
    return force;
}

void Height_Task::send(){
    COMM::send_data(socket_, PORT_BODY_TASK, & data_, sizeof(HumeProtocol::body_task_data), IP_ADDR_MYSELF);
    
    HumeProtocol::pd_1_gain gain_data_send;

    gain_data_send.kp = Kp_[0];
    gain_data_send.kd = Kd_[0];

    COMM::send_data(socket_gain_send_, PORT_HEIGHT_GAIN_FROM_TASK, &gain_data_send,  sizeof(HumeProtocol::pd_1_gain), IP_ADDR_MYSELF);

    //Save Data in a file
    
    jspace::saveVector(data_.body_des, "Body_des", 3);
    jspace::saveVector(data_.body_vel_des, "Body_vel_des", 3);
    jspace::saveVector(hume_system_->getBody(), "Body_pos");
    jspace::saveVector(hume_system_->getBody_vel(), "Body_vel");
}

void Height_Task::set_Gain(){
    printf("[Height Task] In setting Gain \n");
    COMM::recieve_data(socket_recieve_, PORT_HEIGHT_GAIN, & gain_data_, sizeof(HumeProtocol::pd_1_gain), IP_ADDR_MYSELF);
    Kp_[0] = gain_data_.kp;
    Kd_[0] = gain_data_.kd;
    printf("[Height] recieve gain set \n");
}


///////////////////////////////////////////////////////
//////////         HEIGHT TILTING FOOT TASK
///////////////////////////////////////////////////////
Height_Tilting_Foot_Task::Height_Tilting_Foot_Task(HUME_System* hume_system, LinkID link_id):Task(hume_system), link_id_(link_id){
    force_ = Vector::Zero(5);
    Kp_ = Vector(5);
    Kp_(0) = 160.0;
    Kp_(1) = 300.0;

    Kd_ = Vector(5);
    Kd_(0) = 10.0;
    Kd_[1] = 11.0;

    for (int i(2); i<5; ++i){
        Kp_[i] = 150.0;
        Kd_[i] = 10.0;
    }
    Kp_[0] = 180.0;
    Kd_[0] = 15.0;
    
    Kp_[4] = 220.0;
    Kd_[4] = 20.0;
    
    switch(link_id_){
    case LFOOT:
        recieve_gain_port_ = PORT_HEIGHT_TILTING_LEFT_FOOT_GAIN;
        send_gain_port_ = PORT_HEIGHT_TILTING_LEFT_FOOT_GAIN_FROM_TASK;
        break;
    case RFOOT:
        recieve_gain_port_ = PORT_HEIGHT_TILTING_RIGHT_FOOT_GAIN;
        send_gain_port_ = PORT_HEIGHT_TILTING_RIGHT_FOOT_GAIN_FROM_TASK;
        break;
    }
}

void Height_Tilting_Foot_Task::SetTask(const Vector & des, const Vector & vel_des){
    des_ = des;
    vel_des_ = vel_des;

    hume_system_->CopyFoot(curr_foot_pos_, link_id_);
    hume_system_->CopyFootVel(curr_foot_vel_, link_id_);
    
    b_settask_ = true;
}

Matrix Height_Tilting_Foot_Task::getJacobian(const vector<double> & conf){
    Matrix J = Matrix::Zero(5, NUM_JOINT);
    Matrix J_body, J_foot;
    hume_system_->GetFullJacobian(conf, HIP, J_body);
    J.block(0,0, 1, NUM_JOINT) = J_body.block(2,0, 1, NUM_JOINT);
    J(1, Ry) = 1.0;

    hume_system_->GetFullJacobian(conf, link_id_, J_foot);
    J.block(2, 0, 3, NUM_JOINT) = J_foot;
    return J;
}
void Height_Tilting_Foot_Task::setCurrentFoot(const Vector & curr_foot_pos, const Vector & curr_foot_vel){
    curr_foot_pos_ = curr_foot_pos;
    curr_foot_vel_ = curr_foot_vel;
}
Vector Height_Tilting_Foot_Task::getCommand(){
    Vector input(5);
    // Body Height
    input(0) = Kp_(0)*(des_[0] - hume_system_->getBody()(2))
        + Kd_(0)*(vel_des_[0] - hume_system_ -> getBody_vel()(2));
    // Pitch
    input[1] = Kp_[1] * (des_[1] - hume_system_->getOri()(1))
        + Kd_[1] * (vel_des_[1] - hume_system_ ->getOri()[1]);

    for (int i(2); i<5 ; ++i){
        input[i] = Kp_[i] * (des_[i] - curr_foot_pos_[i-2]) +
            Kd_[i] * (vel_des_[i] - curr_foot_vel_[i-2]);
    }
    
    //Save Data -- Body (Height)
    data_body_.body_des[2] = des_[0];
    data_body_.body_vel_des[2] = vel_des_[0];

    data_body_.body_pos[2] = hume_system_->getBody()[2];
    data_body_.body_vel[2] = hume_system_->getBody_vel()[2];

    // Save Data -- Ori (pitch)
    data_ori_.ori_des[1] = des_[1];
    data_ori_.ori_vel_des[1] = vel_des_[1];
    
    data_ori_.ori[1] = hume_system_->getOri()[1];
    data_ori_.ori_vel[1] = hume_system_->getOri_vel()[1];

    //Save Data -- Foot
    for (int i(0); i< 3; ++i){
        data_foot_.foot_des[i] = des_[2 + i];
        data_foot_.foot_vel_des[i] = vel_des_[2+i];
        data_foot_.foot_pos[i] = curr_foot_pos_[i];
        data_foot_.foot_vel[i] = curr_foot_vel_[i];
    }
    
    double limit = 250.0;
    for (int i(0); i< 5; ++i){
        input[i] = crop_value(input[i], -limit, limit, "Height & Tilting");
    }
    // Post Process
    _PostProcess_Task();

    return input;
}

Vector Height_Tilting_Foot_Task::getForce(){
    return force_;
}

void Height_Tilting_Foot_Task::send(){
    //////////////  Task Data Send /////////////
    COMM::send_data(socket_, PORT_BODY_TASK, & data_body_, sizeof(HumeProtocol::body_task_data), IP_ADDR_MYSELF);
    COMM::send_data(socket_2_, PORT_ORI_TASK, & data_ori_, sizeof(HumeProtocol::ori_task_data), IP_ADDR_MYSELF);
    COMM::send_data(socket_3_, PORT_FOOT_TASK, & data_foot_, sizeof(HumeProtocol::foot_task_data), IP_ADDR_MYSELF);

    //////////////  Gain Data Send /////////////
    HumeProtocol::pd_5_gain gain_data_send;
    for(int i(0); i< 5; ++i){
        gain_data_send.kp[i] = Kp_[i];
        gain_data_send.kd[i] = Kd_[i];
    }
    COMM::send_data(socket_gain_send_, send_gain_port_, & gain_data_send, sizeof(HumeProtocol::pd_5_gain), IP_ADDR_MYSELF);

    //Save Data in a file -- Body
    jspace::saveVector(data_body_.body_des, "Body_des", 3);
    jspace::saveVector(data_body_.body_vel_des, "Body_vel_des", 3);
    jspace::saveVector(hume_system_->getBody(), "Body_pos");
    jspace::saveVector(hume_system_->getBody_vel(), "Body_vel");

    // Save Data in a file -- Ori
    jspace::saveVector(data_ori_.ori_des, "Ori_des", 3);
    jspace::saveVector(data_ori_.ori_vel_des, "Ori_vel_des", 3);
    jspace::saveVector(hume_system_->getOri(), "Body_ori");
    jspace::saveVector(hume_system_->getOri_vel(), "Body_ori_vel");

    // Save Data in a file -- Foot
    jspace::saveVector(data_foot_.foot_des, "Foot_des", 3);
    jspace::saveVector(data_foot_.foot_vel_des, "Foot_vel_des", 3);
    jspace::saveVector(data_foot_.foot_pos, "Foot_pos", 3);
    jspace::saveVector(data_foot_.foot_vel, "Foot_vel", 3);
}

void Height_Tilting_Foot_Task::set_Gain(){
    printf("[Height TIlting Foot Task] In Gain Setting \n");
    // Height Gain
    COMM::recieve_data(socket_recieve_, recieve_gain_port_, & gain_data_, sizeof(HumeProtocol::pd_5_gain), IP_ADDR_MYSELF);
    
    for(int i(0); i< 5; ++i){
        Kp_[i] = gain_data_.kp[i];
        Kd_[i] = gain_data_.kd[i];
    }
    printf("[Height Tilting Foot Task] Recieve Height Gain Set \n");
}

///////////////////////////////////////////////////////
//////////         Tilting TASK
///////////////////////////////////////////////////////
Tilting_Task::Tilting_Task(HUME_System* hume_system):Task(hume_system){
    Kp_ = Vector(1);
    Kp_(0) = 150.0;
    Kd_ = Vector(1);
    Kd_(0) = 5.0;
}

void Tilting_Task::SetTask(const Vector & ang_des, const Vector & ang_vel_des){
    ang_d_ = ang_des;
    ang_vel_d_ = ang_vel_des;
    b_settask_ = true;
}

Matrix Tilting_Task::getJacobian(const vector<double> & conf){
    Matrix J = Matrix::Zero(1, NUM_JOINT);
    J(0, Ry) = 1;
    if(b_set_weight_){
        J = weight_[0]*J;
        b_set_weight_ = false;
    }
    return J;
}

void Tilting_Task::set_weight(const Vector & weight){
    weight_ = weight;
    b_set_weight_ = true;
}

Vector Tilting_Task::getCommand(){
    Vector input(1);
    input(0) = Kp_(0)*(ang_d_(0) - hume_system_->getOri()(1))
        + Kd_(0)*(ang_vel_d_(0) - hume_system_ -> getOri_vel()(1));
    // Post Process
    _PostProcess_Task();
    //Save Data
    data_.ori_des[1] = ang_d_[0];
    data_.ori_vel_des[1] = ang_vel_d_[0];
    data_.ori[1] = hume_system_->getOri()[1];
    data_.ori_vel[1] = hume_system_->getOri_vel()[1];
    
    double limit = 60.0;
    input[0] = crop_value(input[0], -limit, limit, "Tilting");

    return input;
}

void Tilting_Task::send(){
    HumeProtocol::pd_1_gain gain_data_send;

    gain_data_send.kp = Kp_[0];
    gain_data_send.kd = Kd_[0];
    
    COMM::send_data(socket_, PORT_ORI_TASK, & data_, sizeof(HumeProtocol::ori_task_data), IP_ADDR_MYSELF);
    COMM::send_data(socket_gain_send_, PORT_TILTING_GAIN_FROM_TASK, & gain_data_send, sizeof(HumeProtocol::pd_1_gain), IP_ADDR_MYSELF);

    // Save Data in a file
    jspace::saveVector(data_.ori_des, "Ori_des", 3);
    jspace::saveVector(data_.ori_vel_des, "Ori_vel_des", 3);
    jspace::saveVector(hume_system_->getOri(), "Body_ori");
    jspace::saveVector(hume_system_->getOri_vel(), "Body_ori_vel");
}

void Tilting_Task::set_Gain(){
    printf("[Tilting Task] In setting Gain \n");
    COMM::recieve_data(socket_recieve_, PORT_TILTING_GAIN, & gain_data_, sizeof(HumeProtocol::pd_1_gain), IP_ADDR_MYSELF);
    
    Kp_[0] = gain_data_.kp;
    Kd_[0] = gain_data_.kd;
    
    printf("[Tilting Task] recieve gain set \n");
}

Vector Tilting_Task::getForce(){
    Vector force(Vector::Zero(1));
    return force;
}

///////////////////////////////////////////////////////
//////////         Orientation TASK
///////////////////////////////////////////////////////

Matrix ORI_Task::getJacobian(const vector<double> & conf){
    Matrix J;
    J = Matrix::Zero(3, NUM_JOINT);
    J(0, Rz) = 1.0;
    J(1, Ry) = 1.0;
    J(2, Rx) = 1.0;
    
    return J;
}
void ORI_Task::SetTask(const Vector & ori_des, const Vector & ori_vel_des){
    ORI_d_ = ori_des;
    ORI_vel_d_ = ori_vel_des;
    b_settask_ = true;
}

Vector ORI_Task::getCommand(){
    Vector input(3);
    for (int i(0); i< 3; ++i){
        input(i) = -Kp_[i]*(hume_system_->getOri()(i) -  ORI_d_(i))
            - Kd_[i]*(hume_system_->getOri_vel()(i) - ORI_vel_d_(i));
        input[i] = crop_value(input[i], -20, 20, "ORI TASK");
    }

    // Post Process
    _PostProcess_Task();

    return input;
}

Vector ORI_Task::getForce(){
    Vector force(Vector::Zero(3));
    return force;
}

///////////////////////////////////////////////////////
//////////         Orientation Ry, Rx TASK
///////////////////////////////////////////////////////
ORI_YX_Task::ORI_YX_Task(HUME_System* hume_system):Task(hume_system){
    Kp_ = Vector(2);
    Kd_ = Vector(2);
    for (int i(0); i< 2; ++i){
        Kp_[i] = 80.0;
        Kd_[i] = 5.0;
    }
    Kp_[1] = 0.0;
    Kd_[1] = 0.0;
}

Matrix ORI_YX_Task::getJacobian(const vector<double> & conf){
    Matrix J;
    J = Matrix::Zero(2, NUM_JOINT);
    J(0, Ry) = 1.0; 
    J(1, Rx) = 1.0;
    
    return J;
}
void ORI_YX_Task::SetTask(const Vector & ori_des, const Vector & ori_vel_des){
    ORI_yx_d_ = ori_des;
    ORI_yx_vel_d_ = ori_vel_des;
    b_settask_ = true;
}

Vector ORI_YX_Task::getCommand(){
    Vector input(2);
    for (int i(0); i< 2; ++i){
        input(i) = -Kp_[i]*(hume_system_->getOri()(i+1) -  ORI_yx_d_(i))
            - Kd_[i]*(hume_system_->getOri_vel()(i+1) - ORI_yx_vel_d_(i));
        input[i] = crop_value(input[i], -35, 35, "ORI YX TASK");
    }
//    pretty_print(input, std::cout, "input ori yx", "");
    // Post Process
    _PostProcess_Task();

    return input;
}

Vector ORI_YX_Task::getForce(){
    Vector force(Vector::Zero(2));
    return force;
}
