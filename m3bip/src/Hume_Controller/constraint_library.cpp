#include "constraint_library.hpp"
#include <iostream>
#include <fstream>

namespace jspace {
////////////   Fixed Constraint  ////////////////
    bool Hume_Fixed_Constraint::updateJcU(const vector<double> & conf, HUME_System* _model){
        // Jc_ = Matrix::Zero(NUM_PASSIVE-1, NUM_JOINT);
        // Jc_.block(0,0, NUM_PASSIVE-2, NUM_PASSIVE-2) = Matrix::Identity(NUM_PASSIVE-2, NUM_PASSIVE-2);
        // Jc_(NUM_PASSIVE -2 , Rx) = 1;
        Jc_ = Matrix::Zero(NUM_PASSIVE, NUM_JOINT);
        Jc_.block(0,0, NUM_PASSIVE, NUM_PASSIVE) = Matrix::Identity(NUM_PASSIVE, NUM_PASSIVE);

        //Update U
        U_ = Matrix::Zero(NUM_ACT_JOINT, NUM_JOINT);
        U_.block(0, NUM_PASSIVE, NUM_ACT_JOINT, NUM_ACT_JOINT) = Matrix::Identity(NUM_ACT_JOINT, NUM_ACT_JOINT);
        return true;
    }

////////////   No Constraint  ////////////////
    bool Hume_No_Constraint::updateJcU(const vector<double> & conf, HUME_System* _model){
        Jc_ = Matrix::Zero(1, NUM_JOINT);
        //Update U
        U_ = Matrix::Zero(NUM_ACT_JOINT, NUM_JOINT);
        U_.block(0, NUM_PASSIVE, NUM_ACT_JOINT, NUM_ACT_JOINT) = Matrix::Identity(NUM_ACT_JOINT, NUM_ACT_JOINT);
        return true;
    }

////////////   Both Constraint  ////////////////
    Hume_Contact_Both::Hume_Contact_Both(): WBC_Constraint(){
        b_internal_ = true;
        b_re_force_ctrl_ = false;
        num_constraint_ = 6;
    }
        
    bool Hume_Contact_Both::updateJcU(const vector<double> & conf, HUME_System * _model)
    {
        Matrix Jfull;
        Jc_ = Matrix::Zero(6, NUM_JOINT);
        //Right Leg
        _model->GetFullJacobian(conf, RFOOT, Jfull);
        Jc_.block(0,0,3, NUM_JOINT) = Jfull;
        
        //Left Leg
        _model->GetFullJacobian(conf, LFOOT, Jfull);
        Jc_.block(3,0,3,NUM_JOINT) = Jfull;

        //Update U
        U_ = Matrix::Zero(NUM_ACT_JOINT, NUM_JOINT);
        U_.block(0, NUM_PASSIVE, NUM_ACT_JOINT, NUM_ACT_JOINT) = Matrix::Identity(NUM_ACT_JOINT, NUM_ACT_JOINT);
        
        Update_Internal_Matrix(_model);
        return true;
    }

    void Hume_Contact_Both::Update_Internal_Matrix(HUME_System* _hume_system){
        W_int_ = Matrix::Zero(2,6);
        Matrix R = Matrix::Zero(3,3);
        Vector lfoot_pos, rfoot_pos;
        _hume_system->CopyFoot(lfoot_pos, LFOOT);
        _hume_system->CopyFoot(rfoot_pos, RFOOT);

        Vector foot_dist ( rfoot_pos-lfoot_pos );
        
        double length(0.0);
        for (int i(0); i< 3; ++i){
            length += pow(foot_dist[i], 2.0);
        }
        length = sqrt(length);
        
        // R(0,0) = foot_dist[0]/ length;
        // R(0,1) = foot_dist[1]/ length;
        // R(1,0) = -R(0,1);
        // R(1,1) = R(0, 0);
        // R(2,2) = 1;

        R.block(0,0, 1, 3) = (foot_dist/ length).transpose();
        R(1,0) = -R(0,1);
        R(1,1) = R(0,0);
        Eigen::Vector3d x(R(0,0), R(0,1), R(0,2));
        Eigen::Vector3d y(R(1,0), R(1,1), R(1,2));
        Eigen::Vector3d x_y(x.cross(y));
        R(2,0) = x_y(0);
        R(2,1) = x_y(1);
        R(2,2) = x_y(2);
        
        Matrix d_t = Matrix::Zero(3,6);
        d_t.block(0, 0, 3, 3) = Matrix::Identity(3,3);
        d_t.block(0,3, 3,3) = -Matrix::Identity(3,3);
        
        Matrix S = Matrix::Zero(2,3);
        S(0, 0) = 1.0;
        S(1, 1) = 1.0;
        W_int_ = S * R * d_t;
    }
    void Hume_Contact_Both::set_constraint_weight(const Vector & weight){
        constraint_weight_ = weight;
        b_constraint_weight_ = true;
    }
    void Hume_Contact_Both::getDesInternalForce(Vector & des_int_force){
        des_int_force = Vector::Zero(2);
        des_int_force[0] = 100.0;
    }
//////////////////////////////////////////////////////////
////////////   Both Constraint  Abduction ////////////////
//////////////////////////////////////////////////////////
    Hume_Contact_Both_Abduction::Hume_Contact_Both_Abduction(): WBC_Constraint(){
        b_internal_ = true;
        b_re_force_ctrl_ = false;
        num_constraint_ = 8;
    }
    void Hume_Contact_Both_Abduction::getDesInternalForce(Vector & des_int_force){
        des_int_force = Vector::Zero(4);
    }

    bool Hume_Contact_Both_Abduction::getSelection(Vector & Fr, Matrix & S){
        S = Matrix::Zero(2, 8);
        if(Fr[2] > 50){
            exit(0);
        }
        if(Fr[2] >-55.0){ // Right Z
            S(0, 2) = 1.0;
            Fr[2] = Fr[2] - (-55.0);
            printf("Right Foot Lift Up\n");
        }
        if(Fr[5] > -55.0){ // Left Z
            S(1, 5) = 1.0;
            Fr[5] = Fr[5] - (-55.0);
            printf("Left Foot Lift Up\n");
        }

        return true;
    }

    bool Hume_Contact_Both_Abduction::updateJcU(const vector<double> & conf, HUME_System * _model)
    {
        Matrix Jfull;
        Jc_ = Matrix::Zero(8, NUM_JOINT);
        //Right Leg
        _model->GetFullJacobian(conf, RFOOT, Jfull);
        Jc_.block(0,0,3, NUM_JOINT) = Jfull;
        
        //Left Leg
        _model->GetFullJacobian(conf, LFOOT, Jfull);
        Jc_.block(3,0,3,NUM_JOINT) = Jfull;

        //Right abduction
        Jc_(6, 6) = 1.0;
        //Left abduction
        Jc_(7, 9) = 1.0;
        
        //Update U
        U_ = Matrix::Zero(NUM_ACT_JOINT,NUM_JOINT);
        U_.block(0, NUM_PASSIVE, NUM_ACT_JOINT, NUM_ACT_JOINT) = Matrix::Identity(NUM_ACT_JOINT,NUM_ACT_JOINT);

        if(b_constraint_weight_){
            for( int i(0); i< num_constraint_; ++i){
                Jc_.block(i,0, 1,NUM_JOINT) = constraint_weight_[i]*Jc_.block(i,0, 1, NUM_JOINT);
            }
            b_constraint_weight_ = false;
        }
        Update_Internal_Matrix(_model);
        return true;
    }

    void Hume_Contact_Both_Abduction::Update_Internal_Matrix(HUME_System* _hume_system){
        W_int_ = Matrix::Zero(2,num_constraint_);
        Matrix R = Matrix::Zero(3,3);
        Vector lfoot_pos, rfoot_pos;
        _hume_system->CopyFoot(lfoot_pos, LFOOT);
        _hume_system->CopyFoot(rfoot_pos, RFOOT);

        Vector foot_dist ( lfoot_pos-rfoot_pos );
        
        double length(0.0);
        for (int i(0); i< 3; ++i){
            length += pow(foot_dist[i], 2.0);
        }
        length = sqrt(length);
        
        R(0,0) = foot_dist[0]/ length;
        R(0,1) = foot_dist[1]/ length;
        R(1,0) = -R(0,1);
        R(1,1) = R(0, 0);
        R(2,2) = 1;
        
        Matrix d_t = Matrix::Zero(3,6);
        d_t.block(0, 0, 3, 3) = Matrix::Identity(3,3);
        d_t.block(0,3, 3,3) = -Matrix::Identity(3,3);
        
        Matrix S = Matrix::Zero(2,3);
        S(0, 0) = 1.0;
        S(1, 1) = 1.0;
        W_int_.block(0,0, 2, 6) = S * R * d_t;
    }
    void Hume_Contact_Both_Abduction::set_constraint_weight(const Vector & weight){
        constraint_weight_ = weight;
        b_constraint_weight_ = true;
    }
    
////////////   Both Constraint W/ Boom  ////////////////
    Hume_Contact_Both_Boom::Hume_Contact_Both_Boom():WBC_Constraint(){
        b_internal_ = true;
        b_re_force_ctrl_ = false;
        num_constraint_ = 9;
    }
    void Hume_Contact_Both_Boom::getDesInternalForce(Vector & des_int_force){
        des_int_force = Vector::Zero(2);
    }

    void Hume_Contact_Both_Boom::set_constraint_weight(const Vector & weight){
        constraint_weight_ = weight;
        b_constraint_weight_ = true;
    }
    bool Hume_Contact_Both_Boom::updateJcU(const vector<double> & conf, HUME_System * _model)
    {
        Jc_ = Matrix::Zero(9, NUM_JOINT);
        Matrix Jfull;
        // Y, Rz, Rx
        Jc_(0, Py) = 1;
        Jc_(1, Rz) = 1;
        Jc_(2, Rx) = 1;
        //Right Leg
        _model->GetFullJacobian(conf, RFOOT, Jfull);
        Jc_.block(3,0,3, NUM_JOINT) = Jfull.block(0,0,3,NUM_JOINT);
        
        //Left Leg
        _model->GetFullJacobian(conf, LFOOT, Jfull);
        Jc_.block(6,0,3,NUM_JOINT) = Jfull.block(0,0,3,NUM_JOINT);

        if(b_constraint_weight_){
            for( int i(0); i< 9; ++i){
                Jc_.block(i,0, 1,NUM_JOINT) = constraint_weight_[i]*Jc_.block(i,0, 1, NUM_JOINT);
            }
        }
        
        //Update U
        U_ = Matrix::Zero(NUM_ACT_JOINT,NUM_JOINT);
        U_.block(0, NUM_PASSIVE, NUM_ACT_JOINT, NUM_ACT_JOINT) = Matrix::Identity(NUM_ACT_JOINT,NUM_ACT_JOINT);

        Update_Internal_Matrix(_model);
        
        return true;
    }
    bool Hume_Contact_Both_Boom::getSelection( Vector & Fr, Matrix & S){
        int idx = 0;
        if(idx == 0){
            S = Matrix::Zero(7, 9);
            // Py, Rz, Rx
            S.block(0,0, 3,3) = Matrix::Identity(3,3);
        // Right X, Y
            S(3,3) = 1.0;
            S(4,4) = 1.0;
            // Left X, Y
            S(5, 6) = 1.0;
            S(6, 7) = 1.0;
        }
        else if( idx ==1){
            S = Matrix::Zero(4,9);
            S(0,3) = 1.0;
            S(1,4) = 1.0;
            S(2,6) = 1.0;
            S(3,7) = 1.0;
        }
        else if(idx ==2){
            S = Matrix::Zero(5,9);
            S(0,Rx) = 1.0;
            S(1, 3) = 1.0;
            S(2,4) = 1.0;
            S(3, 6) = 1.0;
            S(4, 7) = 1.0;
        }
        else if(idx ==3){
            S = Matrix::Zero(6,9);
            S(0,Py) = 1.0;
            S(1,Rx) = 1.0;
            S(2, 3) = 1.0;
            S(3,4) = 1.0;
            S(4, 6) = 1.0;
            S(5, 7) = 1.0;
        }
        else if(idx ==4){
            S = Matrix(9,9);
            S.block(0,0,3,3) = Matrix::Identity(3,3);
            S(3,3) = 1.0;
            S(4,4) = 1.0;
            S(5,5) = 0.0;
            S(6,6) = 1.0;
            S(7,7) = 1.0;
            S(8,8) = 0.0;
        }

    }
    void Hume_Contact_Both_Boom::Update_Internal_Matrix(HUME_System* _hume_system){
        W_int_ = Matrix::Zero(4,9);
        Matrix R = Matrix::Zero(3,3);

        W_int_(0,1) = 1.0;
        W_int_(1,2) = 1.0;
        
        Vector lfoot_pos, rfoot_pos;
        _hume_system->CopyFoot(lfoot_pos, LFOOT);
        _hume_system->CopyFoot(rfoot_pos, RFOOT);

        Vector foot_dist ( rfoot_pos-lfoot_pos );
        
        double length(0.0);
        for (int i(0); i< 3; ++i){
            length += pow(foot_dist[i], 2.0);
        }
        length = sqrt(length);
        
        R(0,0) = foot_dist[0]/ length;
        R(0,1) = foot_dist[1]/ length;
        R(1,0) = -R(0,1);
        R(1,1) = R(0, 0);
        R(2,2) = 1;
        
        Matrix d_t = Matrix::Zero(3,6);
        d_t.block(0, 0, 3, 3) = Matrix::Identity(3,3);
        d_t.block(0,3, 3,3) = -Matrix::Identity(3,3);
            
        Matrix S = Matrix::Zero(2,3);
        S(0, 0) = 1.0;
        S(1, 1) = 1.0;
        W_int_.block(2,3, 2, 6) = S * R * d_t;

    }
////////////   Both Constraint W/ Boom  Constraining X ////////////////
    Hume_Contact_Both_Boom_X::Hume_Contact_Both_Boom_X(): WBC_Constraint(){
        b_internal_ = true;
        b_re_force_ctrl_ = false;
        num_constraint_ = 10;
    }
    void Hume_Contact_Both_Boom_X::getDesInternalForce(Vector & des_int_force){
        des_int_force = Vector::Zero(4);
    }

    void Hume_Contact_Both_Boom_X::set_constraint_weight(const Vector & weight){
        constraint_weight_ = weight;
        b_constraint_weight_ = true;
    }
    void Hume_Contact_Both_Boom_X::set_internal_force_weight(const Vector & int_weight){
        internal_weight_ = int_weight;
        b_internal_weight_ = true;
    }
    bool Hume_Contact_Both_Boom_X::updateJcU(const vector<double> & conf, HUME_System * _model){
        Jc_ = Matrix::Zero(10, NUM_JOINT);
        Matrix Jfull;
        // X, Y, Rz, Rx
        Jc_(0, Px) = 1;        
        Jc_(1, Py) = 1;
        Jc_(2, Rz) = 1;
        Jc_(3, Rx) = 1;
        //Right Leg
        _model->GetFullJacobian(conf, RFOOT, Jfull);
        Jc_.block(4, 0, 3, NUM_JOINT) = Jfull.block(0,0,3,NUM_JOINT);
        
        //Left Leg
        _model->GetFullJacobian(conf, LFOOT, Jfull);
        Jc_.block(7, 0, 3, NUM_JOINT) = Jfull.block(0,0,3,NUM_JOINT);

        if(b_constraint_weight_){
            for( int i(0); i< 10; ++i){
                Jc_.block(i,0, 1,NUM_JOINT) = constraint_weight_[i]*Jc_.block(i,0, 1, NUM_JOINT);
            }
        }
        
        //Update U
        U_ = Matrix::Zero(NUM_ACT_JOINT, NUM_JOINT);
        U_.block(0, NUM_PASSIVE, NUM_ACT_JOINT, NUM_ACT_JOINT) = Matrix::Identity(NUM_ACT_JOINT,NUM_ACT_JOINT);

        Update_Internal_Matrix(_model);

        return true;
    }

    bool Hume_Contact_Both_Boom_X::getSelection( Vector & Fr, Matrix & S){
        S = Matrix::Zero(1, 10);
        S(0, 3) = 1.0;
        S(1,4) = 1.0;
        S(2, 5) = 1.0;
        S(3, 7) = 1.0;
        S(4, 8) = 1.0;
        return true;
    }

    void Hume_Contact_Both_Boom_X::Update_Internal_Matrix(HUME_System  * _hume_system){
        W_int_ = Matrix::Zero(4, 10);
        Matrix R = Matrix::Zero(3,3);

        // W_int_(0,0) = 0.0;
        W_int_(0,2) = 0.0;
        W_int_(1,3) = 1.0;
        
        Vector lfoot_pos, rfoot_pos;
        _hume_system->CopyFoot(lfoot_pos, LFOOT);
        _hume_system->CopyFoot(rfoot_pos, RFOOT);

        Vector foot_dist ( lfoot_pos-rfoot_pos );
        
        double length(0.0);
        for (int i(0); i< 3; ++i){
            length += pow(foot_dist[i], 2.0);
        }
        length = sqrt(length);
        
        R(0,0) = foot_dist[0]/ length;
        R(0,1) = foot_dist[1]/ length;
        R(1,0) = -R(0,1);
        R(1,1) = R(0, 0);
        R(2,2) = 1;
        
        Matrix d_t = Matrix::Zero(3,6);
        d_t.block(0, 0, 3, 3) = Matrix::Identity(3,3);
        d_t.block(0,3, 3,3) = -Matrix::Identity(3,3);
            
        Matrix S = Matrix::Zero(2,3);
        S(0, 0) = 1.0;
        S(1, 1) = 1.0;
        W_int_.block(2,4, 2, 6) = S * R * d_t;
        
        if(b_internal_weight_){
            for(int i(0); i<4; ++i){
                W_int_.block(i,0, 1, 10) = internal_weight_[i] * W_int_.block(i, 0, 1, 10);
            }
        }

        // if(b_change_internal_force_){
        //     W_int_ = Matrix::Zero(5, 10);
        //     W_int_(0,2) = 1.0;
        //     W_int_(1,3) = 1.0;
        //     W_int_(2,7) = internal_weight_[0] * 1.0;
        //     W_int_(3,8) = internal_weight_[0] * 1.0;
        //     W_int_(4, 9) = internal_weight_[0] * 1.0;
        // }
    }
////////  Point Contact //  Boom System // Constaraining X
    Hume_Point_Contact_Boom_X::Hume_Point_Contact_Boom_X(LinkID id): WBC_Constraint(), foot_(id) {
        b_internal_ = true;
        b_re_force_ctrl_ = false;
        num_constraint_ = 7;
    }
    void Hume_Point_Contact_Boom_X::getDesInternalForce(Vector & des_int_force){
        des_int_force = Vector::Zero(1);
    }

    void Hume_Point_Contact_Boom_X::set_constraint_weight(const Vector & weight){
        constraint_weight_ = weight;
        b_constraint_weight_ = true;
    }

    bool Hume_Point_Contact_Boom_X::updateJcU(const vector<double> & conf, HUME_System * _model){
        Jc_ = Matrix::Zero(7, NUM_JOINT);
        Matrix Jfull;
        // X, Y, Rx, Rz
        Jc_(0, Px) = 1;
        Jc_(1, Py) = 1;
        Jc_(2, Rz) = 1;
        Jc_(3, Rx) = 1;

        switch(foot_){
        case RFOOT:
            //Right Leg
            _model->GetFullJacobian(conf, RFOOT, Jfull);
            Jc_.block(4,0, 3, NUM_JOINT) = Jfull.block(0,0, 3, NUM_JOINT);
            break;
        case LFOOT:
            //Left leg
            _model->GetFullJacobian(conf, LFOOT, Jfull);
            Jc_.block(4, 0, 3, NUM_JOINT) = Jfull.block(0, 0, 3, NUM_JOINT);
            break;
        }
        //Update U
        U_ = Matrix::Zero(NUM_ACT_JOINT, NUM_JOINT);
        U_.block(0, NUM_PASSIVE, NUM_ACT_JOINT, NUM_ACT_JOINT) = Matrix::Identity(NUM_ACT_JOINT, NUM_ACT_JOINT);
        Update_Internal_Matrix(_model);
        return true;
    }

    void Hume_Point_Contact_Boom_X::Update_Internal_Matrix(HUME_System * _hume_system){
        // Px, Rx, Rz
        W_int_ = Matrix::Zero(1,7);
        // W_int_(0, 0) = 1.0;
        // W_int_(0, 2) = 1.0;
        W_int_(0, 3)  = 1.0;
    }

////////  Point Contact
    Hume_Point_Contact::Hume_Point_Contact(LinkID id): WBC_Constraint(), foot_(id) {
        b_internal_ = false;
        b_re_force_ctrl_ = false;
        num_constraint_ = 3;
    }

    bool Hume_Point_Contact::updateJcU(const vector<double> & conf, HUME_System * _model){
        switch(foot_){
        case RFOOT:
            //Right Leg
            _model->GetFullJacobian(conf, RFOOT, Jc_);
            break;
        case LFOOT:
            //Left leg
            _model->GetFullJacobian(conf, LFOOT, Jc_);
            break;
        }
        //Update U
        U_ = Matrix::Zero(NUM_ACT_JOINT, NUM_JOINT);
        U_.block(0, NUM_PASSIVE, NUM_ACT_JOINT, NUM_ACT_JOINT) = Matrix::Identity(NUM_ACT_JOINT, NUM_ACT_JOINT);
        if(b_constraint_weight_){
            for(int i(0); i<3; ++i){
                Jc_.block(i,0, 1, NUM_JOINT) = constraint_weight_[i] * Jc_.block(i, 0, 1, NUM_JOINT);
            }
        }
        return true;
    }

    //////////////////////////////////////////
    ////////  Point Contact Abduction
    //////////////////////////////////////////
    Hume_Point_Contact_Abduction::Hume_Point_Contact_Abduction(LinkID id): WBC_Constraint(), foot_(id) {
        b_internal_ = false;
        b_re_force_ctrl_ = false;
        num_constraint_ = 5;
    }

    bool Hume_Point_Contact_Abduction::updateJcU(const vector<double> & conf, HUME_System * _model){
        Jc_ = Matrix::Zero(num_constraint_, NUM_JOINT);
        Matrix Jfoot;
        switch(foot_){
        case RFOOT:
            //Right Leg
            _model->GetFullJacobian(conf, RFOOT, Jfoot);
            break;
        case LFOOT:
            //Left leg
            _model->GetFullJacobian(conf, LFOOT, Jfoot);
            break;
        }
        Jc_.block(0, 0, 3, NUM_JOINT) = Jfoot;
        Jc_(3, 6) = 1.0;
        Jc_(4, 9) = 1.0;
        
        //Update U
        U_ = Matrix::Zero(NUM_ACT_JOINT, NUM_JOINT);
        U_.block(0, NUM_PASSIVE, NUM_ACT_JOINT, NUM_ACT_JOINT) = Matrix::Identity(NUM_ACT_JOINT, NUM_ACT_JOINT);

        return true;
    }


////////////   Both Constraint W/ Y & Yaw  ////////////////    
    bool Hume_Contact_Both_Y_Yaw::updateJcU(const vector<double> & conf, HUME_System * _model)
    {
        Jc_ = Matrix::Zero(8, NUM_JOINT);
        Matrix Jfull;
        // Y, Rx, Rz
        Jc_(0, Py) = 1;
        Jc_(1, Rz) = 1;
        //Right Leg
        _model->GetFullJacobian(conf, RFOOT, Jfull);
        Jc_.block(2,0,3, NUM_JOINT) = Jfull.block(0,0,3,NUM_JOINT);
        
        //Left Leg
        _model->GetFullJacobian(conf, LFOOT, Jfull);
        Jc_.block(5,0,3,NUM_JOINT) = Jfull.block(0,0,3,NUM_JOINT);

        //Update U
        U_ = Matrix::Zero(NUM_ACT_JOINT,NUM_JOINT);
        U_.block(0, NUM_PASSIVE, NUM_ACT_JOINT, NUM_ACT_JOINT) = Matrix::Identity(NUM_ACT_JOINT,NUM_ACT_JOINT);
        return true;
    }
    
////////////   Point Contact Constraint W/ Boom  ////////////////
    Hume_Point_Contact_Boom::Hume_Point_Contact_Boom(LinkID _foot): WBC_Constraint(),foot_(_foot){
        b_internal_ = false;
        b_re_force_ctrl_ = false;
        num_constraint_ = 4;
    }
    bool Hume_Point_Contact_Boom::updateJcU(const vector<double> & conf, HUME_System * _model)
    {
        Jc_ = Matrix::Zero(4, NUM_JOINT);
        Matrix Jfull;
        // Rx
        Jc_(0, Rx) = 1;

        switch (foot_){
        case RFOOT:
            //Right Leg
            _model->GetFullJacobian(conf, RFOOT, Jfull);
            Jc_.block(1,0,3, NUM_JOINT) = Jfull.block(0,0,3,NUM_JOINT);
            break;
        case LFOOT:
            //Left Leg
            _model->GetFullJacobian(conf, LFOOT, Jfull);
            Jc_.block(1,0,3,NUM_JOINT) = Jfull.block(0,0,3,NUM_JOINT);
            break;
        }
        //Update U
        U_ = Matrix::Zero(NUM_ACT_JOINT,NUM_JOINT);
        U_.block(0, NUM_PASSIVE, NUM_ACT_JOINT, NUM_ACT_JOINT) = Matrix::Identity(NUM_ACT_JOINT,NUM_ACT_JOINT);
        return true;
    }
    
////////////   Both XZ Constraint  ////////////////    
    bool Hume_Contact_XZ_Both::updateJcU(const vector<double> & conf, HUME_System * _model)
    {
	Matrix Jfull;
        bool r_contact(true);
        bool l_contact(true);
        //Left Leg
        Matrix Jc_tmp = Matrix::Zero(4, NUM_JOINT);
        _model->GetFullJacobian(conf, LFOOT, Jfull);
        Jc_tmp.block(0,0,1,NUM_JOINT) = Jfull.block(0,0,1,NUM_JOINT);
        Jc_tmp.block(1,0,1,NUM_JOINT) = Jfull.block(2,0,1,NUM_JOINT);

        
        //Right Leg
        _model->GetFullJacobian(conf, RFOOT, Jfull);
        Jc_tmp.block(2,0,1, NUM_JOINT) = Jfull.block(0,0,1,NUM_JOINT);
        Jc_tmp.block(3,0,1, NUM_JOINT) = Jfull.block(2,0,1,NUM_JOINT);

        // if(_model->getFoot_vel(RFOOT)[2]>0){
        //     r_contact = false;
        // }
        // if(_model->getFoot_vel(LFOOT)[2]>0){
        //     l_contact = false;
        // }

        // if(!r_contact && !l_contact){
        //     Jc_ = Matrix::Zero(1,NUM_JOINT);
        // }
        // else if(!r_contact){
        //     Jc_ = Jc_tmp.block(0,0, 2,NUM_JOINT);
        // }
        // else if(!l_contact){
        //     Jc_ = Jc_tmp.block(2,0, 2, NUM_JOINT);
        // }
        // else{
        //     Jc_ = Jc_tmp;
        // }
        Jc_ = Jc_tmp;
        //Update U
        U_ = Matrix::Zero(NUM_ACT_JOINT,NUM_JOINT);
        U_.block(0, NUM_PASSIVE, NUM_ACT_JOINT, NUM_ACT_JOINT) = Matrix::Identity(NUM_ACT_JOINT,NUM_ACT_JOINT);
        return true;
    }
}
