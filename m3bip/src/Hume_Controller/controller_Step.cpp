#include "controller_Step.h"

#include <math.h>
#include <stdio.h>
#include "constraint_library.hpp"
#include "Process.h"

Ctrl_Step::Ctrl_Step(HUME_System * hume_system):
    Controller_Hume(hume_system),
    foot_des_(3), foot_vel_des_(Vector::Zero(3)),
    num_step_(0), state_machine_time_(0.0),
    stance_foot_(RFOOT), swing_foot_(LFOOT),
    landing_loc_(Vector::Zero(3))
{
    dual_contact_task_    = NULL; 
    single_contact_lfoot_ = NULL;
    single_contact_rfoot_ = NULL;
    
    constraint_left_  = NULL;
    constraint_right_ = NULL;

    //Process
    pro_stable_ = new stabilizing(hume_system_);
}

Ctrl_Step::~Ctrl_Step(){
    SAFE_DELETE(dual_contact_task_   ); 
    SAFE_DELETE(single_contact_lfoot_);
    SAFE_DELETE(single_contact_rfoot_);
    
    SAFE_DELETE(constraint_left_ );
    SAFE_DELETE(constraint_right_);

}

void Ctrl_Step::getCurrentCommand(std::vector<double> & command){
    _PreProcessing_Command();
    ++count_command_;
    
    double curr_time = ((double)count_command_)*SERVO_RATE;
    
    if(pro_stable_->do_process(this)){
        _PostProcessing_Command(command);
        count_command_ = 0;
        return;
    }
    
    static int curr_phase = LsDu;
    phase_ = curr_phase;

    if(_IsEndCurrState(curr_phase)){
        phase_ = curr_phase;
        ++curr_phase;
        if(curr_phase == LsRm || curr_phase == RsLm){
            ++num_step_;
            printf("Number of Step: %i \n", num_step_);
        }
        if(curr_phase > 9) { curr_phase = 0; }
        _printf_phase(curr_phase);
    }
    _PostProcessing_Command(command);
}

void Ctrl_Step::setCurrentConfiguration(){
    _SetLocalConfiguration2StanceFoot(stance_foot_);
}

void Ctrl_Step::_end_action(){
    state_machine_time_ = 0.0;
    foot_pre_ = foot_des_;
}

bool Ctrl_Step::_IsEndCurrState(const int curr_phase){
    switch(curr_phase){
    case LsDu:
        stance_foot_ = LFOOT;
        swing_foot_ = RFOOT;
        b_int_gain_left_ = false;
        b_int_gain_right_ = false;
        return !_Supp();
    case LsTran:
        stance_foot_ = LFOOT;
        swing_foot_ = RFOOT;
        b_int_gain_left_ = false;
        b_int_gain_right_ = false;
        return !_Supp_Lift_transition();
    case LsRl:
        stance_foot_ = LFOOT;
        swing_foot_ = RFOOT;
        b_int_gain_left_ = false;
        b_force_int_gain_right_knee_ = true;
        return !_Do_Supp_Lift();
    case LsRm:
        stance_foot_ = LFOOT;
        swing_foot_ = RFOOT;
        b_int_gain_left_ = false;
        b_force_int_gain_right_knee_ = true; 
        return !_Do_Supp_Land();
    case LsRs:
        stance_foot_ = LFOOT;
        swing_foot_ = RFOOT;
        b_int_gain_left_ = false;
        return !_Land_Supp_transition();

    case RsDu:
        stance_foot_ = RFOOT;
        swing_foot_ = LFOOT;
        b_int_gain_left_ = false;
        b_int_gain_right_ = false;
        return !_Supp();
    case RsTran:
        stance_foot_ = RFOOT;
        swing_foot_ = LFOOT;
        b_int_gain_right_ = false;
        return !_Supp_Lift_transition();
    case RsLl:
        stance_foot_ = RFOOT;
        swing_foot_ = LFOOT;
        b_int_gain_right_ = false;
        b_force_int_gain_right_knee_ = true; 
        return !_Do_Supp_Lift();
    case RsLm:
        stance_foot_ = RFOOT;
        swing_foot_ = LFOOT;
        b_int_gain_right_ = false;
        b_force_int_gain_right_knee_ = true;
        return !_Do_Supp_Land();
    case RsLs:
        stance_foot_ = RFOOT;
        swing_foot_ = LFOOT;
        b_int_gain_right_ = false;
        return !_Land_Supp_transition();
    case STOP:
        // stance foot and swing foot are not changed
        b_int_gain_left_ = false;
        b_int_gain_right_ = false;
        return !_Stop();
    }
    return true;
} 


void Ctrl_Step::_printf_phase(int phase){
    switch(phase){
    case LsDu:        printf("**Phase: LSDU\n");        break;
    case LsTran:      printf("**Phase: LSTRAN\n");      break;
    case LsRl:        printf("**Phase: LSRL\n");        break;
    case LsRm:        printf("**Phase: LSRM\n");        break;
    case LsRs:        printf("**Phase: LSRLAND\n");        break;
    case RsDu:        printf("**Phase: RSDU\n");        break;
    case RsTran:      printf("**Phase: RSTRAN\n");      break;
    case RsLl:        printf("**Phase: RSLL\n");        break;
    case RsLm:        printf("**Phase: RSLM\n");        break;
    case RsLs:        printf("**Phase: RSLLAND\n");        break;
    case STOP:        printf("**Phase: Stop\n");        break;
    default: printf(" *** ERROR: PHASE ***\n"); break;
    }
}


//////////////////////////////////////////////////////////////
//////////////////// Ctrl XZ Step ////////////////////////////
//////////////////////////////////////////////////////////////
Virtual_Ctrl_XZ_Step::Virtual_Ctrl_XZ_Step(HUME_System* _hume_system): Ctrl_Step(_hume_system){
}
Virtual_Ctrl_XZ_Step::~Virtual_Ctrl_XZ_Step(){
}

void Virtual_Ctrl_XZ_Step::_get_smooth_changing_foot_force(const Vector & start_force, const Vector & end_force, double duration, double curr_time, Vector & ret_force){
    ret_force = Vector::Zero(4);
    // X
    ret_force[2] = _smooth_changing(start_force[0], end_force[0], duration, curr_time);
    // Z
    ret_force[3] = _smooth_changing(start_force[2], end_force[2], duration, curr_time);
}

bool Virtual_Ctrl_XZ_Step::_Supp_Lift_transition(){
    static bool b_transit(false);
    
    Vector gamma_single_contact, gamma_dual_contact;
    static Vector re_force_dual;

    if(!b_transit){
        std::vector<Task*> dual_contact_task_array;
        _set_dual_contact_task();
        dual_contact_task_array.push_back(dual_contact_task_);
        wbc_2_->MakeTorque(curr_conf_, dual_contact_task_array, constraint_, gamma_dual_contact);
        Cal_FootForce(swing_foot_, gamma_dual_contact, re_force_dual);
        b_transit = true;
    }
    
    // Push down desired foot position
    foot_des_ = foot_pre_;
    foot_des_[2] = 0.0;
    foot_vel_des_ = Vector::Zero(3);

    switch(swing_foot_){
    case RFOOT:
        _set_single_contact_task();
        _get_smooth_changing_foot_force(re_force_dual, Vector::Zero(3), supp_lift_transition_time_, state_machine_time_, single_contact_rfoot_->force_);
        task_array_.push_back(single_contact_rfoot_);

        wbc_->MakeTorque(curr_conf_, task_array_, constraint_left_, gamma_single_contact);
        break;
        
    case LFOOT:
        _set_single_contact_task();
        _get_smooth_changing_foot_force(re_force_dual, Vector::Zero(3), supp_lift_transition_time_, state_machine_time_, single_contact_lfoot_->force_);
        task_array_.push_back(single_contact_lfoot_);
        
        wbc_->MakeTorque(curr_conf_, task_array_, constraint_right_, gamma_single_contact);
        break;
    }
    gamma_ = gamma_single_contact;

    if(state_machine_time_ > supp_lift_transition_time_){
        _end_action();
        _set_apex_foot_location();
        b_transit = false;
        return false;
    }
    state_machine_time_ +=SERVO_RATE;
    return true;
}


bool Virtual_Ctrl_XZ_Step::_Land_Supp_transition(){
    static bool b_transit(false);
    
    Vector gamma_single_contact, gamma_dual_contact;
    Vector re_force_single;
    static Vector re_force_dual;
    double pushing_ratio(0.05);
    std::vector<Task*> dual_contact_task_array;
    _set_dual_contact_task();

    if(!b_transit){
        _set_dual_contact_task();
        dual_contact_task_array.push_back(dual_contact_task_);
        wbc_2_->MakeTorque(curr_conf_, dual_contact_task_array, constraint_, gamma_dual_contact);
        Cal_FootForce(swing_foot_, gamma_dual_contact, re_force_dual);
        b_transit = true;
    }
    
    // Push down desired foot position
    foot_des_ = foot_pre_;
    foot_des_[2] = foot_pre_[2] - pushing_ratio * state_machine_time_;
    foot_vel_des_ = Vector::Zero(3);

    switch(swing_foot_){
    case RFOOT:
        _set_single_contact_task();
        _get_smooth_changing_foot_force(Vector::Zero(3), re_force_dual, land_supp_transition_time_, state_machine_time_, single_contact_rfoot_->force_);
        
        task_array_.push_back(single_contact_rfoot_);
        
        wbc_->MakeTorque(curr_conf_, task_array_, constraint_left_, gamma_single_contact);
        Cal_FootForce(swing_foot_, gamma_single_contact, re_force_single);
        break;
        
    case LFOOT:
        _set_single_contact_task();
        _get_smooth_changing_foot_force(Vector::Zero(3), re_force_dual, land_supp_transition_time_, state_machine_time_, single_contact_lfoot_->force_);
        
        task_array_.push_back(single_contact_lfoot_);
        
        wbc_->MakeTorque(curr_conf_, task_array_, constraint_right_, gamma_single_contact);

        Cal_FootForce(swing_foot_, gamma_single_contact, re_force_single);
        break;
    }

    bool both_contact(false);
    if(hume_system_->RFoot_Contact() && hume_system_->LFoot_Contact()){ both_contact = true; } 
    if( both_contact && re_force_single[2] < re_force_dual[2] * 0.8){
        printf("Change to Dual Contact\n");
        _end_action();
        b_transit = false;
        return false;
    }
    // if(both_contact && state_machine_time_ > supp_lift_transition_time_){
    //     printf("Couldn't reduce reaction force enough\n");
    //     _end_action();
    //     b_transit = false;
    //     return false;
    // }
    gamma_ = gamma_single_contact;

    state_machine_time_ +=SERVO_RATE;
    return true;
}

void Virtual_Ctrl_XZ_Step::_set_apex_foot_location(){
    apex_foot_loc_ = foot_pre_;
    apex_foot_loc_[2] = lifting_height_;
}

bool Virtual_Ctrl_XZ_Step::_Do_Supp_Land(){
    for (int i(0); i< 3; ++i){
        foot_des_[i] = _smooth_changing(foot_pre_[i], landing_loc_[i],
                                        landing_time_, state_machine_time_);
        
        foot_vel_des_[i] = _smooth_changing_vel(foot_pre_[i], landing_loc_[i],
                                                landing_time_, state_machine_time_);
    }
    if(state_machine_time_>landing_time_){
        printf("Contact is not occured Yet, But Move on...\n");
        _end_action();
        return false;
    }
    _set_single_contact_task();
    
    switch(swing_foot_){
    case RFOOT:
        task_array_.push_back(single_contact_rfoot_);
        
        if(hume_system_->RFoot_Contact()){
            printf("Right foot contact occurs\n");
            _end_action();
            return false;
        }
        wbc_->MakeTorque(curr_conf_, task_array_, constraint_left_, gamma_);
        break;

    case LFOOT:
        task_array_.push_back(single_contact_lfoot_);

        if(hume_system_->LFoot_Contact()){
            printf("Left foot contact occurs\n");
            _end_action();
            return false;
        }
        wbc_->MakeTorque(curr_conf_, task_array_, constraint_right_, gamma_);
    }
    state_machine_time_ += SERVO_RATE;
    return true;
}

void Virtual_Ctrl_XZ_Step::_set_dual_contact_task(){
    Vector des(2);
    Vector vel_des(2);
    // Height
    des[0] = pos_ini_[2];
    vel_des[0] = 0.0;
    // Pitch
    des[1] = ori_ini_[1];
    vel_des[1] = 0.0;
    
    dual_contact_task_->SetTask(des, vel_des);
}

void Virtual_Ctrl_XZ_Step::_set_single_contact_task(){
    Vector des(4);
    Vector vel_des(4);
    // Height
    des[0] = pos_ini_[2];
    vel_des[0] = 0.0;
    // Pitch
    des[1] = ori_ini_[1];
    vel_des[1] = 0.0;
    
    des[2] = foot_des_[0];
    des[3] = foot_des_[2];

    vel_des[2] = foot_vel_des_[0];
    vel_des[3] = foot_vel_des_[2];
    
    switch(swing_foot_){
    case LFOOT:
        single_contact_lfoot_->SetTask(des, vel_des);
        break;
    case RFOOT:
        single_contact_rfoot_->SetTask(des, vel_des);
        break;
    }
}
bool Virtual_Ctrl_XZ_Step::_Supp(){
    if(state_machine_time_ > supp_time_){
        _end_action();
        return false;
    }
    _set_dual_contact_task();

    task_array_.push_back(dual_contact_task_);
    wbc_->MakeTorque(curr_conf_, task_array_, constraint_, gamma_);
    hume_system_->CopyFoot(foot_des_, swing_foot_);

    state_machine_time_ +=SERVO_RATE;
    return true;
}





//////////////////////////////////////////////////////////////
//////////////////// Ctrl 3D Step ////////////////////////////
//////////////////////////////////////////////////////////////

Virtual_Ctrl_Step::Virtual_Ctrl_Step(HUME_System* _hume_system): Ctrl_Step(_hume_system){
}
Virtual_Ctrl_Step::~Virtual_Ctrl_Step(){
}

void Virtual_Ctrl_Step::_get_smooth_changing_foot_force(const Vector & start_force, const Vector & end_force, double duration, double curr_time, Vector & ret_force){
    ret_force = Vector::Zero(6);
    // X
    ret_force[3] = _smooth_changing(start_force[0], end_force[0], duration, curr_time);
    // Y
    ret_force[4] = _smooth_changing(start_force[1], end_force[1], duration, curr_time);
    // Z
    ret_force[5] = _smooth_changing(start_force[2], end_force[2], duration, curr_time);
}

bool Virtual_Ctrl_Step::_Supp_Lift_transition(){
    static bool b_transit(false);
    
    Vector gamma_single_contact, gamma_dual_contact;
    static Vector re_force_dual;

    if(!b_transit){
        std::vector<Task*> dual_contact_task_array;
        _set_dual_contact_task();
        dual_contact_task_array.push_back(dual_contact_task_);
        wbc_2_->MakeTorque(curr_conf_, dual_contact_task_array, constraint_, gamma_dual_contact);
        Cal_FootForce(swing_foot_, gamma_dual_contact, re_force_dual);
        b_transit = true;
    }
    
    // Push down desired foot position
    foot_des_ = foot_pre_;
    foot_des_[2] = 0.0;
    foot_vel_des_ = Vector::Zero(3);

    switch(swing_foot_){
    case RFOOT:
        _set_single_contact_task();
        _get_smooth_changing_foot_force(re_force_dual, Vector::Zero(3), supp_lift_transition_time_, state_machine_time_, single_contact_rfoot_->force_);
        task_array_.push_back(single_contact_rfoot_);

        wbc_->MakeTorque(curr_conf_, task_array_, constraint_left_, gamma_single_contact);
        break;
        
    case LFOOT:
        _set_single_contact_task();
        _get_smooth_changing_foot_force(re_force_dual, Vector::Zero(3), supp_lift_transition_time_, state_machine_time_, single_contact_lfoot_->force_);
        task_array_.push_back(single_contact_lfoot_);
        
        wbc_->MakeTorque(curr_conf_, task_array_, constraint_right_, gamma_single_contact);
        break;
    }
    gamma_ = gamma_single_contact;

    if(state_machine_time_ > supp_lift_transition_time_){
        _end_action();
        _set_apex_foot_location();
        b_transit = false;
        return false;
    }
    state_machine_time_ +=SERVO_RATE;
    return true;
}


bool Virtual_Ctrl_Step::_Land_Supp_transition(){
    static bool b_transit(false);
    
    Vector gamma_single_contact, gamma_dual_contact;
    Vector re_force_single;
    static Vector re_force_dual;
    double pushing_ratio(0.05);
    std::vector<Task*> dual_contact_task_array;
    _set_dual_contact_task();

    if(!b_transit){
        _set_dual_contact_task();
        dual_contact_task_array.push_back(dual_contact_task_);
        wbc_2_->MakeTorque(curr_conf_, dual_contact_task_array, constraint_, gamma_dual_contact);
        Cal_FootForce(swing_foot_, gamma_dual_contact, re_force_dual);
        b_transit = true;
    }
    
    // Push down desired foot position
    foot_des_ = foot_pre_;
    foot_des_[2] = foot_pre_[2] - pushing_ratio * state_machine_time_;
    foot_vel_des_ = Vector::Zero(3);

    switch(swing_foot_){
    case RFOOT:
        _set_single_contact_task();
        _get_smooth_changing_foot_force(Vector::Zero(3), re_force_dual, land_supp_transition_time_, state_machine_time_, single_contact_rfoot_->force_);
        
        task_array_.push_back(single_contact_rfoot_);
        
        wbc_->MakeTorque(curr_conf_, task_array_, constraint_left_, gamma_single_contact);
        Cal_FootForce(swing_foot_, gamma_single_contact, re_force_single);
        break;
        
    case LFOOT:
        _set_single_contact_task();
        _get_smooth_changing_foot_force(Vector::Zero(3), re_force_dual, land_supp_transition_time_, state_machine_time_, single_contact_lfoot_->force_);
        
        task_array_.push_back(single_contact_lfoot_);
        
        wbc_->MakeTorque(curr_conf_, task_array_, constraint_right_, gamma_single_contact);

        Cal_FootForce(swing_foot_, gamma_single_contact, re_force_single);
        break;
    }

    bool both_contact(false);
    if(hume_system_->RFoot_Contact() && hume_system_->LFoot_Contact()){ both_contact = true; } 
    if( both_contact && re_force_single[2] < re_force_dual[2] * 0.8){
        printf("Change to Dual Contact\n");
        _end_action();
        b_transit = false;
        return false;
    }
    gamma_ = gamma_single_contact;

    state_machine_time_ +=SERVO_RATE;
    return true;
}

void Virtual_Ctrl_Step::_set_dual_contact_task(){
    Vector des(3);
    Vector vel_des(3);
    // Height
    des[0] = pos_ini_[2];
    vel_des[0] = 0.0;
    // Pitch
    des[1] = ori_ini_[1];
    vel_des[1] = 0.0;
    // Roll
    des[2] = ori_ini_[2];
    vel_des[2] = 0.0;
    
    dual_contact_task_->SetTask(des, vel_des);
}

void Virtual_Ctrl_Step::_set_single_contact_task(){
    Vector des(6);
    Vector vel_des(6);
    // Height
    des[0] = pos_ini_[2];
    vel_des[0] = 0.0;
    // Pitch
    des[1] = ori_ini_[1];
    vel_des[1] = 0.0;
    // Roll 
    des[2] = ori_ini_[2];
    vel_des[2] = 0.0;

    for (int i(0); i<3; ++i){
        des[i+3] = foot_des_[i];
        vel_des[i+3] = foot_vel_des_[i];
    }
    
    switch(swing_foot_){
    case LFOOT:
        single_contact_lfoot_->SetTask(des, vel_des);
        break;
    case RFOOT:
        single_contact_rfoot_->SetTask(des, vel_des);
        break;
    }
}
bool Virtual_Ctrl_Step::_Supp(){
    if(state_machine_time_ > supp_time_){
        _end_action();
        return false;
    }
    _set_dual_contact_task();

    task_array_.push_back(dual_contact_task_);
    wbc_->MakeTorque(curr_conf_, task_array_, constraint_, gamma_);
    hume_system_->CopyFoot(foot_des_, swing_foot_);

    state_machine_time_ +=SERVO_RATE;
    return true;
}
