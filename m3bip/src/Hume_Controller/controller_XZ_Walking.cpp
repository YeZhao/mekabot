#include "controller_XZ_Walking.h"

#include <math.h>
#include <stdio.h>
#include "analytic_solution/analytic_sol.h"
#include "analytic_solution/analytic_jacobian.h"
#include "analytic_solution/analytic_forward_kin.h"
#include "util/pseudo_inverse.hpp"
#include "Process.h"

#include "DH_ICARCV_planner.h"
#include "Gray_planner.h"

#define LANDING_SUPPORT_TRANSITION_TIME 0.04
#define SUPPORT_LIFT_TRANSITION_TIME 0.04
#define LANDING_TIME 0.26
#define LIFTING_TIME 0.24
#define SUPPORTING_TIME 0.02

Ctrl_XZ_Walking::Ctrl_XZ_Walking(HUME_System* _hume_system): Virtual_Ctrl_XZ_Step(_hume_system),
                                                             body_vel_ave_(Vector::Zero(3)),
                                                             body_vel_pre_(Vector::Zero(3)),
                                                             body_vel_pre_2_(Vector::Zero(3)),
                                                             body_vel_pre_3_(Vector::Zero(3)),
                                                             vel_est_(0.0),
                                                             walking_length_(0.0)
{
    com_x_offset_ = -0.0; // 0.8* 5.786/(5.786 + 22.87)
    com_z_offset_ = -0.2;
    //
    stance_foot_ = RFOOT;
    swing_foot_ = LFOOT;
    lifting_time_ = LIFTING_TIME;
    landing_time_ = LANDING_TIME;
    supp_time_    = SUPPORTING_TIME;

    supp_lift_transition_time_ = SUPPORT_LIFT_TRANSITION_TIME;
    land_supp_transition_time_ = LANDING_SUPPORT_TRANSITION_TIME;

    lifting_height_ = 0.16;


    // for Planner
    achieving_ratio_ = 0.85;
    middle_time_x_ = 0.24;
    middle_time_y_ = 0.24;
    
    constraint_ = new Hume_Contact_Both_Abduction();
    constraint_left_ = new Hume_Point_Contact_Abduction(LFOOT);
    constraint_right_ = new Hume_Point_Contact_Abduction(RFOOT);

    // dual_contact_task_ = new Height_Tilting_Task(hume_system_);
    // single_contact_lfoot_ = new Height_Tilting_Foot_XZ_Task(hume_system_, LFOOT);
    // single_contact_rfoot_ = new Height_Tilting_Foot_XZ_Task(hume_system_, RFOOT);
    // XZ_tilting_task_ = new XZ_Tilting_Task(hume_system_);

    // COM
    dual_contact_task_ = new COM_Height_Tilting_Task(hume_system_);
    single_contact_lfoot_ = new COM_Height_Tilting_Foot_XZ_Task(hume_system_, LFOOT);
    single_contact_rfoot_ = new COM_Height_Tilting_Foot_XZ_Task(hume_system_, RFOOT);
    XZ_tilting_task_ = new COM_Tilting_Task(hume_system_);
    
    
    filter_x_vel_ = new digital_lp_filter(CUT_OFF_FRE_X, SERVO_RATE);
    // filter_x_vel_ = new butterworth_filter((int)1.0/SERVO_RATE, SERVO_RATE, CUT_OFF_FRE_X);
    // filter_x_vel_ = new moving_average_filter(10);

    // butter_filter_x_vel_.SetSampleRate(1.0/SERVO_RATE);
    // butter_filter_x_vel_.Set(20.f, 0.0);
    
    printf("[XZ Walking Boom Control] Start\n");
    
    // planner_ = new Gray_planner();
    planner_ = new phase_planner();
    planner_->start();
}

Ctrl_XZ_Walking::~Ctrl_XZ_Walking(){
}

void Ctrl_XZ_Walking::inverted_pendulum_filter(double pos, double vel, double ratio, double foot_pos, double height){
    vel_est_ = ratio * vel_est_ + (1.0 - ratio) * vel;
    _pendulum_update(pos, foot_pos, height);
    
}
void Ctrl_XZ_Walking::_pendulum_update(double pos, double foot_pos, double height){
    double g(9.8);
    double acc = g/height * (pos - foot_pos);
    vel_est_ = vel_est_  + acc * SERVO_RATE;
}

void Ctrl_XZ_Walking::getCurrentCommand(std::vector<double> & command){
    _PreProcessing_Command();
    ++count_command_;
    
    double curr_time = ((double)count_command_)*SERVO_RATE;
    
    if(pro_stable_->do_process(this)){
        _PostProcessing_Command(command);
        count_command_ = 0;
        return;
    }
    if(_move_forward()){
        _PostProcessing_Command(command);
        count_command_ = 0;
        return;
    }
    
    // static int curr_phase = LsDu;
    static int curr_phase = RsDu;
    phase_ = curr_phase;

    // Filter ***
    // filter_x_vel_ -> input(hume_system_->getBody_vel()[0]);

    // COM
    filter_x_vel_ -> input(hume_system_->COM_vel_[0]);

    // Butterworth **
    // body_vel_ave_[0] = butter_filter_x_vel_.Run(hume_system_->getBody_vel()[0]);
    // For Save (Temp)

    // Model Base ******
    // 0.5 ^ ( 1 / t )
    // t (ms: SERVO RATE): low pass filter half life

    // Body 
    // inverted_pendulum_filter(hume_system_->getBody()[0] + com_x_offset_,
    //                          hume_system_->getBody_vel()[0],
    //                          0.9965,
    //                          hume_system_->getFoot(stance_foot_)[0],
    //                          hume_system_->getBody()[2] + com_z_offset_);

    // COM
    inverted_pendulum_filter(hume_system_->COM_pos_[0] + com_x_offset_,
                             hume_system_->COM_vel_[0],
                             0.9965,
                             hume_system_->getFoot(stance_foot_)[0],
                             hume_system_->COM_pos_[2] + com_z_offset_);

    if(num_step_ < 1){
        body_vel_ave_[0] = filter_x_vel_->output();
    }
    else {
        body_vel_ave_[0] = vel_est_;
    }
    hume_system_->body_vel_ave_ = body_vel_ave_[0];    
    // Just for Save...I need to fix this later
    // hume_system_->body_vel_ave_ = vel_est_;
    // hume_system_->body_pos_ave_ = pos_est_;
    
    if(_IsEndCurrState(curr_phase)){
        phase_ = curr_phase;
        ++curr_phase;
        //Stop
        if(walking_length_  + landing_loc_[0] > 3.0 && (curr_phase == RsLm || curr_phase == LsRm)){
            curr_phase = STOP;
        }
        
        if(curr_phase == LsRs || curr_phase == RsLs){
            ++num_step_;
            printf("Number of Step: %i \n", num_step_);
        }
        if(curr_phase > 9) { curr_phase = 0; }
        // Landing Location Save
        if(curr_phase == LsDu || curr_phase == RsDu){
            jspace::saveVector(hume_system_->getFoot(swing_foot_), "landing_loc");
            walking_length_ += hume_system_->getFoot(swing_foot_)[0];
            printf("******LANDING LOCATION: %f \n", hume_system_->getFoot(swing_foot_)[0]);
            printf("******WALKING LENGTH: %f \n", walking_length_);
        }
        _printf_phase(curr_phase);
    }
    _PostProcessing_Command(command);
}

bool Ctrl_XZ_Walking::_Stop(){
    double sagging_height(0.01);
    // State,
    // 0: Landing
    // 1: Transition
    // 2: Slow down
    static int state(0);
    static double ini_vel_x(0.0);
    static double ini_pos_x(0.0);
    static double breaking_time(1.0);
    static double a (-0.5);
    double breaking_length(0.35);
    static double slope(0.2);
    static double orig_landing_loc;
    static bool stop_start(false);
    if(!stop_start){
        orig_landing_loc = landing_loc_[0];
        landing_loc_[0] = orig_landing_loc + 0.40;
        landing_time_ = 0.35;
        _set_foot_x_traj_coeff(foot_vel_des_[0]);
        printf("landing loc: %f \n", landing_loc_[0]);
        stop_start = true;
    }
    switch (state){
    case 0:
        //Right Foot X
        ((Height_Tilting_Foot_XZ_Task*)single_contact_rfoot_)->Kp_[2] = 7550.0;
        // ((Height_Tilting_Foot_XZ_Task*)single_contact_rfoot_)->Kd_[2] = 400.0;
        // ((Height_Tilting_Foot_XZ_Task*)single_contact_rfoot_)->Ki_[2] = 1300.0;
        // // Left Foot X
        ((Height_Tilting_Foot_XZ_Task*)single_contact_lfoot_)->Kp_[2] = 7550.0;
        // ((Height_Tilting_Foot_XZ_Task*)single_contact_lfoot_)->Kd_[2] = 400.0;
        // ((Height_Tilting_Foot_XZ_Task*)single_contact_lfoot_)->Ki_[2] = 1300.0;
        if(!_Do_Supp_Land()){
            ++state;
            _end_action();
            printf("Landing is finished\n");
        }
        break;
    case 1:
        if(!_Land_Supp_transition()){
            ++state;

            ini_vel_x = body_vel_ave_[0];
            ini_pos_x = hume_system_->tot_configuration_[Px];
            breaking_time = ini_vel_x/slope;
            if(breaking_time > 1.3){
                breaking_time = 1.3;
                slope = ini_vel_x/breaking_time;
                a = - ini_vel_x/ breaking_length;
            }
            _end_action();
            printf("Breaking Time: %f \n", breaking_time);
            printf("Landing Transition is finished\n");
        }
        break;
    case 2:
        Vector des(3);
        Vector vel_des(Vector::Zero(3));
        if(state_machine_time_ < breaking_time){
            des[0] = ini_pos_x - breaking_length * exp ( a* state_machine_time_ ) + breaking_length;
            des[1] = _smooth_changing(pos_ini_[2], pos_ini_[2] - sagging_height, breaking_time, state_machine_time_);
            des[2] = ori_ini_[1];
            // Velocity
            vel_des[0] = ini_vel_x * exp( a* state_machine_time_);
            vel_des[1] = _smooth_changing_vel(pos_ini_[2], pos_ini_[2] - sagging_height,
                                              breaking_time,
                                              state_machine_time_);
        }
        else{
            des[0] = ini_pos_x - breaking_length * exp ( a* breaking_time ) + breaking_length;
            des[1] = pos_ini_[2] - sagging_height;
            des[2] = ori_ini_[1];
        }
        ((XZ_Tilting_Task*)XZ_tilting_task_)->Kp_[0] = 100.0;
        ((XZ_Tilting_Task*)XZ_tilting_task_)->Kp_[2] = 300.0;
        XZ_tilting_task_->SetTask(des, vel_des);
        task_array_.push_back(XZ_tilting_task_);
        wbc_->MakeTorque(curr_conf_, task_array_, constraint_, gamma_);
        break;
    }
    state_machine_time_ += SERVO_RATE;
    return true;
}

bool Ctrl_XZ_Walking::_move_forward(){
    phase_ = 11;
    static bool b_start (false);
    if(b_start){
        return false;
    }
    Vector des(3);
    Vector vel_des(Vector::Zero(3));
    double push_vel(0.8);
    Vector re_force;
    // X
    des[0] = pos_ini_[0] + 0.5*push_vel*pow(state_machine_time_, 2.0);
    vel_des[0] = push_vel*state_machine_time_;
    // Z
    des[1] = pos_ini_[2];
    // Pitch
    des[2] = ori_ini_[1];

#ifndef STEP_TEST    
    if(vel_des[0] > 0.3 && des[0] >  -0.11){
#else
    if(true){
#endif
        printf("[Move Forward] End before reaching reaction force threshold\n");
        b_start = true;
        _end_action();
        return false;
    }
    XZ_tilting_task_->SetTask(des, vel_des);
    task_array_.push_back(XZ_tilting_task_);
    
    wbc_->MakeTorque(curr_conf_, task_array_, constraint_, gamma_);

    Cal_FootForce(swing_foot_, gamma_, re_force);
    state_machine_time_ += SERVO_RATE;
    return true;
}

bool Ctrl_XZ_Walking::CalculateLandingLocation(const Vector & stance_foot_loc, double current_time, double local_switch_time, LinkID stance_foot){

    // COM
    Vector COM_Position(hume_system_->COM_pos_);
    // X offset
    COM_Position[0] += com_x_offset_;
    // COM_Position[0] = pos_est_;
    // Z offset
    COM_Position[2] += com_z_offset_;

    
    if(planner_->Calculate_SwitchTime(COM_Position,
                                      // COM
                                      hume_system_->COM_vel_,
                                      // body_vel_ave_,
                                      stance_foot_loc,
                                      stance_foot_,
                                      landing_time_,
                                      lifting_time_,
                                      achieving_ratio_,
                                      LANDING_SUPPORT_TRANSITION_TIME,
                                      SUPPORT_LIFT_TRANSITION_TIME,
                                      supp_time_,
                                      middle_time_x_,
                                      middle_time_y_)){
        //
        landing_time_ = planner_->GetSwitchingTime()
            - (1.0 - achieving_ratio_) * lifting_time_
            - land_supp_transition_time_;
        printf("landing time: %f \n", landing_time_);
        planner_->CopyNextFootLoc(landing_loc_);
#ifdef STEP_TEST
        landing_time_ = LANDING_TIME;
        landing_loc_[0] = hume_system_->COM_pos_[0];
#endif
        return true;
    }
    return false;
}

bool Ctrl_XZ_Walking::CalculateLandingLocation2(const Vector & stance_foot_loc, double current_time, LinkID stance_foot){
    
    Vector COM_Position;
    hume_system_->CopyBody(COM_Position);
    // X offset
    COM_Position[0] += com_x_offset_;
    // Z offset
    COM_Position[2] += com_z_offset_;
    
    if(planner_->Calculate_SwitchTime(COM_Position,
                                      body_vel_ave_,
                                      stance_foot_loc,
                                      stance_foot_,
                                      landing_time_,
                                      lifting_time_,
                                      achieving_ratio_,
                                      LANDING_SUPPORT_TRANSITION_TIME,
                                      SUPPORT_LIFT_TRANSITION_TIME,
                                      supp_time_,
                                      middle_time_x_,
                                      middle_time_y_)){
        //
        land_supp_transition_time_ = planner_->GetLandingTransitionTime();
        supp_lift_transition_time_ = planner_->GetLiftingTransitionTime();
        printf("land supp trans time: %f \n", land_supp_transition_time_);
        printf("supp lift trans time: %f \n", supp_lift_transition_time_);
        planner_->CopyNextFootLoc(landing_loc_);
        return true;
    }
    return false;
}

void Ctrl_XZ_Walking::_set_land_location(){
    landing_loc_[1] = 0.0;
}

bool Ctrl_XZ_Walking::_Do_Supp_Lift(){
    double moving_ratio = 2.5;//0.5;
#ifdef STEP_TEST
    moving_ratio = 0.0;
#endif
    const Vector & COM_vel = hume_system_->getBody_vel();
    
    static bool Iscalculated(false);
    //COMMAND
    foot_des_[0] += moving_ratio*body_vel_ave_[0]*SERVO_RATE;
    foot_vel_des_[0] = body_vel_ave_[0] * moving_ratio;
    
#ifndef STEP_TEST
    if(num_step_ < 1){
        foot_des_[0] += 1.5* (body_vel_ave_[0]/fabs(body_vel_ave_[0]))*SERVO_RATE;
        foot_vel_des_[0] = 1.5* body_vel_ave_[0]/fabs(body_vel_ave_[0]) ;
    }
#endif
    // Capture Point
    // double x_dot(body_vel_ave_[0]);
    // double x (hume_system_->getBody()[0]);
    // double h (hume_system_->getBody()[2]);
    // double g (9.8                       ); 
    // foot_des_[0] = (1 - state_machine_time_)/ lifting_time_ * foot_pre_[0] + 
    //     state_machine_time_/ lifting_time_* 0.5 *(x_dot + sqrt(g/h)*x);
    // foot_vel_des_[0] = state_machine_time_ / lifting_time_ * 0.5*(g/h*x + sqrt(g/h)*x_dot);
    
    //Foot Height
    foot_des_[2] = _smooth_changing(foot_pre_[2], apex_foot_loc_[2], lifting_time_, state_machine_time_);
    foot_vel_des_[2] = _smooth_changing_vel(foot_pre_[2], apex_foot_loc_[2], lifting_time_, state_machine_time_);
    _set_single_contact_task();

    if( state_machine_time_ > achieving_ratio_ *lifting_time_ && !Iscalculated){
        Iscalculated = CalculateLandingLocation(hume_system_->getFoot(stance_foot_), state_machine_time_, switching_time_  + tot_transition_time_, swing_foot_);
        // Iscalculated = CalculateLandingLocation2(hume_system_->getFoot(stance_foot_), state_machine_time_, swing_foot_);

    }

    switch(swing_foot_){
    case RFOOT:
        task_array_.push_back(single_contact_rfoot_);
        wbc_->MakeTorque(curr_conf_, task_array_, constraint_left_, gamma_);
        break;
    case LFOOT:
        task_array_.push_back(single_contact_lfoot_);
        wbc_->MakeTorque(curr_conf_, task_array_, constraint_right_, gamma_);
        break;
    }
    /////////////////////////////////////////////////////
    if(state_machine_time_ > lifting_time_){
        if(Iscalculated){
            _end_action();
            Iscalculated = false;
            _set_foot_x_traj_coeff(foot_vel_des_[0]);
            return false;
        }
        else{
            printf("Cannot go to the next phase on time\n");
            _end_action();
            Iscalculated = false;
            exit(0);
            return false;
        }
    }
    state_machine_time_ += SERVO_RATE;        
    return true;
}


void Ctrl_XZ_Walking::_set_foot_x_traj_coeff(double curr_vel){
    double tf(landing_time_);
    double xf_dot(0.0);
    double xf(landing_loc_[0]);
    double x0(foot_pre_[0]);
    double x0_dot(curr_vel);
    
    d = x0;
    c = x0_dot;

    Matrix tmp_inv(2,2);
    Matrix tmp_coeff(2,2);
    tmp_coeff(0,0) = pow(tf, 3.0);
    tmp_coeff(0,1) = pow(tf,2.0);
    //
    tmp_coeff(1,0) = 3*pow(tf, 2.0);
    tmp_coeff(1,1) = 2*tf;

    pseudoInverse(tmp_coeff, 0.001, tmp_inv, 0);

    Vector tmp_vec(2);
    Vector coeff(2);
    tmp_vec[0] = xf - (c*tf + d);
    tmp_vec[1] = xf_dot - c;

    coeff = tmp_inv * tmp_vec;

    a = coeff[0];
    b = coeff[1];
}

bool Ctrl_XZ_Walking::_Do_Supp_Land(){
    // Height
    foot_des_[2] = _smooth_changing(foot_pre_[2], landing_loc_[2],
                                    landing_time_,
                                    state_machine_time_);
    foot_vel_des_[2] = _smooth_changing_vel(foot_pre_[2], landing_loc_[2],
                                            landing_time_,
                                            state_machine_time_);
    //X
    foot_des_[0] = a * pow(state_machine_time_, 3.0) +
        b * pow(state_machine_time_, 2.0) +
        c * state_machine_time_ +
        d;
    foot_vel_des_[0] = 3* a * pow(state_machine_time_, 2.0) +
        2 * b * state_machine_time_ +
        c;
    
    if(state_machine_time_>landing_time_){
        printf("Contact is not occured Yet, But Move on...\n");
        _end_action();
        return false;
    }
    _set_single_contact_task();
    //Right Foot X
    // ((Height_Tilting_Foot_XZ_Task*)single_contact_rfoot_)->Kp_[2] = 750.0;
    // ((Height_Tilting_Foot_XZ_Task*)single_contact_rfoot_)->Kd_[2] = 400.0;
    // ((Height_Tilting_Foot_XZ_Task*)single_contact_rfoot_)->Ki_[2] = 1300.0;
    // // Left Foot X
    // ((Height_Tilting_Foot_XZ_Task*)single_contact_lfoot_)->Kp_[2] = 750.0;
    // ((Height_Tilting_Foot_XZ_Task*)single_contact_lfoot_)->Kd_[2] = 400.0;
    // ((Height_Tilting_Foot_XZ_Task*)single_contact_lfoot_)->Ki_[2] = 1300.0;

    switch(swing_foot_){
    case RFOOT:
        task_array_.push_back(single_contact_rfoot_);
        
        if(hume_system_->RFoot_Contact()){
            printf("Right foot contact occurs\n");
            _end_action();
            return false;
        }
        wbc_->MakeTorque(curr_conf_, task_array_, constraint_left_, gamma_);
        break;

    case LFOOT:
        task_array_.push_back(single_contact_lfoot_);

        if(hume_system_->LFoot_Contact()){
            printf("Left foot contact occurs\n");
            _end_action();
            return false;
        }
        wbc_->MakeTorque(curr_conf_, task_array_, constraint_right_, gamma_);
    }
    state_machine_time_ += SERVO_RATE;
    return true;
}
