#include "WBC.h"
#include "HumeSystem.h"
#include <stdio.h>
#include "util/pseudo_inverse.hpp"

#define INTER_FORCE

Controller_WBC::Controller_WBC(HUME_System * hume_system, int idx): data_updated_(false),
                                                                    Js(Matrix::Identity(1,1)),
                                                                    jac(Matrix::Identity(1,1 )),
                                                                    U(Matrix::Identity(1,1 )),
                                                                    ainv(Matrix::Identity(1,1)),
                                                                    W_int(Matrix::Identity(1,1)),
                                                                    grav(Vector::Zero(NUM_JOINT))
                                                           
{
    count = 0;
    hume_system_ = hume_system;

    char Js_file_name  [100]; 
    char Jt_file_name  [100]; 
    char U_file_name   [100];    
    char Ainv_file_name[100]; 
    char Wint_file_name[100]; 
    char grav_file_name[100]; 

    
    sprintf(Js_file_name  , "%s%s%s_%i.txt", THIS_COM, "experiment_data/", "Js", idx);
    sprintf(Jt_file_name  , "%s%s%s_%i.txt", THIS_COM, "experiment_data/", "Jt", idx);
    sprintf(U_file_name   , "%s%s%s_%i.txt", THIS_COM, "experiment_data/", "U", idx);
    sprintf(Ainv_file_name, "%s%s%s_%i.txt", THIS_COM, "experiment_data/", "Ainv", idx);
    sprintf(Wint_file_name, "%s%s%s_%i.txt", THIS_COM, "experiment_data/", "Wint", idx);
    sprintf(grav_file_name, "%s%s%s_%i.txt", THIS_COM, "experiment_data/", "grav", idx);
    
    remove(Js_file_name  );  
    remove(Jt_file_name  ); 
    remove(U_file_name   ); 
    remove(Ainv_file_name); 
    remove(Wint_file_name);  
    remove(grav_file_name);
    
    Js_file  .open(Js_file_name  );    
    Jt_file  .open(Jt_file_name  );    
    U_file   .open(U_file_name   );     
    Ainv_file.open(Ainv_file_name);  
    Wint_file.open(Wint_file_name);
    grav_file.open(grav_file_name);
}
Controller_WBC::~Controller_WBC(){
    Js_file.close();
    Jt_file.close();
    U_file.close();
    Ainv_file.close();
    Wint_file.close();
    grav_file.close();
}
void  Controller_WBC::MakeTorque(const vector<double> & conf, vector<Task*> task_array, WBC_Constraint* constraint, Vector & gamma)
{
    config_ = conf;
    /// Check Task
    for (int i(0); i< task_array.size(); ++i){
        if(!task_array[i]->b_settask_){
            printf("Task Setting is incompleted\n");
            exit(0);
        }
    }
    
    double PSEUDO_INV_THRESHOLD(0.001);
    //////////////////////////////////////////////////
    // the magic nullspace sauce...
    
    if ( ! hume_system_->getInverseMassInertia(ainv)) {
        printf("failed to retrieve inverse mass inertia\n");
    }
    
    if ( ! hume_system_->getGravity(grav)) {
        printf("failed to retrieve gravity torques\n");
    }
    
    if (constraint) {
        if(!constraint->updateJcU(conf, hume_system_)) {
            printf( "failed to update Jc\n");
        }
        Js = constraint->Jc_;
    }
    Matrix Nc;
    if (constraint) {
        if (!constraint->getNc(ainv, Nc)) {
            printf("failed to get Nc\n");
        }
    }
    else {
        Nc = Matrix::Identity(NUM_JOINT,NUM_JOINT);
    }
    
    Matrix UNc;
    if (constraint) {
        if (!constraint->getU(U)) {
            printf("failed to get U\n");
        }
        UNc = U*Nc;
    }
    else {
        UNc = Matrix::Identity(NUM_JOINT,NUM_JOINT);
    }  
    
    Matrix phi (UNc * ainv * UNc.transpose());
    Matrix UNcBar;
    if (constraint) {
        Matrix phiinv;
        //XXXX hardcoded sigma threshold
        pseudoInverse(phi, PSEUDO_INV_THRESHOLD, phiinv, 0);
        UNcBar = ainv * UNc.transpose() * phiinv;
    }
    else {
        UNcBar = Matrix::Identity(NUM_JOINT, NUM_JOINT);
    }
    Matrix nstar(Matrix::Identity(NUM_ACT_JOINT, NUM_ACT_JOINT));
    int const n_minus_1(task_array.size() -1);
    
    
///////////////////////////////////////////////////////////////////////
///////////             Multi Task Loop                  //////////////
///////////////////////////////////////////////////////////////////////
    for (size_t ii(0); ii < task_array.size(); ++ii) {
        Task * task(task_array[ii]);
        jac = task->getJacobian(conf);

        Matrix jstar;
        if (ii == 0) {
            jstar = jac * UNcBar;
        }
        else {
            jstar = jac * UNcBar * nstar;
        }

        Matrix jstar_trans = jstar.transpose();
        Matrix jjt(jstar * jstar_trans);
        Vector sv_jstar;

        if (1 == jjt.rows()) {	// work around limitations of Eigen2 SVD.
            sv_jstar = Vector::Ones(1, 1) * jjt.coeff(0, 0);
        }

        Matrix lstar;
        pseudoInverse(jstar * phi * jstar.transpose(), PSEUDO_INV_THRESHOLD, lstar, 0);////&sv_lstar_[ii]);

        Vector pstar;
        pstar = lstar * jstar * UNc * ainv * Nc.transpose()* grav;

        Vector force(task->getForce());

        if (ii == 0) {
            // first time around: initialize gamma
            gamma = jstar.transpose() * (lstar * task->getCommand() + pstar + force);
        }
        else {
            Vector fcomp;
            // here, gamma is still at the previous iteration's value
            fcomp = lstar * jstar * phi * gamma;
            gamma += jstar.transpose() * (lstar * task->getCommand() + pstar + force - fcomp);
        }

        if (ii != n_minus_1){
            Matrix const
                nnext((Matrix::Identity(NUM_ACT_JOINT, NUM_ACT_JOINT) - phi * jstar.transpose()* lstar * jstar)*nstar);
            nstar = nnext;
        }
        data_updated_ = true;
    }

    if(constraint){
#ifdef INTER_FORCE
        if(constraint->reaction_force_control()){
            Matrix Js;
            constraint->getJc(Js);
            Matrix Lambda;
            pseudoInverse(Js * ainv * Js.transpose(), PSEUDO_INV_THRESHOLD, Lambda, 0);
            Matrix Lambda_star;
            Lambda_star = Matrix::Identity(6, 6) - UNc * UNcBar;
            
            Matrix Jsbar, S;

            Jsbar = ainv * Js.transpose() * Lambda;
            
            //Equation(2.21)
            Vector Fr =  Jsbar.transpose()*U.transpose()*gamma - Jsbar.transpose()*grav;

            constraint->getSelection(Fr, S);
            Fr = S*Fr;

            Vector null_tau;
            
            //what is this part?
            Matrix Zstar_bar, Zstar;
            Zstar = S*Jsbar.transpose()*U.transpose()*Lambda_star.transpose();
            Matrix sqrt ( Zstar*Zstar.transpose());
            Matrix sqrt_inv;
            pseudoInverse(sqrt, PSEUDO_INV_THRESHOLD, sqrt_inv, 0);
            Zstar_bar = Zstar.transpose()*sqrt_inv;
            // pseudoInverse(Zstar, PSEUDO_INV_THRESHOLD, Zstar_bar, 0);
            
            null_tau = Zstar_bar*(-Fr);    
            pretty_print(null_tau, std::cout , "reaction force compensator", " ");
            gamma = gamma + Lambda_star.transpose()*null_tau;
        }
///////////////////////////////////////////////
/////       Internal Force Control        
///////////////////////////////////////////////
        if(constraint->remove_internal()){

            constraint->getInternal_Matrix(W_int);
            
            Matrix Js;
            constraint->getJc(Js);
            Matrix Lambda;

            pseudoInverse(Js * ainv * Js.transpose(), PSEUDO_INV_THRESHOLD, Lambda, 0);
            Matrix Lambda_star;
            Lambda_star = Matrix::Identity(6, 6) - UNc * UNcBar;
            
            Matrix Jsbar;
            Vector re_force;
            Vector int_force;
            Jsbar = ainv * Js.transpose() * Lambda;
            re_force = (Jsbar.transpose()*U.transpose()*gamma - Jsbar.transpose()*grav);
            
            int_force =  W_int * re_force;
            Matrix Jil_bar, Jil;
            Vector int_tau_1, int_tau_2;
            
            Jil_bar = W_int * Jsbar.transpose() * U.transpose() * Lambda_star.transpose();
            // //inverse ..
            Matrix sqr_inv;
            pseudoInverse(Jil_bar * Jil_bar.transpose(), PSEUDO_INV_THRESHOLD, sqr_inv, 0);
            Jil = Jil_bar.transpose()*sqr_inv;
            Vector des_int_force;
            constraint->getDesInternalForce(des_int_force);

            //Internal Force Feedback
            Vector K_int (Vector::Zero(2));
            K_int[0] = 1.7;
            K_int[1] = 0.8;

            Vector feedback_internal(2);
            Vector curr_re_force(6);
            for( int i(0); i <3; ++i){
                curr_re_force[i] = hume_system_->curr_RFoot_force_[i];
                curr_re_force[i + 3] = hume_system_->curr_LFoot_force_[i];
            }

            //no z direction control?
            Vector curr_int_force = W_int * curr_re_force;
            for (int i(0); i<2; ++i){
                feedback_internal[i] = K_int[i] * (des_int_force[i] - curr_int_force[i]);
            }

            
            // int_tau_1 = Jil * (des_int_force - int_force  + feedback_internal);
            int_tau_1 = Jil * (des_int_force - int_force);
            // int_tau_1 = Jil * (des_int_force  + feedback_internal);

            
            hume_system_->des_int_force_ = des_int_force;
            hume_system_->ori_int_force_ = int_force;
            
            // pretty_print(des_int_force, std::cout, "desire internal force", "");
            // pretty_print(int_force, std::cout, "[internal force]", "");

            // pretty_print(Jil_bar, std::cout, "jil bar 1", "");
            // pretty_print(Jil, std::cout, "Jil 1", "");
            // pretty_print(int_tau_1, std::cout, "tau 1", "");

            // J2
            // Jil_bar = Lambda_star*U*Jsbar*W_int.transpose();
            // pseudoInverse(Jil_bar, PSEUDO_INV_THRESHOLD, Jil, 0);
            // int_tau_2 = Jil.transpose() * int_force;
            // pretty_print(Jil_bar, std::cout, "jil bar 2", ""); 
            // pretty_print(Jil, std::cout, "Jil 2", "");
            // pretty_print(int_tau_2, std::cout, "tau 2", "");
            // pretty_print(int_tau, std::cout, "int tau", "");
            // pretty_print(Lambda_star, std::cout, "lambda star trans", "");
            // pretty_print(Jil_bar , std::cout, "Jil bar", "");
            // pretty_print(Jil , std::cout, "Jil", "");
            // pretty_print(int_force, std::cout, "int force", "");

            gamma = gamma + Lambda_star.transpose()*int_tau_1;
        }
#endif
    }


}

void Controller_WBC::SaveMatrix(){
    if(data_updated_){
        ++count;
        
        Matrix A_inv;
        Vector Grav;
        hume_system_->getInverseMassInertia(A_inv);
        hume_system_->getGravity(Grav);

        pretty_print(jac  , Jt_file  , "end", "");         
        pretty_print(Js   , Js_file  , "end", "");   
        pretty_print(U    , U_file   , "end", ""); 
        pretty_print(W_int, Wint_file, "end", "");
        pretty_print(Grav ,  grav_file, "end", "");
        pretty_print(A_inv , Ainv_file, "end", "");

        Jt_file  .flush(); 
        Js_file  .flush(); 
        U_file   .flush(); 
        Wint_file.flush(); 
        grav_file.flush(); 
        Ainv_file.flush();        
    }
}
