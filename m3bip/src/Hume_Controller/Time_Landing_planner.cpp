#include "Time_Landing_planner.h"

#include <math.h>
#include "util/wrap_eigen.hpp"

using namespace jspace;
using namespace std;

Time_Landing_planner::Time_Landing_planner() : Planner()
{
    // 0: positive velocity
    // 1: negative velocity
    x_foot_place_limit_(0) =  -0.37; //0.11
    x_foot_place_limit_(1) = 0.37; 
    y_foot_place_limit_(0) =  0.25;
    y_foot_place_limit_(1) = 0.27; //0.4

    boom_system_ratio_ = 1.0; // 22.87/ (22.87 + 5.786) = 0.8
    
    velocity_limit_(0) = 0.95;
    velocity_limit_min_ = 0.25;
    velocity_limit_(1) = -0.95;
}

void Time_Landing_planner::Calculation()
{
    printf("[3D Planner]Calculation Start\n");
    h_ = com_pos_[2];
    foot_pos_ -= com_pos_;
    // Draw The future Phase path before switching time
    Vector x_ini(2);
    x_ini(0) = 0.0;
    x_ini(1) = com_vel_[0];

    vector<Vector> x_phase_path;

    Vector y_ini(2);
    y_ini(0) = 0.0;
    y_ini(1) = com_vel_[1];

    vector<Vector> y_phase_path;

    _MakePhasePath(y_ini, foot_pos_[1], y_phase_path, true);
    _MakePhasePath(x_ini, foot_pos_[0], x_phase_path, true);

    // Set Foot Placement limit
    Vector range_x(2);
    Vector range_y(2);

    range_x(0) = x_foot_place_limit_(0);    //min
    range_x(1) = x_foot_place_limit_(1);    //max

    if (stance_foot_ == RFOOT)
    {    //Right
        range_y(0) = y_foot_place_limit_(0);    //min
        range_y(1) = y_foot_place_limit_(1);    //max
    }
    else if (stance_foot_ == LFOOT)
    {
        range_y(0) = -y_foot_place_limit_(1);    //min
        range_y(1) = -y_foot_place_limit_(0);    //max
    }

    // X  ////////////////////////////////
    next_foot_placement_[0] = foot_placement_planner_.find_foot_placement(*x_phase_path.rbegin(), range_x,
                                                                          middle_time_x_, h_, false);
    printf("****** [X] FOOT position: %f\n\n", next_foot_placement_[0]);

    // Y  ////////////////////////////////
    next_foot_placement_[1] = foot_placement_planner_.find_foot_placement(*y_phase_path.rbegin(), range_y, middle_time_y_, h_, true);
    printf("****** [Y] FOOT position: %f\n\n", next_foot_placement_[1]);

    // Offset from current COM
    for (int i(0); i < 2; ++i)
    {
        next_foot_placement_[i] += com_pos_[i];
    }
    _SavePlannerData(x_phase_path, y_phase_path);
}

bool Time_Landing_planner::Calculate_SwitchTime(const Vector & com_pos, //
                                             const Vector & com_vel, //
                                             const Vector & foot_pos, //
                                             LinkID stance_foot, //
                          double landing_time, //relative to end of lifting time
                          double lifting_time, //duration of total lift trajectory
                          double lifting_ratio, //ratio of planning point to total lift
                          double transition_landing_time, //
                          double transition_lifting_time, //
                          double dual_support_time, //
                          double middle_time_x, //
                          double middle_time_y){
    if(!IsRunning_){
        com_pos_ = com_pos;
        com_vel_ = com_vel;
        foot_pos_ = foot_pos;
        stance_foot_ = stance_foot;
        switching_time_ = (1.0 -  lifting_ratio) * lifting_time + landing_time + transition_landing_time;
        middle_time_x_ = middle_time_x;
        middle_time_y_ = middle_time_y;
        calculating_start_ = true;
        
        printf("\n[3D Planner] Calculating_Switching Time Running Start\n");
    }
     
    if(end_of_calculation_){
        printf("[3D Planner] Calculation_End\n \n");
        end_of_calculation_ = false;
        IsRunning_ = false;
        return true;
    }
    return false;
}
