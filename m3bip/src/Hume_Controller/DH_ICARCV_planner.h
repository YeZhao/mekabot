#ifndef DH_PLANNER_
#define DH_PLANNER_

#include "Planner.h"

using namespace jspace;
using namespace std;

class phase_planner: public Planner
{
public:
    phase_planner();
    virtual ~phase_planner()
	{
	}
    
    bool Calculate_SwitchTime(const Vector & com_pos, //
		const Vector & com_vel, //
		const Vector & foot_pos, //
		LinkID stance_foot, //
		double landing_time, //relative to end of lifting time
		double lifting_time, //duration of total lift trajectory
		double lifting_ratio, //ratio of planning point to total lift
		double transition_landing_time, //
		double transition_lifting_time, //
		double dual_support_time, //
		double middle_time_x, //
                              double middle_time_y);
    void Calculation();
protected:
    // Switching time is summation of
    // remain of lifting time, landing time,
    // land_supp_transition time, dual support time

    // Only landing time can be changed in the planner
};
#endif
