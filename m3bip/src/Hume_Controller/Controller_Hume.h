#ifndef _Controller_Hume_
#define _Controller_Hume_

#include <vector>
#include "WBC.h"
#include "Task.h"
#include "constraint_library.hpp"
#include "HumeSystem.h"

using namespace jspace;
using namespace std;

class process;

class Controller_Hume{
public:
    Controller_Hume(HUME_System * _hume_system);
    Controller_Hume(Controller_Hume * other);
    virtual ~Controller_Hume();
public:
    virtual void getCurrentCommand(std::vector<double> & command) = 0;
    virtual void setCurrentConfiguration() = 0;
    void Cal_FootForce(LinkID _link_id, const Vector& torque, Vector & _force);
    void Cal_curr_FootForce(LinkID _link_id, Vector & _force);
    bool Is_Integral_Gain_Right() { return b_int_gain_right_; }
    bool Is_Integral_Gain_Left() { return b_int_gain_left_; }
    bool Force_Integral_Gain_Left_Knee() { return b_force_int_gain_left_knee_;}
    bool Force_Integral_Gain_Right_Knee() { return b_force_int_gain_right_knee_; }
public:
    Controller_WBC* wbc_;
    Controller_WBC* wbc_2_;
    HUME_System * hume_system_;
    Vector gamma_;
    std::vector<Task*> task_array_;
    WBC_Constraint * constraint_;

    Vector pos_ini_;
    Vector ori_ini_;
    Vector com_ini_;
    Vector left_foot_ini_;
    Vector right_foot_ini_;

    Vector RFoot_force_;
    Vector LFoot_force_;
    Matrix U_;
    double _smooth_changing(double ini, double end, double moving_duration, double curr_time);
    double _smooth_changing_vel(double ini, double end, double moving_duration, double curr_time);
    vector<double> curr_conf_;
    //Stabilizing Process
    process * pro_stable_;
    bool motion_start_;
    int phase_;
    bool b_int_gain_right_;
    bool b_int_gain_left_;
    bool b_force_int_gain_left_knee_;
    bool b_force_int_gain_right_knee_;
    
protected:

    void _COM_Calculation(const vector<double> & curr_conf_, const Vector & curr_vel);    
    void _SetLocalConfiguration2StanceFoot(LinkID _foot);
    void _PreProcessing_Command();
    void _PostProcessing_Command(std::vector<double> & command);
};

#endif
