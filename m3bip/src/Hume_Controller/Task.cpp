#include "Task.h"
#include <stdio.h>

///////////////////////////////////////////////////////
//////////         Abstract TASK Class
///////////////////////////////////////////////////////
Task::Task(HUME_System* hume_system): hume_system_(hume_system), b_settask_(false),
                                      b_set_weight_(false),
                                      socket_(0),
                                      socket_gain_send_(0),
                                      socket_recieve_(0),
                                      num_control_DOF_(0)
{
    // gain_tuner_ = new Gain_Tuner(this);
    // gain_tuner_->start();
}

void Task::_PostProcess_Task(){
    b_settask_ = false;
}
void Task::SetTask(const Vector & des, const Vector & vel_des){
    des_ = des;
    vel_des_ = vel_des;
    b_settask_ = true;
}

double Task::crop_value(double value, double min, double max, char* source){
    if(value> max){
        // printf("%s: Hit The MAX\t", source);
        // printf("Original Value: %f\n", value);
        return max;
    }
    else if(value < min){
        // printf("%s: Hit The MIN\t", source);
        // printf("Original Value: %f\n", value);
        return min;
    }
    else{
        return value;
    }
}

void Task::_SaveBodyData(){
    //Save Data in a file -- Body
    jspace::saveVector(data_body_.body_des, "Body_des", 3);
    jspace::saveVector(data_body_.body_vel_des, "Body_vel_des", 3);
    jspace::saveVector(data_body_.body_pos, "Body_pos", 3);
    jspace::saveVector(data_body_.body_vel, "Body_vel", 3);
}

void Task::_SaveOrientationData(){
    // Save Data in a file -- Ori
    jspace::saveVector(data_ori_.ori_des, "Ori_des", 3);
    jspace::saveVector(data_ori_.ori_vel_des, "Ori_vel_des", 3);
    jspace::saveVector(hume_system_->getOri(), "Body_ori");
    jspace::saveVector(hume_system_->getOri_vel(), "Body_ori_vel");
}

void Task::_SaveFootData(LinkID foot){
    //TODO Save data in a different file according to foot
    // Save Data in a file -- Foot
    jspace::saveVector(data_foot_.foot_des, "Foot_des", 3);
    jspace::saveVector(data_foot_.foot_vel_des, "Foot_vel_des", 3);
    jspace::saveVector(data_foot_.foot_pos, "Foot_pos", 3);
    jspace::saveVector(data_foot_.foot_vel, "Foot_vel", 3);
}

ORI_Task::ORI_Task(HUME_System* hume_system):Task(hume_system){
    Kp_ = Vector(3);
    Kd_ = Vector(3);
    for (int i(0); i< 3; ++i){
        Kp_[i] = 150.0;
        Kd_[i] = 20.0;
    }
}

Body_Task::Body_Task(HUME_System* hume_system):Task(hume_system){
    Kp_ = Vector(3);
    Kd_ = Vector(3);
    for (int i(0); i< 3; ++i){
        Kp_(i) = (90.0);
        Kd_(i) = (8.0);
    }
    Kp_[1] = 30.0;
    Kd_[1] = 3.0;
}   

void Body_Task::SetTask(const Vector & com_des, const Vector & com_vel_des){
    Body_d_ = com_des;
    Body_vel_d_ = com_vel_des;

    b_settask_ = true;
}

Matrix Body_Task::getJacobian(const vector<double> & conf){
    Matrix J;
    hume_system_->GetFullJacobian(conf, HIP, J);
    return J;
}

Vector Body_Task::getCommand(){
    Vector input(3);
    for (int i(0); i< 3; ++i){
        input(i) = Kp_(i)*(Body_d_(i) - hume_system_->getBody()(i))
             + Kd_(i)*(Body_vel_d_(i) - hume_system_->getBody_vel()(i));
    }
    _PostProcess_Task();
    return input; 
}

Vector Body_Task::getForce(){
    Vector force(Vector::Zero(3));
    return force;
}
///////////////////////////////////////////////////////
//////////         Body XZ TASK
///////////////////////////////////////////////////////
Body_XZ_Task::Body_XZ_Task(HUME_System* hume_system):Task(hume_system){
    Kp_ = Vector(2);
    Kd_ = Vector(2);
    for (int i(0); i< 2; ++i){
        Kp_(i) = 200.0;
        Kd_(i) = 10.0;
    }
     Kp_[1] = 250.0;
     Kd_[1] = 15.0;
    // Kp_(1) = 100.0;
    // Kd_(1) = 5.0;
}

void Body_XZ_Task::SetTask(const Vector & body_des, const Vector & body_vel_des){
    Body_d_ = body_des;
    Body_vel_d_ = body_vel_des;
    b_settask_ = true;
}

Matrix Body_XZ_Task::getJacobian(const vector<double> & conf){
    Matrix J = Matrix::Zero(2, NUM_JOINT);
    J(0, Px) = 1;
    J(1, Pz) = 1;

    if(b_set_weight_){
        for(int i(0); i<2; ++i){
            J.block(i,0, 1, NUM_JOINT) = weight_[i] * J.block(i, 0, 1, NUM_JOINT);
        }
        b_set_weight_ = false;
    }
    return J;
}

void Body_XZ_Task::set_weight(const Vector & weight){
    weight_ = weight;
    b_set_weight_ = true;
}

Vector Body_XZ_Task::getCommand(){
    Vector input(2);
    
    input(0) = Kp_(0)*(Body_d_(0) - hume_system_->getBody()(Px))
        + Kd_(0)*(Body_vel_d_(0) - hume_system_ -> getBody_vel()(Px));
    input(1) = Kp_(1)*(Body_d_(1) - hume_system_->getBody()(Pz))
        + Kd_(1)*(Body_vel_d_(1) - hume_system_ -> getBody_vel()(Pz));

    data_.body_des[0] = Body_d_[0];
    data_.body_des[2] = Body_d_[1];
    data_.body_vel_des[0] = Body_vel_d_[0];
    data_.body_vel_des[2] = Body_vel_d_[0];

    data_.body_pos[0] = hume_system_->getBody()[Px];
    data_.body_pos[2] = hume_system_->getBody()[Pz];

    data_.body_vel[0] = hume_system_->getBody_vel()[Px];
    data_.body_vel[2] = hume_system_->getBody_vel()[Pz];

    for ( int i(0); i< 2; ++i){
        input[i] = crop_value(input[i], -100, 100, "Body XZ Task");
    }
    
    // Post Process
    _PostProcess_Task();

    return input;
}
void Body_XZ_Task::send(){
    HumeProtocol::pd_2_gain gain_data_send;
    for(int i(0); i<2; ++i){
        gain_data_send.kp[i] = Kp_[i];
        gain_data_send.kd[i] = Kd_[i];
    }
    
    // COMM::send_data(socket_, PORT_BODY_TASK, & data_, sizeof(HumeProtocol::body_task_data), IP_ADDR_MYSELF);
    // COMM::send_data(socket_gain_send_, PORT_BODY_XZ_GAIN_FROM_TASK, & gain_data_send, sizeof(HumeProtocol::pd_2_gain), IP_ADDR_MYSELF);

    //Save Vector
    jspace::saveValue(hume_system_->getCurrentTime(), "body_xz_time");
    jspace::saveVector(data_.body_des, "Body_des", 3);
    jspace::saveVector(data_.body_vel_des, "Body_vel_des", 3);
    jspace::saveVector(data_.body_pos, "Body_pos", 3 );
    jspace::saveVector(data_.body_vel, "Body_vel", 3);
}

void Body_XZ_Task::set_Gain(){
    printf("[Body XZ Task] In setting Gain\n");
    // COMM::recieve_data(socket_recieve_, PORT_BODY_XZ_GAIN, &gain_data_, sizeof(HumeProtocol::pd_2_gain), IP_ADDR_MYSELF);

    for (int i(0); i<2; ++i){
        Kp_[i] = gain_data_.kp[i];
        Kd_[i] = gain_data_.kd[i];
    }
    printf("[Body XZ Task] recieve gain set\n");
}

Vector Body_XZ_Task::getForce(){
    Vector force(Vector::Zero(2)); 
    return force;
}

///////////////////////////////////////////////////////
//////////         HEIGHT TASK
///////////////////////////////////////////////////////
Height_Task::Height_Task(HUME_System* hume_system):Task(hume_system){
    Kp_ = Vector(1);
    Kp_(0) = 180.0;
    Kd_ = Vector(1);
    Kd_(0) = 5.0;
}

void Height_Task::SetTask(const Vector & height_des, const Vector & height_vel_des){
    height_d_ = height_des;
    height_vel_d_ = height_vel_des;
    b_settask_ = true;
}

Matrix Height_Task::getJacobian(const vector<double> & conf){
    Matrix J = Matrix::Zero(1, NUM_JOINT);
    Matrix J_body;
    hume_system_->GetFullJacobian(conf, HIP, J_body);
    J = J_body.block(2,0, 1, NUM_JOINT);
    return J;
}

Vector Height_Task::getCommand(){
    Vector input(1);
    input(0) = Kp_(0)*(height_d_(0) - hume_system_->getBody()(2))
        + Kd_(0)*(height_vel_d_(0) - hume_system_ -> getBody_vel()(2));
    //Save Data
    data_.body_des[2] = height_d_[0];
    data_.body_vel_des[2] = height_vel_d_[0];
    data_.body_pos[2] = hume_system_->getBody()[2];
    data_.body_vel[2] = hume_system_->getBody_vel()[2];

    double limit = 200.0;
    input[0] = crop_value(input[0], -limit, limit, "Height");
    // Post Process
    _PostProcess_Task();

    return input;
}

Vector Height_Task::getForce(){
    Vector force(Vector::Zero(1));
    return force;
}

void Height_Task::send(){
    // COMM::send_data(socket_, PORT_BODY_TASK, & data_, sizeof(HumeProtocol::body_task_data), IP_ADDR_MYSELF);
    
    HumeProtocol::pd_1_gain gain_data_send;

    gain_data_send.kp = Kp_[0];
    gain_data_send.kd = Kd_[0];

    // COMM::send_data(socket_gain_send_, PORT_HEIGHT_GAIN_FROM_TASK, &gain_data_send,  sizeof(HumeProtocol::pd_1_gain), IP_ADDR_MYSELF);

    //Save Data in a file
    jspace::saveValue(hume_system_->getCurrentTime(), "height_task_time");
    jspace::saveVector(data_.body_des, "Body_des", 3);
    jspace::saveVector(data_.body_vel_des, "Body_vel_des", 3);
    jspace::saveVector(hume_system_->getBody(), "Body_pos");
    jspace::saveVector(hume_system_->getBody_vel(), "Body_vel");
}

void Height_Task::set_Gain(){
    printf("[Height Task] In setting Gain \n");
    // COMM::recieve_data(socket_recieve_, PORT_HEIGHT_GAIN, & gain_data_, sizeof(HumeProtocol::pd_1_gain), IP_ADDR_MYSELF);
    Kp_[0] = gain_data_.kp;
    Kd_[0] = gain_data_.kd;
    printf("[Height] recieve gain set \n");
}
///////////////////////////////////////////////////////
//////////       COM HEIGHT TILTING TASK
///////////////////////////////////////////////////////
COM_Height_Tilting_Task::COM_Height_Tilting_Task(HUME_System* hume_system):Task(hume_system){
    force_ = Vector::Zero(2);
    error_sum_ = Vector::Zero(2);
    
    Kp_ = Vector(2);
    Kd_ = Vector(2);
    Ki_ = Vector(2);
    
    Kp_(0) = 700.0;
    Kp_(1) = 400.0;

    Kd_(0) = 10.0;
    Kd_[1] = 5.0;

    Ki_[0] = 100.0;
    Ki_[1] = 100.0;

    // Kp_(0) = 250.0;
    // Kp_(1) = 300.0;

    // Kd_(0) = 5.0;
    // Kd_[1] = 5.0;

    // Ki_[0] = 150.0;
    // Ki_[1] = 150.0;
    
#ifdef SIMULATION    
        Kp_ = Vector(2);
        Kp_(0) = 40.0;
        Kp_(1) = 50.0;
        
        Kd_ = Vector(2);
        Kd_(0) = 5.0;
        Kd_[1] = 5.0;
#endif
}

void COM_Height_Tilting_Task::SetTask(const Vector & des, const Vector & vel_des){
    des_ = des;
    vel_des_ = vel_des;
    force_ = Vector::Zero(2);
    b_settask_ = true;
}

Matrix COM_Height_Tilting_Task::getJacobian(const vector<double> & conf){
    Matrix J = Matrix::Zero(2, NUM_JOINT);
    Matrix J_com;
    hume_system_->getCOMJacobian(conf, J_com);

    J.block(0,0, 1, NUM_JOINT) = J_com.block(2,0, 1, NUM_JOINT);
    J(1, Ry) = 1.0;
    return J;
}

Vector COM_Height_Tilting_Task::getCommand(){
    Vector input(2);
    // Body Height
    input(0) = Kp_(0)*(des_[0] - hume_system_->COM_pos_[2])
        + Kd_(0)*(vel_des_[0] - hume_system_ ->COM_vel_[2]);
    
    // Pitch
    input[1] = Kp_[1] * (des_[1] - hume_system_->getOri()(1))
        + Kd_[1] * (vel_des_[1] - hume_system_ ->getOri_vel()[1]);

    error_sum_[0] = (des_[0] - hume_system_->tot_configuration_[Pz])*SERVO_RATE;
    error_sum_[1] = (des_[1] - hume_system_->getOri()[1])*SERVO_RATE;

    for (int i(0); i<2; ++i){
        error_sum_[i] = crop_value(error_sum_[i], -50.0, 50.0, "[COM Height & Tilting] error sume");
        input[i] += Ki_[i] * error_sum_[i];
    }
    
    //Save Data -- Body (Heighht)
    data_body_.body_des[2] = des_[0];
    data_body_.body_vel_des[2] = vel_des_[0];

    // data_body_.body_pos[2] = hume_system_->getBody()[2];
    // data_body_.body_vel[2] = hume_system_->getBody_vel()[2];
    data_body_.body_pos[2] = hume_system_->COM_pos_[2];
    data_body_.body_vel[2] = hume_system_->tot_vel_[2];

    // Save Data -- Ori (pitch)
    data_ori_.ori_des[1] = des_[1];
    data_ori_.ori_vel_des[1] = vel_des_[1];
    
    data_ori_.ori[1] = hume_system_->getOri()[1];
    data_ori_.ori_vel[1] = hume_system_->getOri_vel()[1];
    
    double limit = 1000.0;
    for (int i(0); i< 2; ++i){
        input[i] = crop_value(input[i], -limit, limit, "COM Height & Tilting");
    }
    // Post Process
    _PostProcess_Task();

    return input;
}

Vector COM_Height_Tilting_Task::getForce(){
    return force_;
}

void COM_Height_Tilting_Task::send(){
    // COMM::send_data(socket_, PORT_BODY_TASK, & data_body_, sizeof(HumeProtocol::body_task_data), IP_ADDR_MYSELF);
    // COMM::send_data(socket_2_, PORT_ORI_TASK, & data_ori_, sizeof(HumeProtocol::ori_task_data), IP_ADDR_MYSELF);
    
    HumeProtocol::pd_2_gain gain_data_send;
    for(int i(0); i<2 ; ++i){
        gain_data_send.kp[i] = Kp_[i];
        gain_data_send.kd[i] = Kd_[i];
    }

    jspace::saveValue(hume_system_->getCurrentTime(), "COM_height_tilting_task_time");
    //Save Data in a file -- Body
    jspace::saveVector(data_body_.body_des, "Body_des", 3);
    jspace::saveVector(data_body_.body_vel_des, "Body_vel_des", 3);
    jspace::saveVector(data_body_.body_pos, "Body_pos",3);
    jspace::saveVector(data_body_.body_vel, "Body_vel", 3);

    // Save Data in a file -- Ori
    jspace::saveVector(data_ori_.ori_des, "Ori_des", 3);
    jspace::saveVector(data_ori_.ori_vel_des, "Ori_vel_des", 3);
    jspace::saveVector(hume_system_->getOri(), "Body_ori");
    jspace::saveVector(hume_system_->getOri_vel(), "Body_ori_vel");

}

void COM_Height_Tilting_Task::set_Gain(){
}

///////////////////////////////////////////////////////
//////////         HEIGHT TILTING TASK
///////////////////////////////////////////////////////
Height_Tilting_Task::Height_Tilting_Task(HUME_System* hume_system):Task(hume_system){
    force_ = Vector::Zero(2);
    error_sum_ = Vector::Zero(3);
    
    Kp_ = Vector(2);
    Kp_(0) = 600.0;
    Kp_(1) = 300.0;
    // Kp_(0) = 1550.0;
    // Kp_(1) = 80.0;
    
    Kd_ = Vector(2);
    Kd_(0) = 30.0;
    Kd_[1] = 10.0;

    Ki_ = Vector(2);
    Ki_[0] = 150.0;
    Ki_[1] = 100.0;
    
#ifdef SIMULATION    
        Kp_ = Vector(2);
        Kp_(0) = 40.0;
        Kp_(1) = 50.0;
        
        Kd_ = Vector(2);
        Kd_(0) = 5.0;
        Kd_[1] = 5.0;
#endif
}

void Height_Tilting_Task::SetTask(const Vector & des, const Vector & vel_des){
    des_ = des;
    vel_des_ = vel_des;
    force_ = Vector::Zero(2);
    b_settask_ = true;
}

Matrix Height_Tilting_Task::getJacobian(const vector<double> & conf){
    Matrix J = Matrix::Zero(2, NUM_JOINT);
    Matrix J_body;
    hume_system_->GetFullJacobian(conf, HIP, J_body);
    // J.block(0,0, 1, NUM_JOINT) = J_body.block(2,0, 1, NUM_JOINT);
    J(0, Pz) = 1.0;
    J(1, Ry) = 1.0;
    return J;
}

Vector Height_Tilting_Task::getCommand(){
    Vector input(2);
    // Body Height
    // input(0) = Kp_(0)*(des_[0] - hume_system_->getBody()(2))
    //     + Kd_(0)*(vel_des_[0] - hume_system_ -> getBody_vel()(2));
    input(0) = Kp_(0)*(des_[0] - hume_system_->tot_configuration_[Pz])
        + Kd_(0)*(vel_des_[0] - hume_system_ ->tot_vel_[Pz]);

    
    // Pitch
    input[1] = Kp_[1] * (des_[1] - hume_system_->getOri()(1))
        + Kd_[1] * (vel_des_[1] - hume_system_ ->getOri_vel()[1]);

    error_sum_[0] = (des_[0] - hume_system_->tot_configuration_[Pz])*SERVO_RATE;
    error_sum_[1] = (des_[1] - hume_system_->getOri()[1])*SERVO_RATE;

    for (int i(0); i<2; ++i){
        error_sum_[i] = crop_value(error_sum_[i], -10.0, 10.0, "[Height & Tilting] error sume");
        input[i] += Ki_[i] * error_sum_[i];
    }
    

    
    //Save Data -- Body (Heighht)
    data_body_.body_des[2] = des_[0];
    data_body_.body_vel_des[2] = vel_des_[0];

    // data_body_.body_pos[2] = hume_system_->getBody()[2];
    // data_body_.body_vel[2] = hume_system_->getBody_vel()[2];
    data_body_.body_pos[2] = hume_system_->tot_configuration_[2];
    data_body_.body_vel[2] = hume_system_->tot_vel_[2];

    // Save Data -- Ori (pitch)
    data_ori_.ori_des[1] = des_[1];
    data_ori_.ori_vel_des[1] = vel_des_[1];
    
    data_ori_.ori[1] = hume_system_->getOri()[1];
    data_ori_.ori_vel[1] = hume_system_->getOri_vel()[1];
    
    double limit = 1000.0;
    for (int i(0); i< 2; ++i){
        input[i] = crop_value(input[i], -limit, limit, "Height & Tilting");
    }
    // Post Process
    _PostProcess_Task();

    return input;
}

Vector Height_Tilting_Task::getForce(){

    return force_;
}

void Height_Tilting_Task::send(){
    // COMM::send_data(socket_, PORT_BODY_TASK, & data_body_, sizeof(HumeProtocol::body_task_data), IP_ADDR_MYSELF);
    // COMM::send_data(socket_2_, PORT_ORI_TASK, & data_ori_, sizeof(HumeProtocol::ori_task_data), IP_ADDR_MYSELF);
    
    HumeProtocol::pd_2_gain gain_data_send;
    for(int i(0); i<2 ; ++i){
        gain_data_send.kp[i] = Kp_[i];
        gain_data_send.kd[i] = Kd_[i];
    }

    // COMM::send_data(socket_gain_send_, PORT_HEIGHT_TILTING_GAIN_FROM_TASK, &gain_data_send,  sizeof(HumeProtocol::pd_2_gain), IP_ADDR_MYSELF);
    jspace::saveValue(hume_system_->getCurrentTime(), "height_tilting_task_time");
    //Save Data in a file -- Body
    jspace::saveVector(data_body_.body_des, "Body_des", 3);
    jspace::saveVector(data_body_.body_vel_des, "Body_vel_des", 3);
    jspace::saveVector(data_body_.body_pos, "Body_pos",3);
    jspace::saveVector(data_body_.body_vel, "Body_vel", 3);

    // Save Data in a file -- Ori
    jspace::saveVector(data_ori_.ori_des, "Ori_des", 3);
    jspace::saveVector(data_ori_.ori_vel_des, "Ori_vel_des", 3);
    jspace::saveVector(hume_system_->getOri(), "Body_ori");
    jspace::saveVector(hume_system_->getOri_vel(), "Body_ori_vel");

}

void Height_Tilting_Task::set_Gain(){
    printf("[Height TIlting Task] In setting Gain \n");
    // COMM::recieve_data(socket_recieve_, PORT_HEIGHT_TILTING_GAIN, & gain_data_, sizeof(HumeProtocol::pd_2_gain), IP_ADDR_MYSELF);
    for (int i(0); i<2; ++i){
        Kp_[i] = gain_data_.kp[i];
        Kd_[i] = gain_data_.kd[i];
    }
    printf("[Height Tilting Task] recieve gain set \n");
}

///////////////////////////////////////////////////////
//////////         XZ TILTING TASK
///////////////////////////////////////////////////////
XZ_Tilting_Task::XZ_Tilting_Task(HUME_System* hume_system):
    error_sum_(Vector::Zero(3)),
    Task(hume_system){
    
    force_ = Vector::Zero(3);
    Ki_ = Vector::Zero(3);
    
    Kp_ = Vector(3);
    Kp_[0] = 70.0;
    Kp_(1) = 300.0;
    Kp_(2) = 100.0;

    Ki_[0] = 0.0;
    Ki_[1] = 100.0;
    Ki_[2] = 60.0;
    
    // Kp_[0] = 85.0;
    // Kp_(1) = 580.0;
    // Kp_(2) = 50.0;

    
    Kd_ = Vector(3);
    Kd_[0] = 5.0;
    Kd_(1) = 10.0;
    Kd_[2] = 5.0;

#ifdef SIMULATION
    printf("In simulation\n");
    Kp_ = Vector(3);
    Kp_[0] = 100.0;
    Kp_(1) = 100.0;
    Kp_(2) = 250.0;
#endif
}

void XZ_Tilting_Task::SetTask(const Vector & des, const Vector & vel_des){
    des_ = des;
    vel_des_ = vel_des;
    b_settask_ = true;
}

Matrix XZ_Tilting_Task::getJacobian(const vector<double> & conf){
    Matrix J = Matrix::Zero(3, NUM_JOINT);
    // Matrix J_body;
    // hume_system_->GetFullJacobian(conf, HIP, J_body);
    // J.block(0,0, 1, NUM_JOINT) = J_body.block(0,0, 1, NUM_JOINT);
    // J.block(1,0, 1, NUM_JOINT) = J_body.block(2,0, 1, NUM_JOINT);
    J(0, Px) = 1.0;
    J(1, Pz) = 1.0;
    J(2, Ry) = 1.0;
    return J;
}

Vector XZ_Tilting_Task::getCommand(){
    Vector input(3);
    // Body XZ
    // input(0) = Kp_(0)*(des_[0] - hume_system_->getBody()(0))
    //     + Kd_(0)*(vel_des_[0] - hume_system_ -> getBody_vel()(0));
    // input(1) = Kp_(1)*(des_[1] - hume_system_->getBody()(2))
    //     + Kd_(1)*(vel_des_[1] - hume_system_ -> getBody_vel()(2));
    
    // error_sum_[0] = (des_[0] - hume_system_->getBody()[0])*SERVO_RATE;
    // error_sum_[1] = (des_[1] - hume_system_->getBody()[2])*SERVO_RATE;


    input(0) = Kp_(0)*(des_[0] - hume_system_->tot_configuration_[Px])
             + Kd_(0)*(vel_des_[0] - hume_system_ ->tot_vel_[Px]);
    input(1) = Kp_(1)*(des_[1] - hume_system_->tot_configuration_[Pz])
             + Kd_(1)*(vel_des_[1] - hume_system_ ->tot_vel_[Pz]);
    
    error_sum_[0] = (des_[0] - hume_system_->tot_configuration_[Px])*SERVO_RATE;
    error_sum_[1] = (des_[1] - hume_system_->tot_configuration_[Pz])*SERVO_RATE;

    
    // Pitch
    input[2] = Kp_[2] * (des_[2] - hume_system_->getOri()(1))
        + Kd_[2] * (vel_des_[2] - hume_system_ ->getOri()[1]);
    error_sum_[2] += (des_[2] - hume_system_->getOri()[1])*SERVO_RATE;
    

    for(int i(0); i< 3; ++i){
        error_sum_[i] = crop_value(error_sum_[i], -10.0, 10.0, "[XZ & Tilting] error sum");
        input[i] += error_sum_[i] * Ki_[i];
    }
    
    //Save Data -- Body (X, Height)
    data_body_.body_des[0] = des_[0];
    data_body_.body_vel_des[0] = vel_des_[0];
    data_body_.body_des[2] = des_[1];
    data_body_.body_vel_des[2] = vel_des_[1];

    // data_body_.body_pos[0] = hume_system_->getBody()[0];
    // data_body_.body_vel[0] = hume_system_->getBody_vel()[0];
    // data_body_.body_pos[2] = hume_system_->getBody()[2];
    // data_body_.body_vel[2] = hume_system_->getBody_vel()[2];

    data_body_.body_pos[0] = hume_system_->tot_configuration_[Px];
    data_body_.body_vel[0] = hume_system_->tot_vel_[Px];
    data_body_.body_pos[2] = hume_system_->tot_configuration_[Pz];
    data_body_.body_vel[2] = hume_system_->tot_vel_[Pz];
    
    // Save Data -- Ori (pitch)
    data_ori_.ori_des[1] = des_[2];
    data_ori_.ori_vel_des[1] = vel_des_[2];
    
    data_ori_.ori[1] = hume_system_->getOri()[1];
    data_ori_.ori_vel[1] = hume_system_->getOri_vel()[1];
    
    double limit = 1000.0;
    for (int i(0); i< 2; ++i){
        input[i] = crop_value(input[i], -limit, limit, "XZ & Tilting");
    }
    // Post Process
    _PostProcess_Task();

    return input;
}

Vector XZ_Tilting_Task::getForce(){
    
    return force_;
}

void XZ_Tilting_Task::send(){
    // COMM::send_data(socket_, PORT_BODY_TASK, & data_body_, sizeof(HumeProtocol::body_task_data), IP_ADDR_MYSELF);
    // COMM::send_data(socket_2_, PORT_ORI_TASK, & data_ori_, sizeof(HumeProtocol::ori_task_data), IP_ADDR_MYSELF);
    jspace::saveValue(hume_system_->getCurrentTime(), "xz_tilting_task_time");
    //Save Data in a file -- Body
    jspace::saveVector(data_body_.body_des, "Body_des", 3);
    jspace::saveVector(data_body_.body_vel_des, "Body_vel_des", 3);
    jspace::saveVector(data_body_.body_pos, "Body_pos", 3);
    jspace::saveVector(data_body_.body_vel, "Body_vel", 3);

    // Save Data in a file -- Ori
    jspace::saveVector(data_ori_.ori_des, "Ori_des", 3);
    jspace::saveVector(data_ori_.ori_vel_des, "Ori_vel_des", 3);
    jspace::saveVector(hume_system_->getOri(), "Body_ori");
    jspace::saveVector(hume_system_->getOri_vel(), "Body_ori_vel");
}

void XZ_Tilting_Task::set_Gain(){
    //useless
    printf("[XZ TIlting Task] In setting Gain \n");
    // COMM::recieve_data(socket_recieve_, PORT_HEIGHT_TILTING_GAIN, & gain_data_, sizeof(HumeProtocol::pd_3_gain), IP_ADDR_MYSELF);
    for (int i(0); i<3; ++i){
        Kp_[i] = gain_data_.kp[i];
        Kd_[i] = gain_data_.kd[i];
    }
    printf("[XZ Tilting Task] recieve gain set \n");
}

///////////////////////////////////////////////////////
//////////         COM TILTING TASK
///////////////////////////////////////////////////////
COM_Tilting_Task::COM_Tilting_Task(HUME_System* hume_system):
    error_sum_(Vector::Zero(3)),
    Task(hume_system){
    
    force_ = Vector::Zero(3);
    Ki_ = Vector::Zero(3);
    Kp_ = Vector(3);
    Kd_ = Vector(3);

    Kp_[0] = 100.0;
    Kp_(1) = 500.0;
    Kp_(2) = 150.0;

    Ki_[0] = 55.0;
    Ki_[1] = 55.0;
    Ki_[2] = 55.0;

    Kd_[0] = 5.0;
    Kd_(1) = 5.0;
    Kd_[2] = 5.0;

    
    // Kp_[0] = 50.0;
    // Kp_(1) = 150.0;
    // Kp_(2) = 100.0;

    // Ki_[0] = 0.0;
    // Ki_[1] = 10.0;
    // Ki_[2] = 10.0;

    // Kd_[0] = 5.0;
    // Kd_(1) = 5.0;
    // Kd_[2] = 5.0;

    
#ifdef SIMULATION
    printf("In simulation\n");
    Kp_ = Vector(3);
    Kp_[0] = 100.0;
    Kp_(1) = 100.0;
    Kp_(2) = 250.0;
#endif
    }


void COM_Tilting_Task::SetTask(const Vector & des, const Vector & vel_des){
    des_ = des;
    vel_des_ = vel_des;
    b_settask_ = true;
}

Matrix COM_Tilting_Task::getJacobian(const vector<double> & conf){
    Matrix J = Matrix::Zero(3, NUM_JOINT);
    Matrix Jcom;
    hume_system_->getCOMJacobian(conf, Jcom);
    J.block(0,0, 1, NUM_JOINT) = Jcom.block(0, 0, 1, NUM_JOINT);
    J.block(1,0, 1, NUM_JOINT) = Jcom.block(2, 0, 1, NUM_JOINT);
    J(2, Ry) = 1.0;
    return J;
}

Vector COM_Tilting_Task::getCommand(){
    Vector input(3);
    // COM
    input(0) = Kp_(0)*(des_[0] - hume_system_->COM_pos_[0])
             + Kd_(0)*(vel_des_[0] - hume_system_ ->COM_vel_[0]);
    input(1) = Kp_(1)*(des_[1] - hume_system_->COM_pos_[2])
             + Kd_(1)*(vel_des_[1] - hume_system_ ->COM_vel_[2]);
    
    error_sum_[0] = (des_[0] - hume_system_->COM_pos_[0])*SERVO_RATE;
    error_sum_[1] = (des_[1] - hume_system_->COM_pos_[2])*SERVO_RATE;
    
    // Pitch
    input[2] = Kp_[2] * (des_[2] - hume_system_->getOri()(1))
        + Kd_[2] * (vel_des_[2] - hume_system_ ->getOri()[1]);
    error_sum_[2] += (des_[2] - hume_system_->getOri()[1])*SERVO_RATE;
    
    for(int i(0); i< 3; ++i){
        error_sum_[i] = crop_value(error_sum_[i], -10.0, 10.0, "[COM & Tilting] error sum");
        input[i] += error_sum_[i] * Ki_[i];
    }
    
    //Save Data -- Body (X, Height)
    data_body_.body_des[0] = des_[0];
    data_body_.body_vel_des[0] = vel_des_[0];
    data_body_.body_des[2] = des_[1];
    data_body_.body_vel_des[2] = vel_des_[1];

    data_body_.body_pos[0] = hume_system_->COM_pos_[0];
    data_body_.body_vel[0] = hume_system_->COM_vel_[0];
    data_body_.body_pos[2] = hume_system_->COM_pos_[2];
    data_body_.body_vel[2] = hume_system_->COM_vel_[2];
    
    // Save Data -- Ori (pitch)
    data_ori_.ori_des[1] = des_[2];
    data_ori_.ori_vel_des[1] = vel_des_[2];
    
    data_ori_.ori[1] = hume_system_->getOri()[1];
    data_ori_.ori_vel[1] = hume_system_->getOri_vel()[1];
    
    double limit = 1000.0;
    for (int i(0); i< 2; ++i){
        input[i] = crop_value(input[i], -limit, limit, "COM & Tilting");
    }
    // Post Process
    _PostProcess_Task();

    return input;
}

Vector COM_Tilting_Task::getForce(){
    return force_;
}

void COM_Tilting_Task::send(){
    jspace::saveValue(hume_system_->getCurrentTime(), "xz_tilting_task_time");
    //Save Data in a file -- Body
    jspace::saveVector(data_body_.body_des, "Body_des", 3);
    jspace::saveVector(data_body_.body_vel_des, "Body_vel_des", 3);
    jspace::saveVector(data_body_.body_pos, "Body_pos", 3);
    jspace::saveVector(data_body_.body_vel, "Body_vel", 3);

    // Save Data in a file -- Ori
    jspace::saveVector(data_ori_.ori_des, "Ori_des", 3);
    jspace::saveVector(data_ori_.ori_vel_des, "Ori_vel_des", 3);
    jspace::saveVector(hume_system_->getOri(), "Body_ori");
    jspace::saveVector(hume_system_->getOri_vel(), "Body_ori_vel");
}

void COM_Tilting_Task::set_Gain(){
}


///////////////////////////////////////////////////////
//////////         HEIGHT TILTING FOOT TASK
///////////////////////////////////////////////////////
Height_Tilting_Foot_Task::Height_Tilting_Foot_Task(HUME_System* hume_system, LinkID link_id):Task(hume_system), link_id_(link_id){
    force_ = Vector::Zero(5);
    Kp_ = Vector(5);
    Kp_(0) = 160.0;
    Kp_(1) = 300.0;

    Kd_ = Vector(5);
    Kd_(0) = 10.0;
    Kd_[1] = 11.0;

    for (int i(2); i<5; ++i){
        Kp_[i] = 150.0;
        Kd_[i] = 10.0;
    }
    Kp_[0] = 180.0;
    Kd_[0] = 15.0;
    
    Kp_[4] = 220.0;
    Kd_[4] = 20.0;
    
    switch(link_id_){
    case LFOOT:
        recieve_gain_port_ = PORT_HEIGHT_TILTING_LEFT_FOOT_GAIN;
        send_gain_port_ = PORT_HEIGHT_TILTING_LEFT_FOOT_GAIN_FROM_TASK;
        break;
    case RFOOT:
        recieve_gain_port_ = PORT_HEIGHT_TILTING_RIGHT_FOOT_GAIN;
        send_gain_port_ = PORT_HEIGHT_TILTING_RIGHT_FOOT_GAIN_FROM_TASK;
        break;
    }
}

void Height_Tilting_Foot_Task::SetTask(const Vector & des, const Vector & vel_des){
    des_ = des;
    vel_des_ = vel_des;

    hume_system_->CopyFoot(curr_foot_pos_, link_id_);
    hume_system_->CopyFootVel(curr_foot_vel_, link_id_);
    
    b_settask_ = true;
}

Matrix Height_Tilting_Foot_Task::getJacobian(const vector<double> & conf){
    Matrix J = Matrix::Zero(5, NUM_JOINT);
    Matrix J_body, J_foot;
    hume_system_->GetFullJacobian(conf, HIP, J_body);
    J.block(0,0, 1, NUM_JOINT) = J_body.block(2,0, 1, NUM_JOINT);
    J(1, Ry) = 1.0;

    hume_system_->GetFullJacobian(conf, link_id_, J_foot);
    J.block(2, 0, 3, NUM_JOINT) = J_foot;
    return J;
}
void Height_Tilting_Foot_Task::setCurrentFoot(const Vector & curr_foot_pos, const Vector & curr_foot_vel){
    curr_foot_pos_ = curr_foot_pos;
    curr_foot_vel_ = curr_foot_vel;
}
Vector Height_Tilting_Foot_Task::getCommand(){
    Vector input(5);
    // Body Height
    input(0) = Kp_(0)*(des_[0] - hume_system_->getBody()(2))
        + Kd_(0)*(vel_des_[0] - hume_system_ -> getBody_vel()(2));
    // Pitch
    input[1] = Kp_[1] * (des_[1] - hume_system_->getOri()(1))
        + Kd_[1] * (vel_des_[1] - hume_system_ ->getOri()[1]);

    for (int i(2); i<5 ; ++i){
        input[i] = Kp_[i] * (des_[i] - curr_foot_pos_[i-2]) +
            Kd_[i] * (vel_des_[i] - curr_foot_vel_[i-2]);
    }
    
    //Save Data -- Body (Height)
    data_body_.body_des[2] = des_[0];
    data_body_.body_vel_des[2] = vel_des_[0];

    data_body_.body_pos[2] = hume_system_->getBody()[2];
    data_body_.body_vel[2] = hume_system_->getBody_vel()[2];

    // Save Data -- Ori (pitch)
    data_ori_.ori_des[1] = des_[1];
    data_ori_.ori_vel_des[1] = vel_des_[1];
    
    data_ori_.ori[1] = hume_system_->getOri()[1];
    data_ori_.ori_vel[1] = hume_system_->getOri_vel()[1];

    //Save Data -- Foot
    for (int i(0); i< 3; ++i){
        data_foot_.foot_des[i] = des_[2 + i];
        data_foot_.foot_vel_des[i] = vel_des_[2+i];
        data_foot_.foot_pos[i] = curr_foot_pos_[i];
        data_foot_.foot_vel[i] = curr_foot_vel_[i];
    }
    
    double limit = 250.0;
    for (int i(0); i< 5; ++i){
        input[i] = crop_value(input[i], -limit, limit, "Height & Tilting");
    }
    // Post Process
    _PostProcess_Task();

    return input;
}

Vector Height_Tilting_Foot_Task::getForce(){
    return force_;
}

void Height_Tilting_Foot_Task::send(){
    //////////////  Task Data Send /////////////
    // COMM::send_data(socket_, PORT_BODY_TASK, & data_body_, sizeof(HumeProtocol::body_task_data), IP_ADDR_MYSELF);
    // COMM::send_data(socket_2_, PORT_ORI_TASK, & data_ori_, sizeof(HumeProtocol::ori_task_data), IP_ADDR_MYSELF);
    // COMM::send_data(socket_3_, PORT_FOOT_TASK, & data_foot_, sizeof(HumeProtocol::foot_task_data), IP_ADDR_MYSELF);

    //////////////  Gain Data Send /////////////
    HumeProtocol::pd_5_gain gain_data_send;
    for(int i(0); i< 5; ++i){
        gain_data_send.kp[i] = Kp_[i];
        gain_data_send.kd[i] = Kd_[i];
    }
    // COMM::send_data(socket_gain_send_, send_gain_port_, & gain_data_send, sizeof(HumeProtocol::pd_5_gain), IP_ADDR_MYSELF);
    jspace::saveValue(hume_system_->getCurrentTime(), "height_tilting_foot_task_time");
    //Save Data in a file -- Body
    jspace::saveVector(data_body_.body_des, "Body_des", 3);
    jspace::saveVector(data_body_.body_vel_des, "Body_vel_des", 3);
    jspace::saveVector(hume_system_->getBody(), "Body_pos");
    jspace::saveVector(hume_system_->getBody_vel(), "Body_vel");

    // Save Data in a file -- Ori
    jspace::saveVector(data_ori_.ori_des, "Ori_des", 3);
    jspace::saveVector(data_ori_.ori_vel_des, "Ori_vel_des", 3);
    jspace::saveVector(hume_system_->getOri(), "Body_ori");
    jspace::saveVector(hume_system_->getOri_vel(), "Body_ori_vel");

    // Save Data in a file -- Foot
    jspace::saveVector(data_foot_.foot_des, "Foot_des", 3);
    jspace::saveVector(data_foot_.foot_vel_des, "Foot_vel_des", 3);
    jspace::saveVector(data_foot_.foot_pos, "Foot_pos", 3);
    jspace::saveVector(data_foot_.foot_vel, "Foot_vel", 3);
}

void Height_Tilting_Foot_Task::set_Gain(){
    printf("[Height TIlting Foot Task] In Gain Setting \n");
    // Height Gain
    // COMM::recieve_data(socket_recieve_, recieve_gain_port_, & gain_data_, sizeof(HumeProtocol::pd_5_gain), IP_ADDR_MYSELF);
    
    for(int i(0); i< 5; ++i){
        Kp_[i] = gain_data_.kp[i];
        Kd_[i] = gain_data_.kd[i];
    }
    printf("[Height Tilting Foot Task] Recieve Height Gain Set \n");
}

///////////////////////////////////////////////////////
//////////         HEIGHT TILTING FOOT XZ TASK
///////////////////////////////////////////////////////
Height_Tilting_Foot_XZ_Task::Height_Tilting_Foot_XZ_Task(HUME_System* hume_system,
                                                         LinkID link_id):
    Task(hume_system), link_id_(link_id)
{
    // Integral Gain
    num_control_DOF_ = 4;
    error_sum_ = Vector::Zero(num_control_DOF_);
    Ki_ = Vector::Zero(num_control_DOF_);
    // PD Gain
    Kp_ = Vector(4);
    Kd_ = Vector(4);

    force_ = Vector::Zero(num_control_DOF_);

    
#ifdef SIMULATION
    Kp_ = Vector(4);
    Kp_(0) = 80.0;
    Kp_(1) = 110.0;

    Kd_ = Vector(4);
    Kd_(0) = 5.0;
    Kd_[1] = 5.0;

    //Foot
    Kp_[2] = 450.0;
    Kd_[2] = 45.0;
    
    Kp_[3] = 200.0;
    Kd_[3] = 20.0;

    Ki_[2] = 500.0;
    Ki_[3] = 50.0;
#endif
    
    switch(link_id_){
    case LFOOT:
        recieve_gain_port_ = PORT_HEIGHT_TILTING_LEFT_FOOT_XZ_GAIN;
        send_gain_port_ = PORT_HEIGHT_TILTING_LEFT_FOOT_XZ_GAIN_FROM_TASK;
        break;
    case RFOOT:
        recieve_gain_port_ = PORT_HEIGHT_TILTING_RIGHT_FOOT_XZ_GAIN;
        send_gain_port_ = PORT_HEIGHT_TILTING_RIGHT_FOOT_XZ_GAIN_FROM_TASK;
        break;
    }
}

void Height_Tilting_Foot_XZ_Task::SetTask(const Vector & des, const Vector & vel_des){
    des_ = des;
    vel_des_ = vel_des;

    //Height Tilting
    Kp_(0) = 500.0;
    Kp_(1) = 400.0;

    Ki_[0] = 150.0;
    Ki_[1] = 100.0;

    Kd_(0) = 10.0;
    Kd_[1] = 10.0;

    //Foot
    Kp_[2] = 650.0;
    Kd_[2] = 65.0;
    
    Kp_[3] = 500.0;
    Kd_[3] = 65.0;

    Ki_[2] = 950.0;
    Ki_[3] = 1200.0;

    
    // Full Position X, Y, Z
    hume_system_->CopyFoot(curr_foot_pos_, link_id_);
    hume_system_->CopyFootVel(curr_foot_vel_, link_id_);
    
    b_settask_ = true;
}

Matrix Height_Tilting_Foot_XZ_Task::getJacobian(const vector<double> & conf){
    Matrix J = Matrix::Zero(4, NUM_JOINT);
    Matrix J_body, J_foot;
    hume_system_->GetFullJacobian(conf, HIP, J_body);
    // J.block(0,0, 1, NUM_JOINT) = J_body.block(2,0, 1, NUM_JOINT);
    J(0, Pz) = 1.0;
    J(1, Ry) = 1.0;

    hume_system_->GetFullJacobian(conf, link_id_, J_foot);
    J.block(2, 0, 1, NUM_JOINT) = J_foot.block(0,0, 1, NUM_JOINT);
    J.block(3, 0, 1, NUM_JOINT) = J_foot.block(2,0, 1, NUM_JOINT);

    return J;
}
void Height_Tilting_Foot_XZ_Task::setCurrentFoot(const Vector & curr_foot_pos, const Vector & curr_foot_vel){
    curr_foot_pos_ = curr_foot_pos;
    curr_foot_vel_ = curr_foot_vel;
}
Vector Height_Tilting_Foot_XZ_Task::getCommand(){
    Vector input(4);
    // Body Height
    // input(0) = Kp_(0)*(des_[0] - hume_system_->getBody()(2))
    //     + Kd_(0)*(vel_des_[0] - hume_system_ -> getBody_vel()(2));
    input(0) = Kp_(0)*(des_[0] - hume_system_->tot_configuration_[Pz])
        + Kd_(0)*(vel_des_[0] - hume_system_ ->tot_vel_[Pz]);

    // Pitch
    input[1] = Kp_[1] * (des_[1] - hume_system_->getOri()(1))
        + Kd_[1] * (vel_des_[1] - hume_system_ ->getOri()[1]);
    
    // Foot X
    input[2] = Kp_[2] * (des_[2] - curr_foot_pos_[0]) +
        Kd_[2] * (vel_des_[2] - curr_foot_vel_[0]);
    // Foot Z
    input[3] = Kp_[3] * (des_[3] - curr_foot_pos_[2]) +
        Kd_[3] * (vel_des_[3] - curr_foot_vel_[2]);

    //
    error_sum_[2] += (des_[2] - curr_foot_pos_[0])*SERVO_RATE;
    error_sum_[3] += (des_[3] - curr_foot_pos_[2])*SERVO_RATE;
    //
    error_sum_[2] = crop_value(error_sum_[2], -5.0, 5.0, "error sum X");
    error_sum_[3] = crop_value(error_sum_[3], -5.0, 5.0, "error sum Z");

    // Integral Control
    input[2] += Ki_[2] * error_sum_[2];
    input[3] += Ki_[3] * error_sum_[3];
    
    
    //Save Data -- Body (Height)
    data_body_.body_des[2] = des_[0];
    data_body_.body_vel_des[2] = vel_des_[0];

    // data_body_.body_pos[2] = hume_system_->getBody()[2];
    // data_body_.body_vel[2] = hume_system_->getBody_vel()[2];
    data_body_.body_pos[2] = hume_system_->tot_configuration_[Pz];
    data_body_.body_vel[2] = hume_system_->tot_vel_[Pz];
    data_body_.body_pos[0] = hume_system_->tot_configuration_[Px];
    data_body_.body_vel[0] = hume_system_->tot_vel_[Px];
    // Save Data -- Ori (pitch)
    data_ori_.ori_des[1] = des_[1];
    data_ori_.ori_vel_des[1] = vel_des_[1];
    
    data_ori_.ori[1] = hume_system_->getOri()[1];
    data_ori_.ori_vel[1] = hume_system_->getOri_vel()[1];

    //Save Data -- Foot
    data_foot_.foot_des[0] = des_[2];
    data_foot_.foot_des[2] = des_[3];
    data_foot_.foot_vel_des[0] = vel_des_[2];
    data_foot_.foot_vel_des[2] = vel_des_[3];
 
    for (int i(0); i<3; ++i){
        data_foot_.foot_pos[i] = curr_foot_pos_[i];
        data_foot_.foot_vel[i] = curr_foot_vel_[i];
    }

    double limit = 1050.0;
    for (int i(0); i< 4; ++i){
        input[i] = crop_value(input[i], -limit, limit, "Height & Tilting & Foot XZ");
    }
    // Post Process
    _PostProcess_Task();

    return input;
}

Vector Height_Tilting_Foot_XZ_Task::getForce(){
    // switch (link_id_){
    // case RFOOT:
    //     force_[1] = -hume_system_->tot_vel_[7];
    //     break;
    // case LFOOT:
    //     force_[1] = -hume_system_->tot_vel_[10];
    //     break;
    // }
    // force_[0] = 10.0;
    return force_;
}

void Height_Tilting_Foot_XZ_Task::send(){
    //////////////  Task Data Send /////////////
    // COMM::send_data(socket_, PORT_BODY_TASK, & data_body_, sizeof(HumeProtocol::body_task_data), IP_ADDR_MYSELF);
    // COMM::send_data(socket_2_, PORT_ORI_TASK, & data_ori_, sizeof(HumeProtocol::ori_task_data), IP_ADDR_MYSELF);
    // COMM::send_data(socket_3_, PORT_FOOT_TASK, & data_foot_, sizeof(HumeProtocol::foot_task_data), IP_ADDR_MYSELF);

    //////////////  Gain Data Send /////////////
    HumeProtocol::pd_4_gain gain_data_send;
    for(int i(0); i< 4; ++i){
        gain_data_send.kp[i] = Kp_[i];
        gain_data_send.kd[i] = Kd_[i];
    }
    // COMM::send_data(socket_gain_send_, send_gain_port_, & gain_data_send, sizeof(HumeProtocol::pd_5_gain), IP_ADDR_MYSELF);

    jspace::saveValue(hume_system_->getCurrentTime(), "height_tilting_foot_xz_time");
    //Save Data in a file -- Body
    jspace::saveVector(force_, "swing_force");
    jspace::saveVector(data_body_.body_des, "Body_des", 3);
    jspace::saveVector(data_body_.body_vel_des, "Body_vel_des", 3);
    jspace::saveVector(data_body_.body_pos, "Body_pos", 3);
    jspace::saveVector(data_body_.body_vel, "Body_vel", 3);

    // Save Data in a file -- Ori
    jspace::saveVector(data_ori_.ori_des, "Ori_des", 3);
    jspace::saveVector(data_ori_.ori_vel_des, "Ori_vel_des", 3);
    jspace::saveVector(hume_system_->getOri(), "Body_ori");
    jspace::saveVector(hume_system_->getOri_vel(), "Body_ori_vel");

    // Save Data in a file -- Foot
    jspace::saveVector(data_foot_.foot_des, "Foot_des", 3);
    jspace::saveVector(data_foot_.foot_vel_des, "Foot_vel_des", 3);
    jspace::saveVector(data_foot_.foot_pos, "Foot_pos", 3);
    jspace::saveVector(data_foot_.foot_vel, "Foot_vel", 3);
}

void Height_Tilting_Foot_XZ_Task::set_Gain(){
    printf("[Height TIlting Foot Task] In Gain Setting \n");
    // Height Gain
    // COMM::recieve_data(socket_recieve_, recieve_gain_port_, & gain_data_, sizeof(HumeProtocol::pd_4_gain), IP_ADDR_MYSELF);
    
    for(int i(0); i< 4; ++i){
        Kp_[i] = gain_data_.kp[i];
        Kd_[i] = gain_data_.kd[i];
    }
    printf("[Height Tilting Foot Task] Recieve Height Gain Set \n");
}

///////////////////////////////////////////////////////
//////////     COM HEIGHT TILTING FOOT XZ TASK
///////////////////////////////////////////////////////
COM_Height_Tilting_Foot_XZ_Task::COM_Height_Tilting_Foot_XZ_Task(HUME_System* hume_system,
                                                         LinkID link_id):
    Task(hume_system), link_id_(link_id)
{
    // Integral Gain
    num_control_DOF_ = 4;
    error_sum_ = Vector::Zero(num_control_DOF_);
    Ki_ = Vector::Zero(num_control_DOF_);
    // PD Gain
    Kp_ = Vector(4);
    Kd_ = Vector(4);

    force_ = Vector::Zero(num_control_DOF_);
    
    switch(link_id_){
    case LFOOT:
        recieve_gain_port_ = PORT_HEIGHT_TILTING_LEFT_FOOT_XZ_GAIN;
        send_gain_port_ = PORT_HEIGHT_TILTING_LEFT_FOOT_XZ_GAIN_FROM_TASK;
        break;
    case RFOOT:
        recieve_gain_port_ = PORT_HEIGHT_TILTING_RIGHT_FOOT_XZ_GAIN;
        send_gain_port_ = PORT_HEIGHT_TILTING_RIGHT_FOOT_XZ_GAIN_FROM_TASK;
        break;
    }
}

void COM_Height_Tilting_Foot_XZ_Task::SetTask(const Vector & des, const Vector & vel_des){
    des_ = des;
    vel_des_ = vel_des;

    //Height Tilting
    Kp_(0) = 500.0;
    Kp_(1) = 200.0;

    Ki_[0] = 100.0;
    Ki_[1] = 70.0;

    Kd_(0) = 10.0;
    Kd_[1] = 5.0;

    //Foot
    Kp_[2] = 950.0;
    Kd_[2] = 65.0;
    
    Kp_[3] = 700.0;
    Kd_[3] = 65.0;

    Ki_[2] = 950.0;
    Ki_[3] = 1200.0;

    // Kp_(0) = 250.0;
    // Kp_(1) = 250.0;

    // Ki_[0] = 10.0;
    // Ki_[1] = 10.0;

    // Kd_(0) = 5.0;
    // Kd_[1] = 5.0;

    // //Foot
    // Kp_[2] = 1000.0;
    // Kd_[2] = 65.0;
    
    // Kp_[3] = 750.0;
    // Kd_[3] = 65.0;

    // Ki_[2] = 1050.0;
    // Ki_[3] = 1200.0;
    
    // Full Position X, Y, Z
    hume_system_->CopyFoot(curr_foot_pos_, link_id_);
    hume_system_->CopyFootVel(curr_foot_vel_, link_id_);
    
    b_settask_ = true;
}

Matrix COM_Height_Tilting_Foot_XZ_Task::getJacobian(const vector<double> & conf){
    Matrix J = Matrix::Zero(4, NUM_JOINT);
    Matrix J_com, J_foot;
    hume_system_->getCOMJacobian(conf, J_com);
    J.block(0,0, 1, NUM_JOINT) = J_com.block(2,0, 1, NUM_JOINT);
    J(1, Ry) = 1.0;

    hume_system_->GetFullJacobian(conf, link_id_, J_foot);
    J.block(2, 0, 1, NUM_JOINT) = J_foot.block(0,0, 1, NUM_JOINT);
    J.block(3, 0, 1, NUM_JOINT) = J_foot.block(2,0, 1, NUM_JOINT);

    return J;
}
void COM_Height_Tilting_Foot_XZ_Task::setCurrentFoot(const Vector & curr_foot_pos, const Vector & curr_foot_vel){
    curr_foot_pos_ = curr_foot_pos;
    curr_foot_vel_ = curr_foot_vel;
}
Vector COM_Height_Tilting_Foot_XZ_Task::getCommand(){
    Vector input(4);
    // Body Height
    input(0) = Kp_(0)*(des_[0] - hume_system_->COM_pos_[2])
        + Kd_(0)*(vel_des_[0] - hume_system_ ->COM_vel_[2]);

    // Pitch
    input[1] = Kp_[1] * (des_[1] - hume_system_->getOri()(1))
        + Kd_[1] * (vel_des_[1] - hume_system_ ->getOri()[1]);
    
    // Foot X
    input[2] = Kp_[2] * (des_[2] - curr_foot_pos_[0]) +
        Kd_[2] * (vel_des_[2] - curr_foot_vel_[0]);
    // Foot Z
    input[3] = Kp_[3] * (des_[3] - curr_foot_pos_[2]) +
        Kd_[3] * (vel_des_[3] - curr_foot_vel_[2]);

    //
    error_sum_[0] += (des_[0] - hume_system_->COM_pos_[2])*SERVO_RATE;
    error_sum_[1] += (des_[1] - hume_system_->getOri()[1])*SERVO_RATE;
    error_sum_[2] += (des_[2] - curr_foot_pos_[0])*SERVO_RATE;
    error_sum_[3] += (des_[3] - curr_foot_pos_[2])*SERVO_RATE;
  
    // Integral Control
    for(int i(0); i<4; ++i){
        error_sum_[i] = crop_value(error_sum_[i], -5.0, 5.0, "[COM height tilting foot xz]error sum");
        input[i] += Ki_[i] * error_sum_[i];
    }
    
    //Save Data -- Body (Height)
    data_body_.body_des[2] = des_[0];
    data_body_.body_vel_des[2] = vel_des_[0];

    data_body_.body_pos[2] = hume_system_->COM_pos_[2];
    data_body_.body_vel[2] = hume_system_->COM_vel_[2];
    data_body_.body_pos[0] = hume_system_->COM_pos_[0];
    data_body_.body_vel[0] = hume_system_->COM_vel_[0];

    // Save Data -- Ori (pitch)
    data_ori_.ori_des[1] = des_[1];
    data_ori_.ori_vel_des[1] = vel_des_[1];
    
    data_ori_.ori[1] = hume_system_->getOri()[1];
    data_ori_.ori_vel[1] = hume_system_->getOri_vel()[1];

    //Save Data -- Foot
    data_foot_.foot_des[0] = des_[2];
    data_foot_.foot_des[2] = des_[3];
    data_foot_.foot_vel_des[0] = vel_des_[2];
    data_foot_.foot_vel_des[2] = vel_des_[3];
 
    for (int i(0); i<3; ++i){
        data_foot_.foot_pos[i] = curr_foot_pos_[i];
        data_foot_.foot_vel[i] = curr_foot_vel_[i];
    }

    double limit = 1050.0;
    for (int i(0); i< 4; ++i){
        input[i] = crop_value(input[i], -limit, limit, "COM Pos & Foot XZ");
    }
    // Post Process
    _PostProcess_Task();

    return input;
}

Vector COM_Height_Tilting_Foot_XZ_Task::getForce(){
    // force_[0] = 20.0;
    return force_;
}

void COM_Height_Tilting_Foot_XZ_Task::send(){
    //////////////  Gain Data Send /////////////
    HumeProtocol::pd_4_gain gain_data_send;
    for(int i(0); i< 4; ++i){
        gain_data_send.kp[i] = Kp_[i];
        gain_data_send.kd[i] = Kd_[i];
    }

    jspace::saveValue(hume_system_->getCurrentTime(), "height_tilting_foot_xz_time");
    //Save Data in a file -- Body
    jspace::saveVector(force_, "swing_force");
    jspace::saveVector(data_body_.body_des, "Body_des", 3);
    jspace::saveVector(data_body_.body_vel_des, "Body_vel_des", 3);
    jspace::saveVector(data_body_.body_pos, "Body_pos", 3);
    jspace::saveVector(data_body_.body_vel, "Body_vel", 3);

    // Save Data in a file -- Ori
    jspace::saveVector(data_ori_.ori_des, "Ori_des", 3);
    jspace::saveVector(data_ori_.ori_vel_des, "Ori_vel_des", 3);
    jspace::saveVector(hume_system_->getOri(), "Body_ori");
    jspace::saveVector(hume_system_->getOri_vel(), "Body_ori_vel");

    // Save Data in a file -- Foot
    jspace::saveVector(data_foot_.foot_des, "Foot_des", 3);
    jspace::saveVector(data_foot_.foot_vel_des, "Foot_vel_des", 3);
    jspace::saveVector(data_foot_.foot_pos, "Foot_pos", 3);
    jspace::saveVector(data_foot_.foot_vel, "Foot_vel", 3);
}

void COM_Height_Tilting_Foot_XZ_Task::set_Gain(){
}


///////////////////////////////////////////////////////
//////////         HEIGHT TILTING ROLL FOOT TASK
///////////////////////////////////////////////////////
Height_Tilting_Roll_Foot_Task::Height_Tilting_Roll_Foot_Task(HUME_System* hume_system, LinkID link_id):Task(hume_system), link_id_(link_id){
    force_ = Vector::Zero(6);
    Kp_ = Vector(6);
    Kd_ = Vector (6);
    //Height
    Kp_(0) = 230.0;
    //Pitch
    Kp_(1) = 450.0;
    //Roll
    Kp_[2] = 5.0;

    // Height
    Kd_(0) = 10.0;
    // Pitch
    Kd_[1] = 10.0;
    // Roll
    Kd_[2] = 0.0;

    for (int i(3); i<6; ++i){
        Kp_[i] = 1050.0;
        Kd_[i] = 180.0;
    }
    Kp_[5] = 1450.0;
    Kd_[5] = 220.0;
    
    switch(link_id_){
    case LFOOT:
        recieve_gain_port_ = PORT_HEIGHT_TILTING_ROLL_LEFT_FOOT_GAIN;
        send_gain_port_ = PORT_HEIGHT_TILTING_ROLL_LEFT_FOOT_GAIN_FROM_TASK;
        break;
    case RFOOT:
        recieve_gain_port_ = PORT_HEIGHT_TILTING_ROLL_RIGHT_FOOT_GAIN;
        send_gain_port_ = PORT_HEIGHT_TILTING_ROLL_RIGHT_FOOT_GAIN_FROM_TASK;
        break;
    }
}

void Height_Tilting_Roll_Foot_Task::SetTask(const Vector & des, const Vector & vel_des){
    des_ = des;
    vel_des_ = vel_des;

    hume_system_->CopyFoot(curr_foot_pos_, link_id_);
    hume_system_->CopyFootVel(curr_foot_vel_, link_id_);
    
    b_settask_ = true;
}

Matrix Height_Tilting_Roll_Foot_Task::getJacobian(const vector<double> & conf){
    Matrix J = Matrix::Zero(6, NUM_JOINT);
    Matrix J_body, J_foot;
    hume_system_->GetFullJacobian(conf, HIP, J_body);
    J.block(0,0, 1, NUM_JOINT) = J_body.block(2,0, 1, NUM_JOINT);
    J(1, Ry) = 1.0;
    J(2, Rx) = 1.0;
    
    hume_system_->GetFullJacobian(conf, link_id_, J_foot);
    J.block(3, 0, 3, NUM_JOINT) = J_foot;
    return J;
}
void Height_Tilting_Roll_Foot_Task::setCurrentFoot(const Vector & curr_foot_pos, const Vector & curr_foot_vel){
    curr_foot_pos_ = curr_foot_pos;
    curr_foot_vel_ = curr_foot_vel;
}
Vector Height_Tilting_Roll_Foot_Task::getCommand(){
    Vector input(6);
    // Body Height
    input(0) = Kp_(0)*(des_[0] - hume_system_->getBody()(2))
        + Kd_(0)*(vel_des_[0] - hume_system_ -> getBody_vel()(2));
    // Pitch & Roll
    for(int i(1); i<3; ++i){
        input[i] = Kp_[i] * (des_[i] - hume_system_->getOri()(i))
            + Kd_[i] * (vel_des_[i] - hume_system_ ->getOri()[i]);
    }
    
    for (int i(3); i<6 ; ++i){
        input[i] = Kp_[i] * (des_[i] - curr_foot_pos_[i-3]) +
            Kd_[i] * (vel_des_[i] - curr_foot_vel_[i-3]);
    }
    
    //Save Data -- Body (Height)
    data_body_.body_des[2] = des_[0];
    data_body_.body_vel_des[2] = vel_des_[0];

    data_body_.body_pos[2] = hume_system_->getBody()[2];
    data_body_.body_vel[2] = hume_system_->getBody_vel()[2];

    // Save Data -- Ori (pitch)
    for (int i(1) ; i < 3; ++i){
        data_ori_.ori_des[i] = des_[i];
        data_ori_.ori_vel_des[i] = vel_des_[i];
        
        data_ori_.ori[i] = hume_system_->getOri()[i];
        data_ori_.ori_vel[i] = hume_system_->getOri_vel()[i];
    }
    //Save Data -- Foot
    for (int i(0); i< 3; ++i){
        data_foot_.foot_des[i] = des_[3 + i];
        data_foot_.foot_vel_des[i] = vel_des_[3+i];
        data_foot_.foot_pos[i] = curr_foot_pos_[i];
        data_foot_.foot_vel[i] = curr_foot_vel_[i];
    }
    
    double limit = 250.0;
    for (int i(0); i< 6; ++i){
        input[i] = crop_value(input[i], -limit, limit, "Height & Tilting & Roll & Foot");
    }
    // Post Process
    _PostProcess_Task();
    return input;
}

Vector Height_Tilting_Roll_Foot_Task::getForce(){
    return force_;
}

void Height_Tilting_Roll_Foot_Task::send(){
    //////////////  Task Data Send /////////////
    // COMM::send_data(socket_, PORT_BODY_TASK, & data_body_, sizeof(HumeProtocol::body_task_data), IP_ADDR_MYSELF);
    // COMM::send_data(socket_2_, PORT_ORI_TASK, & data_ori_, sizeof(HumeProtocol::ori_task_data), IP_ADDR_MYSELF);
    // COMM::send_data(socket_3_, PORT_FOOT_TASK, & data_foot_, sizeof(HumeProtocol::foot_task_data), IP_ADDR_MYSELF);

    //////////////  Gain Data Send /////////////
    HumeProtocol::pd_6_gain gain_data_send;
    for(int i(0); i< 6; ++i){
        gain_data_send.kp[i] = Kp_[i];
        gain_data_send.kd[i] = Kd_[i];
    }
    // COMM::send_data(socket_gain_send_, send_gain_port_, & gain_data_send, sizeof(HumeProtocol::pd_6_gain), IP_ADDR_MYSELF);
    jspace::saveValue(hume_system_->getCurrentTime(), "height_tilting_roll_foot_time");
    //Save Data in a file -- Body
    jspace::saveVector(data_body_.body_des, "Body_des", 3);
    jspace::saveVector(data_body_.body_vel_des, "Body_vel_des", 3);
    jspace::saveVector(hume_system_->getBody(), "Body_pos");
    jspace::saveVector(hume_system_->getBody_vel(), "Body_vel");

    // Save Data in a file -- Ori
    jspace::saveVector(data_ori_.ori_des, "Ori_des", 3);
    jspace::saveVector(data_ori_.ori_vel_des, "Ori_vel_des", 3);
    jspace::saveVector(hume_system_->getOri(), "Body_ori");
    jspace::saveVector(hume_system_->getOri_vel(), "Body_ori_vel");

    // Save Data in a file -- Foot
    jspace::saveVector(data_foot_.foot_des, "Foot_des", 3);
    jspace::saveVector(data_foot_.foot_vel_des, "Foot_vel_des", 3);
    jspace::saveVector(data_foot_.foot_pos, "Foot_pos", 3);
    jspace::saveVector(data_foot_.foot_vel, "Foot_vel", 3);
}

void Height_Tilting_Roll_Foot_Task::set_Gain(){
    printf("[Height TIlting Roll Foot Task] In Gain Setting \n");
    // Height Gain
    // COMM::recieve_data(socket_recieve_, recieve_gain_port_, & gain_data_, sizeof(HumeProtocol::pd_6_gain), IP_ADDR_MYSELF);
    
    for(int i(0); i< 6; ++i){
        Kp_[i] = gain_data_.kp[i];
        Kd_[i] = gain_data_.kd[i];
    }
    printf("[Height Tilting Roll Foot Task] Recieve Height Gain Set \n");
}

///////////////////////////////////////////////////////
//////////         Tilting TASK
///////////////////////////////////////////////////////
Tilting_Task::Tilting_Task(HUME_System* hume_system):Task(hume_system){
    Kp_ = Vector(1);
    Kp_(0) = 150.0;
    Kd_ = Vector(1);
    Kd_(0) = 5.0;
}

void Tilting_Task::SetTask(const Vector & ang_des, const Vector & ang_vel_des){
    ang_d_ = ang_des;
    ang_vel_d_ = ang_vel_des;
    b_settask_ = true;
}

Matrix Tilting_Task::getJacobian(const vector<double> & conf){
    Matrix J = Matrix::Zero(1, NUM_JOINT);
    J(0, Ry) = 1;
    if(b_set_weight_){
        J = weight_[0]*J;
        b_set_weight_ = false;
    }
    return J;
}

void Tilting_Task::set_weight(const Vector & weight){
    weight_ = weight;
    b_set_weight_ = true;
}

Vector Tilting_Task::getCommand(){
    Vector input(1);
    input(0) = Kp_(0)*(ang_d_(0) - hume_system_->getOri()(1))
        + Kd_(0)*(ang_vel_d_(0) - hume_system_ -> getOri_vel()(1));
    // Post Process
    _PostProcess_Task();
    //Save Data
    data_.ori_des[1] = ang_d_[0];
    data_.ori_vel_des[1] = ang_vel_d_[0];
    data_.ori[1] = hume_system_->getOri()[1];
    data_.ori_vel[1] = hume_system_->getOri_vel()[1];
    
    double limit = 60.0;
    input[0] = crop_value(input[0], -limit, limit, "Tilting");

    return input;
}

void Tilting_Task::send(){
    HumeProtocol::pd_1_gain gain_data_send;

    gain_data_send.kp = Kp_[0];
    gain_data_send.kd = Kd_[0];
    
    // COMM::send_data(socket_, PORT_ORI_TASK, & data_, sizeof(HumeProtocol::ori_task_data), IP_ADDR_MYSELF);
    // COMM::send_data(socket_gain_send_, PORT_TILTING_GAIN_FROM_TASK, & gain_data_send, sizeof(HumeProtocol::pd_1_gain), IP_ADDR_MYSELF);
    jspace::saveValue(hume_system_->getCurrentTime(), "tilting_task_time");
    // Save Data in a file
    jspace::saveVector(data_.ori_des, "Ori_des", 3);
    jspace::saveVector(data_.ori_vel_des, "Ori_vel_des", 3);
    jspace::saveVector(hume_system_->getOri(), "Body_ori");
    jspace::saveVector(hume_system_->getOri_vel(), "Body_ori_vel");
}

void Tilting_Task::set_Gain(){
    printf("[Tilting Task] In setting Gain \n");
    // COMM::recieve_data(socket_recieve_, PORT_TILTING_GAIN, & gain_data_, sizeof(HumeProtocol::pd_1_gain), IP_ADDR_MYSELF);
    
    Kp_[0] = gain_data_.kp;
    Kd_[0] = gain_data_.kd;
    
    printf("[Tilting Task] recieve gain set \n");
}

Vector Tilting_Task::getForce(){
    Vector force(Vector::Zero(1));
    return force;
}

///////////////////////////////////////////////////////
//////////         Orientation TASK
///////////////////////////////////////////////////////

Matrix ORI_Task::getJacobian(const vector<double> & conf){
    Matrix J;
    J = Matrix::Zero(3, NUM_JOINT);
    J(0, Rz) = 1.0;
    J(1, Ry) = 1.0;
    J(2, Rx) = 1.0;
    
    return J;
}
void ORI_Task::SetTask(const Vector & ori_des, const Vector & ori_vel_des){
    ORI_d_ = ori_des;
    ORI_vel_d_ = ori_vel_des;
    b_settask_ = true;
}

Vector ORI_Task::getCommand(){
    Vector input(3);
    for (int i(0); i< 3; ++i){
        input(i) = -Kp_[i]*(hume_system_->getOri()(i) -  ORI_d_(i))
            - Kd_[i]*(hume_system_->getOri_vel()(i) - ORI_vel_d_(i));
        input[i] = crop_value(input[i], -20, 20, "ORI TASK");
    }

    // Post Process
    _PostProcess_Task();

    return input;
}

Vector ORI_Task::getForce(){
    Vector force(Vector::Zero(3));
    return force;
}

///////////////////////////////////////////////////////
//////////         Orientation Ry, Rx TASK
///////////////////////////////////////////////////////
ORI_YX_Task::ORI_YX_Task(HUME_System* hume_system):Task(hume_system){
    Kp_ = Vector(2);
    Kd_ = Vector(2);
    for (int i(0); i< 2; ++i){
        Kp_[i] = 80.0;
        Kd_[i] = 5.0;
    }
    Kp_[1] = 0.0;
    Kd_[1] = 0.0;
}

Matrix ORI_YX_Task::getJacobian(const vector<double> & conf){
    Matrix J;
    J = Matrix::Zero(2, NUM_JOINT);
    J(0, Ry) = 1.0; 
    J(1, Rx) = 1.0;
    
    return J;
}
void ORI_YX_Task::SetTask(const Vector & ori_des, const Vector & ori_vel_des){
    ORI_yx_d_ = ori_des;
    ORI_yx_vel_d_ = ori_vel_des;
    b_settask_ = true;
}

Vector ORI_YX_Task::getCommand(){
    Vector input(2);
    for (int i(0); i< 2; ++i){
        input(i) = -Kp_[i]*(hume_system_->getOri()(i+1) -  ORI_yx_d_(i))
            - Kd_[i]*(hume_system_->getOri_vel()(i+1) - ORI_yx_vel_d_(i));
        input[i] = crop_value(input[i], -35, 35, "ORI YX TASK");
    }
//    pretty_print(input, std::cout, "input ori yx", "");
    // Post Process
    _PostProcess_Task();

    return input;
}

Vector ORI_YX_Task::getForce(){
    Vector force(Vector::Zero(2));
    return force;
}


///////////////////////////////////////////////////////
//////////         FOOT TASK
///////////////////////////////////////////////////////
//task_foot
FOOT_Task::FOOT_Task(HUME_System * hume_system, LinkID link_id):
    Task(hume_system), link_id_(link_id), b_set_curr_foot_(false), error_sum_(Vector::Zero(3))
{

    Kp_ = Vector(3);
    Kd_ = Vector(3);
    Ki_ = Vector(3);

    for (int i(0); i< 3; ++i){
        Kp_(i) = 400.0;
        Kd_(i) = 10.0;
        Ki_(i) = 1400.0;
    }

    switch(link_id){
    case LFOOT:
        recieve_gain_port_ = PORT_LEFT_FOOT_GAIN;
        send_gain_port_ = PORT_LEFT_FOOT_GAIN_FROM_TASK;
        break;
    case RFOOT:
        recieve_gain_port_ = PORT_RIGHT_FOOT_GAIN;
        send_gain_port_ = PORT_RIGHT_FOOT_GAIN_FROM_TASK;
        break;
    }
}

void FOOT_Task::SetTask(const Vector & foot_des, const Vector & foot_vel_des){
    FOOT_d_ = foot_des;
    FOOT_vel_d_ = foot_vel_des;

    if(!b_set_curr_foot_){
        hume_system_->CopyFoot(curr_foot_pos_, link_id_);
        hume_system_->CopyFootVel(curr_foot_vel_, link_id_);
    }
    else{ b_set_curr_foot_ = false; }

    b_settask_ = true;
}
void FOOT_Task::set_weight(const Vector & weight){
    weight_ = weight;
    b_set_weight_ = true;
}
Matrix FOOT_Task::getJacobian(const vector<double> & conf){
    Matrix J;
    hume_system_->GetFullJacobian(conf, link_id_, J);

    if(b_set_weight_){
        for (int i(0); i<3; ++i){
            J.block(i,0, 1, NUM_JOINT) = weight_[i] * J.block(i, 0, 1, NUM_JOINT);
        }
        b_set_weight_ = false;
    }
    
    return J;
}
void FOOT_Task::setCurrentFoot(const Vector & curr_foot_pos, const Vector & curr_foot_vel){
    curr_foot_pos_ = curr_foot_pos;
    curr_foot_vel_ = curr_foot_vel;
    b_set_curr_foot_ = true;
}

Vector FOOT_Task::getCommand(){
    Vector input(3);
    for (int i(0); i< 3; ++i){
        input(i) = -Kp_(i)*(curr_foot_pos_[i] - FOOT_d_(i))
            - Kd_(i)*(curr_foot_vel_[i]- FOOT_vel_d_(i));
        error_sum_[i] += (curr_foot_pos_[i] - FOOT_d_[i])*SERVO_RATE;
    }

    // Integral Control
    for (int i(0); i <3 ; ++i){
        error_sum_[i] = crop_value(error_sum_[i], -100.5, 100.5, "[Foot Task] error sum");
        input[i] += -Ki_[i] * error_sum_[i];
        // Crop
        input[i] = crop_value(input[i], -1650.0, 1650.0, "Foot");

    }
    
    //Save Data
    for (int i(0); i< 3; ++i){
        data_.foot_des[i] = FOOT_d_[i];
        data_.foot_vel_des[i] = FOOT_vel_d_[i];
        data_.foot_pos[i] = curr_foot_pos_[i];
        data_.foot_vel[i] = curr_foot_vel_[i];
    }
    // Post Process
    _PostProcess_Task();
    return input;
}

void FOOT_Task::send(){
    HumeProtocol::pd_3_gain gain_data_send;

    for( int i(0); i<3 ; ++i){
        gain_data_send.kp[i] = Kp_[i];
        gain_data_send.kd[i] = Kd_[i];
        // printf("[%i] Kp, Kd: %f, %f \n",i,  Kp_[i], Kd_[i]); 
    }
    
    // COMM::send_data(socket_, PORT_FOOT_TASK, &data_, sizeof(HumeProtocol::foot_task_data), IP_ADDR_MYSELF);
    // COMM::send_data(socket_gain_send_, send_gain_port_, & gain_data_send, sizeof(HumeProtocol::pd_3_gain), IP_ADDR_MYSELF);
    jspace::saveValue(hume_system_->getCurrentTime(), "foot_task_time");
    //Save Data in a file
    jspace::saveVector(data_.foot_des, "Foot_des", 3);
    jspace::saveVector(data_.foot_vel_des, "Foot_vel_des", 3);
    jspace::saveVector(data_.foot_pos, "Foot_pos", 3);
    jspace::saveVector(data_.foot_vel, "Foot_vel", 3);
}

void FOOT_Task::set_Gain(){
    printf("[Foot Task] In setting Gain\n");
    // COMM::recieve_data(socket_recieve_, recieve_gain_port_, &gain_data_, sizeof(HumeProtocol::pd_3_gain), IP_ADDR_MYSELF);

    for (int i(0); i< 3 ; ++i){
        Kp_[i] = gain_data_.kp[i];
        Kd_[i] = gain_data_.kd[i];
        // printf("[%i] Kp, Kd: %f, %f \n", Kp_[i], Kd_[i]); 
    }
    
    printf("[Foot Task] recieve gain set \n");
}

Vector FOOT_Task::getForce(){
    Vector force(Vector::Zero(3));
    // force[2] = -3.0;
    return force;
}

///////////////////////////////////////////////////////
//////////         Joint TASK
///////////////////////////////////////////////////////

Joint_Task::Joint_Task(HUME_System * system):Task(system){
    Kp_ = Vector::Zero(NUM_ACT_JOINT);
    Kd_ = Vector::Zero(NUM_ACT_JOINT);
    Ki_ = Vector::Zero(NUM_ACT_JOINT);
    error_sum_ = Vector::Zero(NUM_ACT_JOINT);
    
    for (int i(0); i< NUM_ACT_JOINT; ++i){
        Kp_[i] = 850.0;
        Kd_[i] = 10.0;
        Ki_[i] = 950.0;
    }
}

Matrix Joint_Task::getJacobian(const vector<double> & conf){
    Matrix J = Matrix::Zero(NUM_ACT_JOINT, NUM_JOINT);
    J.block(0, NUM_PASSIVE, NUM_ACT_JOINT, NUM_ACT_JOINT) = Matrix::Identity(NUM_ACT_JOINT, NUM_ACT_JOINT);

    return J;
}

Vector Joint_Task::getCommand(){
    Vector input(NUM_ACT_JOINT);
    
    for (int i(0); i< NUM_ACT_JOINT; ++i){
        error_sum_[i] += (jpos_des_[i] - hume_system_->tot_configuration_[i + NUM_PASSIVE])*SERVO_RATE;
        input[i] = Kp_[i]*(jpos_des_[i] - hume_system_->tot_configuration_[i + NUM_PASSIVE])
            + Kd_[i]*(jvel_des_[i] - hume_system_->tot_vel_[i + NUM_PASSIVE]);
    }
    for( int i(0); i< NUM_ACT_JOINT; ++i){
        error_sum_[i] = crop_value(error_sum_[i], -1.0, 1.0, "error sum jpos control");
        input[i] += Ki_[i]*error_sum_[i];
        input[i] = crop_value(input[i], -600.0, 600.0, "Joint Task");    
    }
    _PostProcess_Task();
    return input;
}
void Joint_Task::SetTask(const Vector & jpos_des, const Vector & jvel_des){
    jpos_des_ = jpos_des;
    jvel_des_ = jvel_des;

    b_settask_ = true;
}
Vector Joint_Task::getForce(){
    Vector force(Vector::Zero(NUM_ACT_JOINT));
    return force;
}



///////////////////////////////////////////////////////
//////////         HEIGHT TILTING ROLL TASK
///////////////////////////////////////////////////////
Height_Tilting_Roll_Task::Height_Tilting_Roll_Task(HUME_System* hume_system):Task(hume_system){
    Kp_ = Vector(3);
    Kp_(0) = 230.0;
    Kp_(1) = 400.0;
    Kp_[2] = 5.0;
    
    Kd_ = Vector(3);
    Kd_(0) = 5.0;
    Kd_[1] = 5.0;
    Kd_[2] = 0.0;
}

void Height_Tilting_Roll_Task::SetTask(const Vector & des, const Vector & vel_des){
    des_ = des;
    vel_des_ = vel_des;
    b_settask_ = true;
}

Matrix Height_Tilting_Roll_Task::getJacobian(const vector<double> & conf){
    Matrix J = Matrix::Zero(3, NUM_JOINT);
    Matrix J_body;
    hume_system_->GetFullJacobian(conf, HIP, J_body);
    J.block(0,0, 1, NUM_JOINT) = J_body.block(2,0, 1, NUM_JOINT);
    J(1, Ry) = 1.0;
    J(2, Rx) = 1.0;
    return J;
}

Vector Height_Tilting_Roll_Task::getCommand(){
    Vector input(3);
    // Body Height
    input(0) = Kp_(0)*(des_[0] - hume_system_->getBody()(2))
        + Kd_(0)*(vel_des_[0] - hume_system_ -> getBody_vel()(2));
    // Pitch & Roll
    for (int i(1); i<3; ++i){
        input[i] = Kp_[i] * (des_[i] - hume_system_->getOri()(i))
            + Kd_[i] * (vel_des_[i] - hume_system_ ->getOri()[i]);
    }

    //Save Data -- Body (Heighht)
    data_body_.body_des[2] = des_[0];
    data_body_.body_vel_des[2] = vel_des_[0];

    data_body_.body_pos[2] = hume_system_->getBody()[2];
    data_body_.body_vel[2] = hume_system_->getBody_vel()[2];

    // Save Data -- Ori (pitch)
    for(int i(1); i<3; ++i){
        data_ori_.ori_des[i] = des_[i];
        data_ori_.ori_vel_des[i] = vel_des_[i];
        
        data_ori_.ori[i] = hume_system_->getOri()[i];
        data_ori_.ori_vel[i] = hume_system_->getOri_vel()[i];
    }
    
    double limit = 200.0;
    for (int i(0); i< 2; ++i){
        input[i] = crop_value(input[i], -limit, limit, "Height & Tilting");
    }
    // Post Process
    _PostProcess_Task();

    return input;
}

Vector Height_Tilting_Roll_Task::getForce(){
    Vector force(Vector::Zero(3));
    return force;
}

void Height_Tilting_Roll_Task::send(){
    // COMM::send_data(socket_, PORT_BODY_TASK, & data_body_, sizeof(HumeProtocol::body_task_data), IP_ADDR_MYSELF);
    // COMM::send_data(socket_2_, PORT_ORI_TASK, & data_ori_, sizeof(HumeProtocol::ori_task_data), IP_ADDR_MYSELF);
    
    HumeProtocol::pd_3_gain gain_data_send;
    for(int i(0); i<3 ; ++i){
        gain_data_send.kp[i] = Kp_[i];
        gain_data_send.kd[i] = Kd_[i];
    }

    // COMM::send_data(socket_gain_send_, PORT_HEIGHT_TILTING_ROLL_GAIN_FROM_TASK, &gain_data_send,  sizeof(HumeProtocol::pd_3_gain), IP_ADDR_MYSELF);
    jspace::saveValue(hume_system_->getCurrentTime(), "height_tilting_roll_time");
    //Save Data in a file -- Body
    jspace::saveVector(data_body_.body_des, "Body_des", 3);
    jspace::saveVector(data_body_.body_vel_des, "Body_vel_des", 3);
    jspace::saveVector(hume_system_->getBody(), "Body_pos");
    jspace::saveVector(hume_system_->getBody_vel(), "Body_vel");

    // Save Data in a file -- Ori
    jspace::saveVector(data_ori_.ori_des, "Ori_des", 3);
    jspace::saveVector(data_ori_.ori_vel_des, "Ori_vel_des", 3);
    jspace::saveVector(hume_system_->getOri(), "Body_ori");
    jspace::saveVector(hume_system_->getOri_vel(), "Body_ori_vel");

}

void Height_Tilting_Roll_Task::set_Gain(){
    printf("[Height TIlting Roll Task] In setting Gain \n");
    // COMM::recieve_data(socket_recieve_, PORT_HEIGHT_TILTING_ROLL_GAIN, & gain_data_, sizeof(HumeProtocol::pd_3_gain), IP_ADDR_MYSELF);
    for (int i(0); i<3; ++i){
        Kp_[i] = gain_data_.kp[i];
        Kd_[i] = gain_data_.kd[i];
    }
    printf("[Height Tilting Task] recieve gain set \n");
}
