#ifndef CONTROLLER_XZ_STEP_TEST
#define CONTROLLER_XZ_STEP_TEST

#include "controller_Step.h"


class Ctrl_XZ_Step_Test: public Virtual_Ctrl_XZ_Step{
public:
    Ctrl_XZ_Step_Test(HUME_System* _hume_system);
    virtual ~Ctrl_XZ_Step_Test();

protected:
    virtual bool _Do_Supp_Lift();
    virtual void _set_land_location();

};



#endif
