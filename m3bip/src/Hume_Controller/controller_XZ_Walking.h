#ifndef CONTROLLER_XZ_WALKING_BOOM 
#define CONTROLLER_XZ_WALKING_BOOM 

#include "Controller_Hume.h"
#include "controller_Step.h"
#include "Planner.h"
#include "filters.h"

#define CUT_OFF_FRE_X 30

class Ctrl_XZ_Walking : public Virtual_Ctrl_XZ_Step{
public:
    Ctrl_XZ_Walking(HUME_System* hume_system);
    virtual ~Ctrl_XZ_Walking();
    virtual void getCurrentCommand(std::vector<double> & command);

public:
    bool _move_forward();
    bool CalculateLandingLocation(const Vector & stance_foot_loc, double current_time, double local_switch_time, LinkID stance_foot);
    bool CalculateLandingLocation2(const Vector & stance_foot_loc, double current_time, LinkID stance_foot);
    
///////  Member Variable ///////////////////
    Planner* planner_;
    
    double step_width_;

    double middle_time_x_;
    double middle_time_y_;

    double switching_time_;
    double achieving_ratio_;
    std::vector<Vector> com_pos_trj_supp_;
    std::vector<Vector> com_vel_trj_supp_;

    Task* XZ_tilting_task_;

    Vector body_vel_ave_;
    Vector body_vel_pre_;
    Vector body_vel_pre_2_;
    Vector body_vel_pre_3_;

    double lifting_vel_;
    double remaining_time_;

    double a, b, c, d;
    double com_x_offset_;
    double com_z_offset_;
    double tot_transition_time_;
    double walking_length_;
    filter* filter_x_vel_;
    CFilterButterworth24db butter_filter_x_vel_;

    void inverted_pendulum_filter(double pos, double vel, double ratio, double foot_pos, double height);
    void _pendulum_update(double pos, double foot_pos, double height);
protected:
    double vel_est_;
    double pos_est_; // Not used
    void _set_foot_x_traj_coeff(double curr_vel);
    virtual bool _Do_Supp_Lift();
    virtual bool _Do_Supp_Land();
    virtual bool _Stop();
    virtual void _set_land_location();
    
};

#endif
