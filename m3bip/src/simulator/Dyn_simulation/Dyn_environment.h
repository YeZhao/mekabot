#ifndef		_DYN_ENVIRONMENT_
#define		_DYN_ENVIRONMENT_

#include "Hume.h"
#include "srDyn/srSystem.h"
#include "srDyn/srCollision.h"
#include "Ground.h"
#include "Wedge_front.h"
#include "Wedge_back.h"
#include "LieGroup/LieGroup.h"
#include <vector>
#include "Hume_Controller/HumeSystem.h"

using namespace jspace;
#define SIMULATION_TIME_STEP 0.001

struct state
{
    std::vector<double> conf;
    std::vector<double> jvel;
    std::vector<double> torque   ;
    std::vector<double> euler_ang;
    std::vector<double> ang_vel;
    bool left_foot_contact;
    bool right_foot_contact;
};

class Dyn_environment
{
public:
    Dyn_environment();
    ~Dyn_environment();

    static void ContolFunction(void* _data);
    void Rendering_Fnc();
    void SetCurrentState_All();
    void saveLandingLocation();
public:
    Hume*	m_Hume;
    srSpace*	m_Space;
    Ground*	m_ground;
    Wedge_back* m_wedge_back;
    Wedge_front* m_wedge_front;
    Controller_Hume*  controller_;
    Vector landing_loc_;

    HUME_System* hume_system_;

    std::vector<double> curr_conf_;
    std::vector<double> curr_jvel_;
    std::vector<double> torque_   ;
    std::vector<double> euler_ang_;
    std::vector<double> ang_vel_  ;
    bool left_foot_contact_ ;
    bool right_foot_contact_;

    std::vector<state> history_state_;
};

#endif
