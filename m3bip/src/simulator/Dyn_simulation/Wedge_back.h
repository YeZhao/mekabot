#ifndef _WEDGE_BACK_
#define _WEDGE_BACK_

#include "srDyn/srSpace.h"

class Wedge_back : public srSystem
{
public:
    srLink*		m_WedgeBack;
    srCollision*m_Surface;

public:
    Wedge_back(): m_WedgeBack(NULL), m_Surface(NULL) {};
    ~Wedge_back() { delete m_WedgeBack; delete m_Surface; };
    srSystem*	 BuildWedgeBack() {
        if(m_WedgeBack == NULL)	m_WedgeBack = new srLink;
        if(m_Surface == NULL)		m_Surface = new srCollision;
        m_WedgeBack->GetGeomInfo().SetShape(srGeometryInfo::BOX);
        m_WedgeBack->GetGeomInfo().SetDimension(1.25, 0.45, 0.01);
//        m_WedgeBack->SetFrame(EulerZYX(Vec3(0, 1.05, 0), Vec3(-0.3, 0, 0.54)));
//        m_WedgeBack->SetFrame(EulerZYX(Vec3(0, 0.1, 0), Vec3(-0.6, 0, 0.44)));
        //m_WedgeBack->SetFrame(EulerZYX(Vec3(0, 0.2, 0), Vec3(-0.6, 0, 0.5)));
//        m_WedgeBack->SetFrame(EulerZYX(Vec3(0, 0.7, 0), Vec3(-0.25, 0, 0.6)));

        m_WedgeBack->SetFrame(EulerZYX(Vec3(0, 1, 0), Vec3(-0.45, 0, 0.45)));
        // 0.6, -0.6

        m_WedgeBack->UpdateInertia();

        m_Surface->GetGeomInfo().SetShape(srGeometryInfo::PLANE);//m_WedgeBack->GetGeomInfo().GetShape()
        //m_Surface->GetGeomInfo().SetDimension(m_WedgeBack->GetGeomInfo().GetDimension());
        m_Surface->SetLocalFrame(SE3());
        m_WedgeBack->AddCollision(m_Surface);

        //m_Surface->UpdateBoundingRadius();

        m_WedgeBack->SetFriction(10.65);
        m_WedgeBack->SetDamping(0.5);
        m_WedgeBack->SetRestitution(0.5);
        this->SetBaseLink(m_WedgeBack);
        this->SetBaseLinkType(FIXED);
        this->SetSelfCollision(true);

        return this;
    };
};
#endif //WEDGE_BACK_
