#ifndef _WEDGE_FRONT_
#define _WEDGE_FRONT_

#include "srDyn/srSpace.h"

class Wedge_front : public srSystem
{
public:
    srLink*		m_WedgeFront;
    srCollision*m_Surface;

public:
    Wedge_front(): m_WedgeFront(NULL), m_Surface(NULL) {};
    ~Wedge_front() { delete m_WedgeFront; delete m_Surface; };
    srSystem*	 BuildWedgeFront() {
        if(m_WedgeFront == NULL)	m_WedgeFront = new srLink;
        if(m_Surface == NULL)		m_Surface = new srCollision;
        m_WedgeFront->GetGeomInfo().SetShape(srGeometryInfo::BOX);
        m_WedgeFront->GetGeomInfo().SetDimension(1.25, 0.45, 0.01);
//        m_WedgeFront->SetFrame(EulerZYX(Vec3(0, 1.05, 0), Vec3(-0.3, 0, 0.54)));
//        m_WedgeFront->SetFrame(EulerZYX(Vec3(0, 0.1, 0), Vec3(-0.6, 0, 0.44)));
        //m_WedgeFront->SetFrame(EulerZYX(Vec3(0, 0.2, 0), Vec3(-0.6, 0, 0.5)));
//        m_WedgeFront->SetFrame(EulerZYX(Vec3(0, 0.7, 0), Vec3(-0.25, 0, 0.6)));

        m_WedgeFront->SetFrame(EulerZYX(Vec3(0, -1, 0), Vec3(0.4, 0, 0.45)));
        // 0.6, -0.6

        m_WedgeFront->UpdateInertia();

        m_Surface->GetGeomInfo().SetShape(srGeometryInfo::PLANE);//m_WedgeFront->GetGeomInfo().GetShape()
        //m_Surface->GetGeomInfo().SetDimension(m_WedgeFront->GetGeomInfo().GetDimension());
        m_Surface->SetLocalFrame(SE3());
        m_WedgeFront->AddCollision(m_Surface);

        //m_Surface->UpdateBoundingRadius();

        m_WedgeFront->SetFriction(10.65);
        m_WedgeFront->SetDamping(0.5);
        m_WedgeFront->SetRestitution(0.5);
        this->SetBaseLink(m_WedgeFront);
        this->SetBaseLinkType(FIXED);
        this->SetSelfCollision(true);

        return this;
    };
};
#endif //WEDGE_FRONT_
