#include "Dyn_environment.h"
#include <iostream>
#include <vector>

////////////////////////////////////////////////
#ifdef __APPLE__
#include <GLUT/glut.h>
#endif

#ifdef linux
#include <GL/glut.h>
#endif
////////////////////////////////////////////////

#include "common/utils.h"
#include "util/pseudo_inverse.hpp"
#include "util/wrap_eigen.hpp"

#include "Hume_Controller/Controller_Hume.h"
#include "Hume_Controller/controller_Step.h"

#include "Hume_Controller/controller_Body_Test.h"
#include "analytic_solution/analytic_sol.h"
#include "analytic_solution/analytic_jacobian.h"
#include "Hume_Controller/Process.h"

std::vector<Vec3> g_trajectory;
std::vector<Vec3> g_leftfoot;
std::vector<Vec3> g_rightfoot;

//#define SENSOR_NOISE
#define SENSOR_DELAY 0

Dyn_environment::Dyn_environment():
    curr_conf_(SIM_NUM_RJOINT),
    curr_jvel_(SIM_NUM_RJOINT),
    torque_   (SIM_NUM_RJOINT),
    euler_ang_(3),
    ang_vel_(3),
    left_foot_contact_(false),
    right_foot_contact_(true)
{
    m_Hume = new Hume(Vec3(0.0, 0.0, 0.0), srSystem::FIXED, srJoint::TORQUE);
    m_Space = new srSpace();
    m_ground = new Ground();
    m_wedge_front = new Wedge_front();
    m_wedge_back = new Wedge_back();

    m_Space->AddSystem(m_ground->BuildGround());
    m_Space->AddSystem(m_wedge_front->BuildWedgeFront());
    m_Space->AddSystem(m_wedge_back->BuildWedgeBack());
    m_Space->AddSystem((srSystem*)m_Hume);

    m_Space->DYN_MODE_PRESTEP();
    m_Space->SET_USER_CONTROL_FUNCTION_2(ContolFunction);
    m_Space->SetTimestep(SIMULATION_TIME_STEP);
    m_Space->SetGravity(0.0,0.0,-9.8);
    m_Space->SetNumberofSubstepForRendering(10);
    
    hume_system_ = new HUME_System();
    hume_system_->ori_mode_ = 3;
    
    hume_system_->preparing_count_ = 1;
    ((Data_Gathering*)hume_system_->data_gathering_)->Data_Gathering_Holding_ = 10000;
    ((stabilizing*)hume_system_->controller_->pro_stable_)->stable_move_time_ = 0.3;
    ((stabilizing*)hume_system_->controller_->pro_stable_)->stable_lifting_time_ = 1.6;
    ((stabilizing*)hume_system_->controller_->pro_stable_)->lifting_height_ = 0.05;

#ifdef WALKING_3D
    ((stabilizing*)hume_system_->controller_->pro_stable_)->stable_move_time_ = 0.001;
    ((stabilizing*)hume_system_->controller_->pro_stable_)->stable_lifting_time_ = 0.001;
    ((stabilizing*)hume_system_->controller_->pro_stable_)->lifting_height_ = 0.0;
#endif

#ifdef Stepping_Test
    // ((Ctrl_Step_Test*)(hume_system_->controller_))->supp_time_ = 0.1;
    // ((Ctrl_Step_Test*)(hume_system_->controller_))->supp_lift_transition_time_ = 0.7;
#endif
    
    landing_loc_ = Vector::Zero(2);
    
    printf("left foot height: %f, \n",m_Hume->m_Link[HumeID::SIM_LFOOT].GetMassCenter()[2]);
    printf("right foot height: %f, \n",m_Hume->m_Link[HumeID::SIM_RFOOT].GetMassCenter()[2]);
}	

void Dyn_environment::ContolFunction( void* _data )
{

    static int iter(0);

    Dyn_environment* pDyn_env = (Dyn_environment*)_data;

    pDyn_env->SetCurrentState_All();

    int return_state;
    std::vector<double> command;
    if(iter<SENSOR_DELAY){
        return_state = iter;
    }
    else{
        return_state = iter - SENSOR_DELAY;
    }
    bool motion_start;
    std::vector<int32_t> dummy_1(6);
    std::vector<int32_t> dummy_2(6);
///////////////////////////////////////////////////////////////////    
    pDyn_env->hume_system_->getTorqueInput(
        pDyn_env->history_state_[return_state].conf,
        pDyn_env->history_state_[return_state].jvel,
        pDyn_env->history_state_[return_state].torque   ,
        pDyn_env->history_state_[return_state].euler_ang,
        pDyn_env->history_state_[return_state].ang_vel,
        pDyn_env->history_state_[return_state].left_foot_contact,
        pDyn_env->history_state_[return_state].right_foot_contact,
        dummy_1,
        dummy_2,
        command, motion_start);
    
    for(int i(0); i<SIM_NUM_RJOINT; ++i){
        pDyn_env->m_Hume->Full_Joint_State_[i + SIM_NUM_PASSIVE]->m_rCommand = -command[i];
    }
    
    //////////////////////
    //  PASSIVE Joint   //
    //////////////////////
    for (int i(0); i<SIM_NUM_PASSIVE; ++i){
        pDyn_env->m_Hume->Full_Joint_State_[i]->m_rCommand = 0.0;
    }

#if defined (SIM_BOOM_SYSTEM) || defined (SIM_BOOM_X_SYSTEM)
    //FIX Abduction Joint
    pDyn_env->m_Hume->Full_Joint_State_[HumeID::SIM_J_LEFT_HIP1]->m_rCommand = 0.0;
    pDyn_env->m_Hume->Full_Joint_State_[HumeID::SIM_J_RIGHT_HIP1]->m_rCommand = 0.0;
    
    double rx_angle ( pDyn_env->m_Hume->Full_Joint_State_[3+ HumeID::SIM_Rx]->m_rValue[0]);
    double rx_ang_vel ( pDyn_env->m_Hume->Full_Joint_State_[3+ HumeID::SIM_Rx]->m_rValue[1]);

    double y_dis (pDyn_env->m_Hume->Full_Joint_State_[HumeID::SIM_Y]->m_rValue[0]);
    double y_vel (pDyn_env->m_Hume->Full_Joint_State_[HumeID::SIM_Y]->m_rValue[1]);
    
    pDyn_env->m_Hume->Full_Joint_State_[3 + HumeID::SIM_Rx]->m_rCommand = -100.0*pow(40*rx_angle,3.0) - 370.5 * rx_ang_vel;
    pDyn_env->m_Hume->Full_Joint_State_[ HumeID::SIM_Y]->m_rCommand = -540.0*tan(y_dis*20.0) - 255.5 * y_vel;
#endif    
//////////////////////////////////////////////////////////////////////////////

#ifdef WALKING_3D
    pDyn_env->saveLandingLocation();
#endif
    ++iter;
}

void Dyn_environment::saveLandingLocation(){
    Ctrl_Step* ctrl_3d = (Ctrl_Step*)hume_system_->controller_;
    double theta(0.0);
    Vec3 offset;
    if(ctrl_3d->phase_ == LsRm || ctrl_3d->phase_ == RsLm){
         
        Vector land_loc(2);
        theta  = m_Hume->Full_Joint_State_[SIM_NUM_PASSIVE_P]->m_rValue[0];
        offset = m_Hume->m_Link[ctrl_3d->stance_foot_].GetMassCenter();
        land_loc[0] = ctrl_3d->landing_loc_[0]*cos(theta) - ctrl_3d->landing_loc_[1]*sin(theta);
        land_loc[1] = ctrl_3d->landing_loc_[0]*sin(theta) + ctrl_3d->landing_loc_[1]*cos(theta);
        land_loc[0] += offset[0];
        land_loc[1] += offset[1];

        landing_loc_ = land_loc;
    }
}

void Dyn_environment::SetCurrentState_All(){

    Vec3 ang_noise;
    Vec3 ang_vel_noise;

#ifdef SENSOR_NOISE
    for (int i(0); i< 3; ++i){
        ang_noise[i] = 0.05*generator_white_noise(0.0, 1.0);
        ang_vel_noise[i] = 0.01*generator_white_noise(0.0, 1.0);
    }
#endif
    
    for (int i(0); i< SIM_NUM_RJOINT; ++i){
        curr_conf_[i] = -m_Hume->Full_Joint_State_[i + SIM_NUM_PASSIVE]->m_rValue[0];
        curr_jvel_[i] = -m_Hume->Full_Joint_State_[i + SIM_NUM_PASSIVE]->m_rValue[1];
        torque_ [i]   = -m_Hume->Full_Joint_State_[i + SIM_NUM_PASSIVE]->m_rValue[3];
    }
    // R x, y, z
    for (int i(0); i<3; ++i){
        euler_ang_[2-i] = m_Hume->Full_Joint_State_[i+ SIM_NUM_PASSIVE_P] -> m_rValue[0] + ang_noise[i];
        ang_vel_[2-i] = m_Hume->Full_Joint_State_[i + SIM_NUM_PASSIVE_P] -> m_rValue[1] + ang_vel_noise[i];
    }
    if(m_Hume->m_Link[HumeID::SIM_LFOOT].GetMassCenter()[2] < SIM_FOOT_RADIUS + 0.005){
        left_foot_contact_ = true;
    }
    else{ left_foot_contact_ = false; }

    if(m_Hume->m_Link[HumeID::SIM_RFOOT].GetMassCenter()[2] < SIM_FOOT_RADIUS + 0.005){
        right_foot_contact_ = true;
    }
    else{ right_foot_contact_ = false; }

    state curr_state;
    curr_state.conf               =   curr_conf_;
    curr_state.jvel               =   curr_jvel_;
    curr_state.torque             =   torque_   ;
    curr_state.euler_ang          =   euler_ang_;
    curr_state.ang_vel            =   ang_vel_;
    curr_state.left_foot_contact  =   left_foot_contact_;
    curr_state.right_foot_contact =   right_foot_contact_;

    history_state_.push_back(curr_state);
}

Dyn_environment::~Dyn_environment()
{
    SR_SAFE_DELETE(m_Hume);
    SR_SAFE_DELETE(m_Space);
    SR_SAFE_DELETE(m_ground);
    SR_SAFE_DELETE(m_wedge_front);
    SR_SAFE_DELETE(m_wedge_back);
}

void Dyn_environment::Rendering_Fnc()
{
    double radi(0.05);
    glColor3f(0.5f, 0.1f, 0.5f);
    glBegin(GL_LINE_LOOP);
    double theta(0.0);
    for (int i(0); i<3600; ++i){
        theta += DEG2RAD(i*0.1);
        glVertex2f(landing_loc_[0] + cos(theta)*radi, landing_loc_[1] + sin(theta)*radi);
        
    }
    glEnd();

    glColor3f(0.5f, 0.1f, 0.1f);
    glBegin(GL_LINE_STRIP);
    for (unsigned int i(0); i<g_trajectory.size(); ++i)
    {
        glVertex3f(g_trajectory[i][0], g_trajectory[i][1], g_trajectory[i][2]);
    }
    glEnd();

    glColor3f(0.5f, 1.1f, 0.1f);
    glBegin(GL_LINE_STRIP);
    for (unsigned int i(0); i<g_leftfoot.size(); ++i)
    {
        glVertex3f(g_leftfoot[i][0],
                   g_leftfoot[i][1],
                   g_leftfoot[i][2]);
    }
    glEnd();

    glColor3f(0.5f, 0.1f, 1.1f);
    glBegin(GL_LINE_STRIP);
    for (unsigned int i(0); i<g_rightfoot.size(); ++i)
    {
        glVertex3f(g_rightfoot[i][0],
                   g_rightfoot[i][1],
                   g_rightfoot[i][2]);
    }
    glEnd();

    double small_ratio(0.02);
    // Right Foot Force
    Vec3 right_foot = m_Hume->m_Link[HumeID::SIM_RFOOT].GetFrame().GetPosition();
    Vector right_force(3);
    for (int i(0); i<3; ++i){
        right_force[i] = right_foot[i] + small_ratio * hume_system_->curr_RFoot_force_[i];
    }
    glColor3f(0.3f, 0.1f, 0.1f);
    glBegin(GL_LINES);
    glVertex3f(right_foot[0],  right_foot[1],  right_foot[2]);
    glVertex3f(right_force[0], right_force[1], right_force[2]);
    glEnd();

    // Left Foot Force
    Vec3 left_foot = m_Hume->m_Link[HumeID::SIM_LFOOT].GetFrame().GetPosition();
    Vector left_force(3);
    for (int i(0); i<3; ++i){
        left_force[i] = left_foot[i] + small_ratio * hume_system_->curr_LFoot_force_[i];
    }
    glColor3f(0.3f, 0.1f, 0.1f);
    glBegin(GL_LINES);
    glVertex3f(left_foot[0],  left_foot[1],  left_foot[2]);
    glVertex3f(left_force[0], left_force[1], left_force[2]);
    glEnd();
}

