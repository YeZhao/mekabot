#ifndef _STAIR3_
#define _STAIR3_

#include "srDyn/srSpace.h"

class Stair3 : public srSystem
{
public:
    srLink*		m_Stair;
    srCollision*m_Surface;

    // srLink*     m_Stair2;
    // srCollision*m_Surface2;

public:
    Stair3(): m_Stair(NULL), m_Surface(NULL){};
    ~Stair3() { delete m_Stair; delete m_Surface;};
    srSystem*	 BuildStair() {
        if(m_Stair == NULL)	m_Stair = new srLink;
        if(m_Surface == NULL)		m_Surface = new srCollision;
        m_Stair->GetGeomInfo().SetShape(srGeometryInfo::BOX);
        m_Stair->GetGeomInfo().SetDimension(0.35, 0.45, 0.01);//(1.75, 0.45, 0.05);
        m_Stair->SetFrame(EulerZYX(Vec3(0, 0, 0), Vec3(0.6, 0, 0.8)));//(EulerZYX(Vec3(0, 0.87, 0), Vec3(-1.4, 0, 0.866)));
        m_Stair->UpdateInertia();

        m_Surface->GetGeomInfo().SetShape(srGeometryInfo::BOX);
        //m_Surface->GetGeomInfo().SetDimension(1.75, 1.45, 0.01);//(1.75, 0.45, 0.05);
        m_Surface->SetLocalFrame(SE3());

        m_Stair->AddCollision(m_Surface);

        //m_Surface->UpdateBoundingRadius();

        m_Stair->SetFriction(10.65);
        m_Stair->SetDamping(0.5);
        m_Stair->SetRestitution(0.5);
    
        this->SetBaseLink(m_Stair);
        this->SetBaseLinkType(FIXED);
        this->SetSelfCollision(true);

        return this;
    };
};
#endif //STAIRS_
