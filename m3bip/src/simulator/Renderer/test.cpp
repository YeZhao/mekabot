#include <iostream>
#include <fstream>
#include <Windows.h>
#include "Header.h"
#include<string>
 
using namespace std;
 
long File_Size;
unsigned short Chunk_Id;
unsigned int Chunk_Length;
unsigned char l_char;
unsigned short length;
int ObjectBlockChunk;
 
void Load3ds(char * Path)
{
        std::string fn = Path;
        if(fn.substr(fn.find_last_of(".") + 1) == "3ds")
        {
                FILE * myfile;
                myfile = fopen ( Path , "rb" );
 
                ///get file size
                fseek (myfile , 0 , SEEK_END);
                File_Size = ftell (myfile);
                rewind (myfile);
 
                while (ftell(myfile) < File_Size) //Loop to scan the whole file
                {
                        fread (&Chunk_Id, 2, 1, myfile);
                        fread (&Chunk_Length, 4, 1, myfile);
 
                        switch (Chunk_Id)
                        {
                                case 0x4d4d:
                                        Obj = new Object3d();
                                        break;
 
                                case 0x3d3d:
                                        break;
 
                                case 0x4000://set name
                                        /*
                                        cout<<"4000 found"<<endl;
                                        cout<<"name: ";*/
                                        Obj->Name = new char;
                                        ObjectBlockChunk = 0;
                                        do
                                        {
                                                fread (&l_char, 1, 1, myfile);
                                                Obj->Name[ObjectBlockChunk]=l_char;
                                                ObjectBlockChunk++;
                                        }
                                        while(l_char != '\0' && ObjectBlockChunk<20);
 
                                        break;
 
                                case 0x4100://vertex, index info
                                        break;
 
                                case 0x4110: // set vertices
                                        fread (&length, sizeof (unsigned short), 1, myfile);
                               
                                        Obj->VertexCount = length*3;
                                        Obj->vertices = new ColorVertex[Obj->VertexCount];
 
                                        for(int i = 0; i<length; i++)
                                        {
                                                fread (&Obj->vertices[i].x, sizeof(float), 1, myfile);
                                                fread (&Obj->vertices[i].y, sizeof(float), 1, myfile);
                                                fread (&Obj->vertices[i].z, sizeof(float), 1, myfile);
                                                Obj->vertices[i].color = D3DCOLOR_ARGB(100, i*50, i*50, i*50);
                                        }
                                        break;
 
                                case 0x4120://set indices
                                        {
                                                fread (&length, sizeof (unsigned short), 1, myfile);
 
                                                Obj->IndexCount = length*3;
                                                Obj->indices = new WORD[Obj->IndexCount];
 
                                                short *TempFlag;
                                                for (int i = 0; i<length*3; i++)
                                                {
                                                        fread (&Obj->indices[i], sizeof (unsigned short), 1, myfile);
                                                        i++;
                                                        fread (&Obj->indices[i], sizeof (unsigned short), 1, myfile);
                                                        i++;
                                                        fread (&Obj->indices[i], sizeof (unsigned short), 1, myfile);
 
                                                        fread (&TempFlag, sizeof(short), 1, myfile);
                                                }                                      
                                        }
                                        break;
 
                                case 0x4140://texture map
                                        fread (&length, sizeof (unsigned short), 1, myfile);
                                        for (int i=0; i<length; i++)
                                        {
                                                fread (&Obj->vertices[i].u, sizeof (float), 1, myfile);
                                                fread (&Obj->vertices[i].v, sizeof (float), 1, myfile);
                                        }
                                        break;
 
                                default://if anything else fails, new chunk
                                        fseek(myfile, Chunk_Length-6, SEEK_CUR);
                        }
                }
                Obj->SetColorVertexBuffer();
                Obj->SetColorIndexBuffer();    
                ObjectList.push_back(Obj);
 
                fclose(myfile);
        }
        else
        {
                MessageBox(NULL, L"Wrong file type, select .3ds file", NULL, NULL);
        }
        return;
}
