/*
 * Daemon.hpp
 *
 *    Created on: Apr 7, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */


#ifndef DAEMON_HPP_
#define DAEMON_HPP_

#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
namespace uta{
namespace threads{

class Daemon
{
	pthread_t daemonThread;
	bool firstLoopFlag;
	bool isRunning;
protected:
	virtual void loop(void)=0;
	void terminate();
	bool isFirstLoop();
public:
	Daemon(void);
	void run(void);
	virtual ~Daemon(void);
	void start();
};
void *runDaemon(void * arg);
void sigint(int signo);
}
}


#endif /* DAEMON_HPP_ */
