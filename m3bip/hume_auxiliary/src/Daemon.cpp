/*
 * Daemon.cpp
 *
 *    Created on: Apr 7, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#include "Daemon.hpp"
#include "./../../src/Hume_Controller/HumeSystem.h"

namespace uta{
namespace threads{

Daemon::Daemon(void) :
			daemonThread(),
			firstLoopFlag(false),
			isRunning(false)
{

}
Daemon::~Daemon()
{
	pthread_cancel(daemonThread);
	pthread_join(daemonThread, NULL);
}
void Daemon::terminate()
{
	isRunning = false;
}
bool Daemon::isFirstLoop()
{
	return firstLoopFlag;
}
void Daemon::run()
{
	isRunning = true;
	firstLoopFlag = true;
	while (isRunning)
	{
		pthread_testcancel();
                printf("running the thread\n");
	}
}
void *runDaemon(void * arg)
{
	printf("Starting daemon Thread\n");
	((Daemon*) arg)->run();
	return NULL;
}
void sigint(int signo)
{
	(void) signo;
}
void Daemon::start()
{
	sigset_t sigset, oldset;
	sigemptyset(&sigset);
	sigaddset(&sigset, SIGINT);
	pthread_sigmask(SIG_BLOCK, &sigset, &oldset);
	pthread_create(&daemonThread, NULL, runDaemon, this);
	struct sigaction s;
	s.sa_handler = sigint;
	sigemptyset(&s.sa_mask);
	s.sa_flags = 0;
	sigaction(SIGINT, &s, NULL);
	pthread_sigmask(SIG_SETMASK, &oldset, NULL);
}


}
}
