clear all
clc 
close all

%% 

read_file;
read_phase_file;
original_x = 100;
position_x = original_x;
position_y = 700;
width = 560;
height = 420;
increase_x = 560;
increase_y = 420;

x_limit = 1800;

for i = 1:num_plan*2
  fig(i) = figure('position', [position_x, position_y, width, height]);
  position_x = position_x + width;
  if( position_x + width > x_limit)
    position_y = position_y -height-100;
    position_x = original_x;
  end
end

foot_change

%%%%%%% Phase space 
for jj = 1: num_plan
  %% X
  int_phase_path_x = x_phase_path_cell{jj, 3};
  x_phase_path = x_phase_path_cell{jj,1};
  nx_phase_path = x_phase_path_cell{jj,2};
  figure(fig(2*jj-1))
  hold on
  plot(pos_com(1,:), vel_com(1,:), 'r-o', 'linewidth',3.5);
  plot(pos_com(1,change_frame_right), vel_com(1,change_frame_right), 'g*', 'MarkerSize',15);
  plot(pos_com(1,change_frame_left), vel_com(1,change_frame_left), 'g*', 'MarkerSize',15);
  %foot
  plot(foot_pl(1,jj), 0.0, 'color', 'black','marker', 'o', 'markersize', 11);
  plot(foot_st(1,jj), 0.0, 'color', 'red','marker', 'o', 'markersize', 11);
  plot(land_left(1,jj), 0.0, 'color', 'blue','marker', 'x', 'markersize', 12);
  plot(land_right(1,jj), 0.0, 'color', 'blue','marker', 'x', 'markersize', 12);

  plot(x_phase_path(1,:), x_phase_path(2,:), 'color','black','linewidth', 2.5);
  plot(nx_phase_path(1,:), nx_phase_path(2,:), '*');
  plot(int_phase_path_x(1,:), int_phase_path_x(2, :), 'r-');
  if(frame_x(jj) == 0)
    frame_x(jj) = 1;
  end
  plot(int_phase_path_x(1,frame_x(jj)), int_phase_path_x(2,frame_x(jj)), 'o','markersize',15);
  
  hold off
  ylim([-1.2 1.2]);
  xlim([-1, 1]);
  title('Phase Portrait_x');
  %% Y
  int_phase_path_y = y_phase_path_cell{jj,3};
  y_phase_path = y_phase_path_cell{jj,1};
  ny_phase_path = y_phase_path_cell{jj,2};
  figure(fig(2*jj))
  hold on
  plot(pos_com(2,:), vel_com(2,:), 'r-o','linewidth', 3.5);
  plot(pos_com(2,change_frame_right), vel_com(2,change_frame_right), 'g*', 'MarkerSize',15);
  plot(pos_com(2,change_frame_left), vel_com(2,change_frame_left), 'g*', 'MarkerSize',15);
  %foot
  plot(foot_pl(2,jj), 0.0, 'color', 'black', 'marker','o', 'markersize', 11);
  plot(foot_st(2,jj), 0.0, 'color', 'red','marker', 'o', 'markersize', 11);
  plot(land_left (2,jj), 0.0, 'color', 'blue','marker', 'x', 'markersize', 12);
  plot(land_right(2,jj), 0.0, 'color', 'blue','marker', 'x', 'markersize', 12);

  if(frame_y(jj) ==0)
    frame_y(jj) = 1;
  end
  plot(y_phase_path(1,:), y_phase_path(2,:), 'color','black','linewidth',2.5);
  plot(ny_phase_path(1,:), ny_phase_path(2,:), '*');
  plot(int_phase_path_y(1,:), int_phase_path_y(2,:), 'r-');
  plot(int_phase_path_y(1,frame_y(jj)), int_phase_path_y(2,frame_y(jj)), 'o','markersize',15);
  hold off
  ylim([-1.2, 1.2])
  xlim([-1, 1])
  title('Phase Portrait_y');
end

%% Close File
close_file;