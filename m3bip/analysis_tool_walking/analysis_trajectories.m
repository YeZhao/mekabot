clc
clear all
close all
%% 
%path = '~/new_control_pc_mekabot/m3bip/experiment_data';
path = '~/mekabot/m3bip/experiment_data';

read_trj_file;

original_x = 100;
position_x = original_x;
position_y = 700;
width = 560;
height = 420;
increase_x = 560;
increase_y = 420;

x_limit = 1800;

num_figure = 6;
for i = 1:num_figure
  fig(i) = figure('position', [position_x, position_y, width, height]);
  position_x = position_x + width;
  if( position_x + width > x_limit)
    position_y = position_y -height;
    position_x = original_x;
  end
end

% shortening the length
min_length = length(x);
x = x(1:min_length);
task_time = x(min_length - length(des_com)+1:end);
task_length = length(task_time);
%% Phase Changing Frame
j = 1;
for i = 1:length(phase)-1
  if(phase(i+1) ~= phase(i))
    phase_changing_frame(j) = i+1;
    j = j+1;
  end
end
phase_task = phase(min_length - length(des_com) + 1:end);
j =1;
for i = 1:length(phase_task) -1
    if(phase_task(i + 1) ~= phase_task(i))
        phase_changing_frame_task(j) = i+1;
        j = j+1;
    end
end
%phase_changing_frame = 1;

%% Draw Figure
%%%%%%%%%% Torque %%%%%%%%%%%%%%
figure(fig(1))
for i = 2:3
  tmp(i-1) = subplot(2,1,i-1);
  hold on
  plot(x, gamma(i,1:min_length) ,'b--','linewidth',5);
  plot(x, torque(i,1:min_length) ,'r-', 'linewidth',1.2);
  hold off
  if(i~=3) set(gca, 'XTick',[]); end
  set(gca, 'fontsize',12);
  fn_draw_phase(x, phase_changing_frame);
  axis tight
  ylabel('$N \cdot m$', 'fontsize', 12, 'interpreter', 'latex');
end
%title(tmp(1), 'Torque (right)');
xlabel('Time (sec)','fontsize', 12, 'interpreter', 'latex');

figure(fig(2))
for i = 2:3
  tmp(i-1) = subplot(2,1,i-1);
  hold on
  plot(x, gamma(i+3,1:min_length) ,'b--','linewidth',3);
  plot(x, torque(i+3,1:min_length) ,'r-', 'linewidth',1.2);
  hold off
  fn_draw_phase(x, phase_changing_frame);
  axis tight
  ylabel('$N \cdot m$', 'fontsize', 12, 'interpreter', 'latex');
  if(i~=3) set(gca, 'XTick',[]); end
  set(gca, 'fontsize',12);
end
xlabel('Time (sec)','fontsize', 12, 'interpreter', 'latex');
%title(tmp(1), 'Torque (left)');

%%%%%%%%% Height & Tilting %%%%%%%%%%%%%
figure(fig(3))
Draw_body_ori_traj
figure(fig(4))
Draw_body_ori_vel_traj


%%%%%%%% Foot Task %%%%%%%
figure(fig(5))
Draw_foot_traj
figure(fig(6))
Draw_foot_vel_traj
