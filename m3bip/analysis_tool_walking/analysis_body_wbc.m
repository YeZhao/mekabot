clear all
clc 
close all
fclose all;
%% 
%path = '~/new_control_pc_mekabot/m3bip/experiment_data';
path = '~/mekabot_version1/m3bip/experiment_data';

body_read_file;

original_x = 100;
position_x = original_x;
position_y = 700;
width = 560;
height = 420;
increase_x = 560;
increase_y = 420;

x_limit = 1800;

num_figure = 8;
for i = 1:num_figure
  fig(i) = figure('position', [position_x, position_y, width, height]);
  position_x = position_x + width;
  if( position_x + width > x_limit)
    position_y = position_y -height;
    position_x = original_x;
  end
end

% shortening the length
min_length = length(x)-2;
x = x(1:min_length);
task_time = x(min_length - length(des_com)+1:end);
task_length = length(task_time);
%% Phase Changing Frame
j = 1;
for i = 1:length(phase)-1
  if(phase(i+1) ~= phase(i))
    phase_changing_frame(j) = i+1;
    phase_changing_frame_task(j) = i + 1 - min_length + length(des_com) -1 ;
    j = j+1;
  end
end
%phase_changing_frame = 1;

%% Draw Figure
% Torque
figure(fig(1))

for i = 1:3
  tmp(i) = subplot(3,1,i)
  hold on
  plot(x, gamma(i,1:min_length) ,'b--','linewidth',5);
  plot(x, torque(i,1:min_length) ,'r-', 'linewidth',1.2);
  hold off
  if(i~=3) set(gca, 'XTick',[]); end
  ylim([-30, 10])  
  set(gca, 'fontsize',12);
  draw_phase(x, phase_changing_frame);
end
title(tmp(1), 'Torque (right)');
xlabel('Time (sec)','fontsize', 12);

figure(fig(2))
for i = 1:3
  tmp(i) = subplot(3,1,i)
  hold on
  plot(x, gamma(i+3,1:min_length) ,'b--','linewidth',3);
  plot(x, torque(i+3,1:min_length) ,'r-', 'linewidth',1.2);
  hold off
  if(i~=3) set(gca, 'XTick',[]); end
  ylim([-30, 10])    
  set(gca, 'fontsize',12);
end
xlabel('Time (sec)','fontsize', 12);
title(tmp(1), 'Torque (left)');
%%%%%%%%%%%%%%%%%%%   END of Torque %%%%%%%%%%%%

%% Left Foot Force

figure(fig(7))
Draw_Cartesian(x, zeros(size(force_left)), force_left, phase_changing_frame, 'Left Foot Force');

%% Right Foot Force

figure(fig(8))
Draw_Cartesian(x, zeros(size(force_right)), force_right, phase_changing_frame, 'Right Foot Force');

%%%%%%%%%%%%%%%%%% Joint Velocity
figure(fig(3))
for i = 1:3
  subplot(3,1,i)
  plot(x, jvel(i+6,1:min_length), 'LineWidth', 3)
  if(i~=3) set(gca, 'XTick',[]); end
  set(gca, 'fontsize',12);
end
draw_phase(x, phase_changing_frame);
xlabel('Time (sec)','fontsize', 12);


figure(fig(4))
for i = 1:3
  subplot(3,1,i)
  hold on 
  plot(x, jvel(i+9,1:min_length), 'LineWidth', 3);

  hold off
end
xlabel('Time (sec)','fontsize', 12);
title('Joint Velocity (left)', 'fontsize', 15);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Cartesian Space
figure(fig(5))

subplot(3,1, 1)
  hold on
plot(task_time, des_com(3,:), '--', 'Linewidth', 3);
plot(task_time, pos_com(3,:),'r-','linewidth', 3);
hold off
ylabel('Height','fontsize',12)
set(gca, 'fontsize',12, 'XTick', []);
draw_phase(task_time, phase_changing_frame_task);
axis tight
  
subplot(312)
hold on
plot(task_time, des_com(1, :), '--', 'linewidth', 3);
plot(task_time, pos_com(1, :), 'r-', 'linewidth',3);
hold off
ylabel('X', 'fontsize',12)

axis tight
set(gca, 'fontsize',12, 'XTick', []);
%
subplot(313)
hold on
plot(task_time, orientation_d(2, 1:task_length), '--', 'linewidth', 3);
plot(task_time, orientation(2, 1:task_length),  'r-', 'linewidth', 3);
hold off
ylabel('Pitch', 'fontsize',12)
axis tight
  set(gca, 'fontsize',12);

xlabel('Time (sec)','fontsize', 12);

% Cartesian Space
figure(fig(6))

subplot(3,1, 1)
  hold on
plot(task_time, des_vel(3,:), '--', 'Linewidth', 3);
plot(task_time, vel_com(3,:),'r-','linewidth', 3);
hold off
ylabel('Vel Height','fontsize',12)
set(gca, 'fontsize',12, 'XTick', []);
axis tight
ylim([-1.0, 1.0])
  
subplot(312)
hold on
plot(task_time, des_vel(1, 1:task_length), '--', 'linewidth', 3);
plot(task_time, vel_com(1, 1:task_length), 'r-', 'linewidth',3);
hold off
ylabel('\dot{X}', 'fontsize',12)

axis tight
set(gca, 'fontsize',12, 'XTick', []);
%
subplot(313)
hold on
plot(task_time, ori_vel_d(2, 1:task_length), '--', 'linewidth', 3);
plot(task_time, ori_vel(2, 1:task_length),  'r-', 'linewidth', 3);
hold off
ylabel('Vel Pitch', 'fontsize',12)
axis tight
  set(gca, 'fontsize',12);

xlabel('Time (sec)','fontsize', 12);


