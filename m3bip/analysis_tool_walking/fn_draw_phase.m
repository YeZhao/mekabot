function ret = fn_draw_phase(x, phase_changing_frame)
    hold on
    ylimit = get(gca, 'ylim');
    for j = 1:length(phase_changing_frame)
      line([x(phase_changing_frame(j)), x(phase_changing_frame(j))], ...
           [ ylimit(1), ylimit(2)], 'color','k');
    end
    hold off
end