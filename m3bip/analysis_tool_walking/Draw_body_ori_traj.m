subplot(311)
  hold on
plot(task_time, des_com(3,:), '--', 'Linewidth', 3);
plot(task_time, pos_com(3,:),'r-','linewidth', 3);
hold off
ylabel('Height','fontsize',12, 'interpreter', 'latex')
set(gca, 'fontsize',12, 'XTick', []);
fn_draw_phase(task_time, phase_changing_frame_task);
axis tight
%
subplot(312)
hold on
plot(task_time, orientation_d(2, 1:task_length), '--', 'linewidth', 3);
plot(task_time, orientation(2, 1:task_length),  'r-', 'linewidth', 3);
hold off
ylabel('Pitch', 'fontsize',12, 'interpreter', 'latex')
set(gca, 'fontsize',12);
fn_draw_phase(task_time, phase_changing_frame_task);
xlabel('Time (sec)','fontsize', 12);
axis tight

subplot(313)
hold on
plot(task_time, des_com(1, :), '--', 'linewidth', 3);
plot(task_time, pos_com(1,:),  'r-', 'linewidth', 3);
hold off
ylabel('X', 'fontsize',12, 'interpreter', 'latex')
set(gca, 'fontsize',12);
fn_draw_phase(task_time, phase_changing_frame_task);
xlabel('Time (sec)','fontsize', 12);
axis tight