% clear all
% close all
% clc
% 
% path = '~/mekabot/m3bip/experiment_data';
% configure_read_file;

test = false;

Mabduction = 2.1656; 
MThigh = 2.4194; 
Mbody = 10.61; 
Mcalf = 0.20;
Mtot = 2*Mabduction +  2*MThigh + Mbody + 2*Mcalf;


len = length(conf);
pos_com = zeros(3,len);
vel_com = zeros(3, len);


for i = 1: len
    pos_com(:,i) = forward_kinematics(conf(:, i), 0);  
end

for i = 1:len
   vel_com(:, i) = Jacobian(conf(:,i), 0) * jvel(:,i);   
end


% 
% filtered_vel = fn_vel_filter(jvel(5,:));
% for i = 1: 7
% jvel(i,:) = 0.0*jvel(6,:);
% end
% jvel(7,:) = 0.0*jvel(7,:);
% jvel(10,:) = 0.0*jvel(5,:);
% 

if(test)
figure
for i = 1:12
subplot(3,4, i)
plot(jvel(i,:));
end

figure
for i = 1:12
subplot(3,4, i)
plot(conf(i,:));
end

body_vel = zeros(3,len);
right_abduction_vel = zeros(3,len);
right_thigh_vel = zeros(3, len);
right_calf_vel = zeros(3, len);
left_abduction_vel = zeros(3, len);
left_thigh_vel = zeros(3, len);
left_calf_vel = zeros(3, len);

for i = 1:len
   body_vel(:, i) = Mbody/Mtot * Jacobian(conf(:,i), 0) * jvel(:,i);
   right_abduction_vel(:, i) = Mabduction/Mtot * Jacobian(conf(:,i), 1) *jvel(:,i) ;
   right_thigh_vel(:, i) = MThigh/Mtot * Jacobian(conf(:, i), 2) * jvel(:, i);
   right_calf_vel(:, i) = Mcalf/Mtot * Jacobian(conf(:, i), 3) * jvel(:, i);
   left_abduction_vel(:, i) = Mabduction/Mtot * Jacobian(conf(:, i), 4) * jvel(:,i);
   left_thigh_vel(:, i) = MThigh/Mtot * Jacobian(conf(:, i), 5) * jvel(:, i) ;
   left_calf_vel(:, i) = Mcalf/Mtot * Jacobian(conf(:, i), 6) * jvel(:, i);
end
vel_com = body_vel + ... 
          right_abduction_vel + right_thigh_vel + right_calf_vel + ...
          left_abduction_vel + left_thigh_vel + left_calf_vel;
end
      % figure 
% plot(vel_com(1,4000:5000));