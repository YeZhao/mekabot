clear all
clc 
close all
fclose all;
%% 
path = '~/new_control_pc_mekabot/m3bip/experiment_data';
read_walking_file;

original_x = 100;
position_x = original_x;
position_y = 700;
width = 560;
height = 420;
increase_x = 560;
increase_y = 420;

x_limit = 1800;

num_figure = 11;
for i = 1:num_figure
  fig(i) = figure('position', [position_x, position_y, width, height]);
  position_x = position_x + width;
  if( position_x + width > x_limit)
    position_y = position_y -height;
    position_x = original_x;
  end
end

% shortening the length
min_length = length(x);
x = x(1:min_length);
phase = phase(1:min_length);

%% Phase Changing Frame
phase_changing_frame = 1;
j = 1;
for i = 1:length(phase)-1
  if(phase(i+1) ~= phase(i))
    phase_changing_frame(j) = i+1;
    j = j+1;
  end
end



%% Draw Figure
% Torque
figure(fig(1))

for i = 1:3
  subplot(3,1,i)
  hold on
  plot(x, gamma(i,1:min_length) ,'b--','linewidth',5);
  plot(x, torque(i,1:min_length) ,'r-', 'linewidth',1.2);
  hold off
  if(i~=3) set(gca, 'XTick',[]); end
  set(gca, 'fontsize',12);
  draw_phase(x, phase_changing_frame);
end
xlabel('Time (sec)','fontsize', 12);

figure(fig(2))
for i = 1:3
  subplot(3,1,i)
  hold on
  plot(x, gamma(i+3,1:min_length) ,'b--','linewidth',3);
  plot(x, torque(i+3,1:min_length) ,'r-', 'linewidth',1.2);
  hold off
  if(i~=3) set(gca, 'XTick',[]); end
  set(gca, 'fontsize',12);
  draw_phase(x, phase_changing_frame);
end
xlabel('Time (sec)','fontsize', 12);
title('Command (left)', 'fontsize', 15);
%%%%%%%%%%%%%%%%%%%   END of Torque %%%%%%%%%%%%


%%%%%%%%%%%%%%%%%% Joint Velocity
figure(fig(3))
for i = 1:3
  subplot(3,1,i)
  plot(x, jvel(i+6,1:min_length), 'LineWidth', 3)
  if(i~=3) set(gca, 'XTick',[]); end
  set(gca, 'fontsize',12);
  draw_phase(x, phase_changing_frame);
end
xlabel('Time (sec)','fontsize', 12);


figure(fig(4))
for i = 1:3
  subplot(3,1,i)
  hold on 
  plot(x, jvel(i+9,1:min_length), 'LineWidth', 3);
  draw_phase(x, phase_changing_frame);
  hold off
end
xlabel('Time (sec)','fontsize', 12);
title('Joint Velocity (left)', 'fontsize', 15);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Cartesian Space
figure(fig(5))

subplot(3,1, 1)
  hold on
plot(x, des_com(3,1:min_length), '--', 'Linewidth', 3);
plot(x, pos_com(3,1:min_length),'r-','linewidth', 3);
hold off
ylabel('Height','fontsize',12)
  set(gca, 'fontsize',12, 'XTick', []);
  draw_phase(x, phase_changing_frame);
  axis tight
  
subplot(312)
hold on
plot(x, orientation_d(1, 1:min_length), '--', 'linewidth', 3);
plot(x, orientation(1, 1:min_length), 'r-', 'linewidth',3);
hold off
ylabel('Roll', 'fontsize',12)
draw_phase(x, phase_changing_frame);
axis tight
set(gca, 'fontsize',12, 'XTick', []);
%
subplot(313)
hold on
plot(x, orientation_d(2, 1:min_length), '--', 'linewidth', 3);
plot(x, orientation(2, 1:min_length),  'r-', 'linewidth', 3);
hold off
ylabel('Pitch', 'fontsize',12)
draw_phase(x, phase_changing_frame);
axis tight
  set(gca, 'fontsize',12);

xlabel('Time (sec)','fontsize', 12);

%% Right Foot
figure(fig(6))
Draw_Cartesian(x, des_right, pos_right, phase_changing_frame, 'Right Foot');


%% Left Foot
figure(fig(7))
Draw_Cartesian(x, des_left, pos_left, phase_changing_frame, 'Left Foot');


%% Right Foot vel
figure(fig(8))
Draw_Cartesian(x, des_foot_vel, vel_right, phase_changing_frame, 'Right Foot Vel');


%% Left Foot vel
figure(fig(9))
Draw_Cartesian(x, des_foot_vel, vel_left, phase_changing_frame, 'Left Foot Vel');

%% Body
figure(fig(10))
Draw_Cartesian(x, zeros(size(vel_com)), vel_com, phase_changing_frame, 'Body Velocity');

figure(fig(11))
Draw_Cartesian(x, des_com, pos_com, phase_changing_frame, 'Body');

% figure
% plot(pos_com_walking(3,:))