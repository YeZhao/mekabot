clear all
clc 
close all
fclose all
%% 
% path = '~/new_control_pc_mekabot/m3bip/experiment_data';
path = '~/mekabot/m3bip/experiment_data';

global com_x_offset

plan_start = 13;
num_plan = 17;

read_walking_file;
read_walking_phase_file;
rebuild_com;
com_x_offset = -0.02;

%%%%%%%%%% Array Window %%%%%%%%%%%%%%%%%%%%%%%%%
original_x = 100;
position_x = original_x;
position_y = 700;
width = 560;
height = 460;
increase_x = 560;
increase_y = 820;

x_limit = 1800;
y_plot = 0;
for i = 1:(num_plan - plan_start+1)*(1 + y_plot)
  fig(i) = figure('position', [position_x, position_y, width, height]);
  position_x = position_x + width;
  if( position_x + width > x_limit)
    position_y = position_y - increase_y;
    position_x = original_x;
  end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
parcing_phase;

j = 1;
phase_changing_frame = 1;
phase_changing_frame_task = 1;
for i = 1:length(phase)-1
  if(phase(i+1) ~= phase(i))
    phase_changing_frame(j) = i+1;
    j = j+1;
  end
end

axis_font_size = 30;
font_size = 35;
line_width = 6;
%foot_change
kk = 1;
%%%%%%% Phase space 
next_stance_l = mod(plan_start,2);
next_stance_r = -mod(plan_start,2)+1;
vel_filter = false;

%%%%%%   Low Pass Filter  %%%%%%%%%%%%%%%%%%%%%%
t_s = x(end)/length(x);
w_c = 25;

Lpf_in_prev = zeros(1,2);
Lpf_out_prev = zeros(1,2);

den = 2500*t_s*t_s*w_c*w_c  + 7071*t_s*w_c + 10000;

Lpf_in1 = 2500*t_s*t_s*w_c*w_c / den;
Lpf_in2 = 5000*t_s*t_s*w_c*w_c / den;
Lpf_in3 = 2500*t_s*t_s*w_c*w_c / den;
Lpf_out1 = -(5000*t_s*t_s*w_c*w_c  - 20000) / den;
Lpf_out2 = -(2500*t_s*t_s*w_c*w_c  - 7071*t_s*w_c + 10000) / den;

f_jvel = zeros(size(jvel(1:3,:)));
for j = 1:3
for i = 1: length(x)
    lpf_in = vel_com(j,i);
    lpf_out = Lpf_in1*lpf_in + Lpf_in2*Lpf_in_prev(1) + Lpf_in3*Lpf_in_prev(2) + ... %input component
              Lpf_out1*Lpf_out_prev(1) + Lpf_out2*Lpf_out_prev(2); %output component
    Lpf_in_prev(2) = Lpf_in_prev(1);
    Lpf_in_prev(1) = lpf_in;
    Lpf_out_prev(2) = Lpf_out_prev(1);
    Lpf_out_prev(1) = lpf_out;
    
    f_jvel(j,i) = lpf_out;
end
end
jvel(1:3, :) = f_jvel;
%%%%%%   End of Low Pass Filter  %%%%%%%%%%%%%%%

for jj = plan_start: num_plan
  %% X
  ori = 0.0;
  x_phase_path = x_phase_path_cell{jj,1};
  nx_phase_path = x_phase_path_cell{jj,2};
  figure(fig(jj - plan_start + 1))
  
  hold on
  % Frame
  start_fr = stance_frame(jj) +2 ;
  middle_fr_end = stance_frame(jj+1) -2;
  middle_fr_start = stance_frame(jj+1) +2;
  end_fr = stance_frame(jj+2) -2;
  %
  start_supp_lift_trans = min(find(phase_changing_frame > start_fr));
  start_supp_lift_trans = phase_changing_frame(start_supp_lift_trans);
  %
  end_supp_lift_trans = min(find(phase_changing_frame>start_supp_lift_trans));
  end_supp_lift_trans = phase_changing_frame(end_supp_lift_trans);
  end_supp_lift_trans  = end_supp_lift_trans -1;
  %
  start_land = min(find(phase_changing_frame > end_supp_lift_trans + 1));
  start_land = phase_changing_frame(start_land);
  %
  start_land_supp_trans = min(find(phase_changing_frame> start_land));
  start_land_supp_trans = phase_changing_frame(start_land_supp_trans);
  %Foot
  plot_foot_place(0, jj, ori, com_pos_pl(:,jj), foot_pl(:,jj), ...
                  foot_st(:,jj), landing_loc(:, jj));
              
              
  % Planned Phase Path
  idx_zero_x = 1;
  for kkk = 1:length(nx_phase_path)
      if(abs(nx_phase_path(2,kkk)) < 1.5)
          idx_zero_x = kkk;
      end
  end
  hold on
  plot(x_phase_path(1,:), x_phase_path(2,:), 'color','black','linewidth', line_width);
  plot(nx_phase_path(1,1:idx_zero_x), nx_phase_path(2,1:idx_zero_x), '--', 'linewidth', line_width);
  hold off 
  
  % phase portrait
   hold on
   if(vel_filter)
       vel_com_1 = fn_vel_filter(vel_com(:, start_fr:middle_fr_end));
       vel_com_2 = fn_vel_filter(vel_com(:, middle_fr_start:end_fr));
   else
%        vel_com_1 = vel_com(:, start_fr:middle_fr_end);
%        vel_com_2 = vel_com(:, middle_fr_start:end_fr);
       vel_com_1 = jvel(1:3, start_fr:middle_fr_end);
       vel_com_2 = jvel(1:3, middle_fr_start:end_fr);

   end
   
   plot_phase_portrait(0,ori, com_pos_pl(:,jj), ...
                        pos_com(:,start_fr:middle_fr_end), ...
                        vel_com_1, ...
                        start_supp_lift_trans - start_fr, ...
                        end_supp_lift_trans - start_fr, ...
                        start_land_supp_trans - start_fr);
   plot_phase_portrait_2(0,ori, pos_com(:,middle_fr_end) - com_pos_pl(:,jj), ... 
                           pos_com(:,middle_fr_start:end_fr), ...
                           vel_com_2);
                       
   hold off

  %axis tight
 %  ylim([-0.4 0.1]);
 %  xlim([-0.13, 0.1]);
  set(gca, 'fontsize', axis_font_size);
  xlabel('$x$','interpreter', 'latex','fontsize',font_size);
  ylabel('$\dot{x}$','interpreter', 'latex', 'fontsize', font_size);
  %title('Phase Portrait_x');

  %% Y
  if(y_plot)
  
  int_phase_path_y = y_phase_path_cell{jj,3};
  y_phase_path = y_phase_path_cell{jj,1};
  ny_phase_path = y_phase_path_cell{jj,2};
  figure(fig(jj + num_plot))
  hold on

  %foot
  plot_foot_place(1,jj,ori,com_pos_pl(:,jj), ... 
      foot_pl(:,jj), foot_st(:,jj), ... 
      pos_left(:, stance_frame(jj + next_stance_l) - next_stance_l), ...
      pos_right(:,stance_frame(jj + next_stance_r) - next_stance_r));
  
  set(gca, 'fontsize',axis_font_size);
  % phase portrait
  plot_phase_portrait(1,ori, com_pos_pl(:,jj), pos_com(:,start_fr:middle_fr_start), vel_com(:,start_fr:middle_fr_start));
  plot_phase_portrait_2(1,ori, com_pos_pl(:,jj), pos_com(:,middle_fr_start:end_fr), vel_com(:,middle_fr_start:end_fr));
  
  idx_zero_y = 1;
  for kkk = 1:length(ny_phase_path)
      if(abs(ny_phase_path(2,kkk)) < 0.15)
          idx_zero_y = kkk;
      end
  end
  
  hold on
  plot(y_phase_path(1,:), y_phase_path(2,:), 'color','black','linewidth',line_width);
  plot(ny_phase_path(1,1:idx_zero_y), ny_phase_path(2,1:idx_zero_y), '--', 'linewidth', line_width);
  hold off
    
  %axis tight
  % ylim([-0.42, 0.42])
  % xlim([-0.21, 0.21])
  xlabel('$y$','interpreter', 'latex','fontsize',font_size);
  ylabel('$\dot{y}$','interpreter', 'latex', 'fontsize', font_size);
    %title('Phase Portrait_y'); 
  end
  

  tmp = next_stance_l;
  next_stance_l = next_stance_r;
  next_stance_r = tmp;
  kk = kk +1;
end
% figure
% t = linspace(0,1, length(jvel));
% plot(t, jvel(1,:), t, vel_com(1,:))

fclose all;



 % plot(int_phase_path_x(1,:), int_phase_path_x(2, :), 'r-');
%   if(frame_x(jj) == 0)
%     frame_x(jj) = 1;
%   end
%  plot(int_phase_path_x(1,frame_x(jj)), int_phase_path_x(2,frame_x(jj)), 'o','markersize',15);
  

  %   plot(int_phase_path_y(1,:), int_phase_path_y(2,:), 'r-');
%   if(frame_y(jj) ==0)
%     frame_y(jj) = 1;
%   end
%   plot(int_phase_path_y(1,frame_y(jj)), int_phase_path_y(2,frame_y(jj)), 'o','markersize',15);
