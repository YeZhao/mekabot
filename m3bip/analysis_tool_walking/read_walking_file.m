x = fn_read_file(path, 'time', 1);
% Desired
des_com  = fn_read_file(path, 'Body_des', 3);
des_vel = fn_read_file(path, 'Body_vel_des', 3);
orientation_d  = fn_read_file(path, 'Ori_des', 3);
ori_vel_d = fn_read_file(path, 'Ori_vel_des',3);
gamma    = fn_read_file(path, 'gamma',6);

des_foot = fn_read_file(path, 'Foot_des', 3);
des_foot_vel = fn_read_file(path, 'Foot_vel_des', 3);
% des_right = fn_read_file(path, 'right_foot_des',3);
% des_left = fn_read_file(path, 'left_foot_des', 3);
% pos_left  = fn_read_file(path, 'curr_Left_foot', 3);
% pos_right = fn_read_file(path, 'curr_Right_foot', 3);
pos_com   = fn_read_file(path, 'Body_pos', 3);
%pos_com_walking = fn_read_file(path, 'Body_3D_Walking', 3);
vel_com   = fn_read_file(path, 'Body_vel',3);
orientation = fn_read_file(path, 'Body_ori',3);
ori_vel = fn_read_file(path, 'Body_ori_vel',3);
pos_foot = fn_read_file(path, 'Foot_pos', 3);
vel_foot  = fn_read_file(path, 'Foot_vel', 3);
% vel_right = fn_read_file(path, 'Right_foot_vel', 3);
force_left = fn_read_file(path, 'Left_foot_force', 3);
force_right = fn_read_file(path, 'Right_foot_force', 3);    

torque = fn_read_file(path, 'torque', 6);
conf   = fn_read_file(path, 'curr_conf', 12);
jvel = fn_read_file(path, 'curr_vel', 12);

phase = fn_read_file(path, 'phase', 1);

rcontact = fn_read_file(path, 'rfoot_contact',1);
lcontact = fn_read_file(path, 'lfoot_contact', 1);