function ret = plot_phase_portrait_2(axis, theta, com_pos_offset, ...
    pos_com, vel_com, color, linesize)

offset_com_pos = pos_com + ((-pos_com(:,1) + com_pos_offset)*ones(1, length(pos_com))); 

local_com_pos = offset_com_pos(1,:)*cos(theta + axis*0.5*pi) + ...
                offset_com_pos(2,:)*sin(theta + axis*0.5*pi);
local_vel_com = vel_com(1,:)*cos(theta + axis*0.5*pi) + ...
                vel_com(2,:)*sin(theta + axis*0.5*pi);
            hold on
plot(local_com_pos, local_vel_com, color, 'linewidth', linesize);
hold off
j = 1;            
for i = 1:length(local_com_pos)
    if(i == 50*(j-1) + 1)
    sparce_pos(j) = local_com_pos(i);
    sparce_vel(j) = local_vel_com(i);
    j = j+1;
    end
end
hold on
% plot(local_com_pos, local_vel_com, 'color','red','maker','+'); %, 'linewidth', 1.5);

hold off
end