clc
clear all
close all

%%
h = 0.9;
g = 9.8;


x = @(t,xp, x0, x0_dot) (-0.5*(sqrt(h/g)*x0_dot - (x0 - xp))...
    *exp(-sqrt(g/h)*t)...
    + 0.5*(sqrt(h/g)*x0_dot + (x0 -xp))* ...
    exp(sqrt(g/h)*t)) + xp;
x_dot = @(t,xp, x0, x0_dot) (0.5*(x0_dot - sqrt(g/h)*(x0 - xp))...
    * exp(-sqrt(g/h)*t)...
    + 0.5*(x0_dot + sqrt(g/h)*(x0-xp))* ...
    exp(sqrt(g/h)*t));

t = 0:0.001:0.1;
x1 = x(t,0.25, 0, 0);
x1_dot = x_dot(t, 0.25, 0, 0);


x2 = x(t,-.25, x1(end),x1_dot(end));
x2_dot = x_dot(t, -0.25, x1(end),x1_dot(end));


hold on
plot(x1, x1_dot)
plot(x2, x2_dot)
hold off

i = 1;
x_foot = -0.25;
com_st = -0.2;
while(true)
    t = 0.01*i;
    xc(i) = x(t, x_foot, com_st, 0.0);
    xc_dot(i) = x_dot(t, x_foot, com_st, 0.0);
    x_curr = xc(i);
    if(x_curr >0.0)
        break;
    end
    i = i+1;
end
% xc = x(t, -0.25, -.2, 0.0);
% xc_dot = x_dot(t, -0.25, -0.2, 0.0);

figure
plot(xc, xc_dot)
