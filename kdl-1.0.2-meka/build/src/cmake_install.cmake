# Install script for directory: /home/meka/mekabot/kdl-1.0.2-meka/src

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/kdl" TYPE FILE FILES
    "/home/meka/mekabot/kdl-1.0.2-meka/src/chainidsolver_recursive_newton_euler.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/velocityprofile_rect.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/chainiksolvervel_wdls.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/treejnttojacsolver.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/treefksolverpos_recursive.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/trajectory_stationary.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/framevel.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/chainjnttojacsolver.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/treeiksolver.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/jntarrayvel.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/rotationalinertia.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/trajectory.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/chainiksolverpos_nr_jl.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/stiffness.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/path_composite.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/jntarrayacc.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/chainfksolver.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/treefksolver.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/rigidbodyinertia.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/path_line.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/velocityprofile_traphalf.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/motion.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/chainiksolver.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/frames.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/jacobian.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/rotational_interpolation_sa.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/jntarray.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/frameacc.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/chainfksolvervel_recursive.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/kinfam.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/trajectory_composite.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/velocityprofile_trap.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/tree.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/path_point.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/path_circle.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/chainiksolvervel_pinv_givens.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/velocityprofile.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/chainfksolverpos_recursive.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/velocityprofile_dirac.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/trajectory_segment.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/chainiksolverpos_nr.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/kdl.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/path_cyclic_closed.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/path.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/path_roundedcomposite.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/chain.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/treeiksolvervel_wdls.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/frames_io.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/kinfam_io.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/chainiksolvervel_pinv.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/segment.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/frameacc_io.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/framevel_io.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/rotational_interpolation.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/joint.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/treeiksolverpos_nr_jl.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/chainidsolver.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/framevel.inl"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/frames.inl"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/frameacc.inl"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/kdl/utilities" TYPE FILE FILES
    "/home/meka/mekabot/kdl-1.0.2-meka/src/utilities/rallNd.h"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/utilities/rall2d_io.h"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/utilities/error.h"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/utilities/utility.h"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/utilities/rall1d.h"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/utilities/error_stack.h"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/utilities/rall1d_io.h"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/utilities/traits.h"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/utilities/utility_io.h"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/utilities/kdl-config.h"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/utilities/rall2d.h"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/utilities/svd_HH.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/utilities/svd_eigen_HH.hpp"
    "/home/meka/mekabot/kdl-1.0.2-meka/src/utilities/svd_eigen_Macie.hpp"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/meka/mekabot/kdl-1.0.2-meka/build/src/orocos-kdl.pc")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FOREACH(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/liborocos-kdl.so.1.0.2"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/liborocos-kdl.so.1.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/liborocos-kdl.so"
      )
    IF(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      FILE(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    ENDIF()
  ENDFOREACH()
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES
    "/home/meka/mekabot/kdl-1.0.2-meka/build/src/liborocos-kdl.so.1.0.2"
    "/home/meka/mekabot/kdl-1.0.2-meka/build/src/liborocos-kdl.so.1.0"
    "/home/meka/mekabot/kdl-1.0.2-meka/build/src/liborocos-kdl.so"
    )
  FOREACH(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/liborocos-kdl.so.1.0.2"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/liborocos-kdl.so.1.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/liborocos-kdl.so"
      )
    IF(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      IF(CMAKE_INSTALL_DO_STRIP)
        EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "${file}")
      ENDIF(CMAKE_INSTALL_DO_STRIP)
    ENDIF()
  ENDFOREACH()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  INCLUDE("/home/meka/mekabot/kdl-1.0.2-meka/build/src/bindings/python/cmake_install.cmake")

ENDIF(NOT CMAKE_INSTALL_LOCAL_ONLY)

