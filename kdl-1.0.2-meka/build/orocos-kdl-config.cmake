# Locate KDL install directory

# This module defines
# KDL_INSTALL where to find include, lib, bin, etc.
# KDL_FOUND, is set to true

INCLUDE (FindPkgConfig)

IF ( PKG_CONFIG_FOUND )

  set(ENV{PKG_CONFIG_PATH} "/usr/local/lib/pkgconfig/")
  message( "Looking for KDL in: /usr/local")
  if(KDL_FIND_REQUIRED)
    pkg_search_module(KDL REQUIRED orocos-kdl)
  else(KDL_FIND_REQUIRED)
    pkg_search_module(KDL orocos-kdl)
  endif(KDL_FIND_REQUIRED)
  
  IF( KDL_FOUND )
    IF(NOT KDL_FIND_QUIETLY)
      message("   Includes in: ${KDL_INCLUDE_DIRS}")
      message(    "Compiler flags: ${KDL_CFLAGS}")  
      message("   Libraries: ${KDL_LIBRARIES}")
      message("   Libraries in: ${KDL_LIBRARY_DIRS}")
      message("   Linker flags : ${KDL_LD_FLAGS}")
      message("   Defines: ${KDL_DEFINES}")
    ENDIF(NOT KDL_FIND_QUIETLY)
  ELSE(KDL_FOUND)  
    IF(KDL_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "Could not find KDL")
    ENDIF(KDL_FIND_REQUIRED)
  ENDIF ( KDL_FOUND )
ELSE  ( PKG_CONFIG_FOUND )

  MESSAGE( FATAL_ERROR "Can't find KDL without pkgconfig !")

ENDIF ( PKG_CONFIG_FOUND )
