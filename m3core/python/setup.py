from distutils.core import setup
setup(
    name = "m3core",
    packages = ["m3", "m3rt"],
    version = "1.4.0",
    description = "Python bindings for the M3 real time control software",
    author = "Meka Robotics LLC",
    author_email = "info@mekabot.com",
    url = "http://mekabot.com",
)
