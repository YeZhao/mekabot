(cl:in-package m3_msgs-msg)
(cl:export '(NAME-VAL
          NAME
          STATE-VAL
          STATE
          TIMESTAMP-VAL
          TIMESTAMP
          RATE-VAL
          RATE
          VERSION-VAL
          VERSION
))