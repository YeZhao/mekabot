
(cl:in-package :asdf)

(defsystem "m3_msgs-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "M3BaseStatus" :depends-on ("_package_M3BaseStatus"))
    (:file "_package_M3BaseStatus" :depends-on ("_package"))
  ))